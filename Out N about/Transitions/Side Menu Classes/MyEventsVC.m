//
//  MyEventsVC.m
//  OutNAbout
//
//  Created by Ram Kumar on 29/06/16.
//  Copyright © 2016 tecHindustan. All rights reserved.
//

#import "MyEventsVC.h"
#import "CarbonKit.h"
#import "PGDrawerTransition.h"
#import "NavigationViewController.h"
#import "AppDelegate.h"
#import "SearchEventsVC.h"
#import "CommonAPI's.pch"
#import "FavoriteEventsVC.h"
#import "UpcomingEventsVC.h"
#import "MyEvents.h"

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wdeprecated-declarations"

@interface MyEventsVC ()<CarbonTabSwipeNavigationDelegate>


@property (nonatomic, strong) PGDrawerTransition *drawerTransition;
@property (nonatomic, strong) NavigationViewController *navigationViewController;

@end

@implementation MyEventsVC
{
    NSArray *items;
    NSString *access_token;
    CarbonTabSwipeNavigation *carbonTabSwipeNavigation;
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    // self.drawerViewController = [[ExamDrawerViewController alloc] init];
    UIStoryboard *story = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    self.navigationViewController = [story instantiateViewControllerWithIdentifier:@"drawerView"];
    self.drawerTransition = [[PGDrawerTransition alloc] initWithTargetViewController:self
                                                                drawerViewController:self.navigationViewController];
    
    
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor whiteColor]};
    
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:0.95 green:0.28 blue:0.31 alpha:1.0];
    
    items = @[@"My Events",@"Upcoming",@"Favorites"];
    
    carbonTabSwipeNavigation = [[CarbonTabSwipeNavigation alloc] initWithItems:items delegate:self];
    
    [carbonTabSwipeNavigation insertIntoRootViewController:self];
    
    [self style];
    
    
    UIBarButtonItem *searchButton = [[UIBarButtonItem alloc]
                                     initWithImage:[UIImage imageNamed:@"ic_search@3x.png"]
                                     style:UIBarButtonItemStyleBordered
                                     target:self
                                     action:@selector(searchView)];
    searchButton.tintColor = [UIColor whiteColor];
    self.navigationItem.rightBarButtonItems = [NSArray arrayWithObjects:searchButton, nil];
   
    
    UILabel* titleLabelNav = [[UILabel alloc] initWithFrame:CGRectMake(0,40,320,40)];
    titleLabelNav.textAlignment = NSTextAlignmentLeft;
    titleLabelNav.text = NSLocalizedString(@"My Events",@"");
    titleLabelNav.textColor = [UIColor whiteColor];
    self.navigationItem.titleView = titleLabelNav;

}



-(void)searchView
{
    
    UIStoryboard *story = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    SearchEventsVC *searchEvent = [story instantiateViewControllerWithIdentifier:@"searchView"];
    
    [self.navigationController pushViewController:searchEvent animated:YES];
    
    
}


- (void)style {
    
    UIColor *color = [UIColor yellowColor];//[UIColor colorWithRed:24.0 / 255 green:75.0 / 255 blue:152.0 / 255 alpha:1]
    self.navigationController.navigationBar.translucent = NO;
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    //self.navigationController.navigationBar.barTintColor = color;
    self.navigationController.navigationBar.barStyle = UIBarStyleBlackTranslucent;
    carbonTabSwipeNavigation.carbonSegmentedControl.backgroundColor = [UIColor colorWithRed:0.95 green:0.28 blue:0.31 alpha:1.0];
    carbonTabSwipeNavigation.toolbar.translucent = NO;
    [carbonTabSwipeNavigation setIndicatorColor:color];
    [carbonTabSwipeNavigation setTabExtraWidth:30];
    [carbonTabSwipeNavigation.carbonSegmentedControl setWidth:150 forSegmentAtIndex:0];
    [carbonTabSwipeNavigation.carbonSegmentedControl setWidth:150 forSegmentAtIndex:1];
    [carbonTabSwipeNavigation.carbonSegmentedControl setWidth:150 forSegmentAtIndex:2];
    //    [carbonTabSwipeNavigation.carbonSegmentedControl setWidth:100 forSegmentAtIndex:3];
    //    [carbonTabSwipeNavigation.carbonSegmentedControl setWidth:105 forSegmentAtIndex:4];
    
    
    // Custimize segmented control
    [carbonTabSwipeNavigation setNormalColor:[[UIColor whiteColor] colorWithAlphaComponent:1.0]
                                        font:[UIFont boldSystemFontOfSize:13]];
    [carbonTabSwipeNavigation setSelectedColor:color font:[UIFont boldSystemFontOfSize:14]];
}

#pragma mark - CarbonTabSwipeNavigation Delegate
// required

- (nonnull UIViewController *)carbonTabSwipeNavigation:
(nonnull CarbonTabSwipeNavigation *)carbontTabSwipeNavigation
                                 viewControllerAtIndex:(NSUInteger)index {
    switch (index) {
        case 0:
            return [self.storyboard instantiateViewControllerWithIdentifier:@"myEventView"];
            
        case 1:
            return [self.storyboard instantiateViewControllerWithIdentifier:@"upcomingEventView"];
            
        default:
            return [self.storyboard instantiateViewControllerWithIdentifier:@"favEventsView"];
    }
}


- (void)carbonTabSwipeNavigation:(nonnull CarbonTabSwipeNavigation *)carbonTabSwipeNavigation
                  didMoveAtIndex:(NSUInteger)index {
    NSLog(@"Did move at index: %ld", index);
}

- (UIBarPosition)barPositionForCarbonTabSwipeNavigation:
(nonnull CarbonTabSwipeNavigation *)carbonTabSwipeNavigation {
    return UIBarPositionTop; // default UIBarPositionTop
}






- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    

    
}




#pragma mark - usee these methods for slide bar menu ============================


- (void)viewWillAppear:(BOOL)animated
 {
 [super viewWillAppear:animated];
 

    UIBarButtonItem *left = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"ic_menu@3x.png"] style:UIBarButtonItemStyleDone target:self action:@selector(open)];
     left.tintColor = [UIColor whiteColor];
 self.navigationItem.leftBarButtonItem = left;
 
 }

- (void)open
{
    [self.drawerTransition presentDrawerViewController];
}


@end
