//
//  CreateEventVC.m
//  OutNAbout
//
//  Created by Ram Kumar on 29/06/16.
//  Copyright © 2016 tecHindustan. All rights reserved.
//

#import "CreateEventVC.h"
#import "BSKeyboardControls.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import <QuartzCore/QuartzCore.h>
#import "CarbonKit.h"
#import "AppDelegate.h"
#import "PGDrawerTransition.h"
#import "NavigationViewController.h"

#import "LPPopupListView.h"
#import "SDWebImageManager.h"
#import "UIView+Frame.h"
#import "ABCGooglePlacesSearchViewController.h"
#import "CommonAPI's.pch"
#import "SVProgressHUD.h"
#import "ViewController.h"
#import "MyEventsVC.h"

#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wdeprecated-declarations"

@interface CreateEventVC ()<UIImagePickerControllerDelegate,UINavigationControllerDelegate,UIGestureRecognizerDelegate,UITextFieldDelegate,UITextViewDelegate,BSKeyboardControlsDelegate,LPPopupListViewDelegate,UIGestureRecognizerDelegate,ABCGooglePlacesSearchViewControllerDelegate,UITableViewDelegate,UITableViewDataSource,UIActionSheetDelegate,UIPickerViewDelegate,UIPickerViewDataSource>

@property (nonatomic, strong) PGDrawerTransition *drawerTransition;
@property (nonatomic, strong) NavigationViewController *navigationViewController;
@property (nonatomic, strong) ViewController *viewControllerObject;
@property (nonatomic, strong) MyEventsVC *myEventsVCObject;

//@property (nonatomic, strong) BSKeyboardControls *keyboardControls;

@property (nonatomic, strong) NSString *selectedImagePath;
@property (nonatomic, strong) NSMutableIndexSet *selectedIndexes;
@property (nonatomic, strong) UIImage *selectedImage;


@end

@implementation CreateEventVC
{
    UIImagePickerController *mediaPicker;
    NSMutableDictionary *dictImages;
    UIView *loadingView;
    UIActivityIndicatorView *activityView;
    NSString *imageName;
    UIImage *imagePath;
    NSMutableArray *json;
    NSMutableArray *artistJson;
    NSMutableArray *catTitle;
    NSMutableArray *catId;
    NSMutableArray *artist_Title;
    NSMutableArray *artist_Id;
    
    NSMutableArray *currencyJson;
    NSMutableArray *currency;
    NSMutableArray *currency_Id;
    NSArray *occurrenceArray;
    NSString *pickerType;
    
    NSString *occurrencePostString;
    
     UIAlertView *alert;
    NSString *artistIDNo;
    NSString *categoryIDNo;

    NSMutableArray *arrayArtistSelectedRows;
    NSMutableArray *arrayCategorySelectedRows;
    NSString *firstName;
    NSString *lastName;
    NSString *password;
     NSString *access_token;
    
    NSString *latitudes;
    NSString *longitudes;
    NSString *catOrArtistKeyType;
    UIAlertController * alertForFields;
    UIDatePicker *datePicker;
     UIPickerView *pickerView;
    
    UITapGestureRecognizer *tapGestureForStartDate;
    UITapGestureRecognizer *tapGestureForStartTime;

    UITapGestureRecognizer *tapGestureForEndDate;
    UITapGestureRecognizer *tapGestureForEndTime;

}

@synthesize selectedItems;

- (void)viewDidLoad {
    
    [super viewDidLoad];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    access_token = [defaults objectForKey:@"access_token"];
    catOrArtistKeyType=@"";
    pickerType=@"";
    
    
    UIStoryboard *story = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    self.navigationViewController = [story instantiateViewControllerWithIdentifier:@"drawerView"];
    self.drawerTransition = [[PGDrawerTransition alloc] initWithTargetViewController:self
                                                                drawerViewController:self.navigationViewController];
    

    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor whiteColor]};
    
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:0.95 green:0.28 blue:0.31 alpha:1.0];
    
    
    occurrenceArray = @[@"Once",@"Weekly",@"Monthly"];
    
    if (IS_IPHONE_5) {
        _scrollView.contentSize =CGSizeMake(320,700);

    }else
    {
   // _scrollView.contentSize =CGSizeMake(320, 1000);

    
    
    CGFloat scrollViewHeight = 0.0f;
    for (UIView* view in _scrollView.subviews)
    {
        scrollViewHeight += view.frame.size.height-144;
    }
    
    [_scrollView setContentSize:(CGSizeMake(320, scrollViewHeight))];
    }
    _scrollView.bounces = NO;
    _scrollView.keyboardDismissMode = UIScrollViewKeyboardDismissModeOnDrag;
    self.navigationController.navigationBar.translucent = NO;

//    _scrollView.showsHorizontalScrollIndicator = NO;
//    _scrollView.scrollEnabled = YES;
    
    _barButtonItem = [[UIBarButtonItem alloc]init];
    [_barButtonItem setImage:[UIImage imageNamed:@"ic_menu@3x.png"]];
    _barButtonItem.tintColor = [UIColor whiteColor];
    self.navigationItem.leftBarButtonItem = _barButtonItem;
    
    _rightBarButtonItem = [[UIBarButtonItem alloc]init];
    [_rightBarButtonItem setImage:[UIImage imageNamed:@"ic_tick@3x.png"]];
    _rightBarButtonItem.tintColor = [UIColor whiteColor];
    [_rightBarButtonItem setTarget:self];
    [_rightBarButtonItem setAction:@selector(createEventAction)];
    self.navigationItem.rightBarButtonItem = _rightBarButtonItem;
    
    UILabel* titleLabelNav = [[UILabel alloc] initWithFrame:CGRectMake(0,40,320,40)];
    titleLabelNav.textAlignment = NSTextAlignmentLeft;
    titleLabelNav.text = NSLocalizedString(@"Create Event",@"");
    titleLabelNav.textColor = [UIColor whiteColor];
    self.navigationItem.titleView = titleLabelNav;
    
    [self allTextfieldTextEdges];
    [self allTextfieldLeftImages];
    [self userNameAndImage];
    
    if (APP_DELEGATE.isServerReachable)
    {
    [self getCategoriesFromURL];
    [self getArtistFromURL];
    [self getCurrenciesFromURL];
    }
    else
    {
        
        [[[UIAlertView alloc]initWithTitle:@"OutNAbout" message:@"Internet connection is not available. Please try again."  delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil]show];
    }
    
    // tap gesture for select categories / artists / location = = = = = = ==
    
    UITapGestureRecognizer *categoryTapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(selectCategories:)];
      categoryTapGesture.delegate=self;
    [self.viewOnCategoryTF addGestureRecognizer:categoryTapGesture];
    
    UITapGestureRecognizer *artistTapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(selectArtists:)];
    artistTapGesture.delegate=self;
    [self.viewOnSelectArtists addGestureRecognizer:artistTapGesture];

    UITapGestureRecognizer *tapGestureForLocation = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(selectDesiredLocation:)];
    tapGestureForLocation.delegate=self;
    [self.viewOnSelectLocation addGestureRecognizer:tapGestureForLocation];
    
    UITapGestureRecognizer *tapGestureForCurrency = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(selectCurrency:)];
    tapGestureForCurrency.delegate=self;
    [self.viewOnCurrency addGestureRecognizer:tapGestureForCurrency];
    
    UITapGestureRecognizer *tapGestureForOccurrence = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(selectOccurrence:)];
    tapGestureForOccurrence.delegate=self;
    [self.viewOnOccurrence addGestureRecognizer:tapGestureForOccurrence];

    tapGestureForStartDate = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(selectDatesAndTime:)];
    tapGestureForStartDate.delegate=self;
    [self.viewOnStartDate addGestureRecognizer:tapGestureForStartDate];
    
    tapGestureForStartTime = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(selectDatesAndTime:)];
    tapGestureForStartTime.delegate=self;
    [self.viewOnStartTime addGestureRecognizer:tapGestureForStartTime];
    
    tapGestureForEndDate = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(selectDatesAndTime:)];
    tapGestureForEndDate.delegate=self;
    [self.viewOnEndDate addGestureRecognizer:tapGestureForEndDate];
    
    tapGestureForEndTime = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(selectDatesAndTime:)];
    tapGestureForEndTime.delegate=self;
    [self.viewOnEndTime addGestureRecognizer:tapGestureForEndTime];
    
    
    [_descriptionView setDelegate:self];
    [self.view addSubview:_dimBackgroundView];
    [_dimBackgroundView addSubview:_popupView];
//    [_popupView addSubview:_tableViewListing];
    [_dimBackgroundView setBackgroundColor:[[UIColor blackColor] colorWithAlphaComponent:0.6]];
    [_dimBackgroundView setHidden:YES];
     [self popupTableView];
}



#pragma mark - get categories and artists by URL hitting 

-(void)getCurrenciesFromURL
{
    currencyJson = [[NSMutableArray alloc]init];
    currency = [[NSMutableArray alloc]init];
    currency_Id = [[NSMutableArray alloc]init];
    
    NSError *error;
    NSString *url_string = [NSString stringWithFormat:(API_URL @"allcurrencies")];
    NSData *data = [NSData dataWithContentsOfURL: [NSURL URLWithString:url_string]];
    NSDictionary *jsonData = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
    currencyJson = jsonData[@"data"];
    
    for (NSDictionary *cat in currencyJson)
    {
        
        NSString *categoryId = cat[@"id"];
        [currency_Id addObject:categoryId];
        
        NSString *categoryTitle = cat[@"currency"];
        [currency addObject:categoryTitle];
        
    }
    [_tableViewListing reloadData];
    
     NSLog(@"currencyID: %@  currency %@", currency_Id , currency);
    
}


-(void)getCategoriesFromURL
{
    json = [[NSMutableArray alloc]init];
    catTitle = [[NSMutableArray alloc]init];
    catId = [[NSMutableArray alloc]init];
    
   
    
    NSError *error;
    NSString *url_string = [NSString stringWithFormat:(API_URL @"allCategories")];
    NSData *data = [NSData dataWithContentsOfURL: [NSURL URLWithString:url_string]];
    NSDictionary *jsonData = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
    json = jsonData[@"data"];
    
    for (NSDictionary *cat in json)
    {
        
        NSString *categoryId = cat[@"cat_id"];
        [catId addObject:categoryId];
        
        NSString *categoryTitle = cat[@"cat_title"];
        [catTitle addObject:categoryTitle];
        
    }
    [_tableViewListing reloadData];
        
        
    
    
   

    // NSLog(@"json: %@", json);
    
}


-(void)getArtistFromURL
{
    artistJson = [[NSMutableArray alloc]init];
    artist_Title = [[NSMutableArray alloc]init];
    artist_Id = [[NSMutableArray alloc]init];
    
    NSError *error;
    NSString *url_string = [NSString stringWithFormat:(API_URL @"allArtists")];
    NSData *data = [NSData dataWithContentsOfURL: [NSURL URLWithString:url_string]];
    NSDictionary *jsonData = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
    artistJson = jsonData[@"data"];
    
    for (NSDictionary *artist in artistJson)
    {
        
        NSString *artistid = artist[@"artist_id"];
        [artist_Id addObject:artistid];
        
        NSString *artistTitle = artist[@"artist_name"];
        [artist_Title addObject:artistTitle];
        
    }
    [_tableViewListing reloadData];

    // NSLog(@"json: %@", json);
}

//




- (void) selectDatesAndTime: (UITapGestureRecognizer *)gesture
{
[_scrollView endEditing:YES];
    
//    if ([self.view viewWithTag:9]) {
//        return;
//    }
    CGRect toolbarTargetFrame = CGRectMake(0, self.view.bounds.size.height-216-44, 320, 44);
    CGRect datePickerTargetFrame = CGRectMake(0, self.view.bounds.size.height-216, 320, 216);
    
    UIView *darkView = [[UIView alloc] initWithFrame:self.view.bounds] ;
    darkView.alpha = 0;
    darkView.backgroundColor = [UIColor blackColor];
    darkView.tag = 9;
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissDatePickerView:)] ;
    [darkView addGestureRecognizer:tapGesture];
    [self.view addSubview:darkView];
    

    datePicker = [[UIDatePicker alloc] initWithFrame:CGRectMake(0, self.view.bounds.size.height+44, 320, 216)];
    datePicker.tag = 10;
     [datePicker setDate:[NSDate date]];
    datePicker.backgroundColor = [UIColor whiteColor];
 
    UIToolbar *toolBar = [[UIToolbar alloc] initWithFrame:CGRectMake(20, self.view.bounds.size.height, 320, 44)];
    toolBar.tag = 11;
    toolBar.barStyle = UIBarStyleBlackTranslucent;
    
    UIBarButtonItem *spacer = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    
       UIBarButtonItem *cancelButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(cancelDatePicker:)];
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(dismissDatePicker:)];
    
//    datePicker.hidden=NO;
//    darkView.hidden=NO;
//    toolBar.hidden=NO;
    if (gesture==tapGestureForStartDate) {

        doneButton.tag=100;
        cancelButton.tag=100;
        datePicker.datePickerMode = UIDatePickerModeDate;
        [datePicker addTarget:self action:@selector(startDateTextField:) forControlEvents:UIControlEventValueChanged];
        [_startDateTF setInputView:datePicker];

    }
    
    else if (gesture==tapGestureForStartTime) {
         doneButton.tag=101;
         cancelButton.tag=101;
        datePicker.datePickerMode = UIDatePickerModeTime;
        [datePicker addTarget:self action:@selector(startTimeTextField:) forControlEvents:UIControlEventValueChanged];
        [_startTimeTF setInputView:datePicker];

    }
    else if (gesture==tapGestureForEndDate) {
         doneButton.tag=102;
         cancelButton.tag=102;
        datePicker.datePickerMode = UIDatePickerModeDate;
        [datePicker addTarget:self action:@selector(endDateTextField:) forControlEvents:UIControlEventValueChanged];
        [_endDateTF setInputView:datePicker];

    }
    else if (gesture==tapGestureForEndTime) {
        
        if (_startTimeTF.text.length) {
            doneButton.tag=103;
            cancelButton.tag=103;
            datePicker.datePickerMode = UIDatePickerModeTime;
            [datePicker addTarget:self action:@selector(endTimeTextField:) forControlEvents:UIControlEventValueChanged];
            [_endTimeTF setInputView:datePicker];

        }
        else{
            datePicker.hidden=YES;
            darkView.hidden=YES;
            toolBar.hidden=YES;
            
            [[[UIAlertView alloc]initWithTitle:@"OutNAbout" message:@"Select Start time first."  delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil]show];
        }
 
    }

    [self.view addSubview:datePicker];
    
    [toolBar setItems:[NSArray arrayWithObjects:cancelButton,spacer, doneButton, nil]];
    [self.view addSubview:toolBar];
    
    [UIView beginAnimations:@"MoveIn" context:nil];
    toolBar.frame = toolbarTargetFrame;
    datePicker.frame = datePickerTargetFrame;
    darkView.alpha = 0.5;
    [UIView commitAnimations];

}



- (void)removeViews:(id)object {
    [[self.view viewWithTag:9] removeFromSuperview];
    [[self.view viewWithTag:10] removeFromSuperview];
    [[self.view viewWithTag:11] removeFromSuperview];
}



- (void)cancelDatePicker:(id)sender {
     UIBarButtonItem *tag =(UIBarButtonItem*)sender;
    
    if (tag.tag==100) {
        _startDateTF.text = nil;
    }
     else if (tag.tag==101) {
         _startTimeTF.text = nil;
         _endTimeTF.text = nil;
     }
    
     else if (tag.tag==102) {
         _endDateTF.text = nil;
     }
     else if (tag.tag==103) {
         _endTimeTF.text = nil;
     }
    
    CGRect toolbarTargetFrame = CGRectMake(0, self.view.bounds.size.height, 320, 44);
    CGRect datePickerTargetFrame = CGRectMake(0, self.view.bounds.size.height+44, 320, 216);
    
    [UIView beginAnimations:@"MoveOut" context:nil];
    [self.view viewWithTag:9].alpha = 0;
    [self.view viewWithTag:10].frame = datePickerTargetFrame;
    [self.view viewWithTag:11].frame = toolbarTargetFrame;
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDidStopSelector:@selector(removeViews:)];
    [UIView commitAnimations];

    
}

- (void)dismissDatePickerView:(id)sender {
    
    CGRect toolbarTargetFrame = CGRectMake(0, self.view.bounds.size.height, 320, 44);
    CGRect datePickerTargetFrame = CGRectMake(0, self.view.bounds.size.height+44, 320, 216);
    
    [UIView beginAnimations:@"MoveOut" context:nil];
    [self.view viewWithTag:9].alpha = 0;
    [self.view viewWithTag:10].frame = datePickerTargetFrame;
    [self.view viewWithTag:11].frame = toolbarTargetFrame;
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDidStopSelector:@selector(removeViews:)];
    [UIView commitAnimations];

    
}

- (void)dismissDatePicker:(id)sender {
    
    UIBarButtonItem *tag =(UIBarButtonItem*)sender;
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    NSDate *eventDate = datePicker.date;
    if (tag.tag==100) {

        [dateFormat setDateFormat:@"yyyy/MM/dd"];
        NSString *dateString = [dateFormat stringFromDate:eventDate];
        _startDateTF.text = [NSString stringWithFormat:@"%@",dateString];

    }
    else if (tag.tag==101) {
        

        [dateFormat setDateFormat:@"hh:mm a"];
        NSString *timeString = [dateFormat stringFromDate:eventDate];
        _startTimeTF.text = [NSString stringWithFormat:@"%@",timeString];
        
    }
    else if (tag.tag==102) {
        
        [dateFormat setDateFormat:@"yyyy/MM/dd"];
        NSString *dateString = [dateFormat stringFromDate:eventDate];
        _endDateTF.text = [NSString stringWithFormat:@"%@",dateString];
        
    }
    else if (tag.tag==103) {
        
        [dateFormat setDateFormat:@"hh:mm a"];
        NSString *timeString = [dateFormat stringFromDate:eventDate];
        _endTimeTF.text = [NSString stringWithFormat:@"%@",timeString];
        
    }

    
    CGRect toolbarTargetFrame = CGRectMake(0, self.view.bounds.size.height, 320, 44);
    CGRect datePickerTargetFrame = CGRectMake(0, self.view.bounds.size.height+44, 320, 216);
    
    [UIView beginAnimations:@"MoveOut" context:nil];
    [self.view viewWithTag:9].alpha = 0;
    [self.view viewWithTag:10].frame = datePickerTargetFrame;
    [self.view viewWithTag:11].frame = toolbarTargetFrame;
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDidStopSelector:@selector(removeViews:)];
    [UIView commitAnimations];
}








-(void) startTimeTextField:(UIDatePicker *)sender
{
    
    
    
    UIDatePicker *picker = (UIDatePicker*)_startTimeTF.inputView;
        // [picker setMaximumDate:[NSDate date]];
    
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        NSDate *eventDate = picker.date;
        [dateFormat setDateFormat:@"hh:mm a"];
    
        NSString *startTimeString = [dateFormat stringFromDate:eventDate];
     _startTimeTF.text = [NSString stringWithFormat:@"%@",startTimeString];
 
}

-(void) endTimeTextField:(UIDatePicker *)sender
{
    
        UIDatePicker *picker = (UIDatePicker*)_endTimeTF.inputView;
        // [picker setMaximumDate:[NSDate date]];
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        NSDate *eventDate = picker.date;
        [dateFormat setDateFormat:@"hh:mm a"];
    
        NSString *endTimeString = [dateFormat stringFromDate:eventDate];
        _endTimeTF.text = [NSString stringWithFormat:@"%@",endTimeString];

}

-(void) startDateTextField:(UIDatePicker *)sender
{


    UIDatePicker *picker = (UIDatePicker*)_startDateTF.inputView;
    // [picker setMaximumDate:[NSDate date]];
    [picker setMinimumDate:[NSDate date]];
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    NSDate *eventDate = sender.date;
    [dateFormat setDateFormat:@"yyyy/MM/dd"];
    
    NSString *dateString = [dateFormat stringFromDate:eventDate];
    _startDateTF.text = [NSString stringWithFormat:@"%@",dateString];
}

-(void) endDateTextField:(UIDatePicker *)sender
{
    
    UIDatePicker *picker = (UIDatePicker*)_endDateTF.inputView;
    //  [picker setMaximumDate:[NSDate date]];
    [picker setMinimumDate:[NSDate date]];
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    NSDate *eventDate = picker.date;
    [dateFormat setDateFormat:@"yyyy/MM/dd"];
    
    NSString *dateString = [dateFormat stringFromDate:eventDate];
    _endDateTF.text = [NSString stringWithFormat:@"%@",dateString];
    
}




- (void) selectOccurrence: (UITapGestureRecognizer *)gesture
{
    pickerType=@"occurrence";
    [self openPickerViewForCurrencyAndOccurrence];
}



- (void) selectCurrency: (UITapGestureRecognizer *)gesture
{
    pickerType=@"currency";

    [self openPickerViewForCurrencyAndOccurrence];


}



-(void)openPickerViewForCurrencyAndOccurrence
{
    
    [_scrollView endEditing:YES];
    
    CGRect toolbarTargetFrame = CGRectMake(0, self.view.bounds.size.height-216-44, 320, 44);
    CGRect datePickerTargetFrame = CGRectMake(0, self.view.bounds.size.height-216, 320, 216);
    
    UIView *darkView1 = [[UIView alloc] initWithFrame:self.view.bounds] ;
    darkView1.alpha = 0;
    darkView1.backgroundColor = [UIColor blackColor];
    darkView1.tag = 9;
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissPickerView:)] ;
    [darkView1 addGestureRecognizer:tapGesture];
    [self.view addSubview:darkView1];
    
    pickerView = [[UIPickerView alloc] initWithFrame:CGRectMake(0, self.view.bounds.size.height+44, 320, 216)];
    pickerView.showsSelectionIndicator = YES;
    pickerView.hidden = NO;
    pickerView.tag = 10;
    pickerView.delegate = self;
    pickerView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:pickerView];
    
    UIToolbar *toolBar = [[UIToolbar alloc] initWithFrame:CGRectMake(20, self.view.bounds.size.height, 320, 44)];
    toolBar.tag = 11;
    toolBar.barStyle = UIBarStyleBlackTranslucent;
    
    UIBarButtonItem *spacer = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    
    UIBarButtonItem *cancelButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(cancelPicker:)];
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(donePickerView:)];
    
    [toolBar setItems:[NSArray arrayWithObjects:cancelButton,spacer, doneButton, nil]];
    [self.view addSubview:toolBar];
    
    [UIView beginAnimations:@"MoveIn" context:nil];
    toolBar.frame = toolbarTargetFrame;
    pickerView.frame = datePickerTargetFrame;
    darkView1.alpha = 0.5;
    [UIView commitAnimations];
}

- (void)removedViews:(id)object {
    [[self.view viewWithTag:9] removeFromSuperview];
    [[self.view viewWithTag:10] removeFromSuperview];
    [[self.view viewWithTag:11] removeFromSuperview];

}


- (void)cancelPicker:(id)sender {
    
    if ([pickerType isEqualToString:@"currency"]) {
    
    _currencyTF.text = nil;
        
    }
    
    else
    {
        _occurrenceTF.text = nil;
    }
    
    CGRect toolbarTargetFrame = CGRectMake(0, self.view.bounds.size.height, 320, 44);
    CGRect datePickerTargetFrame = CGRectMake(0, self.view.bounds.size.height+44, 320, 216);
    
    [UIView beginAnimations:@"MoveOut" context:nil];
    [self.view viewWithTag:9].alpha = 0;
    [self.view viewWithTag:10].frame = datePickerTargetFrame;
    [self.view viewWithTag:11].frame = toolbarTargetFrame;
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDidStopSelector:@selector(removedViews:)];
    [UIView commitAnimations];
    

    pickerType=@"";
}

- (void)dismissPickerView:(id)sender {
    
    CGRect toolbarTargetFrame = CGRectMake(0, self.view.bounds.size.height, 320, 44);
    CGRect datePickerTargetFrame = CGRectMake(0, self.view.bounds.size.height+44, 320, 216);
    
    [UIView beginAnimations:@"MoveOut" context:nil];
    [self.view viewWithTag:9].alpha = 0;
    [self.view viewWithTag:10].frame = datePickerTargetFrame;
    [self.view viewWithTag:11].frame = toolbarTargetFrame;
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDidStopSelector:@selector(removeViews:)];
    [UIView commitAnimations];
    
    pickerType=@"";

}


- (void)donePickerView:(id)sender {
    
    if ([pickerType isEqualToString:@"currency"]) {
    
    NSString *YourselectedTitle = [currency objectAtIndex:[pickerView selectedRowInComponent:0]];//
    _currencyTF.text = YourselectedTitle;
        
    }
    
    else
    {
        NSString *YourselectedTitle = [occurrenceArray objectAtIndex:[pickerView selectedRowInComponent:0]];//
        if ([YourselectedTitle isEqualToString:@"Once"]) {
            occurrencePostString=@"0";
        }
        else if ([YourselectedTitle isEqualToString:@"Weekly"]) {
            occurrencePostString=@"1";
        }else{
             occurrencePostString=@"2";
        }
        
        _occurrenceTF.text = YourselectedTitle;
    }

    CGRect toolbarTargetFrame = CGRectMake(0, self.view.bounds.size.height, 320, 44);
    CGRect datePickerTargetFrame = CGRectMake(0, self.view.bounds.size.height+44, 320, 216);
    
    [UIView beginAnimations:@"MoveOut" context:nil];
    [self.view viewWithTag:9].alpha = 0;
    [self.view viewWithTag:10].frame = datePickerTargetFrame;
    [self.view viewWithTag:11].frame = toolbarTargetFrame;
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDidStopSelector:@selector(removeViews:)];
    [UIView commitAnimations];
    
    
    pickerType=@"";

}




- (void) selectArtists: (UITapGestureRecognizer *)gesture
{
    [_scrollView endEditing:YES];
    catOrArtistKeyType = @"artist";
    _selectHeadingLabel.text = @"Select Artists";
    [_dimBackgroundView setHidden:NO];
    [_popupView setHidden:NO];
    [_tableViewListing reloadData];


    //[_scrollView setBackgroundColor:[UIColor lightGrayColor]];
}


- (void) selectCategories: (UITapGestureRecognizer *)gesture
{
    [_scrollView endEditing:YES];
    catOrArtistKeyType=@"catagory";
    _selectHeadingLabel.text = @"Select Categories";
    [_dimBackgroundView setHidden:NO];
    [_popupView setHidden:NO];
    [_tableViewListing reloadData];

}



- (IBAction)submitArtistButtonAction:(id)sender {
    
    if ([catOrArtistKeyType isEqualToString:@"artist"])
    {
        _artistTF.text =[[self getArtistTitle] componentsJoinedByString:@","];
        artistIDNo=[[self getArtistId] componentsJoinedByString:@","];

    }else{
        _categoriesTF.text =[[self getCategoryTitle] componentsJoinedByString:@","];
        categoryIDNo=[[self getCategoryId] componentsJoinedByString:@","];

    }
       NSLog(@"artist ID :%@",artistIDNo);
       NSLog(@"category ID :%@",categoryIDNo);

     [_dimBackgroundView setHidden:YES];
     catOrArtistKeyType = @"";
}


// get artist and category index by title and ID's 

-(NSArray *)getArtistTitle {
    NSMutableArray *selections = [[NSMutableArray alloc] init];
    
    for(NSIndexPath *indexPath in arrayArtistSelectedRows) {
        [selections addObject:[artist_Title objectAtIndex:indexPath.row]];
    }
    return selections;
}


-(NSArray *)getArtistId {
    NSMutableArray *selections = [[NSMutableArray alloc] init];
    
    for(NSIndexPath *indexPath in arrayArtistSelectedRows) {
        [selections addObject:[artist_Id objectAtIndex:indexPath.row]];
    }
    return selections;
}


-(NSArray *)getCategoryTitle {
    NSMutableArray *selections = [[NSMutableArray alloc] init];
    
    for(NSIndexPath *indexPath in arrayCategorySelectedRows) {
        [selections addObject:[catTitle objectAtIndex:indexPath.row]];
    }
    return selections;
}


-(NSArray *)getCategoryId {
    NSMutableArray *selections = [[NSMutableArray alloc] init];
    
    for(NSIndexPath *indexPath in arrayCategorySelectedRows) {
        [selections addObject:[catId objectAtIndex:indexPath.row]];
    }
    return selections;
}

//



#pragma mark - Show table view popup using tableview delegate methods = = = = === == =  == = = = = = = = = ==

-(void)popupTableView
{
    arrayArtistSelectedRows = [[NSMutableArray alloc] init];
    arrayCategorySelectedRows= [[NSMutableArray alloc] init];

//    _tableViewListing = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height) style:UITableViewStylePlain];
    _tableViewListing.delegate = self;
    _tableViewListing.dataSource = self;
    
    [_popupView addSubview:_tableViewListing];
}



-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if ([catOrArtistKeyType isEqualToString:@"artist"])
    {
    return artist_Title.count;
    }
    else{
    return catTitle.count;
    }
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *cellIdent = @"cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdent];
    
    if(cell == nil)
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdent];
    cell.textLabel.font = [UIFont systemFontOfSize:13.0f];
    
    if ([catOrArtistKeyType isEqualToString:@"artist"])
    {
     cell.textLabel.text = [artist_Title objectAtIndex:indexPath.row];
        if([arrayArtistSelectedRows containsObject:indexPath])
        { cell.accessoryType = UITableViewCellAccessoryCheckmark;
        } else
        { cell.accessoryType = UITableViewCellAccessoryNone;
        }
        
    }
    else{
        cell.textLabel.text = [catTitle objectAtIndex:indexPath.row];//
        if([arrayCategorySelectedRows containsObject:indexPath])
        { cell.accessoryType = UITableViewCellAccessoryCheckmark;
        } else
        { cell.accessoryType = UITableViewCellAccessoryNone;
        }
    }
    
    return cell;
}



-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    if ([catOrArtistKeyType isEqualToString:@"artist"])
    {
    
    if(cell.accessoryType == UITableViewCellAccessoryNone) {
        
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
        [arrayArtistSelectedRows addObject:indexPath];
    }
    else {
        cell.accessoryType = UITableViewCellAccessoryNone;
        [arrayArtistSelectedRows removeObject:indexPath];
    }
    }
    else{
        if(cell.accessoryType == UITableViewCellAccessoryNone) {
            
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
            [arrayCategorySelectedRows addObject:indexPath];
        }
        else {
            cell.accessoryType = UITableViewCellAccessoryNone;
            [arrayCategorySelectedRows removeObject:indexPath];
        }
    }
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}






//- (void) handleTapFrom: (UITapGestureRecognizer *)gesture
//{
//    
//    catOrArtistKeyType=@"catagory";
////            float paddingTopBottom = 20.0f;
////            float paddingLeftRight = 20.0f;
////            
////            CGPoint point = CGPointMake(paddingLeftRight, (self.navigationController.navigationBar.frame.size.height + paddingTopBottom) + paddingTopBottom);
////            CGSize size = CGSizeMake((self.view.frame.size.width - (paddingLeftRight * 2)), self.view.frame.size.height - ((self.navigationController.navigationBar.frame.size.height + paddingTopBottom) + (paddingTopBottom * 2)));
////            
////            LPPopupListView *listView = [[LPPopupListView alloc] initWithTitle:@"Category list" list:[self categoryList] selectedIndexes:self.selectedIndexes point:point size:size multipleSelection:YES disableBackgroundInteraction:YES];
////            listView.delegate = self;
////            
////            [listView showInView:self.navigationController.view animated:YES];
//
//}



//#pragma mark - LPPopupListViewDelegate
//
//- (void)popupListView:(LPPopupListView *)popUpListView didSelectIndex:(NSInteger)index
//{
//  //  NSLog(@"popUpListView - didSelectIndex: %ld", (long)index);
//}
//
//- (void)popupListViewDidHide:(LPPopupListView *)popUpListView selectedIndexes:(NSIndexSet *)selectedIndexes
//{
//    
//    self.selectedIndexes = [[NSMutableIndexSet alloc] initWithIndexSet:selectedIndexes];
//    
//    self.categoriesTF.text = @"";
//    [selectedIndexes enumerateIndexesUsingBlock:^(NSUInteger idx, BOOL *stop)
//    {
//        
//      self.categoriesTF.text = [self.categoriesTF.text stringByAppendingFormat:@"%@\n", [[self categoryList] objectAtIndex:idx] ];
//        
//        //self.categoriesTF.text = [[[[self categoryList] objectAtIndex:idx] componentsSeparatedByString:@", "]];
//    }];
//
//}



//#pragma mark - Array List
//
//- (NSArray *)categoryList
//{
//    return catTitle;
//}
//
//- (NSArray *)categoryIDs
//{
//    return catId;
//}







-(void)allTextfieldLeftImages
{
    
    UIImageView *arrow = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"ic_selectRed@3x.png"]];
    arrow.frame = CGRectMake(0.0, 0.0, arrow.image.size.width+20.0, arrow.image.size.height);
    arrow.contentMode = UIViewContentModeCenter;
    _startDateTF.rightView = arrow;
    _startDateTF.rightViewMode = UITextFieldViewModeAlways;

    UIImageView *arrow2 = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"ic_selectRed@3x.png"]];
    arrow2.frame = CGRectMake(0.0, 0.0, arrow2.image.size.width+20.0, arrow2.image.size.height);
    arrow2.contentMode = UIViewContentModeCenter;
    _startTimeTF.rightView = arrow2;
    _startTimeTF.rightViewMode = UITextFieldViewModeAlways;
    
    
    UIImageView *arrow3 = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"ic_selectRed@3x.png"]];
    arrow3.frame = CGRectMake(0.0, 0.0, arrow3.image.size.width+20.0, arrow3.image.size.height);
    arrow3.contentMode = UIViewContentModeCenter;
      _endDateTF.rightView = arrow3;
    _endDateTF.rightViewMode = UITextFieldViewModeAlways;
    
    UIImageView *arrow4 = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"ic_selectRed@3x.png"]];
    arrow4.frame = CGRectMake(0.0, 0.0, arrow4.image.size.width+20.0, arrow4.image.size.height);
    arrow4.contentMode = UIViewContentModeCenter;
    _endTimeTF.rightView = arrow4;
    _endTimeTF.rightViewMode = UITextFieldViewModeAlways;
    
    UIImageView *catImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"ic_categories@3x.png"]];
    catImage.frame = CGRectMake(0.0, 0.0, catImage.image.size.width+20.0, catImage.image.size.height);
    catImage.contentMode = UIViewContentModeCenter;
    _categoriesTF.leftView = catImage;
    _categoriesTF.leftViewMode = UITextFieldViewModeAlways;

    UIImageView *costImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"ic_costNew@3x.png"]];
    costImage.frame = CGRectMake(0.0, 0.0, costImage.image.size.width+15.0, costImage.image.size.height);
    costImage.contentMode = UIViewContentModeCenter;
    _costTF.leftView = costImage;
   _costTF.leftViewMode = UITextFieldViewModeAlways;
    
    UIImageView *currencyImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"ic_currencyN@3x.png"]];
    currencyImage.frame = CGRectMake(0.0, 0.0, currencyImage.image.size.width+15.0, currencyImage.image.size.height);
    currencyImage.contentMode = UIViewContentModeCenter;
    _currencyTF.leftView = currencyImage;
    _currencyTF.leftViewMode = UITextFieldViewModeAlways;
    
    UIImageView *occurrenceImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"occurance@3x.png"]];
    occurrenceImage.frame = CGRectMake(0.0, 0.0, occurrenceImage.image.size.width+20.0, occurrenceImage.image.size.height);
    occurrenceImage.contentMode = UIViewContentModeCenter;
    _occurrenceTF.leftView = occurrenceImage;
    _occurrenceTF.leftViewMode = UITextFieldViewModeAlways;
    
    UIImageView *descriptionImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"ic_description@3x.png"]];
    descriptionImage.frame = CGRectMake(0.0, 0.0, descriptionImage.image.size.width+20.0, descriptionImage.image.size.height);
    descriptionImage.contentMode = UIViewContentModeCenter;
    _descriptionForIconTF.leftView = descriptionImage;
    _descriptionForIconTF.leftViewMode = UITextFieldViewModeAlways;
    
    UIImageView *costOfferImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"ic_offersNew@3x.png"]];
    costOfferImage.frame = CGRectMake(0.0, 0.0, costOfferImage.image.size.width+20.0, costOfferImage.image.size.height);
    costOfferImage.contentMode = UIViewContentModeCenter;
    _costOffersTF.leftView = costOfferImage;
    _costOffersTF.leftViewMode = UITextFieldViewModeAlways;

    UIImageView *artistImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"ic_artist@3x.png"]];
    artistImage.frame = CGRectMake(0.0, 0.0, artistImage.image.size.width+20.0, artistImage.image.size.height);
    artistImage.contentMode = UIViewContentModeCenter;
    _artistTF.leftView = artistImage;
    _artistTF.leftViewMode = UITextFieldViewModeAlways;

    UIImageView *venueImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"ic_vanue@3x.png"]];
    venueImage.frame = CGRectMake(0.0, 0.0, venueImage.image.size.width+20.0, venueImage.image.size.height);
    venueImage.contentMode = UIViewContentModeCenter;
    _addVenueTF.leftView = venueImage;
    _addVenueTF.leftViewMode = UITextFieldViewModeAlways;
    
    UIImageView *locationImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"add_location@3x.png"]];
    locationImage.frame = CGRectMake(0.0, 0.0, locationImage.image.size.width+20.0, locationImage.image.size.height);
    locationImage.contentMode = UIViewContentModeCenter;
    _enterLocationTF.leftView = locationImage;
    _enterLocationTF.leftViewMode = UITextFieldViewModeAlways;
    


}




-(void)allTextfieldTextEdges
{

    _categoriesTF.enabled=NO;
    _artistTF.enabled=NO;
    _enterLocationTF.enabled=NO;
    
    [_costTF setKeyboardType:UIKeyboardTypeNumberPad];
    
    UIToolbar* numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 50)];
    numberToolbar.barStyle = UIBarStyleBlackTranslucent;
    numberToolbar.backgroundColor=[UIColor blackColor];
    numberToolbar.items = @[
                            [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                            [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(doneWithNumberPad)]];
    [numberToolbar sizeToFit];
    _costTF.inputAccessoryView = numberToolbar;
    
    //_startDateTF.enabled=NO;
    //_endDateTF.enabled=NO;
    //_startTimeTF.enabled=NO;
    //_endTimeTF.enabled=NO;

    //set text tint color for all textfields =======
    
    _eventTitleTF.tintColor= [UIColor redColor];
    _startDateTF.tintColor= [UIColor redColor];
    _endDateTF.tintColor= [UIColor redColor];
    _startTimeTF.tintColor= [UIColor redColor];
    _endTimeTF.tintColor= [UIColor redColor];
    _costTF.tintColor= [UIColor redColor];
    _currencyTF.tintColor= [UIColor redColor];
    _costOffersTF.tintColor= [UIColor redColor];
    _descriptionView.tintColor= [UIColor redColor];
    _categoriesTF.tintColor= [UIColor redColor];
    _artistTF.tintColor= [UIColor redColor];
    _addVenueTF.tintColor= [UIColor redColor];
    _enterLocationTF.tintColor= [UIColor redColor];
    
    // set text edges for all textfields ======

    _eventTitleTF.layer.sublayerTransform = CATransform3DMakeTranslation(5, 0, 0);
    _startDateTF.layer.sublayerTransform = CATransform3DMakeTranslation(5, 0, 0);
    _endDateTF.layer.sublayerTransform = CATransform3DMakeTranslation(5, 0, 0);
    _startTimeTF.layer.sublayerTransform = CATransform3DMakeTranslation(5, 0, 0);
    _endTimeTF.layer.sublayerTransform = CATransform3DMakeTranslation(5, 0, 0);
    _costTF.layer.sublayerTransform = CATransform3DMakeTranslation(10, 0, 0);
    _currencyTF.layer.sublayerTransform = CATransform3DMakeTranslation(10, 0, 0);
    _occurrenceTF.layer.sublayerTransform = CATransform3DMakeTranslation(10, 0, 0);
    _costOffersTF.layer.sublayerTransform = CATransform3DMakeTranslation(10, 0, 0);
    _descriptionForIconTF.layer.sublayerTransform = CATransform3DMakeTranslation(10, 0, 0);
    _descriptionView.layer.sublayerTransform = CATransform3DMakeTranslation(5, 0, 0);
    _categoriesTF.layer.sublayerTransform = CATransform3DMakeTranslation(10, 0, 0);
    _artistTF.layer.sublayerTransform = CATransform3DMakeTranslation(10, 0, 0);
    
    _addVenueTF.layer.sublayerTransform = CATransform3DMakeTranslation(10, 0, 0);
    _enterLocationTF.layer.sublayerTransform = CATransform3DMakeTranslation(10, 0, 0);

    
    
//   self.descriptionView.text=@"This returns the size of the rectangle that fits the given string with the given font. Pass in a size with the desired width and a maximum height, and then you can look at the height returned to fit the text. There is a version that lets you specify line break mode also.This returns the size of the rectangle that fits the given string with the given font. Pass in a size with the desired width and a maximum height, and then you can look at the height returned to fit the text. There is a version that lets you specify line break mode yes.";

    
//    CGFloat fixedWidth = self.descriptionView.frame.size.width;
//    CGSize newSize = [self.descriptionView sizeThatFits:CGSizeMake(fixedWidth, MAXFLOAT)];
//    CGRect newFrame = self.descriptionView.frame;
//    newFrame.size = CGSizeMake(fmaxf(newSize.width, fixedWidth), newSize.height);
//    self.descriptionView.frame=newFrame;
    
    
    [_startDateTF.layer setMasksToBounds:YES];
    [_startDateTF.layer setCornerRadius:4.0f];
    
    [_startTimeTF.layer setMasksToBounds:YES];
    [_startTimeTF.layer setCornerRadius:4.0f];
    
    [_endDateTF.layer setMasksToBounds:YES];
    [_endDateTF.layer setCornerRadius:4.0f];
    
    [_endTimeTF.layer setMasksToBounds:YES];
    [_endTimeTF.layer setCornerRadius:4.0f];
    
    [_currencyTF.layer setMasksToBounds:YES];
    [_currencyTF.layer setCornerRadius:4.0f];
    
    [_costTF.layer setMasksToBounds:YES];
    [_costTF.layer setCornerRadius:4.0f];

    

    
    _eventTitleTF.delegate = self;
    _startDateTF.delegate = self;
    _endDateTF.delegate = self;
    _startTimeTF.delegate = self;
    _endTimeTF.delegate = self;
    _currencyTF.delegate =self;
    _costTF.delegate = self;
    _costOffersTF.delegate = self;
    _descriptionView.delegate = self;
    _categoriesTF.delegate = self;
    
    _artistTF.delegate = self;
    _addVenueTF.delegate = self;
    _enterLocationTF.delegate = self;
  
}


-(void)cancelNumberPad{
    [_costTF resignFirstResponder];
    _costTF.text = @"";
}

-(void)doneWithNumberPad{
    //NSString *numberFromTheKeyboard = numberTextField.text;
    [_costTF resignFirstResponder];
}




-(void)userNameAndImage
{
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *loginType = [defaults objectForKey:@"loginType"];
    NSString *fbUserPic = [defaults objectForKey:@"fbProfilepic"];
    NSURL *fburl=[NSURL URLWithString:fbUserPic];

    NSLog(@"fb picture**** %@",fbUserPic);
    NSString *gpUserPic = [defaults objectForKey:@"gpUserPic"];
    NSURL *url=[NSURL URLWithString:gpUserPic];

    _userImageView.layer.cornerRadius =_userImageView.frame.size.width / 2;
    _userImageView.clipsToBounds = YES;
    _userImageView.userInteractionEnabled = YES;
    
    if ([loginType isEqualToString:@"fb"]) {
        
        
        SDWebImageManager *manager = [SDWebImageManager sharedManager];
        [manager downloadImageWithURL:fburl
                              options:1
                             progress:^(NSInteger receivedSize, NSInteger  expectedSize) {
                             }
                            completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                                if (image) {
                                    dispatch_async(dispatch_get_main_queue(), ^{
                                        _userImageView.image = image;
                                        
                                    });
                                }
                            }
         ];

        
//        FBSDKProfilePictureView *profilePictureview = [[FBSDKProfilePictureView alloc]initWithFrame:_userImageView.frame];
//        profilePictureview.layer.cornerRadius = 40.0f;
//        profilePictureview.layer.masksToBounds = YES;

        
        firstName = [defaults objectForKey:@"fbFirstName"];
        lastName = [defaults objectForKey:@"fbLastName"];
    }
    
    else if ([loginType isEqualToString:@"gp"]) {

                    SDWebImageManager *manager = [SDWebImageManager sharedManager];
                    [manager downloadImageWithURL:url
                                          options:1
                                         progress:^(NSInteger receivedSize, NSInteger  expectedSize) {
                                         }
                                        completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                                            if (image) {
                                                dispatch_async(dispatch_get_main_queue(), ^{
                                                    _userImageView.image = image;
        
                                                });
                                            }
                                        }
                     ];
        
        
        
        firstName = [defaults objectForKey:@"gpFirstName"];
        lastName = [defaults objectForKey:@"gpLastName"];

    }
    
    else
    {
        firstName = [defaults objectForKey:@"first_name"];
        lastName = [defaults objectForKey:@"last_name"];
       // [_userImageView setImage:[UIImage imageNamed:@"event_default.jpg"]];
        //[cell.contentView addSubview:proflePic];
        [self eventPostedByUserDetail];

    }

    _userNameLabel.text = [NSString stringWithFormat:@"%@ %@",firstName,lastName];
    _userNameLabel.textColor = [UIColor blackColor];
    _userNameLabel.backgroundColor = [UIColor clearColor];

    
}


-(void)eventPostedByUserDetail
{
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
   // access_token = [defaults objectForKey:@"access_token"];
//    password = [defaults objectForKey:@"password"];
    NSString *UserPicture = [defaults objectForKey:@"user_pic"];
    
    if ([UserPicture isEqualToString:@""]) {
        _userImageView.image = [UIImage imageNamed:@"icon-user-default.png"];
    }
    
    else{
    
    NSString *imageURL = @"http://agiledevelopers.in/outnabout/uploads/";
    NSString *url_Img_FULL = [imageURL stringByAppendingPathComponent:UserPicture];
    NSURL *userImagePic = [NSURL URLWithString:url_Img_FULL];

    // set user profile picture=====
    
    SDWebImageManager *manager = [SDWebImageManager sharedManager];
    [manager downloadImageWithURL:userImagePic
                          options:0
                         progress:^(NSInteger receivedSize, NSInteger  expectedSize) {
                         }
                        completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                            if (image) {
                                dispatch_async(dispatch_get_main_queue(), ^{
                                    _userImageView.image = image;
                                    
                                    _userImageView.layer.borderWidth = 1.5;
                                    _userImageView.layer.borderColor = [UIColor colorWithRed:1.00 green:0.41 blue:0.41 alpha:1.0].CGColor;
            
                                });
                            }
                        }
     ];
        
    }

}


#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wdeprecated-declarations"



- (IBAction)galleryButtonAction:(id)sender
{

    mediaPicker = [[UIImagePickerController alloc] init];
    mediaPicker.delegate = self;
    mediaPicker.allowsEditing = NO;
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil
                                                             delegate:self
                                                    cancelButtonTitle:@"Cancel"
                                               destructiveButtonTitle:nil
                                                    otherButtonTitles:@"Take photo", @"Choose existing", nil];
    [actionSheet showInView:self.view];

}







-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 0) {

//        if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
//            return;
//        }
        mediaPicker.sourceType = UIImagePickerControllerSourceTypeCamera;

    } else if (buttonIndex == 1) {
        mediaPicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    }
    else {
        return;
    }
    [self presentViewController:mediaPicker animated:YES completion:nil];
}





- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    [self dismissViewControllerAnimated:YES completion:nil];
    
    
    dictImages=[[NSMutableDictionary alloc]init];
    
    // [KiiViewUtilities showProgressHUD:@"Processing..." withView:self.view];
    
    imagePath = [info valueForKey: UIImagePickerControllerOriginalImage];
    
    UIImagePickerControllerSourceType sourceType = picker.sourceType;
    
    ALAssetsLibrary *assetsLibrary = [[ALAssetsLibrary alloc] init];
    
    imagePath = [info valueForKey: UIImagePickerControllerOriginalImage];
    
    NSString *docDirPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *filePath =  [docDirPath stringByAppendingPathComponent:@"myImage.png"];
    NSLog (@"File Path = %@", filePath);
    
    
    imageName = [NSString stringWithFormat:@"%0.0f.jpg",[[NSDate date] timeIntervalSince1970]];
    
    [dictImages setObject:imagePath forKey:@"profilepic"];
    
    
    if (sourceType == UIImagePickerControllerSourceTypeCamera) {
        UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage];
        
        // Request to save the image to camera roll
        [assetsLibrary writeImageToSavedPhotosAlbum:[image CGImage] orientation:(ALAssetOrientation) [image imageOrientation] completionBlock:^(NSURL *assetURL, NSError *error) {
            if (error) {
              
            } else {
                NSLog(@"url %@", assetURL);
                
                [self saveImageDataToTemporaryArea:assetsLibrary withAssetUrl:assetURL];
            }
        }];
        
//        _galleryImageView.image= nil;
//        _galleryImageView = nil;
        
    } else {
        NSURL *url = (NSURL *) [info valueForKey:UIImagePickerControllerReferenceURL];
        
        
        [self saveImageDataToTemporaryArea:assetsLibrary withAssetUrl:url];
    }
}





-(void)saveImageDataToTemporaryArea:(ALAssetsLibrary *)assetsLibrary withAssetUrl:(NSURL *)url
{
    [assetsLibrary assetForURL:url
                   resultBlock:^(ALAsset *asset) {
                       ALAssetRepresentation *representation = [asset defaultRepresentation];
                       NSString *fileUTI = [representation UTI];
                       NSString *fileName = [representation filename];
                       ALAssetOrientation orientation = (ALAssetOrientation) [[asset valueForProperty:@"ALAssetPropertyOrientation"] intValue];
                       CGImageRef cgImg = [representation fullResolutionImage];
                       self.selectedImage = [UIImage imageWithCGImage:cgImg scale:1.0 orientation:(UIImageOrientation) orientation];
                       NSData *data = nil;
                       if ([fileUTI isEqualToString:@"public.png"]) {
                           data = UIImagePNGRepresentation(self.selectedImage);
                       } else {
                           data = UIImageJPEGRepresentation(self.selectedImage, 95);
                       }
                       
                       NSError *error = nil;
                       self.selectedImagePath = [NSTemporaryDirectory() stringByAppendingPathComponent:fileName];
                       [data writeToFile:self.selectedImagePath options:NSDataWritingAtomic error:&error];
                       if (error != nil) {
                           self.selectedImage = nil;
                           self.galleryImageView = nil;
                           self.galleryImageView.image = nil;
                           NSLog(@"Failed to save : %i - %s", errno, strerror(errno));
                           //                           [KiiViewUtilities hideProgressHUD:self.view];
                           //                           [KiiViewUtilities showFailureHUD:@"Selected photo can not be used" withView:self.view];
                           return;
                       }
                       //self.galleryImageView.contentMode = UIViewContentModeScaleAspectFit;
                        self.galleryImageView.contentMode = UIViewContentModeScaleToFill;
                       self.galleryImageView.image = self.selectedImage;
                       
                       //                       [KiiViewUtilities hideProgressHUD:self.view];
                       //                       [KiiViewUtilities showSuccessHUD:@"Photo selected" withView:self.view];
                   }
                  failureBlock:^(NSError *error) {
                      //                      [KiiViewUtilities hideProgressHUD:self.view];
                      //                      [KiiViewUtilities showFailureHUD:@"Selected photo can not be used" withView:self.view];
                  }];
}


- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [self dismissViewControllerAnimated:YES completion:nil];
}







// create a new event, here is a action = = = = = = = = = = = = = = = = = = = = =

-(void)createEventAction
{
  
    BOOL isValidString = [self checkTextfieldValidations];
    if (isValidString) {
        [self imagePostToServer];
    
    }
    
   

    
}



-(void)imagePostToServer
{
    
    [SVProgressHUD showWithStatus:@"Please wait..." maskType:SVProgressHUDMaskTypeBlack];

    
    [self.connection cancel];
    
    NSMutableData *data = [[NSMutableData alloc] init];
    self.receivedData = data;
    
    // Dictionary that holds post parameters. You can set your post parameters that your server accepts or programmed to accept.
    NSMutableDictionary* _params = [[NSMutableDictionary alloc] init];
    [_params setObject:access_token forKey:@"app_token"];
    [_params setObject:_eventTitleTF.text forKey:@"title"];
    [_params setObject:_descriptionView.text forKey:@"description"];
    [_params setObject:_enterLocationTF.text forKey:@"state"];
    [_params setObject:_addVenueTF.text forKey:@"venue"];
    [_params setObject:_costTF.text forKey:@"cost"];
    [_params setObject:_currencyTF.text forKey:@"currency"];
    [_params setObject:latitudes forKey:@"latitude"];
    [_params setObject:longitudes forKey:@"longitude"];
    [_params setObject:categoryIDNo forKey:@"categories"];
    [_params setObject:artistIDNo forKey:@"artists"];
    [_params setObject:_startTimeTF.text forKey:@"start_time"];
    [_params setObject:_endTimeTF.text forKey:@"end_time"];
    [_params setObject:_startDateTF.text forKey:@"date"];
    [_params setObject:_endDateTF.text forKey:@"enddate"];
    [_params setObject:_costOffersTF.text forKey:@"costoffers"];
    [_params setObject:occurrencePostString forKey:@"reoccuring"];
    
    NSLog(@" Token:%@\n Title:%@\n discription:%@\n location:%@\n venue:%@\n cost:%@\n lat:%@\n lng:%@\n catId:%@\n artistId:%@\n startTime:%@\n endTime:%@\n startDate:%@\n endDate:%@\n costOffer:%@\n currency:%@\n",access_token,_eventTitleTF.text,_descriptionView.text,_enterLocationTF.text,_addVenueTF.text,_costTF.text,latitudes,longitudes,categoryIDNo,artistIDNo,_startTimeTF.text,_endTimeTF.text,_startDateTF.text,_endDateTF.text,_costOffersTF.text,_currencyTF.text);
    
    
    
    
    if (APP_DELEGATE.isServerReachable)
    {
    
    // the boundary string : a random string, that will not repeat in post data, to separate post data fields.
    NSString *BoundaryConstant = @"----------V2ymHFg03ehbqgZCaKO6jy";
    
    // string constant for the post parameter 'file'. My server uses this name: `file`. Your's may differ
    NSString* FileParamConstant = @"userfile";
    
    // the server url to which the image (or the media) is uploaded. Use your server url here
    NSURL* requestURL = [NSURL URLWithString:(API_URL @"createEvent")];
    
    // create request
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    [request setHTTPShouldHandleCookies:NO];
    [request setTimeoutInterval:30];
    [request setHTTPMethod:@"POST"];
    
    // set Content-Type in HTTP header
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", BoundaryConstant];
    [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    // post body
    NSMutableData *body = [NSMutableData data];
    
    // add params (all params are strings)
    for (NSString *param in _params) {
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", param] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@\r\n", [_params objectForKey:param]] dataUsingEncoding:NSUTF8StringEncoding]];
    }
    
    // add image data
    NSData *imageData = UIImageJPEGRepresentation(self.selectedImage, 1.0);
    if (imageData)
    {
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"image.jpg\"\r\n", FileParamConstant] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithString:@"Content-Type: image/jpeg\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        
        // [body appendData:[NSData dataWithData:imageData]];
        
        [body appendData:imageData];
        [body appendData:[[NSString stringWithFormat:@"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    }
    
    [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
    
    // setting the body of the post to the reqeust
    [request setHTTPBody:body];
    
    // set the content-length
    NSString *postLength = [NSString stringWithFormat:@"%d", [body length]];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    
    // set URL
    [request setURL:requestURL];
    
    NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    self.connection = connection;
    
    [connection start];
        
        
    }
    
    else
    {
        [SVProgressHUD dismiss];
        [[[UIAlertView alloc]initWithTitle:@"OutNAbout" message:@"Internet connection is not available. Please try again."  delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil]show];
    }
    
}




#pragma mark - NSURLConnection delegate methods =  ==  == = = = = = = =  = = = = = = = = = = = = = = = = = = = = = = = = = = = = =

-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data{
    [self.receivedData appendData:data];
}


-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error{
    
    NSLog(@"eeeeeeee  %@" , error);
    [SVProgressHUD dismiss];
    [[[UIAlertView alloc]initWithTitle:@"OutNAbout" message:@"The network connection was lost."  delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil]show];

}

-(void)connectionDidFinishLoading:(NSURLConnection *)connection{
    
    //NSString *htmlSTR = [[NSString alloc] initWithData:self.receivedData
                                              //encoding:NSUTF8StringEncoding];
    // NSLog(@" ffff     %@" , htmlSTR);
    
    
    NSError *error = nil;
    NSDictionary *jsonArray = [NSJSONSerialization JSONObjectWithData: self.receivedData options: NSJSONReadingMutableContainers error: &error];

   NSString *code = [jsonArray[@"code"] stringValue];
       if ([code isEqualToString:@"200"]) {
   
        [SVProgressHUD dismiss];
       NSString *msg = jsonArray[@"msg"];
        [[[UIAlertView alloc]initWithTitle:@"OutNAbout" message:msg delegate:self cancelButtonTitle:@"Done" otherButtonTitles: nil]show];
           
           
           
           
    }
      else{
           [SVProgressHUD dismiss];
     }
    
    occurrencePostString=@"";
}




- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    if (alertView.tag ==122) {
        if (buttonIndex==0) {
            _endTimeTF.text=nil;
        }
    }
    else if (alertView.tag==123)
    {
        if (buttonIndex==0) {
            _endDateTF.text=nil;
        }
    }
    
    else{
    if (buttonIndex==0) {
    //    [self.navigationController popViewControllerAnimated:YES];
        
        
        UIStoryboard *story = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        _myEventsVCObject  = [story instantiateViewControllerWithIdentifier:@"myEventsView"];
        UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:_myEventsVCObject];
        [self presentViewController:nav animated:NO completion:nil];
        [self.view removeFromSuperview];
        
        
        
        _eventTitleTF.text= @"";
        _startDateTF.text= @"";
        _endDateTF.text= @"";
        _startTimeTF.text= @"";
        _endTimeTF.text= @"";
        _costTF.text= @"";
        _currencyTF.text= @"";
        _occurrenceTF.text= @"";
        _costOffersTF.text= @"";
        _descriptionView.text= @"";
        _categoriesTF.text= @"";
        _artistTF.text= @"";
        _addVenueTF.text= @"";
        _enterLocationTF.text= @"";
        
        
        
    }
    }
}









-(BOOL)checkTextfieldValidations{
    Boolean isValid = true;
    
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy/MM/dd"];
    
    NSDate *endDate1 = [dateFormatter dateFromString:_endDateTF.text];
    NSDate *startDate2 = [dateFormatter dateFromString:_startDateTF.text];

    NSDateFormatter *timeFormatter = [[NSDateFormatter alloc] init];
    [timeFormatter setDateFormat:@"hh:mm a"];
    
    NSDate *endTime= [timeFormatter dateFromString:_endTimeTF.text];
    NSDate *startTime = [timeFormatter dateFromString:_startTimeTF.text];
    
    
    
    CGImageRef cgref = [self.selectedImage CGImage];
    CIImage *cim = [self.selectedImage CIImage];
    
    if (cim == nil && cgref == NULL)
    {
        isValid = false;
    
        
        alert = [[UIAlertView alloc] initWithTitle:@"OutNAbout" message:@"Image can't be empty!"  delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];

    }
    
   else if ([_eventTitleTF.text isEqualToString:@""]) {
        isValid = false;
        

       alert = [[UIAlertView alloc] initWithTitle:@"OutNAbout" message:@"Event title can't be empty!"  delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
       [alert show];
       
    }
    
   else if ([_startDateTF.text isEqualToString:@""]) {
        
        isValid = false;

        alert = [[UIAlertView alloc] initWithTitle:@"OutNAbout" message:@"Start date can't be empty!"  delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }
    
    
    else if ([_endDateTF.text isEqualToString:@""] ) {
        
        isValid = false;
        
        alert = [[UIAlertView alloc] initWithTitle:@"OutNAbout" message:@"End date can't be empty!"  delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }
    
    else if (_startDateTF.text.length && _endDateTF.text.length)
    {
        

        
    //if ([_startDateTF.text isEqualToString:_endDateTF.text])
        if(endTime > startTime && [_startDateTF.text isEqualToString:_endDateTF.text])
        {
            NSString *startTimeString = _startTimeTF.text;
            NSLog(@"start time is %@",startTimeString);
            
            UIDatePicker *picker = (UIDatePicker*)_endTimeTF.inputView;
            // [picker setMaximumDate:[NSDate date]];
            NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
            NSDate *eventDate = picker.date;
            [dateFormat setDateFormat:@"hh:mm a"];
            
            NSString *endTimeString = [dateFormat stringFromDate:eventDate];
            
            if (_endTimeTF.text.length) {

                if (startTimeString.length) {

                    NSComparisonResult result;
                    result = [endTimeString compare:startTimeString]; // comparing two dates
                    if(result==NSOrderedDescending) { // it checks like this LastDate > FirstDate
                        NSLog(@"correct");
                    }
                    else{
                        NSLog(@"error");
                        isValid = false;
                        
                        UIAlertView *alertView= [[UIAlertView alloc]initWithTitle:@"OutNAbout" message:@"End time cannot less then Start time!"  delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                        alertView.tag = 122;
                        [alertView show];
                    }
                }
                else{
                    isValid = false;
                    
                    alert = [[UIAlertView alloc] initWithTitle:@"OutNAbout" message:@"Start time can't be empty!"  delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                    [alert show];
                }
            }
            else{
                
                
            }
        }
        


    else if (endDate1 < startDate2){

        NSString *startDateString = _startDateTF.text;
        NSLog(@"start time is %@",startDateString);
        
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy/MM/dd"];
        
        NSDate *date1 = [dateFormatter dateFromString:_endDateTF.text];
        NSDate *date2 = [dateFormatter dateFromString:_startDateTF.text];

        

            if ([date1 compare:date2] == NSOrderedAscending) {
            NSLog(@"date1 is earlier than date2");
                UIAlertView *alertView= [[UIAlertView alloc]initWithTitle:@"OutNAbout" message:@"End date cannot less then Start date!"  delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                            alertView.tag = 123;
                            [alertView show];
        }
    }
        
        
        
        
        
     else if ([_startTimeTF.text isEqualToString:@""]) {
         
         isValid = false;

         alert = [[UIAlertView alloc] initWithTitle:@"OutNAbout" message:@"Start time can't be empty!"  delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
         [alert show];
     }

     else if ([_costTF.text isEqualToString:@""] && ![_currencyTF.text isEqualToString:@""]) {
         
         isValid = false;
         
         alert = [[UIAlertView alloc] initWithTitle:@"OutNAbout" message:@"Price can't be empty!"  delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
         [alert show];
     }
        
     else if ([_currencyTF.text isEqualToString:@""] && ![_costTF.text isEqualToString:@""]) {
         
         isValid = false;
         
         alert = [[UIAlertView alloc] initWithTitle:@"OutNAbout" message:@"Currency can't be empty!"  delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
         [alert show];
     }
        
        
     else if ([_costOffersTF.text isEqualToString:@""]) {
         isValid = false;
    
         alert = [[UIAlertView alloc] initWithTitle:@"OutNAbout" message:@"Cost Offer can't be empty!"  delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
         [alert show];
     }
        
     else if ([_descriptionView.text isEqualToString:@""]) {
         
         isValid = false;
         
         alert = [[UIAlertView alloc] initWithTitle:@"OutNAbout" message:@"Please add description!"  delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
         [alert show];
     }
        
        
        
     else if ([_categoriesTF.text isEqualToString:@""]) {
         
         isValid = false;
         
         alert = [[UIAlertView alloc] initWithTitle:@"OutNAbout" message:@"Select categories!"  delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
         [alert show];
     }

     else if ([_artistTF.text isEqualToString:@""]) {
         
         isValid = false;
         //
         alert = [[UIAlertView alloc] initWithTitle:@"OutNAbout" message:@"Select artists!"  delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
         [alert show];
     }
   
     else if ([_enterLocationTF.text isEqualToString:@""]) {
         
         isValid = false;
     
         alert = [[UIAlertView alloc] initWithTitle:@"OutNAbout" message:@"Location can't be empty!"  delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
         [alert show];
     }

     else if ([_addVenueTF.text isEqualToString:@""]) {
         
         isValid = false;
         
         alert = [[UIAlertView alloc] initWithTitle:@"OutNAbout" message:@"Venue can't be empty!"  delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
         [alert show];
     }

   
    }
    

    return isValid;
}








//- (void)textViewDidChange:(UITextView *)textView
//{
//    CGFloat fixedWidth = textView.frame.size.width;
//    CGSize newSize = [textView sizeThatFits:CGSizeMake(fixedWidth, MAXFLOAT)];
//    CGRect newFrame = textView.frame;
//    newFrame.size = CGSizeMake(fmaxf(newSize.width, fixedWidth), newSize.height+10);
//    textView.frame = newFrame;
//}


-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    [_popupView setHidden:YES];
    return YES;
}


- (void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
     [_scrollView endEditing:YES];
    // [_popupView setHidden:YES];
    // [_dimBackgroundView setHidden:YES];
     catOrArtistKeyType = @"";
}


//- (void)hideTextField:(UITextField *)textField {
//    
//    // do whatever you have to do
//    
//    [textField resignFirstResponder];
//}



-(void)textFieldDidBeginEditing:(UITextField *)textField
{
//    [UIView beginAnimations:nil context:nil];
//    [UIView setAnimationDuration:.3];
//    [UIView setAnimationBeginsFromCurrentState:TRUE];
//    self.view.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y -200., self.view.frame.size.width, self.view.frame.size.height);
//    
//    [UIView commitAnimations];
    
    
}


-(void)textFieldDidEndEditing:(UITextField *)textField
{
    
    [textField resignFirstResponder];
//    [UIView beginAnimations:nil context:nil];
//    [UIView setAnimationDuration:.3];
//    [UIView setAnimationBeginsFromCurrentState:TRUE];
//    self.view.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y +200., self.view.frame.size.width, self.view.frame.size.height);
//    
//    [UIView commitAnimations];
    
}




-(void)textViewDidBeginEditing:(UITextView *)textView
{
    _addDescriptionPlaceHolder.hidden = YES;
    
}

-(void)textViewDidEndEditing:(UITextView *)textView
{
    if ([_descriptionView.text isEqualToString:@""]) {
        _addDescriptionPlaceHolder.hidden = NO;
        
    }else{
        _addDescriptionPlaceHolder.hidden = YES;
    }
}



#pragma mark - usee these methods for slide bar menu ============================

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    
    UIBarButtonItem *left = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"ic_menu@3x.png"] style:UIBarButtonItemStyleDone target:self action:@selector(open)];
    left.tintColor = [UIColor whiteColor];
    self.navigationItem.leftBarButtonItem = left;
    
}

- (void)open
{
    [self.drawerTransition presentDrawerViewController];
}




#pragma mark - google places methods use for get latitude and longitude ==== = = == = = = =



- (void) selectDesiredLocation: (UITapGestureRecognizer *)gesture
{
    ABCGooglePlacesSearchViewController *searchViewController = [[ABCGooglePlacesSearchViewController alloc] init];
    [searchViewController setDelegate:self];
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:searchViewController];
    [self presentViewController:navigationController animated:YES completion:nil];

}

-(void)searchViewController:(ABCGooglePlacesSearchViewController *)controller didReturnPlace:(ABCGooglePlace *)place {
    
//    [self.nameLabel setText:place.name];
//    [self.addressLabel setText:place.formatted_address];
//    
   NSString *coordinatesString = [NSString stringWithFormat:@"(%f,%f)",place.location.coordinate.latitude, place.location.coordinate.longitude];
//    [self.coordinatesLabel setText:coordinatesString];
    
    latitudes =[NSString stringWithFormat:@"%f",  place.location.coordinate.latitude];
    longitudes =[NSString stringWithFormat:@"%f",  place.location.coordinate.longitude];

    NSLog(@"location name :%@",place.name );
    NSLog(@"location name :%@",place.formatted_address);
    NSLog(@"location name :%@",coordinatesString);

    _enterLocationTF.text =place.formatted_address;
    NSLog(@"latitude :%@  longitude :%@",latitudes,longitudes);

    
}



#pragma mark - picker view data sources = = = = == = = = = = = = =  = = = = = = = = == = = = =  = = ==  = = = = =

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView; {
    return 1;
}
//Rows in each Column

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component; {
    if ([pickerType isEqualToString:@"currency"]) {
        return currency.count;

    }
    else{
    return occurrenceArray.count;
    }
}




-(NSString*) pickerView:(UIPickerView*)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    if ([pickerType isEqualToString:@"currency"]) {
    return [currency objectAtIndex:row];
    }
    
    else
    {
        return [occurrenceArray objectAtIndex:row];
    }
}


- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component;
{
    //Write the required logic here that should happen after you select a row in Picker View.
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
    
}



@end
