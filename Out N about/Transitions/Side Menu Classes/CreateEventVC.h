//
//  CreateEventVC.h
//  OutNAbout
//
//  Created by Ram Kumar on 29/06/16.
//  Copyright © 2016 tecHindustan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CreateEventVC : UIViewController

@property (retain, nonatomic) NSURLConnection *connection;

@property (retain, nonatomic) NSMutableData *receivedData;


@property (strong, nonatomic) IBOutlet UIBarButtonItem *barButtonItem;

@property (strong, nonatomic)  UIBarButtonItem *rightBarButtonItem;

@property (weak, nonatomic) IBOutlet UIButton *galleryButton;
@property (weak, nonatomic) IBOutlet UIImageView *galleryImageView;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

@property (weak, nonatomic) IBOutlet UILabel *addDescriptionPlaceHolder;



@property (weak, nonatomic) IBOutlet UIView *viewOnCategoryTF;
@property (weak, nonatomic) IBOutlet UIView *viewOnSelectLocation;
@property (weak, nonatomic) IBOutlet UIView *viewOnSelectArtists;
@property (weak, nonatomic) IBOutlet UIView *viewOnStartDate;
@property (weak, nonatomic) IBOutlet UIView *viewOnEndDate;
@property (weak, nonatomic) IBOutlet UIView *viewOnStartTime;
@property (weak, nonatomic) IBOutlet UIView *viewOnEndTime;
@property (weak, nonatomic) IBOutlet UIView *viewOnCurrency;
@property (weak, nonatomic) IBOutlet UIView *viewOnOccurrence;


@property (weak, nonatomic) IBOutlet UIImageView *userImageView;
@property (weak, nonatomic) IBOutlet UILabel *userNameLabel;

@property (weak, nonatomic) IBOutlet UITextField *eventTitleTF;
@property (weak, nonatomic) IBOutlet UITextField *startDateTF;
@property (weak, nonatomic) IBOutlet UITextField *startTimeTF;
@property (weak, nonatomic) IBOutlet UITextField *endDateTF;
@property (weak, nonatomic) IBOutlet UITextField *endTimeTF;
@property (weak, nonatomic) IBOutlet UITextField *costTF;
@property (weak, nonatomic) IBOutlet UITextField *costOffersTF;
@property (weak, nonatomic) IBOutlet UITextField *descriptionForIconTF;
@property (weak, nonatomic) IBOutlet UITextField *occurrenceTF;
@property (weak, nonatomic) IBOutlet UITextView *descriptionView;
@property (weak, nonatomic) IBOutlet UITextField *currencyTF;

@property (weak, nonatomic) IBOutlet UITextField *artistTF;
@property (weak, nonatomic) IBOutlet UITextField *enterLocationTF;
@property (weak, nonatomic) IBOutlet UITextField *addVenueTF;

@property (weak, nonatomic) IBOutlet UITextField *categoriesTF;
@property(nonatomic,strong) NSMutableArray *selectedItems;



@property (weak, nonatomic) IBOutlet UIView *dimBackgroundView;
@property (weak, nonatomic) IBOutlet UILabel *selectHeadingLabel;

@property (weak, nonatomic) IBOutlet UIView *popupView;
@property (weak, nonatomic) IBOutlet UITableView *tableViewListing;




@end
