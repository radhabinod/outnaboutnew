//
//  ChangePasswordVC.m
//  OutNAbout
//
//  Created by Ram Kumar on 18/11/16.
//  Copyright © 2016 tecHindustan. All rights reserved.
//

#import "ChangePasswordVC.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import <SystemConfiguration/SystemConfiguration.h>
#import "SDWebImageManager.h"
#import "PGDrawerTransition.h"
#import "AppDelegate.h"
#import "NavigationViewController.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import "CommonAPI's.pch"

#import "SVProgressHUD.h"

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wdeprecated-declarations"

@interface ChangePasswordVC ()<NSURLConnectionDataDelegate,UITextFieldDelegate>

@end

@implementation ChangePasswordVC
{
     NSString *password;
    NSString *access_token;
    NSString *newAccess_token;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
        UIButton *button =  [UIButton buttonWithType:UIButtonTypeSystem];
        [button setImage:[UIImage imageNamed:@"ic_arrow@3x.png"] forState:UIControlStateNormal];
        [button addTarget:self action:@selector(handleBack:)forControlEvents:UIControlEventTouchUpInside];
        [button setFrame:CGRectMake(0, 0, 53, 31)];
        [button setImageEdgeInsets:UIEdgeInsetsMake(0, -30, 0, 0)];
        button.backgroundColor = [UIColor clearColor];
        UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(30, 5, 240, 20)];
        [label setFont:[UIFont fontWithName:@"Arial-BoldMT" size:17]];
        [label setText:@"Change Password"];
        label.textAlignment = UITextAlignmentLeft;
        [label setTextColor:[UIColor whiteColor]];
        [label setBackgroundColor:[UIColor clearColor]];
        [button addSubview:label];
        UIBarButtonItem *barButton = [[UIBarButtonItem alloc] initWithCustomView:button];
        self.navigationItem.leftBarButtonItem = barButton;
        self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    
    self.navigationItem.hidesBackButton = YES;
    
    UIBarButtonItem*  changePasswordButton = [[UIBarButtonItem alloc]
                                      initWithImage:[UIImage imageNamed:@"ic_tick@3x.png"]
                                      style:UIBarButtonItemStylePlain
                                      target:self
                                      action:@selector(changePasswordAction)];
    changePasswordButton.tintColor = [UIColor whiteColor];
    self.navigationItem.rightBarButtonItem=changePasswordButton;
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    password = [defaults objectForKey:@"password"];
    access_token = [defaults objectForKey:@"access_token"];

    
    
    _oldPasswordTF.delegate = self;
    _passTF.delegate = self;
    _confirmPassTF.delegate = self;
    
    
    
    UIImageView *oldPassImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"ic_password@3x.png"]];
    oldPassImage.frame = CGRectMake(0.0, 0.0, oldPassImage.image.size.width+20.0, oldPassImage.image.size.height);
    oldPassImage.contentMode = UIViewContentModeCenter;
    _oldPasswordTF.leftView = oldPassImage;
    _oldPasswordTF.leftViewMode = UITextFieldViewModeAlways;
    
    UIImageView *passImage1 = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"ic_password@3x.png"]];
    passImage1.frame = CGRectMake(0.0, 0.0, passImage1.image.size.width+20.0, passImage1.image.size.height);
    passImage1.contentMode = UIViewContentModeCenter;
    _passTF.leftView = passImage1;
    _passTF.leftViewMode = UITextFieldViewModeAlways;
    
    UIImageView *passImage2 = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"ic_password@3x.png"]];
    passImage2.frame = CGRectMake(0.0, 0.0, passImage2.image.size.width+20.0, passImage2.image.size.height);
    passImage2.contentMode = UIViewContentModeCenter;
    _confirmPassTF.leftView = passImage2;
    _confirmPassTF.leftViewMode = UITextFieldViewModeAlways;

}


-(void)changePasswordAction
{
    
    if (![_oldPasswordTF.text length]) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"OutNAbout" message:@"Old password is required."  delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        return;
    }
    else if (![_oldPasswordTF.text isEqualToString:password])
    {
        [[[UIAlertView alloc]initWithTitle:@"OutNAbout" message:@"Old password did not match!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil]show];
    }
    else if (![_passTF.text length])
    {
        [[[UIAlertView alloc]initWithTitle:@"OutNAbout" message:@"New password is required." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil]show];
    }
    else if (![_confirmPassTF.text length])
    {
        [[[UIAlertView alloc]initWithTitle:@"OutNAbout" message:@"Confirm password is required." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil]show];
    }
    else if (![_passTF.text isEqualToString:_confirmPassTF.text]) {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"OutNAbout" message:@"New password and Confirm password MUST be same."  delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        return;

    }
    else
    {
        
    }
    
}


-(void)changePasswordPostMethod
{
    [SVProgressHUD showWithStatus:@"Please wait..." maskType:SVProgressHUDMaskTypeBlack];
    
    if (APP_DELEGATE.isServerReachable)
    {
        
        [self.connection cancel];
        
        NSMutableData *data = [[NSMutableData alloc] init];
        self.receivedData = data;
        
        NSURL *url = [NSURL URLWithString:(API_URL @"changepassword")];
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[url standardizedURL]];
        
        [request setHTTPMethod:@"POST"];
        NSString *postData = [NSString stringWithFormat:@"app_token=%@&password=%@",access_token,_passTF.text];
        
        [request setValue:@"application/x-www-form-urlencoded; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
        
        [request setHTTPBody:[postData dataUsingEncoding:NSUTF8StringEncoding]];
        
        NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
        self.connection = connection;
        
        [connection start];
    }
    else
    {
        [SVProgressHUD dismiss];
        [[[UIAlertView alloc]initWithTitle:@"OutNAbout" message:@"Internet connection is not available. Please try again."  delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil]show];
    }

}


-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data{
    [self.receivedData appendData:data];
}


-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error{
    
    [SVProgressHUD dismiss];
    [[[UIAlertView alloc]initWithTitle:@"OutNAbout" message:@"The network connection was lost."  delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil]show];
    
    NSLog(@"eeeeeeee  %@" , error);
}



-(void)connectionDidFinishLoading:(NSURLConnection *)connection{
    
    //NSString *htmlSTR = [[NSString alloc] initWithData:self.receivedData
    //                encoding:NSUTF8StringEncoding];
    // NSLog(@" ffff     %@" , htmlSTR);
    
    
    NSError *error = nil;
    NSDictionary *jsonArray = [NSJSONSerialization JSONObjectWithData: self.receivedData options: NSJSONReadingMutableContainers error: &error];
    
    newAccess_token = jsonArray[@"access_token"];
     NSLog(@"new access token *** :%@",newAccess_token);
    
    NSString *code = [jsonArray[@"code"] stringValue];
    if ([code isEqualToString:@"200"]) {

    
    NSString *msg = jsonArray[@"msg"];
    [[[UIAlertView alloc]initWithTitle:@"OutNAbout" message:msg delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil]show];

    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:newAccess_token forKey:@"access_token"];
    [defaults synchronize];
        
    [SVProgressHUD dismiss];
        
    }

    
}



-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    if(buttonIndex==0)
    {
        [self.navigationController popViewControllerAnimated:YES];
    }
    
    
}




- (void) handleBack:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}







-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    return YES;
}


- (void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [[self view] endEditing:YES];
}




-(void)textFieldDidBeginEditing:(UITextField *)textField
{

    
    
}


-(void)textFieldDidEndEditing:(UITextField *)textField
{

    
}






- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


@end
