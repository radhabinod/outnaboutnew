//
//  EditProfileVC.m
//  OutNAbout
//
//  Created by Ram Kumar on 12/08/16.
//  Copyright © 2016 tecHindustan. All rights reserved.
//

#import "EditProfileVC.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import <SystemConfiguration/SystemConfiguration.h>
#import "SDWebImageManager.h"
#import "PGDrawerTransition.h"
#import "AppDelegate.h"
#import "NavigationViewController.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import "CommonAPI's.pch"

#import "SVProgressHUD.h"

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wdeprecated-declarations"

@interface EditProfileVC ()<UIGestureRecognizerDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UITextFieldDelegate,UIActionSheetDelegate>
@property (nonatomic, strong) UIImage *selectedImage;
@property (nonatomic, strong) NSString *selectedImagePath;

@property (nonatomic, strong) PGDrawerTransition *drawerTransition;
@property (nonatomic, strong) NavigationViewController *navigationViewController;

@end

@implementation EditProfileVC
{
    UIImagePickerController *mediaPicker;
    NSMutableDictionary *dictImages;
    NSString *imageName;
    UIImage *imagePath;
    
    NSString *firstName;
    NSString *lastName;
    NSString *password;
    NSString *finalPassword;
    NSString *access_token;
    UIImage *yourImage;
    
    UIView *loadingView;
    UIActivityIndicatorView *activityView;
}

@synthesize screenType;

- (void)viewDidLoad {
    [super viewDidLoad];
    
//    _userNameTF.delegate = self;
//    _oldPasswordTF.delegate = self;
//    _passwordTF.delegate = self;

    
//    UIButton *button =  [UIButton buttonWithType:UIButtonTypeSystem];
//    [button setImage:[UIImage imageNamed:@"ic_arrow@3x.png"] forState:UIControlStateNormal];
//    [button addTarget:self action:@selector(handleBack:)forControlEvents:UIControlEventTouchUpInside];
//    [button setFrame:CGRectMake(0, 0, 53, 31)];
//    [button setImageEdgeInsets:UIEdgeInsetsMake(0, -30, 0, 0)];
//    button.backgroundColor = [UIColor clearColor];
//    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(30, 5, 240, 20)];
//    [label setFont:[UIFont fontWithName:@"Arial-BoldMT" size:17]];
//    [label setText:@"Edit Profile"];
//    label.textAlignment = UITextAlignmentLeft;
//    [label setTextColor:[UIColor whiteColor]];
//    [label setBackgroundColor:[UIColor clearColor]];
//    [button addSubview:label];
//    UIBarButtonItem *barButton = [[UIBarButtonItem alloc] initWithCustomView:button];
//    self.navigationItem.leftBarButtonItem = barButton;
//    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    
    self.navigationItem.hidesBackButton = YES;
    self.navigationController.navigationBar.hidden=YES;
    [self userNameAndImage];
    
//    _rightBarButtonItem = [[UIBarButtonItem alloc]init];
//    [_rightBarButtonItem setImage:[UIImage imageNamed:@"ic_tick@3x.png"]];
//    _rightBarButtonItem.tintColor = [UIColor whiteColor];
//    [_rightBarButtonItem setTarget:self];
//    [_rightBarButtonItem setAction:@selector(updateUserProfileAction)];
//    self.navigationItem.rightBarButtonItem = _rightBarButtonItem;
    
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    access_token = [defaults objectForKey:@"access_token"];
    password = [defaults objectForKey:@"password"];
    NSString *UserPicture = [defaults objectForKey:@"user_pic"];
    
    
    if (APP_DELEGATE.isServerReachable)
    {
    
    if ([UserPicture isEqualToString:@""]) {
        _userImageView.image = [UIImage imageNamed:@"icon-user-default.png"];
        _blurImageView.image = [UIImage imageNamed:@"icon-user-default.png"];
        
        UIBlurEffect *blur = [UIBlurEffect effectWithStyle:UIBlurEffectStyleLight];
        UIVisualEffectView *effectView = [[UIVisualEffectView alloc]initWithEffect:blur];
        effectView.alpha =1.0;
        effectView.frame = _blurImageView.bounds;
        [_blurImageView addSubview:effectView];

    }
    
    else{
        
        [SVProgressHUD showWithStatus:@"Loading..." maskType:SVProgressHUDMaskTypeBlack];

    
    NSString *imageURL = @"http://agiledevelopers.in/outnabout/uploads/";
    NSString *url_Img_FULL = [imageURL stringByAppendingPathComponent:UserPicture];
    NSURL *userImagePic = [NSURL URLWithString:url_Img_FULL];
    
    
 // set user profile picture=====
    
    SDWebImageManager *manager = [SDWebImageManager sharedManager];
    [manager downloadImageWithURL:userImagePic
                          options:0
                         progress:^(NSInteger receivedSize, NSInteger  expectedSize) {
                         }
                        completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                            if (image) {
                                dispatch_async(dispatch_get_main_queue(), ^{
                                    _userImageView.image = image;
                                    _blurImageView.image = image;
                                    
                                    _userImageView.layer.borderWidth = 1.5;
                                    _userImageView.layer.borderColor = [UIColor colorWithRed:1.00 green:0.41 blue:0.41 alpha:1.0].CGColor;
                                    
                                    _userImageView.layer.shadowColor = [UIColor blueColor].CGColor;
                                    _userImageView.layer.shadowOffset = CGSizeMake(0, 1);
                                    _userImageView.layer.shadowOpacity = 1;
                                    _userImageView.layer.shadowRadius = 6.0;
                                    _userImageView.clipsToBounds = YES;
                                    
                                    self.selectedImage = image;
                                    
                                    
                                    UIBlurEffect *blur = [UIBlurEffect effectWithStyle:UIBlurEffectStyleLight];
                                    
                                    // create vibrancy effect
                                //    UIVibrancyEffect *vibrancy = [UIVibrancyEffect effectForBlurEffect:blur];
                                    
                                    // add blur to an effect view
                                    UIVisualEffectView *effectView = [[UIVisualEffectView alloc]initWithEffect:blur];
                                    effectView.backgroundColor = [UIColor colorWithRed:0.23 green:0.75 blue:0.61 alpha:1.0];
                                    effectView.alpha =0.8;
                                    effectView.frame = _blurImageView.bounds;
                                    
                                    // add vibrancy to yet another effect view
                                    //   UIVisualEffectView *vibrantView = [[UIVisualEffectView alloc]initWithEffect:vibrancy];
                                    // vibrantView.frame = self.view.frame;
                                    
                                    [_blurImageView addSubview:effectView];

                                    [SVProgressHUD dismiss];
                                    
//                                    CIImage *imageToBlur = [CIImage imageWithCGImage:image.CGImage];
//                                    
//                                    CIFilter *gaussianBlurFilter = [CIFilter filterWithName: @"CIGaussianBlur"];
//                                    [gaussianBlurFilter setValue:imageToBlur forKey: @"inputImage"];
//                                    [gaussianBlurFilter setValue:[NSNumber numberWithFloat:3] forKey: @"inputRadius"]; //change number to increase/decrease blur
//                                    
//                                    CIImage *resultImage = [gaussianBlurFilter valueForKey: @"outputImage"];
//                                    
//                                    UIImage *blurredImage = [[UIImage alloc] initWithCIImage:resultImage];
//                                    self.blurImageView.image = blurredImage;

                                });
                            }
                    }
     ];
    }
    
    
    }
    
    else
    {
        [[[UIAlertView alloc]initWithTitle:@"OutNAbout" message:@"Internet connection is not available. Please try again."  delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil]show];
        
        _userImageView.image = [UIImage imageNamed:@"icon-user-default.png"];
        _blurImageView.image = [UIImage imageNamed:@"icon-user-default.png"];
        
        UIBlurEffect *blur = [UIBlurEffect effectWithStyle:UIBlurEffectStyleLight];
        UIVisualEffectView *effectView = [[UIVisualEffectView alloc]initWithEffect:blur];
        effectView.alpha =1.0;
        effectView.frame = _blurImageView.bounds;
        [_blurImageView addSubview:effectView];

    }
    

    // tap gesture for select categories
    
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTapFrom:)];
  //  tapGestureRecognizer.delegate=self;
    //tapGestureRecognizer.numberOfTapsRequired = 1;
    [_userImageView setUserInteractionEnabled:YES];
    [_userImageView addGestureRecognizer:tapGestureRecognizer];
    

}


//-(void) showActivityIndicator
//{
//
//    loadingView = [[UIView alloc]initWithFrame:CGRectMake(100, 400, 80, 80)];
//    loadingView.backgroundColor = [UIColor colorWithWhite:0. alpha:0.6];
//    loadingView.center = self.view.center;
//    loadingView.layer.cornerRadius = 5;
//    
//    activityView=[[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
//    activityView.center = CGPointMake(loadingView.frame.size.width / 2.0, 35);
//    [activityView startAnimating];
//    activityView.tag = 100;
//    [loadingView addSubview:activityView];
//    
//    UILabel* lblLoading = [[UILabel alloc]initWithFrame:CGRectMake(0, 48, 80, 30)];
//    lblLoading.text = @"Loading...";
//    lblLoading.textColor = [UIColor whiteColor];
//    lblLoading.font = [UIFont fontWithName:lblLoading.font.fontName size:15];
//    lblLoading.textAlignment = NSTextAlignmentCenter;
//    [loadingView addSubview:lblLoading];
//    
//    [self.view addSubview:loadingView];
//}



//-(void) removeLoadingView
//{
//    [loadingView removeFromSuperview];
//}



//-(void)updateUserProfileAction
//{
//    if ([_passwordTF.text isEqualToString:@""] || [_oldPasswordTF.text isEqualToString:@""] ) {
//        [[[UIAlertView alloc]initWithTitle:@"Message" message:@"You did not make any changes!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil]show];
//    }
//    
//   else if ([_oldPasswordTF.text isEqualToString:password]   || ![_passwordTF.text isEqualToString:@""]) {
//       
//       [self showActivityIndicator];
//      // [self imagePostToServer];
//
//    }
//    else if (![_oldPasswordTF.text isEqualToString:password])
//    {
//        [[[UIAlertView alloc]initWithTitle:@"Message" message:@"Old password did not match!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil]show];
//    }
//    
//}







- (IBAction)updateProfileAction:(id)sender
{
//    if ([_oldPasswordTF.text isEqualToString:@""] && [_passwordTF.text isEqualToString:@""])
//    {
//        finalPassword = password;
//        NSLog(@"old passs %@",finalPassword);
//        // [self showActivityIndicator];
//
//        [self imagePostToServer];
//    }
//    
//       else if (![_oldPasswordTF.text isEqualToString:password])
//    {
//        [[[UIAlertView alloc]initWithTitle:@"Message" message:@"Old password did not match!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil]show];
//    }
//    
//        else if (![_oldPasswordTF.text isEqualToString:@""] && [_passwordTF.text isEqualToString:@""]) {
//            
//            
//            //[self showActivityIndicator];
//            finalPassword = password;
//            NSLog(@"old passs %@",finalPassword);
//            [self imagePostToServer];
//    
//        }
//        else{
//             // [self showActivityIndicator];
//
//            finalPassword =[NSString stringWithFormat:@"%@",_passwordTF.text];
//            NSLog(@"new password %@",finalPassword);
//            [self imagePostToServer];
//        }
    
    
    if (![_firstNameTF.text length])
    {
        [[[UIAlertView alloc]initWithTitle:@"OutNAbout" message:@"Firstname can't be empty." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil]show];
    }
    else if (![_lastNameTF.text length])
    {
        [[[UIAlertView alloc]initWithTitle:@"OutNAbout" message:@"Lastname can't be empty." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil]show];
    }
else
{
    [self imagePostToServer];
}
    
    
}


- (IBAction)backButtonAction:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}








#pragma mark - set gallery image action here

- (void) handleTapFrom: (UITapGestureRecognizer *)gesture
{

    mediaPicker = [[UIImagePickerController alloc] init];
    mediaPicker.delegate = self;
    mediaPicker.allowsEditing = NO;
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil
                                                             delegate:self
                                                    cancelButtonTitle:@"Cancel"
                                               destructiveButtonTitle:nil
                                                    otherButtonTitles:@"Take photo", @"Choose existing", nil];
    [actionSheet showInView:self.view];
    
}




-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 0) {

        mediaPicker.sourceType = UIImagePickerControllerSourceTypeCamera;
        
    } else if (buttonIndex == 1) {
        mediaPicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    }
    else {
        return;
    }
    [self presentViewController:mediaPicker animated:YES completion:nil];
}







- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    [self dismissViewControllerAnimated:YES completion:nil];
    
    
    dictImages=[[NSMutableDictionary alloc]init];
    
    
    imagePath = [info valueForKey: UIImagePickerControllerOriginalImage];
    
    UIImagePickerControllerSourceType sourceType = picker.sourceType;
    
    ALAssetsLibrary *assetsLibrary  = [[ALAssetsLibrary alloc] init];
    
    imagePath = [info valueForKey: UIImagePickerControllerOriginalImage];
    
    NSString *docDirPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *filePath =  [docDirPath stringByAppendingPathComponent:@"myImage.png"];
    NSLog (@"File Path = %@", filePath);
    
    
    imageName = [NSString stringWithFormat:@"%0.0f.jpg",[[NSDate date] timeIntervalSince1970]];
    
    [dictImages setObject:imagePath forKey:@"profilepic"];
    
    
    if (sourceType == UIImagePickerControllerSourceTypeCamera) {
        UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage];
        
        // Request to save the image to camera roll
        [assetsLibrary writeImageToSavedPhotosAlbum:[image CGImage] orientation:(ALAssetOrientation) [image imageOrientation] completionBlock:^(NSURL *assetURL, NSError *error) {
            if (error) {
  
            } else {
                NSLog(@"url %@", assetURL);
                
                [self saveImageDataToTemporaryArea:assetsLibrary withAssetUrl:assetURL];
            }
        }];
        
//        _userImageView.image= nil;
//        _userImageView = nil;
//        _blurImageView.image= nil;
//        _blurImageView = nil;
        
    }
    else {
        NSURL *url = (NSURL *) [info valueForKey:UIImagePickerControllerReferenceURL];
        
        [self saveImageDataToTemporaryArea:assetsLibrary withAssetUrl:url];
    }
}






-(void)saveImageDataToTemporaryArea:(ALAssetsLibrary *)assetsLibrary withAssetUrl:(NSURL *)url
{
    [assetsLibrary assetForURL:url
                   resultBlock:^(ALAsset *asset) {
                       ALAssetRepresentation *representation = [asset defaultRepresentation];
                       NSString *fileUTI = [representation UTI];
                       NSString *fileName = [representation filename];
                       ALAssetOrientation orientation = (ALAssetOrientation) [[asset valueForProperty:@"ALAssetPropertyOrientation"] intValue];
                       CGImageRef cgImg = [representation fullResolutionImage];
                       _selectedImage = [UIImage imageWithCGImage:cgImg scale:1.0 orientation:(UIImageOrientation) orientation];
                       NSData *data = nil;
                       if ([fileUTI isEqualToString:@"public.png"]) {
                           data = UIImagePNGRepresentation(self.selectedImage);
                       } else {
                           data = UIImageJPEGRepresentation(self.selectedImage, 95);
                       }
                       
                       NSError *error = nil;
                       _selectedImagePath = [NSTemporaryDirectory() stringByAppendingPathComponent:fileName];
                       [data writeToFile:self.selectedImagePath options:NSDataWritingAtomic error:&error];
                       if (error != nil) {
                           self.selectedImage = nil;
                           self.userImageView = nil;
                           self.userImageView.image = nil;
                           
                           _blurImageView.image= nil;
                           _blurImageView = nil;
                           NSLog(@"Failed to save : %i - %s", errno, strerror(errno));
                           
                           return;
                       }
                      // self.userImageView.contentMode = UIViewContentModeScaleToFill;
                       _userImageView.image = _selectedImage;
                       _userImageView.layer.borderWidth = 1.5;
                       _userImageView.layer.borderColor = [UIColor colorWithRed:1.00 green:0.41 blue:0.41 alpha:1.0].CGColor;
                       
                       self.blurImageView.contentMode = UIViewContentModeScaleToFill;
                       self.blurImageView.image = self.selectedImage;
                       
                      // NSLog(@"image user display*** %@",self.selectedImage);
                       
                       UIBlurEffect *blur = [UIBlurEffect effectWithStyle:UIBlurEffectStyleLight];
                       
                       // create vibrancy effect
                      // UIVibrancyEffect *vibrancy = [UIVibrancyEffect effectForBlurEffect:blur];
                       
                       // add blur to an effect view
                       UIVisualEffectView *effectView = [[UIVisualEffectView alloc]initWithEffect:blur];
                       effectView.alpha = 0.8;
                       effectView.backgroundColor = [UIColor colorWithRed:0.23 green:0.75 blue:0.61 alpha:1.0];
                       effectView.frame = _blurImageView.bounds;
                       
                       // add vibrancy to yet another effect view
                       //   UIVisualEffectView *vibrantView = [[UIVisualEffectView alloc]initWithEffect:vibrancy];
                       // vibrantView.frame = self.view.frame;
                       
                       [_blurImageView addSubview:effectView];
                       //   [_blurImageView addSubview:vibrantView];
                       
                       
                       
                       
                       
//                       //  UIImage *originalImage = [UIImage imageNamed:@"image-to-blur.png"];
//                            CIImage *imageToBlur = [CIImage imageWithCGImage:self.selectedImage.CGImage];
//                        
//                        CIFilter *gaussianBlurFilter = [CIFilter filterWithName: @"CIGaussianBlur"];
//                        [gaussianBlurFilter setValue:imageToBlur forKey: @"inputImage"];
//                        [gaussianBlurFilter setValue:[NSNumber numberWithFloat:3] forKey: @"inputRadius"]; //change number to increase/decrease blur
//                        
//                        CIImage *resultImage = [gaussianBlurFilter valueForKey: @"outputImage"];
//                        
//                        
//                        
//                        UIImage *blurredImage = [[UIImage alloc] initWithCIImage:resultImage];
//                        
//                        self.blurImageView.image = blurredImage;
                       
                       
                       
                       
                   }
                  failureBlock:^(NSError *error) {
                      
                  }];
}



- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [self dismissViewControllerAnimated:YES completion:nil];
       self.navigationController.navigationBar.hidden=YES;
}





-(void)imagePostToServer
{
    
    [SVProgressHUD showWithStatus:@"Please wait..." maskType:SVProgressHUDMaskTypeBlack];

    
    [self.connection cancel];
    
    NSMutableData *data = [[NSMutableData alloc] init];
    self.receivedData = data;
    
    // Dictionary that holds post parameters. You can set your post parameters that your server accepts or programmed to accept.
    NSMutableDictionary* _params = [[NSMutableDictionary alloc] init];
    [_params setObject:access_token forKey:@"app_token"];
    [_params setObject:[NSString stringWithFormat:@"%@ %@",_firstNameTF.text,_lastNameTF.text] forKey:@"fullname"];
    
    
    
    if (APP_DELEGATE.isServerReachable)
    {
    
    // the boundary string : a random string, that will not repeat in post data, to separate post data fields.
    NSString *BoundaryConstant = @"----------V2ymHFg03ehbqgZCaKO6jy";
    
    // string constant for the post parameter 'file'. My server uses this name: `file`. Your's may differ
    NSString* FileParamConstant = @"userfile";
    
    // the server url to which the image (or the media) is uploaded. Use your server url here
    NSURL* requestURL = [NSURL URLWithString:(API_URL @"changeprofile")];
    
    // create request
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    [request setHTTPShouldHandleCookies:NO];
    [request setTimeoutInterval:30];
    [request setHTTPMethod:@"POST"];
    
    // set Content-Type in HTTP header
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", BoundaryConstant];
    [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    // post body
    NSMutableData *body = [NSMutableData data];
    
    // add params (all params are strings)
    for (NSString *param in _params) {
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", param] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@\r\n", [_params objectForKey:param]] dataUsingEncoding:NSUTF8StringEncoding]];
    }
    
    // add image data
     // yourImage= [UIImage imageNamed:@"ic_bg@3x.png"];
    
    NSData *imageData = UIImageJPEGRepresentation(self.selectedImage, 1.0);
    if (imageData)
    {
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"image.jpg\"\r\n", FileParamConstant] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithString:@"Content-Type: image/jpeg\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        
        // [body appendData:[NSData dataWithData:imageData]];
        
        [body appendData:imageData];
        [body appendData:[[NSString stringWithFormat:@"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    }
    
    [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
    
    // setting the body of the post to the reqeust
    [request setHTTPBody:body];
    
    // set the content-length
    NSString *postLength = [NSString stringWithFormat:@"%d", [body length]];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    
    // set URL
    [request setURL:requestURL];
    
    NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    self.connection = connection;
    
    [connection start];
        
    }
    
    else
    {
        [SVProgressHUD dismiss];
        [[[UIAlertView alloc]initWithTitle:@"OutNAbout" message:@"Internet connection is not available. Please try again."  delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil]show];
    }
    
}




#pragma mark - NSURLConnection delegate methods =  ==  == = = = = = = =  = = = = = = = = = = = = = = = = = = = = = = = = = = = = =

-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data{
    [self.receivedData appendData:data];
   // [self removeLoadingView];
    [SVProgressHUD dismiss];
}


-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error{
    
    NSLog(@"eeeeeeee  %@" , error);
    [SVProgressHUD dismiss];
    [[[UIAlertView alloc]initWithTitle:@"OutNAbout" message:@"The network connection was lost."  delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil]show];

}

-(void)connectionDidFinishLoading:(NSURLConnection *)connection{
    
   // NSString *htmlSTR = [[NSString alloc] initWithData:self.receivedData
                                       //       encoding:NSUTF8StringEncoding];
    // NSLog(@" ffff     %@" , htmlSTR);
    
    
    NSError *error = nil;
    NSDictionary *jsonArray = [NSJSONSerialization JSONObjectWithData: self.receivedData options: NSJSONReadingMutableContainers error: &error];
    
    NSString *newUserPic =jsonArray[@"image"];
    NSString *userFirstName =jsonArray[@"firstname"];
    NSString *userLastName =jsonArray[@"lastname"];

   
    NSString *code = [jsonArray[@"code"] stringValue];
    if ([code isEqualToString:@"200"]) {
        
        
        
        NSString *msg = jsonArray[@"msg"];
        [[[UIAlertView alloc]initWithTitle:@"OutNAbout" message:msg delegate:nil cancelButtonTitle:@"Done" otherButtonTitles: nil]show];
        
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults setObject:newUserPic forKey:@"user_pic"];
        [defaults setObject:userFirstName forKey:@"first_name"];
        [defaults setObject:userLastName forKey:@"last_name"];

        [defaults synchronize];
        [SVProgressHUD dismiss];
    }
    
    else{
        
    }
}



-(void)userNameAndImage
{
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *loginType = [defaults objectForKey:@"loginType"];
    NSString *gpUserPic = [defaults objectForKey:@"gpUserPic"];
    NSURL *url=[NSURL URLWithString:gpUserPic];
    //_userImageView.contentMode = UIViewContentModeScaleAspectFill;
    
    _userImageView.layer.cornerRadius =_userImageView.frame.size.width / 2;
    _userImageView.clipsToBounds = YES;
    
    if ([loginType isEqualToString:@"fb"]) {
        
        
        FBSDKProfilePictureView *profilePictureview = [[FBSDKProfilePictureView alloc]initWithFrame:_userImageView.frame];
        profilePictureview.layer.cornerRadius = 40.0f;
        profilePictureview.layer.masksToBounds = YES;

        
        firstName = [defaults objectForKey:@"fbFirstName"];
        lastName = [defaults objectForKey:@"fbLastName"];
    }
    
    else if ([loginType isEqualToString:@"gp"]) {
        
                    SDWebImageManager *manager = [SDWebImageManager sharedManager];
                    [manager downloadImageWithURL:url
                                          options:1
                                         progress:^(NSInteger receivedSize, NSInteger  expectedSize) {
                                         }
                                        completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                                            if (image) {
                                                dispatch_async(dispatch_get_main_queue(), ^{
                                                    _userImageView.image = image;
                                                    _blurImageView.image = image;

                                                });
                                            }
                                        }
                     ];
        
        
        
        firstName = [defaults objectForKey:@"gpFirstName"];
        lastName = [defaults objectForKey:@"gpLastName"];
        
    }

    
    else
    {
        firstName = [defaults objectForKey:@"first_name"];
        lastName = [defaults objectForKey:@"last_name"];
        
//        [_userImageView setImage:[UIImage imageNamed:@"ic_user@3x.png"]];
//        [_blurImageView setImage:[UIImage imageNamed:@"ic_user_bg@3x.png"]];
       // [_blurImageView setAlpha:1.0f];
        
        // create effect
//       UIBlurEffect *blur = [UIBlurEffect effectWithStyle:UIBlurEffectStyleLight];
////        
////        // add effect to an effect view
//        UIVisualEffectView *effectView = [[UIVisualEffectView alloc]initWithEffect:blur];
//        effectView.frame = _blurImageView.bounds;//self.view.frame
//       [_blurImageView addSubview:effectView];
        
    }
    
    
    _firstNameTF.layer.sublayerTransform = CATransform3DMakeTranslation(5, 0, 0);
    _lastNameTF.layer.sublayerTransform = CATransform3DMakeTranslation(5, 0, 0);
   // _passwordTF.layer.sublayerTransform = CATransform3DMakeTranslation(5, 0, 0);

    _firstNameTF.text = [NSString stringWithFormat:@"%@",firstName];
    _firstNameTF.textColor = [UIColor blackColor];
    _firstNameTF.font= [UIFont systemFontOfSize:16.0f];
    
    _lastNameTF.text = [NSString stringWithFormat:@"%@",lastName];
    _lastNameTF.textColor = [UIColor blackColor];
    _lastNameTF.font= [UIFont systemFontOfSize:16.0f];
    
    _userNameLabel.text= [NSString stringWithFormat:@"%@ %@",firstName,lastName];
    _userNameLabel.textColor = [UIColor whiteColor];

   // _userNameLabel.backgroundColor = [UIColor clearColor];
    //  _userNameLabel.textAlignment = NSTextAlignmentCenter;
 
}



- (void) handleBack:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}





-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    return YES;
}


- (void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [[self view] endEditing:YES];
}




-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:.3];
    [UIView setAnimationBeginsFromCurrentState:TRUE];
    self.view.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y -100., self.view.frame.size.width, self.view.frame.size.height);
    
    [UIView commitAnimations];
    
    
}


-(void)textFieldDidEndEditing:(UITextField *)textField
{
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:.3];
    [UIView setAnimationBeginsFromCurrentState:TRUE];
    self.view.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y +100., self.view.frame.size.width, self.view.frame.size.height);
    
    [UIView commitAnimations];
    
}



//-(void) toggleTextFieldSecureEntry: (UITextField*) textField {
//    BOOL isFirstResponder = textField.isFirstResponder; //store whether textfield is firstResponder
//    
//    if (isFirstResponder) [textField resignFirstResponder]; //resign first responder if needed, so that setting the attribute to YES works
//    textField.secureTextEntry = !textField.secureTextEntry; //change the secureText attribute to opposite
//    if (isFirstResponder) [self.oldPasswordTF becomeFirstResponder]; //give the field focus again, if it was first responder initially
//}




#pragma mark - usee these methods for slide bar menu ============================


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    
    if ([screenType isEqualToString:@"UpdateProfile"]) {
  
    UIStoryboard *story = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    self.navigationViewController = [story instantiateViewControllerWithIdentifier:@"drawerView"];
    self.drawerTransition = [[PGDrawerTransition alloc] initWithTargetViewController:self
                                                                drawerViewController:self.navigationViewController];

    UIBarButtonItem *left = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"ic_menu@3x.png"] style:UIBarButtonItemStyleDone target:self action:@selector(open)];
    left.tintColor = [UIColor whiteColor];
    self.navigationItem.leftBarButtonItem = left;
        
    }
    else{
        
    }
    
}



-(void)viewWillDisappear:(BOOL)animated
{
    self.navigationController.navigationBar.hidden=YES;

//    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:0.95 green:0.28 blue:0.31 alpha:1.0];
//    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    
    [super viewWillDisappear:animated];
}



- (void)open
{
    [self.drawerTransition presentDrawerViewController];
}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


@end
