//
//  EditProfileVC.h
//  OutNAbout
//
//  Created by Ram Kumar on 12/08/16.
//  Copyright © 2016 tecHindustan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EditProfileVC : UIViewController

@property (retain, nonatomic) NSURLConnection *connection;

@property (retain, nonatomic) NSMutableData *receivedData;

@property (weak, nonatomic) IBOutlet UIImageView *blurImageView;
@property (weak, nonatomic) IBOutlet UIImageView *userImageView;


@property (strong, nonatomic)  UIBarButtonItem *rightBarButtonItem;

@property (weak, nonatomic) IBOutlet UITextField *lastNameTF;
@property (weak, nonatomic) IBOutlet UITextField *firstNameTF;

@property(nonatomic,strong) NSString *screenType;
@property (weak, nonatomic) IBOutlet UILabel *userNameLabel;

@end
