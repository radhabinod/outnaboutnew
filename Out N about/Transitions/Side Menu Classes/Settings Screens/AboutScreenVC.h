//
//  AboutScreenVC.h
//  OutNAbout
//
//  Created by Ram Kumar on 13/08/16.
//  Copyright © 2016 tecHindustan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AboutScreenVC : UIViewController
@property (weak, nonatomic) IBOutlet UIWebView *webView;

@property(nonatomic,strong) NSString *link;
@property(nonatomic,strong) NSString *screenTitle;

@end
