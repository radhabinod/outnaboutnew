//
//  FeedbackVC.m
//  OutNAbout
//
//  Created by Ram Kumar on 21/10/16.
//  Copyright © 2016 tecHindustan. All rights reserved.
//

#import "FeedbackVC.h"
#import "AppDelegate.h"
#import "SingleEventVC.h"
#import "ALToastView.h"
#import "SDWebImageManager.h"
#import "CarbonKit.h"
#import "PGDrawerTransition.h"
#import "NavigationViewController.h"
#import "CommonAPI's.pch"

#import "SVProgressHUD.h"

@interface FeedbackVC ()<UITextViewDelegate,UITextFieldDelegate,NSURLConnectionDataDelegate>

@end

@implementation FeedbackVC

- (void)viewDidLoad {
    [super viewDidLoad];    
    
    UIButton *button =  [UIButton buttonWithType:UIButtonTypeSystem];
    [button setImage:[UIImage imageNamed:@"ic_arrow@3x.png"] forState:UIControlStateNormal];
    [button addTarget:self action:@selector(handleBack:)forControlEvents:UIControlEventTouchUpInside];
    [button setFrame:CGRectMake(0, 0, 53, 31)];
    [button setImageEdgeInsets:UIEdgeInsetsMake(0, -30, 0, 0)];
    button.backgroundColor = [UIColor clearColor];
    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(30, 5, 240, 20)];
    [label setFont:[UIFont fontWithName:@"Arial-BoldMT" size:17]];
    [label setText:@"Feedback"];
    label.textAlignment = NSTextAlignmentLeft;
    [label setTextColor:[UIColor whiteColor]];
    [label setBackgroundColor:[UIColor clearColor]];
    [button addSubview:label];
    
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    
    
    UIBarButtonItem *barButton = [[UIBarButtonItem alloc] initWithCustomView:button];
    self.navigationItem.leftBarButtonItem = barButton;
    
    self.navigationItem.hidesBackButton = YES;
    
    
//    _messageTextView.layer.borderWidth = 0.5;
//    _messageTextView.layer.borderColor = [UIColor blackColor].CGColor;
    _messageTextView.delegate = self;
    
    _nameTF.delegate = self;
    _emailTF.delegate = self;

    
}



- (void) handleBack:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}



#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wdeprecated-declarations"

- (IBAction)submitFeedbackAction:(id)sender {
    
        NSString *emailReg = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
        NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailReg];
    
    if ([_nameTF.text isEqualToString:@""]) {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"OutNAbout" message:@"Please enter name."  delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        return;

    }
    else
        if ([_emailTF.text isEqualToString:@""]) {
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"OutNAbout" message:@"Please enter email."  delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
            return;

            
        }
    
    
        else if ([emailTest evaluateWithObject:_emailTF.text] == NO) {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"OutNAbout" message:@"Please enter a valid email address."  delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
            return;

            
        }
        else if ([_messageTextView.text isEqualToString:@""]) {
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"OutNAbout" message:@"Enter some feedbacks."  delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
            return;

        }
    
        else{
            
        }
    
}






-(void)feedBackPostMethod
{
    
    [SVProgressHUD showWithStatus:@"Please wait..." maskType:SVProgressHUDMaskTypeBlack];
    
    if (APP_DELEGATE.isServerReachable)
    {
    
    [self.connection cancel];
    
    NSMutableData *data = [[NSMutableData alloc] init];
    self.receivedData = data;
    
    NSURL *url = [NSURL URLWithString:(API_URL @"customerfeedback")];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[url standardizedURL]];
    
    [request setHTTPMethod:@"POST"];
    NSString *postData = [NSString stringWithFormat:@"name=%@&email=%@&message=%@",_nameTF.text,_emailTF.text,_messageTextView.text];
    
    [request setValue:@"application/x-www-form-urlencoded; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    
    [request setHTTPBody:[postData dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    self.connection = connection;
    
    [connection start];
    }
    else
    {
        [SVProgressHUD dismiss];
        [[[UIAlertView alloc]initWithTitle:@"OutNAbout" message:@"Internet connection is not available. Please try again."  delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil]show];
    }
    
}




#pragma mark - NSURLConnection delegate methods =  ==  == = = = = = = =  = = = = = = = = = = = = = = = = = = = = = = = = = = = = =

-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data{
    [self.receivedData appendData:data];
}


-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error{
    
    NSLog(@"eeeeeeee  %@" , error);
}

-(void)connectionDidFinishLoading:(NSURLConnection *)connection{
    
   // NSString *htmlSTR = [[NSString alloc] initWithData:self.receivedData
                                             // encoding:NSUTF8StringEncoding];
    // NSLog(@" ffff     %@" , htmlSTR);
    
    
    NSError *error = nil;
  id object = [NSJSONSerialization JSONObjectWithData: self.receivedData options: NSJSONReadingMutableContainers error: &error];
    if (error)
    {
                NSLog(@"Error: %@",[error description]);
    }
    if ([object isKindOfClass:[NSDictionary class]] == YES)
    {
        [SVProgressHUD dismiss];
        dispatch_async(dispatch_get_main_queue(), ^(void){
          
            [[[UIAlertView alloc]initWithTitle:@"OutNAbout" message:object[@"msg"] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil]show];
                 });
    }
    
    
}



-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{

        if(buttonIndex==0)
        {
            [self.navigationController popViewControllerAnimated:YES];
        }
   
    
}



#pragma clang diagnostic pop


#pragma mark - uitextfield delegates = = = = == = = = == = = == = = = == = = =

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    
}
- (void)textFieldDidEndEditing:(UITextField *)textField
{
    
}

-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    return YES;
}


- (void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [[self view] endEditing:YES];
}


#pragma mark - TextView delegates = = = = == = = = == = = == = = = == = = =

-(void)textViewDidBeginEditing:(UITextView *)textView
{
    _messagePlaceHolder.hidden = YES;
    
}

-(void)textViewDidChangeSelection:(UITextView *)textView
{
    if ([_messageTextView.text isEqualToString:@""]) {
        _messagePlaceHolder.hidden = NO;
        
    }else{
        _messagePlaceHolder.hidden = YES;
    }
}

-(void)textViewDidEndEditing:(UITextView *)textView
{
    if ([_messageTextView.text isEqualToString:@""]) {
        _messagePlaceHolder.hidden = NO;
        
    }else{
        _messagePlaceHolder.hidden = YES;
    }
}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
    
}


@end
