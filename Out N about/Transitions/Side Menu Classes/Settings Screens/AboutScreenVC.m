//
//  AboutScreenVC.m
//  OutNAbout
//
//  Created by Ram Kumar on 13/08/16.
//  Copyright © 2016 tecHindustan. All rights reserved.
//

#import "AboutScreenVC.h"
#import "CommonAPI's.pch"
#import "AppDelegate.h"
#import "SVProgressHUD.h"

@interface AboutScreenVC ()<UIWebViewDelegate>

@end

@implementation AboutScreenVC

{
    UIView *loadingView;
    UIActivityIndicatorView *activityView;

}
@synthesize link;


- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIButton *button =  [UIButton buttonWithType:UIButtonTypeSystem];
    [button setImage:[UIImage imageNamed:@"ic_arrow@3x.png"] forState:UIControlStateNormal];
    [button addTarget:self action:@selector(handleBack:)forControlEvents:UIControlEventTouchUpInside];
    [button setFrame:CGRectMake(0, 0, 53, 31)];
    [button setImageEdgeInsets:UIEdgeInsetsMake(0, -30, 0, 0)];
    button.backgroundColor = [UIColor clearColor];
    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(30, 5, 240, 20)];
    [label setFont:[UIFont fontWithName:@"Arial-BoldMT" size:17]];
    [label setText:_screenTitle];
    label.textAlignment = NSTextAlignmentLeft;
    [label setTextColor:[UIColor whiteColor]];
    [label setBackgroundColor:[UIColor clearColor]];
    [button addSubview:label];
    
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];

    
    UIBarButtonItem *barButton = [[UIBarButtonItem alloc] initWithCustomView:button];
    self.navigationItem.leftBarButtonItem = barButton;
    
    self.navigationItem.hidesBackButton = YES;
    
    if (APP_DELEGATE.isServerReachable)
    {
        

    
    NSString *urlString = link;
    NSURL *url = [NSURL URLWithString:urlString];
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:url];
    _webView.delegate = self;
    [_webView loadRequest:urlRequest];
    
   //[self showActivityIndicator];
    [SVProgressHUD showWithStatus:@"Loading.." maskType:SVProgressHUDMaskTypeBlack];
        
    }
    else
    {
        [[[UIAlertView alloc]initWithTitle:@"OutNAbout" message:@"Internet connection is not available. Please try again."  delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil]show];
    }

}


- (void) handleBack:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}




//-(void) showActivityIndicator
//{
//    //Add a UIView in your .h and give it the same property as you have given to your webView.
//    //Also ensure that you synthesize these properties on top of your implementation file
//    
//    loadingView = [[UIView alloc]initWithFrame:CGRectMake(100, 400, 80, 80)];
//    loadingView.backgroundColor = [UIColor colorWithWhite:0. alpha:0.6];
//    loadingView.center = self.view.center;
//    loadingView.layer.cornerRadius = 5;
//    
//    activityView=[[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
//    activityView.center = CGPointMake(loadingView.frame.size.width / 2.0, 35);
//    [activityView startAnimating];
//    activityView.tag = 100;
//    [loadingView addSubview:activityView];
//    
//    UILabel* lblLoading = [[UILabel alloc]initWithFrame:CGRectMake(0, 48, 80, 30)];
//    lblLoading.text = @"Loading...";
//    lblLoading.textColor = [UIColor whiteColor];
//    lblLoading.font = [UIFont fontWithName:lblLoading.font.fontName size:15];
//    lblLoading.textAlignment = NSTextAlignmentCenter;
//    [loadingView addSubview:lblLoading];
//    
//    [self.view addSubview:loadingView];
//}


//-(void) removeLoadingView
//{
//    [loadingView removeFromSuperview];
//}



-(void)webViewDidFinishLoad:(UIWebView *)webView {
    
   // [self removeLoadingView];
    [SVProgressHUD dismiss];
   // NSLog(@"finish");
}


-(void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {
    
  //  [self removeLoadingView];
    [SVProgressHUD dismiss];
  //  NSLog(@"Error for WEBVIEW: %@", [error description]);
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}

@end
