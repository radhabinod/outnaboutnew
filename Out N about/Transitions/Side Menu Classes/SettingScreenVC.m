//
//  SettingScreenVC.m
//  OutNAbout
//
//  Created by Ram Kumar on 21/07/16.
//  Copyright © 2016 tecHindustan. All rights reserved.
//

#import "SettingScreenVC.h"
#import "ALToastView.h"
#import "CarbonKit.h"
#import "AppDelegate.h"
#import "PGDrawerTransition.h"
#import "NavigationViewController.h"
#import "EditProfileVC.h"
#import "AboutScreenVC.h"
#import "CommonAPI's.pch"
#import "FeedbackVC.h"
#import "ChangePasswordVC.h"
#import "ViewController.h"

#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wdeprecated-declarations"

@interface SettingScreenVC ()<UITableViewDataSource,UITableViewDelegate>

@property (nonatomic, strong) PGDrawerTransition *drawerTransition;
@property (nonatomic, strong) NavigationViewController *navigationViewController;
@property (nonatomic, strong) ViewController *viewControllerObject;
@end

@implementation SettingScreenVC
{
    NSArray *profileArray;
    NSArray *notifArray;
    NSArray *socialAccArray;
    NSString *access_token;
    
    NSString *rememberCheck;
    NSString *username;
    NSString *password;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
//    UIStoryboard *story = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//    self.navigationViewController = [story instantiateViewControllerWithIdentifier:@"drawerView"];
//    self.drawerTransition = [[PGDrawerTransition alloc] initWithTargetViewController:self
//                                                                drawerViewController:self.navigationViewController];
    
    profileArray = @[@"Edit Profile",@"Change Password"];
    notifArray = @[@"Enable Push Notifications"];
    socialAccArray = @[@"About Us",@"Help Center",@"Our Supporting Partners",@"Tell a friend",@"Feedback",@"Logout"];
    
    

    UIButton *button =  [UIButton buttonWithType:UIButtonTypeSystem];
    [button setImage:[UIImage imageNamed:@"ic_arrow@3x.png"] forState:UIControlStateNormal];
    [button addTarget:self action:@selector(handleBack:)forControlEvents:UIControlEventTouchUpInside];
    [button setFrame:CGRectMake(0, 0, 53, 31)];
    [button setImageEdgeInsets:UIEdgeInsetsMake(0, -30, 0, 0)];
    button.backgroundColor = [UIColor clearColor];
    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(30, 5, 240, 20)];
    [label setFont:[UIFont fontWithName:@"Arial-BoldMT" size:17]];
    [label setText:@"Settings"];
    label.textAlignment = NSTextAlignmentLeft;
    [label setTextColor:[UIColor whiteColor]];
    [label setBackgroundColor:[UIColor clearColor]];
    [button addSubview:label];
    
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];

    UIBarButtonItem *barButton = [[UIBarButtonItem alloc] initWithCustomView:button];
    self.navigationItem.leftBarButtonItem = barButton;
    
    self.navigationItem.hidesBackButton = YES;

    
    
//    
//    UILabel* titleLabelNav = [[UILabel alloc] initWithFrame:CGRectMake(0,40,320,40)];
//    titleLabelNav.textAlignment = NSTextAlignmentLeft;
//    titleLabelNav.text = NSLocalizedString(@"Settings",@"");
//    titleLabelNav.textColor = [UIColor whiteColor];
//    self.navigationItem.titleView = titleLabelNav;

    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor whiteColor]};
    
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:0.95 green:0.28 blue:0.31 alpha:1.0];
    
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    access_token = [defaults objectForKey:@"access_token"];
    username = [defaults objectForKey:@"username"];
    password = [defaults objectForKey:@"password"];
    rememberCheck = [defaults objectForKey:@"rememCheck"];

    [self createTableview];

}


- (void) handleBack:(id)sender
{
    UIStoryboard *story = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    _viewControllerObject  = [story instantiateViewControllerWithIdentifier:@"viewController"];
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:_viewControllerObject];
    [self presentViewController:nav animated:NO completion:nil];
    //[self.view removeFromSuperview];

}



-(void)createTableview
{
    _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 50, self.view.frame.size.width, self.view.frame.size.height-50) style:UITableViewStyleGrouped];

    self.tableView.delegate=self;
    self.tableView.dataSource=self;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:_tableView];
}



#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section==0)
    {
        return [profileArray count];
    }
    else if (section==1){
        return [notifArray count];
    }
    else{
         return [socialAccArray count];
    }
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if(section == 0)
        return @"Profile";
    else if(section == 1)
        return @"Notification";
    else
        return @"Information";
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"];
        
        
        
    }
    
    
    if (indexPath.section==0) {
  
        cell.textLabel.text = [profileArray objectAtIndex:indexPath.row];
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }
    else if (indexPath.section==1) {
        cell.textLabel.text = [notifArray objectAtIndex:indexPath.row];
        
//        UIButton *tickButton = [UIButton buttonWithType:UIButtonTypeCustom];
//        tickButton.frame = CGRectMake(320, 13, 20, 20);
//        [tickButton setImage:[UIImage imageNamed:@"ic_Select_enable@3x.png"] forState:UIControlStateNormal];
//        //[tickButton addTarget:self action:@selector(incl) forControlEvents:UIControlStateNormal];
//        [cell.contentView addSubview:tickButton];
        
        UISwitch *switchview = [[UISwitch alloc] initWithFrame:CGRectZero];
        [switchview addTarget:self action:@selector(toggleNotificationSwitch:) forControlEvents:UIControlEventValueChanged];
        cell.accessoryView = switchview;

        
    }
    else{
        cell.textLabel.text = [socialAccArray objectAtIndex:indexPath.row];
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;

    }
    
    return cell;
}





-(void)toggleNotificationSwitch:(UISwitch *)aswitch{
    
    if ([aswitch isOn]) {
        [aswitch setOn:NO animated:YES];
        NSLog(@"Switch is OFF");
    } else {
        [aswitch setOn:YES animated:YES];
        NSLog(@"Switch is ON");
    }

}



-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *loginType = [defaults objectForKey:@"loginType"];

    
    
    if (indexPath.section==0)
    {
        if (indexPath.row==0)
        {
            if ([loginType isEqualToString:@"fb"]) {
                [[[UIAlertView alloc]initWithTitle:@"Facebook login!" message:@"Can't edit your profile." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
            }
            else if ([loginType isEqualToString:@"gp"]) {
           [[[UIAlertView alloc]initWithTitle:@"Google login!" message:@"Can't edit your profile." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
            }
            else{
            UIStoryboard *story = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            EditProfileVC *profileView = [story instantiateViewControllerWithIdentifier:@"editProfile"];
            [self.navigationController pushViewController:profileView animated:YES];
            }
        }
        
        else  if (indexPath.row==1)
        {
            if ([loginType isEqualToString:@"fb"]) {
                [[[UIAlertView alloc]initWithTitle:@"Facebook login!" message:@"Can't edit your profile." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
            }
            else if ([loginType isEqualToString:@"gp"]) {
                [[[UIAlertView alloc]initWithTitle:@"Google login!" message:@"Can't edit your profile." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
            }
            else{
                UIStoryboard *story = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                ChangePasswordVC *changeProfileView = [story instantiateViewControllerWithIdentifier:@"changepassword"];
                [self.navigationController pushViewController:changeProfileView animated:YES];
            }
        }

    }
    else if (indexPath.section==2)
    {
        
    if (indexPath.row==0)
    {
        UIStoryboard *story = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        AboutScreenVC *aboutView = [story instantiateViewControllerWithIdentifier:@"aboutView"];
        aboutView.link = SOCIAL_URL @"Aboutus";
        aboutView.screenTitle = @"About us";
        [ self.navigationController pushViewController:aboutView animated:YES];
    }
        
       else if (indexPath.row==1)
        {
            UIStoryboard *story = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            AboutScreenVC *aboutView = [story instantiateViewControllerWithIdentifier:@"aboutView"];
            aboutView.link = SOCIAL_URL @"Help";
            aboutView.screenTitle = @"Help Center";

            [ self.navigationController pushViewController:aboutView animated:YES];
        }
      else  if (indexPath.row==2)
        {
            UIStoryboard *story = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            AboutScreenVC *aboutView = [story instantiateViewControllerWithIdentifier:@"aboutView"];
            aboutView.link = SOCIAL_URL @"Partners";
            aboutView.screenTitle = @"Our Supporting Partners";

            [ self.navigationController pushViewController:aboutView animated:YES];
        }
        
      else  if (indexPath.row==3)
      {
       [self Url:[NSURL URLWithString:@"OutNAbout,Let me recommend you this application.Enjoy! download this app from App Store."]];
          
      }
        
    else if (indexPath.row==4)
    {
        UIStoryboard *story = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        FeedbackVC *feedbackView = [story instantiateViewControllerWithIdentifier:@"feedbackView"];
        [ self.navigationController pushViewController:feedbackView animated:YES];

    }
        else if (indexPath.row==5)
        {
            
            [self didLogout];
        }
        
    }
    
    else{
  //  [[[UIAlertView alloc]initWithTitle:@"Message!" message:@"Could not perform any action yet." delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles: nil]show];
        
        
     //   [self.drawerTransition dismissDrawerViewController];
       // [self dismissViewControllerAnimated:NO completion:nil];
        
        
    }
}





- (void)Url:(NSURL *)url
{
    NSMutableArray *sharingItems = [NSMutableArray new];
    if (url) {
        [sharingItems addObject:url];
    }
    UIActivityViewController *activityController = [[UIActivityViewController alloc] initWithActivityItems:sharingItems applicationActivities:nil];
    [self presentViewController:activityController animated:YES completion:nil];
}




-(void)didLogout
{
    
    
    
    NSLog(@"logout clicked");
    
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:@"Message"
                                  message:@"Do you really want to logout?"
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* okButton = [UIAlertAction
                               actionWithTitle:@"OK"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action)
                               {
                                   
                                   if (APP_DELEGATE.isServerReachable)
                                   {

                                   
                                   [self logoutFuction];
                                   FBSDKLoginManager *loginManager = [[FBSDKLoginManager alloc] init];
                                   [loginManager logOut];
                                   
                                   [FBSDKAccessToken setCurrentAccessToken:nil];
                                   [FBSDKProfile setCurrentProfile:nil];
                                   
                                   
                                   
                                   FBSDKGraphRequest *request = [[FBSDKGraphRequest alloc]
                                                                 initWithGraphPath:@"me/permissions/" parameters:nil HTTPMethod:@"DELETE"];
                                   [request   startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
                                       
                                       NSLog(@"Delete facebook login");
                                       
                                   }];
                                   
                                   
                                   [[NSUserDefaults standardUserDefaults] setPersistentDomain:[NSDictionary dictionary] forName:[[NSBundle mainBundle] bundleIdentifier]];
                                   
                                   NSUserDefaults * defs = [NSUserDefaults standardUserDefaults];
                                   // [defs removeObjectForKey:@"username"];
                                   //    [defs removeObjectForKey:@"password"];
                                   NSDictionary * dict = [defs dictionaryRepresentation];
                                   for (id key in dict) {
                                       [defs removeObjectForKey:key];
                                   }
                                   [defs synchronize];
                                   
                                   
                                   
                                   [self resetDefaults];
                                       
                                       
                                       
                                       
                                       dispatch_async( dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                                           // Add code here to do background processing

                                           UIStoryboard *story = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                                           _viewControllerObject  = [story instantiateViewControllerWithIdentifier:@"viewController"];
                                           UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:_viewControllerObject];
                                           [self presentViewController:nav animated:NO completion:nil];
                                           [self.view removeFromSuperview];
                                            [self  dismissModalStack];

                                           dispatch_async( dispatch_get_main_queue(), ^{
                                               // Add code here to update the UI/send notifications based on the
                                               // results of the background processing
                                               
                                               [self performSegueWithIdentifier:@"tologin" sender:nil];
                                           });
                                       });
                                       
                                       
    
                                   
                                       access_token=nil;
            
                                       if (rememberCheck==nil) {
                                           rememberCheck=@"";
                                       }
                                       
                                       
                                       NSDictionary *userDetails = @{@"userName":username,@"password":password,@"ifCheck":rememberCheck};
                                       
                                       if ([rememberCheck isEqualToString:@"check"] && username.length) {

                                           [[NSNotificationCenter defaultCenter] postNotificationName:@"UpdateRememberMe" object:self userInfo:userDetails];
                                       }
                                       else{

                                       }
                                       
                                       
        
                                       
                                       
                                       

                                   }
                                   
                                   else
                                   {
                                       [[[UIAlertView alloc]initWithTitle:@"OutNAbout" message:@"Internet connection is not available. Please try again."  delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil]show];
                                   }

                                   
                                   
                               }];
    UIAlertAction* cancelButton = [UIAlertAction
                                   actionWithTitle:@"Cancel"
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action)
                                   {
                                       //Handel your yes please button action here
                                       
                                       
                                   }];
    
    [alert addAction:okButton];
    [alert addAction:cancelButton];
    
    
    [self presentViewController:alert animated:YES completion:nil];

}





-(void)logoutFuction
{
    
    if (APP_DELEGATE.isServerReachable)
    {
    [self.connection cancel];
    
    NSMutableData *data = [[NSMutableData alloc] init];
    self.receivedData = data;
    
    NSURL *url = [NSURL URLWithString:(API_URL @"logout")];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[url standardizedURL]];
    
    [request setHTTPMethod:@"POST"];
    NSString *postData = [NSString stringWithFormat:@"app_token=%@",access_token];
    
    
    [request setValue:@"application/x-www-form-urlencoded; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    
    //set post data of request
    [request setHTTPBody:[postData dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    self.connection = connection;
    
    [connection start];
    
}

else
{
    [[[UIAlertView alloc]initWithTitle:@"OutNAbout" message:@"Internet connection is not available. Please try again."  delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil]show];
}

}



#pragma mark - NSURLConnection delegate methods =  ==  == = = = = = = =  = = = = = = = = = = = = = = = = = = = = = = = = = = =

-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data{
    [self.receivedData appendData:data];
}


-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error{
    
    NSLog(@"eeeeeeee  %@" , error);
}



-(void)connectionDidFinishLoading:(NSURLConnection *)connection{
    
  //  NSString *htmlSTR = [[NSString alloc] initWithData:self.receivedData
                                       //       encoding:NSUTF8StringEncoding];
    // NSLog(@" ffff     %@" , htmlSTR);
    
    
  //  NSError *error = nil;
  //  NSDictionary *jsonArray = [NSJSONSerialization JSONObjectWithData: self.receivedData options: NSJSONReadingMutableContainers error: &error];  //I am using sbjson to parse
    
    
}




- (void)resetDefaults {
    NSUserDefaults * defs = [NSUserDefaults standardUserDefaults];
    // [defs removeObjectForKey:@"username"];
    //    [defs removeObjectForKey:@"password"];
    NSDictionary * dict = [defs dictionaryRepresentation];
    for (id key in dict) {
        [defs removeObjectForKey:key];
    }
    [defs synchronize];
}







-(void)dismissModalStack {
    UIViewController *vc = self.presentingViewController;
    while (vc.presentingViewController) {
        vc = vc.presentingViewController;
    }
    [vc dismissViewControllerAnimated:YES completion:NULL];
}


#pragma mark - usee these methods for slide bar menu ============================


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.hidden=NO;
    
//    UIBarButtonItem *left = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"ic_menu@3x.png"] style:UIBarButtonItemStyleDone target:self action:@selector(open)];
//    left.tintColor = [UIColor whiteColor];
//    self.navigationItem.leftBarButtonItem = left;
    
}

//- (void)open
//{
//    [self.drawerTransition presentDrawerViewController];
//}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
    
}



@end
