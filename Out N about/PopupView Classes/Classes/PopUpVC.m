//
//  PopUpVC.m
//  OutNAbout
//
//  Created by Ram Kumar on 15/07/16.
//  Copyright © 2016 tecHindustan. All rights reserved.
//

#import "PopUpVC.h"
#import "SingleEventVC.h"
#import "BIZPopupViewController.h"
#import "SVProgressHUD.h"
#import "CommonAPI's.pch"
#import "AppDelegate.h"

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wdeprecated-declarations"

@interface PopUpVC ()<UITextViewDelegate>

#define API_URL @"http://agiledevelopers.in/outnabout/webservices/users/"

@end

@implementation PopUpVC

{
    NSString *access_token;
    NSString *ratingValue;
    BIZPopupViewController *BIZPopupViewControllerObject;
}
@synthesize rateView,statusLabel,commentTV,submitButton,eventIdString;


- (void)viewDidLoad {
    [super viewDidLoad];
     [commentTV setDelegate:self];
    // Do any additional setup after loading the view.
    NSLog(@"single event is  %@", eventIdString);
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    access_token = [defaults objectForKey:@"access_token"];
    NSString *comment  = [defaults objectForKey:@"comment"];
    ratingValue=[defaults objectForKey:@"rating"] ;
    float rates  = [[defaults objectForKey:@"rating"] floatValue];
    
    commentTV.text = comment;
    
    self.rateView.notSelectedImage = [UIImage imageNamed:@"ic_star_blank@3x.png"];
    self.rateView.halfSelectedImage = [UIImage imageNamed:@"ic_star_half@3x.png"];
    self.rateView.fullSelectedImage = [UIImage imageNamed:@"ic_star_fill@3x.png"];
    self.rateView.rating = rates;
    self.rateView.editable = YES;
    self.rateView.maxRating = 5;
    self.rateView.delegate = self;

    
}


- (void)viewDidUnload
{
    [self setRateView:nil];
    [self setStatusLabel:nil];
    
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}





- (IBAction)commentAndRatingSubmitAction:(id)sender
{
    if (!commentTV.text.length && [ratingValue isEqualToString:@"0.000000"]) {
        [[[UIAlertView alloc]initWithTitle:@"OutNAbout" message:@"Please enter rating or comment." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil]show];
    }
    else if (!commentTV.text.length && [ratingValue isEqualToString:@""]) {
         [[[UIAlertView alloc]initWithTitle:@"OutNAbout" message:@"Please enter rating or comment." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil]show];
    }
    else {
//            [[[UIAlertView alloc]initWithTitle:@"Message!" message:@"Please enter rating." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil]show];
  
    [self postMethodForCommentsRating];
        
        
        
    //[self dismissPopupViewControllerAnimatedPassiveAction];
        
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults setObject:commentTV.text forKey:@"comment"];
        [defaults setObject:ratingValue forKey:@"rating"];
        [defaults synchronize];
   
    }
    NSLog(@"comment value *****%@     RATING %@",commentTV.text,self.statusLabel.text);
}




- (void)rateView:(RateView *)rateView ratingDidChange:(float)rating {
    //self.statusLabel.text = [NSString stringWithFormat:@"Rating: %f", rating];
    ratingValue = [NSString stringWithFormat:@"%f", rating];
    
}


-(void)postMethodForCommentsRating
{
    [SVProgressHUD showWithStatus:@"Waiting.." maskType:SVProgressHUDMaskTypeBlack];
    
    if (APP_DELEGATE.isServerReachable)
    {

    [self.connection cancel];
    
    //initialize new mutable data
    NSMutableData *data = [[NSMutableData alloc] init];
    self.receivedData = data;
    
    NSURL *url = [NSURL URLWithString:(API_URL @"commentsAndRating")];
    
    //initialize a request from url
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[url standardizedURL]];
    
    //set http method
    [request setHTTPMethod:@"POST"];
    //initialize a post data
    
    NSString *postData = [NSString stringWithFormat:@"event_id=%@ & app_token=%@ & comment=%@ & rating=%@", eventIdString,access_token,commentTV.text,ratingValue];
    
    [request setValue:@"application/x-www-form-urlencoded; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    
    [request setHTTPBody:[postData dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    self.connection = connection;
    
    //start the connection
    [connection start];
        
    }
    else
    {
        [SVProgressHUD dismiss];
        [[[UIAlertView alloc]initWithTitle:@"OutNAbout" message:@"Internet connection is not available. Please try again."  delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil]show];
    }
}


#pragma mark - NSURL Post methods 


-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data{
    [self.receivedData appendData:data];
}


-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    NSLog(@"eeeeeeee  %@" , error);
}


-(void)connectionDidFinishLoading:(NSURLConnection *)connection{
    
    //NSString *htmlSTR = [[NSString alloc] initWithData:self.receivedData
                                          //    encoding:NSUTF8StringEncoding];
    
    NSError *error = nil;
    NSDictionary *jsonArray = [NSJSONSerialization JSONObjectWithData: self.receivedData options: NSJSONReadingMutableContainers error: &error];
    
    NSString *msg = jsonArray[@"msg"];
    [SVProgressHUD dismiss];
    if ([msg isEqualToString:@"Comment and rating inserted Successfully"]) {
//        [[[UIAlertView alloc]initWithTitle:@"Submited" message:msg delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil]show];
         [self dismissViewControllerAnimated:YES completion:nil];
        
         NSDictionary *message = @{@"message":msg};
         [[NSNotificationCenter defaultCenter] postNotificationName:@"RefreshSingleEventView" object:self userInfo:message];

    }
    else
    {
        
    }
    
  
    //NSLog(@"json response is :%@",jsonArray);
    
}

//- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
//
//{
//    if (buttonIndex==0) {
//        
//    }
//}







- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}






-(void)textViewDidBeginEditing:(UITextView *)textView
{
    _enterCommentLabel.hidden = YES;
    
}

-(void)textViewDidChangeSelection:(UITextView *)textView
{
    if ([commentTV.text isEqualToString:@""]) {
        _enterCommentLabel.hidden = NO;
        
    }else{
        _enterCommentLabel.hidden = YES;
    }
}

-(void)textViewDidEndEditing:(UITextView *)textView
{
    if ([commentTV.text isEqualToString:@""]) {
        _enterCommentLabel.hidden = NO;

    }else{
    _enterCommentLabel.hidden = YES;
    }
}





- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    
}



@end
