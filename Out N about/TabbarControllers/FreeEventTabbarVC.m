//
//  FreeEventTabbarVC.m
//  OutNAbout
//
//  Created by Ram Kumar on 30/06/16.
//  Copyright © 2016 tecHindustan. All rights reserved.
//

#import "FreeEventTabbarVC.h"
#import "SingleEventVC.h"
#import "AppDelegate.h"
#import "ALToastView.h"
#import "SDWebImageManager.h"
#import "CarbonKit.h"
#import "CommonAPI's.pch"
#import "SVProgressHUD.h"
#import "UITableView+DragLoad.h"


#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wdeprecated-declarations"

@interface FreeEventTabbarVC ()<UITableViewDataSource,UITableViewDelegate,NSURLConnectionDataDelegate,UITableViewDragLoadDelegate,CLLocationManagerDelegate>
@property (nonatomic, strong) CLLocation* currentLocation;

@end

@implementation FreeEventTabbarVC
{
    NSMutableArray *updatedArray;
    NSNumber *eventsCount;
    int limit;
    NSString *latitude;
    NSString *longitude;
    
    NSMutableArray *imagesArray;
    NSMutableArray *venueArray;
    NSMutableArray *dateArray;
    NSMutableArray *startTimeArray;
    NSMutableArray *endTimeArray;
    NSMutableArray *endDateArray;
    NSMutableArray *shareArray;
    NSMutableArray *categoryArray;
    NSMutableArray *eventIdArray;
    NSMutableArray *favoriteArray;
    NSMutableArray *eventViewsArray;
    NSMutableArray *costArray;
    NSMutableArray *descriptionArray;
    NSString *access_token;
    NSMutableArray *titleLabel;
     NSMutableArray *stateArray;
    NSMutableArray *latitudeArray;
    NSMutableArray *longitudeArray;
    NSMutableArray *eventTypeArray;
    NSMutableArray *currencyArray;

    NSString *filterType;
    CarbonSwipeRefresh *newRefresh;
    UIActivityIndicatorView *activityView;
    UIView* loadingView;
    UIRefreshControl *refresh;
    CGRect rectText;
    CGRect rectFavImage;
    CGRect rectCatText;
    CGRect rectSponsorImage;
    
    NSString *timezoneString;
    
    CLLocationManager *locationManager;
    CLGeocoder *geocoder;
    CLPlacemark *placemark;
    MKPointAnnotation *annotation;


}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    limit = 0;
    eventsCount=@0;
    
    updatedArray = [[NSMutableArray alloc]init];

    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    access_token = [defaults objectForKey:@"access_token"];
     timezoneString= [defaults objectForKey:@"timezone"];
    
    [self.view addSubview:_dimCountBackground];
    
//    refresh = [[UIRefreshControl alloc]init];
//    refresh.backgroundColor = [UIColor blackColor];
//    refresh.tintColor = [UIColor whiteColor];
//    [refresh addTarget:self action:@selector(freeEventrefreshTable) forControlEvents:UIControlEventValueChanged];
//    [_imagesTableView addSubview:refresh];
    
    
    newRefresh = [[CarbonSwipeRefresh alloc] initWithScrollView:_imagesTableView];
    [self.view addSubview:newRefresh];
    
    [newRefresh setColors:@[
                            [UIColor blackColor],
                            [UIColor redColor],
                            [UIColor colorWithRed:0.95 green:0.28 blue:0.31 alpha:1.0],
                            [UIColor greenColor]
                            ]];
    
    [newRefresh addTarget:self action:@selector(refresh:) forControlEvents:UIControlEventValueChanged];

    
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(receiveToggleAuthUINotifications:)
     name:@"filterType4"
     object:nil];
    
    [self createTableView];
    [self postMethod];


}





- (void) receiveToggleAuthUINotifications:(NSNotification *) notification {
    if ([[notification name] isEqualToString:@"filterType4"]) {
        
        latitude = @"";
        longitude = @"";
        
        if ([notification userInfo][@"byDate"])
        {
            filterType =@"1";
            limit=0;
            [updatedArray removeAllObjects];
            [self filterPostMethod];
            [_imagesTableView reloadData];
            
        }else if ([notification userInfo][@"byNear"])
        {
            filterType =@"4";
            limit=0;
            [updatedArray removeAllObjects];
            [self getCurrentLoaction];
            [_imagesTableView reloadData];
            
        }else{
            filterType =@"";
            limit=0;
            [updatedArray removeAllObjects];
            [self postMethod];
            [_imagesTableView reloadData];
            
        }
        
    }
}





-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
   
      // [_imagesTableView reloadData];
    
//    loadingView = [[UIView alloc]initWithFrame:CGRectMake(100, 400, 80, 80)];
//    loadingView.backgroundColor = [UIColor colorWithWhite:0. alpha:0.6];
//    loadingView.center = self.view.center;
//    loadingView.layer.cornerRadius = 5;
//    
//    activityView=[[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
//    activityView.center = CGPointMake(loadingView.frame.size.width / 2.0, 35);
//    [activityView startAnimating];
//    activityView.tag = 100;
//    [loadingView addSubview:activityView];
//    
//    UILabel* lblLoading = [[UILabel alloc]initWithFrame:CGRectMake(0, 48, 80, 30)];
//    lblLoading.text = @"Loading...";
//    lblLoading.textColor = [UIColor whiteColor];
//    lblLoading.font = [UIFont fontWithName:lblLoading.font.fontName size:15];
//    lblLoading.textAlignment = NSTextAlignmentCenter;
//    [loadingView addSubview:lblLoading];
//    
//    [self.view addSubview:loadingView];
    
    
    
    
}


- (void)refresh:(id)sender {
    NSLog(@"REFRESH");
    if (APP_DELEGATE.isServerReachable)
    {
        [newRefresh startRefreshing];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 5 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            
            limit=0;
            [updatedArray removeAllObjects];
            [self postMethod];
            [_imagesTableView reloadData];
            
            
            [self endRefreshing];
            
            
            
        });
        
        
    }
    else
    {
        [newRefresh endRefreshing];
        [SVProgressHUD dismiss];
        _eventCountLabel.hidden=YES;
        [updatedArray removeAllObjects];
        [_imagesTableView reloadData];
        [[[UIAlertView alloc]initWithTitle:@"OutNAbout" message:@"Internet connection is not available. Please try again."  delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil]show];
    }
}


- (void)endRefreshing {
    [newRefresh endRefreshing];
}


- (void)finishLoadMore
{
    
    if (filterType.length) {
        [SVProgressHUD showWithStatus:@"Loading Events..." maskType:SVProgressHUDMaskTypeBlack];
        [self filterPostMethod];
    }
    else
    {
        [self postMethod];
    }
    
    //   [_imagesTableView finishLoadMore];
    //[_imagesTableView reloadData];
}


//-(void)freeEventrefreshTable
//{
//    [refresh beginRefreshing];
//    [self postMethod];
//    [_imagesTableView reloadData];
//    
//    if (refresh) {
//        
//        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
//        [formatter setDateFormat:@"MMM d, h:mm a"];
//        NSString *title = [NSString stringWithFormat:@"Last update: %@", [formatter stringFromDate:[NSDate date]]];
//        NSDictionary *attrsDictionary = [NSDictionary dictionaryWithObject:[UIColor whiteColor]
//                                                                    forKey:NSForegroundColorAttributeName];
//        NSAttributedString *attributedTitle = [[NSAttributedString alloc] initWithString:title attributes:attrsDictionary];
//        refresh.attributedTitle = attributedTitle;
//        
//        [ALToastView toastInView:self.view withText:@"Events Updated"];
//    }
//    
//    [refresh endRefreshing];
//}



-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
   // [_imagesTableView reloadData];
    

    
}



-(void)getCurrentLoaction
{
    locationManager = [CLLocationManager new];
    locationManager.delegate = self;
    
    if ([locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)])
    {
        [locationManager requestWhenInUseAuthorization];
    }
    locationManager.desiredAccuracy = kCLLocationAccuracyKilometer;
    locationManager.activityType = CLActivityTypeFitness;
    locationManager.distanceFilter = 500;
    [locationManager startUpdatingLocation];
}



- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    _currentLocation = [locations objectAtIndex:0];
    [locationManager stopUpdatingLocation];
    geocoder = [[CLGeocoder alloc] init];
    
    
    CLLocationCoordinate2D coordinate = [_currentLocation coordinate];
    
    
    latitude = [NSString stringWithFormat:@"%f", coordinate.latitude];
    longitude = [NSString stringWithFormat:@"%f", coordinate.longitude];
    
    NSString *display_coordinates=[NSString stringWithFormat:@"Latitude %f and Longitude %f",coordinate.longitude,coordinate.latitude];
    
    annotation = [[MKPointAnnotation alloc] init];
    [annotation setCoordinate:coordinate];
    
    NSLog(@"latitude :%@",latitude);
    NSLog(@"dLongitude : %@",longitude);
    
    [geocoder reverseGeocodeLocation:_currentLocation completionHandler:^(NSArray *placemarks, NSError *error)
     {
         if (!(error))
         {
             placemark = [placemarks objectAtIndex:0];
             
             
             NSLog(@"\nCurrent Location Detected\n");
             NSLog(@"placemark %@",placemark);
             
             NSString *Area = [[NSString alloc]initWithString:placemark.locality];
             NSLog(@"Timezone -%@",placemark.timeZone);
             
             [annotation setTitle:Area];
             [annotation setSubtitle:display_coordinates];
             
             [self filterPostMethod];
             
         }
         else
         {
             NSLog(@"Geocode failed with error %@", error);
             NSLog(@"\nCurrent Location Not Detected\n");
         }
     }];
    
}





-(void)filterPostMethod
{
    if (APP_DELEGATE.isServerReachable)
    {
    [self.connection cancel];
    
    NSMutableData *data = [[NSMutableData alloc] init];
    self.receivedData = data;
    
    NSURL *url = [NSURL URLWithString:(API_URL @"freevents")];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[url standardizedURL]];
    
    [request setHTTPMethod:@"POST"];
    NSString *postData = [NSString stringWithFormat:@"app_token=%@&filter=%@&latitude=%@&longitude=%@&startlimit=%d",access_token,filterType,latitude,longitude,limit];
    
    
    [request setValue:@"application/x-www-form-urlencoded; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    
    //set post data of request
    [request setHTTPBody:[postData dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    self.connection = connection;
    
    [connection start];
    }
    else
    {
        [SVProgressHUD dismiss];
        [_imagesTableView finishLoadMore];
        [[[UIAlertView alloc]initWithTitle:@"OutNAbout" message:@"Internet connection is not available. Please try again."  delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil]show];
    }
}



-(void)postMethod
{
    [SVProgressHUD showWithStatus:@"Loading Events..." maskType:SVProgressHUDMaskTypeBlack];
    
    if (APP_DELEGATE.isServerReachable)
    {

    [self.connection cancel];
    
    //initialize new mutable data
    NSMutableData *data = [[NSMutableData alloc] init];
    self.receivedData = data;
    
    //initialize url that is going to be fetched.
    NSURL *url = [NSURL URLWithString:(API_URL @"freevents")];
    
    //initialize a request from url
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[url standardizedURL]];
    
    //set http method
    [request setHTTPMethod:@"POST"];
    //initialize a post data
    NSString *postData = [NSString stringWithFormat:@"app_token=%@&startlimit=%d",access_token,limit];
    
    [request setValue:@"application/x-www-form-urlencoded; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    
    //set post data of request
    [request setHTTPBody:[postData dataUsingEncoding:NSUTF8StringEncoding]];
    
    //initialize a connection from request
    NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    self.connection = connection;
    
    //start the connection
    [connection start];
        
    }
    
    else
    {
        [SVProgressHUD dismiss];
[_imagesTableView finishLoadMore];
        [[[UIAlertView alloc]initWithTitle:@"OutNAbout" message:@"Internet connection is not available. Please try again."  delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil]show];
    }
    
}





#pragma mark - NSURLConnection delegate methods =  ==  == = = = = = = =  = = = = = = = = = = = = = = = = = = = = = = = = = = = = =

-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data{
    [self.receivedData appendData:data];
}


-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error{
    
    [SVProgressHUD dismiss];
    [_imagesTableView finishLoadMore];
    [[[UIAlertView alloc]initWithTitle:@"OutNAbout" message:@"The network connection was lost."  delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil]show];

    NSLog(@"eeeeeeee  %@" , error);
}



-(void)connectionDidFinishLoading:(NSURLConnection *)connection{
    
  //  NSString *htmlSTR = [[NSString alloc] initWithData:self.receivedData
                              //                encoding:NSUTF8StringEncoding];
    // NSLog(@" ffff     %@" , htmlSTR);
    
    
    NSError *error = nil;
    NSDictionary *jsonArray = [NSJSONSerialization JSONObjectWithData: self.receivedData options: NSJSONReadingMutableContainers error: &error];  //I am using sbjson to parse
    
    
     limit=limit+5;
    
    //  NSString *code = [jsonArray[@"code"] stringValue];
    NSMutableArray *data = jsonArray[@"data"];
    
     [updatedArray addObjectsFromArray:data];
    
    
    
    NSString *msg = jsonArray[@"msg"];
    if ([msg isEqualToString:@"Permission denied Invalid access token!"]) {
        // [loadingView setHidden:YES];
        [SVProgressHUD dismiss];
        _eventCountLabel.hidden=YES;
        [[[UIAlertView alloc]initWithTitle:@"Warning!" message:@"Permission denied login again." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil]show];
        
    }else{
        
    }

    
    imagesArray = [[NSMutableArray alloc]init];
    titleLabel = [[NSMutableArray alloc]init];
    venueArray = [[NSMutableArray alloc]init];
    dateArray = [[NSMutableArray alloc]init];
    startTimeArray = [[NSMutableArray alloc]init];
    categoryArray = [[NSMutableArray alloc]init];
    endTimeArray = [[NSMutableArray alloc]init];
    eventIdArray = [[NSMutableArray alloc]init];
    eventViewsArray = [[NSMutableArray alloc]init];
    favoriteArray = [[NSMutableArray alloc]init];
    costArray = [[NSMutableArray alloc]init];
    descriptionArray = [[NSMutableArray alloc]init];
    endDateArray= [[NSMutableArray alloc]init];
    shareArray= [[NSMutableArray alloc]init];
     stateArray= [[NSMutableArray alloc]init];

    latitudeArray= [[NSMutableArray alloc]init];
    longitudeArray= [[NSMutableArray alloc]init];
       eventTypeArray= [[NSMutableArray alloc]init];
    currencyArray= [[NSMutableArray alloc]init];
    
    NSString *msgs = jsonArray[@"msg"];
    if ([msgs isEqualToString:@"No record found!"]) {
        [SVProgressHUD dismiss];
        _eventCountLabel.hidden=YES;
        [[[UIAlertView alloc]initWithTitle:@"Message!" message:msg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil]show];
    }
    else{
     _eventCountLabel.hidden=NO;
        eventsCount = jsonArray[@"count"];
        _eventCountLabel.text = [NSString stringWithFormat:@"Showing Events : %lu/%@",(unsigned long)updatedArray.count, eventsCount];
        
        if (updatedArray.count==[eventsCount doubleValue]) {
            _imagesTableView.showLoadMoreView=NO;
        }
        else
        {
            _imagesTableView.showLoadMoreView=YES;
            
        }
        

    for (NSDictionary *d  in  updatedArray) {
        
        NSString *imageString = d[@"image"];
        [imagesArray  addObject:imageString];
        
        NSString *titleText = d[@"title"];
        [titleLabel addObject:titleText];
        
        NSString *venueString = d[@"venue"];
        [venueArray  addObject:venueString];
        
        NSString *stateString = d[@"state"];
        [stateArray addObject:stateString];
        
        NSString *currencyString = d[@"currency"];
        [currencyArray addObject:currencyString];
        
        NSString *latitudeString = d[@"latitude"];
        [latitudeArray  addObject:latitudeString];
        
        NSString *eventTypeString = d[@"event_type"];
        [eventTypeArray  addObject:eventTypeString];
        
        NSString *longitudeString = d[@"longitude"];
        [longitudeArray  addObject:longitudeString];
        
        NSString *shareString = d[@"share"];
        [shareArray  addObject:shareString];
        
        NSString *dateString = d[@"date"];
        [dateArray addObject:dateString];
        
        NSString *strtTimeString = d[@"start_time"];
        [startTimeArray  addObject:strtTimeString];
        
        NSString *endTimeString = d[@"end_time"];
        [endTimeArray  addObject:endTimeString];
        
        NSString *endDateString = d[@"enddate"];
        [endDateArray  addObject:endDateString];

        
        NSString *eventIdString = d[@"event_id"];
        [eventIdArray  addObject:eventIdString];
        
        NSString *eventViewsString = d[@"event_views"];
        [eventViewsArray  addObject:eventViewsString];
        
        NSString *favoriteString = d[@"favorite"];
        [favoriteArray  addObject:favoriteString];
        
        NSString *costString = d[@"cost"];
        [costArray  addObject:costString];
        
        NSString *descriptionString = d[@"description"];
        [descriptionArray addObject:descriptionString];

        
        NSString *cats = @"";
        NSMutableArray *response =d[@"categories"];
        
        for (NSDictionary *dict in response) {
            
            cats = [cats stringByAppendingString:[NSString stringWithFormat:@"%@,",dict[@"cat_title"]]];
            
        }
        
        if([[cats substringFromIndex:cats.length - 1] isEqualToString:@","])
        {
            cats = [cats substringToIndex:cats.length - 1];
        }
        [categoryArray addObject:cats];
        
    }
         [_imagesTableView reloadData];
    }

   
}




-(void)createTableView
{
   // self.imagesTableView = [[UITableView alloc] initWithFrame: CGRectMake(0, 100, self.view.frame.size.width, self.view.frame.size.height) style:UITableViewStylePlain];
    //  self.edgesForExtendedLayout = UIRectEdgeAll;
    
    //self.imagesTableView.contentInset = UIEdgeInsetsMake(CGRectGetMaxY(self.tabBarController.tabBar.frame), 0, CGRectGetHeight(self.tabBarController.tabBar.frame), 0);
    
  //  if (IS_IPHONE_6) {
  //      _imagesTableView.frame = CGRectMake(0, 102, self.view.frame.size.width, self.view.frame.size.height-102);
  //  }

    
    self.imagesTableView.delegate=self;
    self.imagesTableView.dataSource=self;
    self.imagesTableView.backgroundColor=[UIColor blackColor];
    [_imagesTableView setDragDelegate:self refreshDatePermanentKey:@"FriendList"];
    _imagesTableView.showLoadMoreView = YES;
    _imagesTableView.showRefreshView=NO;
    self.imagesTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view insertSubview:self.imagesTableView atIndex:0];

    //[self.view addSubview:self.imagesTableView];
}


#pragma mark - tableview delegate methods ================= ================ ================ ================ ================

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        return  300;
    }
    else{
        return  190;
    }
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    NSInteger numOfSections = 0;
    if ([imagesArray count] > 0)
    {
      //  self.imagesTableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
        numOfSections                 = 1;
        self.imagesTableView.backgroundView = nil;
    }
    else
    {
        UILabel *noDataLabel         = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.imagesTableView.bounds.size.width, self.imagesTableView.bounds.size.height)];
        noDataLabel.text             = @"No free events available now.";
        noDataLabel.textColor        = [UIColor blackColor];
        noDataLabel.textAlignment    = NSTextAlignmentCenter;
        noDataLabel.font = [UIFont fontWithName:@"EuphemiaUCAS-Bold " size:16.0f];
        self.imagesTableView.backgroundView = noDataLabel;
        self.imagesTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    }
    return numOfSections;
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return imagesArray.count;
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        self.imagesTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        cell.layer.borderWidth = 5.0;
        cell.layer.borderColor = [UIColor colorWithRed:0.13 green:0.19 blue:0.25 alpha:1.0].CGColor;
        
        cell.imageView.image = nil;
        cell.textLabel.text = nil;
        
        UILabel *TitleTextLabel = [[UILabel alloc]initWithFrame:CGRectMake(cell.frame.origin.x+12, 50, cell.frame.size.width-20, 50)];
        TitleTextLabel.tag = 101;
        [cell.contentView addSubview:TitleTextLabel];
        
        UILabel *dateLabel = [[UILabel alloc]initWithFrame:CGRectMake(cell.frame.origin.x+35, 75, 300, 50)];
        dateLabel.tag = 102;
        [cell.contentView addSubview:dateLabel];
        
        UILabel *endDateLabel = [[UILabel alloc]initWithFrame:CGRectMake(cell.frame.origin.x+122, 75, 300, 50)];
        endDateLabel.tag = 109;
        [cell.contentView addSubview:endDateLabel];
        
        UILabel *startTimeLabel = [[UILabel alloc]initWithFrame:CGRectMake(cell.frame.origin.x+35, 95, 300, 50)];
        startTimeLabel.tag = 103;
        [cell.contentView addSubview:startTimeLabel];
        
        UILabel *timezoneLabel = [[UILabel alloc]initWithFrame:CGRectMake(cell.frame.origin.x+100, 95, cell.frame.size.width-20, 50)];
        timezoneLabel.tag = 108;
        [cell.contentView addSubview:timezoneLabel];
        
        UILabel *stateLabel = [[UILabel alloc]initWithFrame:CGRectMake(cell.frame.origin.x+35, 115, 283, 50)];
        stateLabel.tag = 104;
        [cell.contentView addSubview:stateLabel];
        
        
        if (IS_IPHONE_5)
        {
            rectSponsorImage =CGRectMake(cell.frame.size.width - 53, 5, 48, 48);
        }else{
            
            rectSponsorImage =CGRectMake(cell.frame.size.width - 2, 7, 50, 50);
        }
        UIImageView *sponsorImage = [[UIImageView alloc]initWithFrame:rectSponsorImage];
        sponsorImage.tag = 105;
        [cell.contentView addSubview:sponsorImage];
        
        if (IS_IPHONE_5)
        {
            rectCatText = CGRectMake(10, 155, 230,26);
            
        }else{
            rectCatText = CGRectMake(10, 155, 250,26);
        }
        UITextField *catField = [[UITextField alloc]initWithFrame:rectCatText];
        catField.tag = 1023;
        [cell.contentView addSubview:catField];
        
        
        if (IS_IPHONE_5)
        {
            rectText = CGRectMake(cell.frame.size.width - 50, 152, 60, 30);
        }else
        {
            rectText = CGRectMake(cell.frame.size.width - 20, 150, 80, 30);
        }
        UILabel *viewsLabel = [[UILabel alloc]initWithFrame:rectText];
        viewsLabel.tag = 106;
        [cell.contentView addSubview:viewsLabel];
        
        
        if (IS_IPHONE_5)
        {
            rectFavImage = CGRectMake(cell.frame.size.width - 60, 162, 23, 10);
        }else{
            rectFavImage = CGRectMake(cell.frame.size.width - 25, 160, 23, 10);
        }
        UIImageView *favImage = [[UIImageView alloc]initWithFrame:rectFavImage];
        favImage.tag = 1024;
        [cell.contentView addSubview:favImage];
        
        
        cell.tag = indexPath.row ;
        
     }
    
    //http://agiledevelopers.in/outnabout/assets/timthumb.php?src=http://agiledevelopers.in/outnabout/uploads/%@&h=320&w=500
    NSString *arrayString = [NSString stringWithFormat:(FULL_IMAGE @"%@"),[imagesArray objectAtIndex:indexPath.row]];
    
    NSURL *url =[NSURL URLWithString:arrayString];
    
    SDWebImageManager *manager = [SDWebImageManager sharedManager];
    [manager downloadImageWithURL:url
                          options:0
                         progress:^(NSInteger receivedSize, NSInteger  expectedSize) {
                             // progression tracking code
                         }
                        completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                            if (image) {
                                dispatch_async(dispatch_get_main_queue(), ^{
                                    UITableViewCell *updateCell = (id)[tableView cellForRowAtIndexPath:indexPath];
                                    if (updateCell)
                                        //  updateCell.imageView.image = image;
                                        cell.backgroundView = [[UIImageView alloc] initWithImage:image];
                                    
                                    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, cell.frame.size.width, cell.frame.size.height)];
                                    CAGradientLayer *gradient = [CAGradientLayer layer];
                                    gradient.frame = view.bounds;
                                    
                                    gradient.colors = [NSArray arrayWithObjects:(id)[[UIColor clearColor] CGColor], (id)[[UIColor colorWithRed:0.10 green:0.10 blue:0.10 alpha:1.0] CGColor], nil];
                                    [cell.backgroundView.layer insertSublayer:gradient atIndex:0];
                                });
                            }
                            
                             [SVProgressHUD dismiss];
                            [_imagesTableView finishLoadMore];
                           // [ALToastView toastInView:self.view withText:@"Events Updated"];
                        }
     ];
    
    
    UIImageView *favImage = (UIImageView *)[cell viewWithTag:1024];
    favImage.image=[UIImage imageNamed:@"ic_view@3x.png"];
    
    UIImageView *sponsorImage = (UIImageView *)[cell viewWithTag:105];
    NSString *eventType = [eventTypeArray objectAtIndex:indexPath.row];
    if ([eventType isEqualToString:@"1"]) {
        sponsorImage.image=[UIImage imageNamed:@"ic_sponsor@3x.png"];
    }else{
        sponsorImage.image=nil;
    }
    
    UIImageView *timeImage = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"ic_timer@3x.png"]];
    timeImage.frame = CGRectMake(12, 90, 15, 15);
    [cell.contentView addSubview:timeImage];
    
    UIImageView *timeImage1 = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"ic_timer@3x.png"]];
    timeImage1.frame = CGRectMake(12, 110, 15, 15);
    [cell.contentView addSubview:timeImage1];

    
    UIImageView *locationImage = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"ic_location@3x.png"]];
    locationImage.frame = CGRectMake(12, 130, 15, 18);
    [cell.contentView addSubview:locationImage];
    
    UILabel *timezoneLabel = (UILabel *)[cell viewWithTag:108];
    timezoneLabel.text = [NSString stringWithFormat:@"%@ ",timezoneString] ;
    [timezoneLabel setTextColor:[UIColor whiteColor]];
    timezoneLabel.numberOfLines = 0;
    timezoneLabel.backgroundColor = [UIColor clearColor];
    [timezoneLabel setFont:[UIFont fontWithName:@"Avenir-Medium" size:13.0f]];

    UILabel *TitleTextLabel = (UILabel *)[cell viewWithTag:101];
    TitleTextLabel.text = [NSString stringWithFormat:@"%@ ",[titleLabel objectAtIndex:indexPath.row]];
    [TitleTextLabel setTextColor:[UIColor whiteColor]];
    [TitleTextLabel setFont:[UIFont fontWithName:@"EuphemiaUCAS-Bold" size:17.0f]];
    
    NSString *startDateMatch = [dateArray objectAtIndex:indexPath.row];
    NSString *endDateMatch =  [endDateArray objectAtIndex:indexPath.row];
    UILabel *dateLabel = (UILabel *)[cell viewWithTag:102];
    UILabel *endDateLabel = (UILabel *)[cell viewWithTag:109];
    if ([startDateMatch isEqualToString:endDateMatch]) {
        endDateLabel.text = @"";
        dateLabel.text = [NSString stringWithFormat:@"%@ ",[dateArray objectAtIndex:indexPath.row]];
    }
    else
    {
        dateLabel.text = [NSString stringWithFormat:@"%@ -",[dateArray objectAtIndex:indexPath.row]];
        endDateLabel.text = [NSString stringWithFormat:@"%@",[endDateArray objectAtIndex:indexPath.row]];
    }
    [endDateLabel setTextColor:[UIColor whiteColor]];
    [endDateLabel setFont:[UIFont fontWithName:@"Avenir-Medium" size:13.0f]];
    [dateLabel setTextColor:[UIColor whiteColor]];
    [dateLabel setFont:[UIFont fontWithName:@"Avenir-Medium" size:13.0f]];
    UILabel *startTimeLabel = (UILabel *)[cell viewWithTag:103];
    startTimeLabel.text = [NSString stringWithFormat:@"%@ ",[startTimeArray objectAtIndex:indexPath.row]];
    [startTimeLabel setTextColor:[UIColor whiteColor]];
    [startTimeLabel setFont:[UIFont fontWithName:@"Avenir-Medium" size:13.0f]];
    
    UILabel *stateLabel = (UILabel *)[cell viewWithTag:104];
    stateLabel.text = [NSString stringWithFormat:@"%@ ",[stateArray objectAtIndex:indexPath.row]];
    [stateLabel setTextColor:[UIColor whiteColor]];
    [stateLabel setFont:[UIFont fontWithName:@"Avenir-Medium" size:13.0f]];
    
    UITextField *catField=(UITextField *)[cell viewWithTag:1023];
    //catField.text = [NSString stringWithFormat:@"%@  ",[categoryArray objectAtIndex:indexPath.row]];
    catField.borderStyle = UITextBorderStyleLine;
    catField.font= [UIFont systemFontOfSize:12.0f];
    catField.layer.sublayerTransform = CATransform3DMakeTranslation(5, 0, 0);
    catField.backgroundColor = [UIColor clearColor];
    catField.enabled=NO;
    [catField setAllowsEditingTextAttributes:NO];
    catField.textColor =[UIColor whiteColor];
    catField.layer.borderColor = [UIColor whiteColor].CGColor;
    catField.layer.borderWidth = 1.0f;
    
    NSString *name = [categoryArray objectAtIndex:indexPath.row];
    if (IS_IPHONE_6)
    {
        NSRange stringRange = {0, MIN([name length], 45)};
        catField.text = [name substringWithRange:stringRange];
    }
    else{
        NSRange stringRange = {0, MIN([name length], 38)};
        catField.text = [name substringWithRange:stringRange];
    }
    
    CGFloat width =  [catField.text sizeWithFont:catField.font].width;
    rectCatText = CGRectMake(10, 155, width+10,26);
    catField.frame=rectCatText;
    
    
    UILabel *viewsLabel = (UILabel *)[cell viewWithTag:106];
    viewsLabel.text = [NSString stringWithFormat:@"%@ ",[eventViewsArray objectAtIndex:indexPath.row]];
    [viewsLabel setTextColor:[UIColor whiteColor]];
    viewsLabel.textAlignment = NSTextAlignmentCenter;
    viewsLabel.backgroundColor = [UIColor clearColor];
    [viewsLabel setFont:[UIFont fontWithName:@"Avenir-Medium" size:15.0f]];
    
    
//    UIView *_lineDiv=[[UIView alloc] init];
//    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
//    {
//        _lineDiv.frame=CGRectMake(0, 250.0f, self.view.frame.size.width, 5.0);
//    }else{
//        _lineDiv.frame=CGRectMake(0, 190.0f, self.view.frame.size.width, 1.0);
//        
//    }
//    _lineDiv.backgroundColor=[UIColor blackColor];
//    //_lineDiv.backgroundColor=[UIColor colorWithRed:221.0f/255.0 green:223.0f/255.0 blue:224.0f/255.0 alpha:1.0];
//    [cell addSubview:_lineDiv];
    
    return cell;
    
}



-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UIStoryboard *story = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    SingleEventVC *singleEvent = [story instantiateViewControllerWithIdentifier:@"singleView"];
    singleEvent.singleTitle =[titleLabel objectAtIndex:indexPath.row];
    singleEvent.singleCategoryString =[categoryArray objectAtIndex:indexPath.row];
    singleEvent.costString =[costArray objectAtIndex:indexPath.row];
    singleEvent.descriptionString =[descriptionArray objectAtIndex:indexPath.row];
    singleEvent.favoriteString =[favoriteArray objectAtIndex:indexPath.row];
    singleEvent.shareLinkString = [shareArray objectAtIndex:indexPath.row];
    singleEvent.eventLatitude = [latitudeArray objectAtIndex:indexPath.row];
    singleEvent.eventLongitude = [longitudeArray objectAtIndex:indexPath.row];
    singleEvent.eventState = [stateArray objectAtIndex:indexPath.row];
    singleEvent.currencyString = [currencyArray objectAtIndex:indexPath.row];
    singleEvent.eventIdString =[eventIdArray objectAtIndex:indexPath.row];
    singleEvent.venueString =[venueArray objectAtIndex:indexPath.row];
    singleEvent.endDateOrTimeString =[NSString stringWithFormat:@" End Date: %@ (%@)",[endDateArray objectAtIndex:indexPath.row],[endTimeArray objectAtIndex:indexPath.row]];
    
    singleEvent.startDateOrTimeString =[NSString stringWithFormat:@" Start Date: %@ (%@)",[dateArray objectAtIndex:indexPath.row],[startTimeArray objectAtIndex:indexPath.row]];
    
    singleEvent.singleImageString =[NSString stringWithFormat:(FULL_IMAGE @"%@"),[imagesArray objectAtIndex:indexPath.row]];
    
    
    [self.navigationController pushViewController:singleEvent animated:YES];
    
    
    // [NSThread detachNewThreadSelector:@selector(threadStartAnimating:) toTarget:self withObject:nil];
    
}




-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    CAGradientLayer *grad = [CAGradientLayer layer];
    grad.frame = CGRectMake(0, cell.contentView.frame.size.height/2 - 24, cell.frame.size.width, 120);
    grad.colors = [NSArray arrayWithObjects:(id)[[UIColor clearColor] CGColor], (id)[[UIColor blackColor] CGColor],nil];
    
    [cell setBackgroundView:[[UIView alloc] init]];
    [cell.backgroundView.layer insertSublayer:grad atIndex:0];
    
}



#pragma mark - Drag delegate methods
- (void)dragTableDidTriggerLoadMore:(UITableView *)tableView
{
    //send load more request(generally network request) here
    
    [self performSelector:@selector(finishLoadMore) withObject:nil afterDelay:2];
}

- (void)dragTableLoadMoreCanceled:(UITableView *)tableView
{
    //cancel load more request(generally network request) here
    
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(finishLoadMore) object:nil];
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
    
    
}



@end
