//
//  NearByEventsVC.h
//  OutNAbout
//
//  Created by Ram Kumar on 11/10/16.
//  Copyright © 2016 tecHindustan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NearByEventsVC : UIViewController


@property (weak, nonatomic) IBOutlet UITableView *imagesTableView;
@property (retain, nonatomic) NSURLConnection *connection;

@property (retain, nonatomic) NSMutableData *receivedData;

@property (retain, nonatomic) NSString *loginTypeReceive;

@property (weak, nonatomic) IBOutlet UILabel *eventCountLabel;
@property (weak, nonatomic) IBOutlet UIView *dimCountBackground;
@end
