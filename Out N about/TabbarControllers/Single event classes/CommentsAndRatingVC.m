//
//  CommentsAndRatingVC.m
//  OutNAbout
//
//  Created by Ram Kumar on 18/11/16.
//  Copyright © 2016 tecHindustan. All rights reserved.
//

#import "CommentsAndRatingVC.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import <SystemConfiguration/SystemConfiguration.h>
#import "SDWebImageManager.h"
#import "PGDrawerTransition.h"
#import "AppDelegate.h"
#import "NavigationViewController.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import "CommonAPI's.pch"

#import "SVProgressHUD.h"

@interface CommentsAndRatingVC ()<UITableViewDelegate,UITableViewDataSource>

@end

@implementation CommentsAndRatingVC
@synthesize userCommentsArray,userNamesArray,userRatingArray,userImagesArray,userCommemtTimeArray;

- (void)viewDidLoad {
    [super viewDidLoad];
    
  
    userCommentsArray = [[NSMutableArray alloc]init];
    userNamesArray= [[NSMutableArray alloc]init];
    userRatingArray= [[NSMutableArray alloc]init];
    
    UIButton *button =  [UIButton buttonWithType:UIButtonTypeSystem];
    [button setImage:[UIImage imageNamed:@"ic_arrow@3x.png"] forState:UIControlStateNormal];
    [button addTarget:self action:@selector(handleBack:)forControlEvents:UIControlEventTouchUpInside];
    [button setFrame:CGRectMake(0, 0, 53, 31)];
    [button setImageEdgeInsets:UIEdgeInsetsMake(0, -30, 0, 0)];
    button.backgroundColor = [UIColor clearColor];
    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(30, 5, 240, 20)];
    [label setFont:[UIFont fontWithName:@"Arial-BoldMT" size:17]];
    [label setText:@"Comments And Ratings"];
    label.textAlignment = NSTextAlignmentLeft;
    [label setTextColor:[UIColor whiteColor]];
    [label setBackgroundColor:[UIColor clearColor]];
    [button addSubview:label];
    UIBarButtonItem *barButton = [[UIBarButtonItem alloc] initWithCustomView:button];
    self.navigationItem.leftBarButtonItem = barButton;
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    
    self.navigationItem.hidesBackButton = YES;
    self.view.backgroundColor = [UIColor whiteColor];
    
    
    NSLog(@"comment %@  user %@", _userCommentsString,_userNamesString);
    userCommentsArray = [_userCommentsString mutableCopy];
    userNamesArray= [_userNamesString mutableCopy];
    userRatingArray = [_userRatingsString mutableCopy];
    userImagesArray= [_userImagesString mutableCopy];
    userCommemtTimeArray= [_userCommemtTimeString mutableCopy];
    NSLog(@"comments***8 %@  username**** %@", userCommentsArray,userNamesArray);

      [self createTableView];
    
    [_reviewsTableView reloadData];
}



- (void) handleBack:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)rateView:(RateView *)rateView ratingDidChange:(float)rating {
}




# pragma mark - create comments table view  =  = === = == = = = = == = == = = == = = == = = == = == 


-(void)createTableView
{
    
    self.reviewsTableView.delegate=self;
    self.reviewsTableView.dataSource=self;
    self.reviewsTableView.backgroundColor=[UIColor whiteColor];
    
    //self.reviewsTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    //self.imagesTableView.separatorInset = UIEdgeInsetsMake(10, 10, 10, 10);
    [self.view addSubview:_reviewsTableView];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        return  60;
    }
    else{
        return  80;
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    NSInteger numOfSections = 0;
    if ([userCommentsArray count] > 0)
    {
        self.reviewsTableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
        numOfSections                 = 1;
        self.reviewsTableView.backgroundView = nil;
    }
    else
    {
        UILabel *noDataLabel         = [[UILabel alloc] initWithFrame:CGRectMake(0,0, self.reviewsTableView.bounds.size.width, self.reviewsTableView.bounds.size.height)];
        noDataLabel.text             = @"No comments.";
        noDataLabel.textColor        = [UIColor blackColor];
         noDataLabel.textAlignment    = NSTextAlignmentCenter;
        [noDataLabel setNumberOfLines:0];
        [noDataLabel sizeToFit];
        noDataLabel.font = [UIFont fontWithName:@"EuphemiaUCAS-Bold " size:16.0f];
        self.reviewsTableView.backgroundView = noDataLabel;
        self.reviewsTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    }
    return numOfSections;
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return userCommentsArray.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil)
    {
        
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
        
        //cell.selectionStyle = UITableViewCellSelectionStyleNone;
        self.reviewsTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        
        
        RateView *ratingView = [[RateView alloc]initWithFrame:CGRectMake(cell.frame.origin.x+80, 38, 80,12)];
        ratingView.tag = 100;
        [cell.contentView addSubview:ratingView];
        
        
        UILabel *userNameLabel = [[UILabel alloc]initWithFrame:CGRectMake(cell.frame.origin.x+82, 0, cell.frame.size.width-20, 50)];
        userNameLabel.tag = 101;
        [cell.contentView addSubview:userNameLabel];
    
        UILabel *commentsLabel = [[UILabel alloc]initWithFrame:CGRectMake(cell.frame.origin.x+80, 40, cell.frame.size.width-90, 50)];
        commentsLabel.tag = 102;
        [cell.contentView addSubview:commentsLabel];
        
        UIImageView *userImageView = [[UIImageView alloc]initWithFrame:CGRectMake(cell.frame.origin.x+10, 15, 60, 60)];
        userImageView.tag = 103;
        [cell.contentView addSubview:userImageView];
        
        UILabel *createdTimeLabel = [[UILabel alloc]initWithFrame:CGRectMake(cell.frame.origin.x+170, 38, 100, 13)];
        createdTimeLabel.tag = 104;
        [cell.contentView addSubview:createdTimeLabel];

    }
    
    
    
    
    
    
    
    NSString *arrayString = [NSString stringWithFormat:(FULL_IMAGE @"%@"),[userImagesArray objectAtIndex:indexPath.row]];
    
    NSURL *url =[NSURL URLWithString:arrayString];
    
    SDWebImageManager *manager = [SDWebImageManager sharedManager];
    [manager downloadImageWithURL:url
                          options:0
                         progress:^(NSInteger receivedSize, NSInteger  expectedSize) {
                             // progression tracking code
                         }
                        completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                            if (image) {
                                dispatch_async(dispatch_get_main_queue(), ^{
                                    UITableViewCell *updateCell = (id)[tableView cellForRowAtIndexPath:indexPath];
                                    if (updateCell){
                                        //  updateCell.imageView.image = image;
                 
                                    
                                    UIImageView *userImageView = (UIImageView *)[cell viewWithTag:103];
                                    userImageView.layer.cornerRadius =userImageView.frame.size.width / 2;
                                    userImageView.layer.borderWidth=1;
                                    userImageView.layer.borderColor = [UIColor blackColor].CGColor;
                                    userImageView.clipsToBounds = YES;
                                    [userImageView setImage:image];
                                        
                                    }
                                    
                                });
                            }
                            
                            // [loadingView setHidden:YES];
                            [SVProgressHUD dismiss];
                        }
     ];

    
    
    
    
    
    
    
    
    
    
    
    float rates = [[userRatingArray objectAtIndex:indexPath.row] floatValue];
    RateView *ratingView = (RateView *)[cell viewWithTag:100];
    ratingView.notSelectedImage = [UIImage imageNamed:@"ic_star_Redblank@3x.png"];
    ratingView.halfSelectedImage = [UIImage imageNamed:@"ic_star_Redhalf@3x.png"];
    ratingView.fullSelectedImage = [UIImage imageNamed:@"ic_star_Redfill@3x.png"];
    ratingView.rating = rates;
    ratingView.editable = YES;
    ratingView.maxRating = 5;
    ratingView.delegate = self;
    
    UILabel *userNameLabel = (UILabel *)[cell viewWithTag:101];
    userNameLabel.text = [NSString stringWithFormat:@"%@ ",[userNamesArray objectAtIndex:indexPath.row]];
    [userNameLabel setTextColor:[UIColor blackColor]];
   //  userNameLabel.backgroundColor = [UIColor greenColor];
    [userNameLabel setFont:[UIFont fontWithName:@"EuphemiaUCAS-Bold" size:17.0f]];
    
    UILabel *createdTimeLabel = (UILabel *)[cell viewWithTag:104];
    createdTimeLabel.text = [NSString stringWithFormat:@"%@ ",[userCommemtTimeArray objectAtIndex:indexPath.row]];
    [createdTimeLabel setTextColor:[UIColor grayColor]];
    [createdTimeLabel setFont:[UIFont fontWithName:@"Avenir-Book" size:10.0f]];

    
    UILabel *commentsLabel = (UILabel *)[cell viewWithTag:102];
    commentsLabel.text = [NSString stringWithFormat:@"%@ ",[userCommentsArray objectAtIndex:indexPath.row]];
    [commentsLabel setTextColor:[UIColor blackColor]];
  //  commentsLabel.backgroundColor = [UIColor redColor];
    [commentsLabel setFont:[UIFont fontWithName:@"Avenir-Book" size:13.0f]];
    

    
    
//    cell.textLabel.text =  [NSString stringWithFormat:@"%@",[userCommentsArray objectAtIndex:indexPath.row]];
//    cell.detailTextLabel.text =[NSString stringWithFormat:@"%@",[userNamesArray objectAtIndex:indexPath.row]];
    
 
    
    return cell;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}






- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
    
}



@end
