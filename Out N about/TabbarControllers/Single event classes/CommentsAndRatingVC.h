//
//  CommentsAndRatingVC.h
//  OutNAbout
//
//  Created by Ram Kumar on 18/11/16.
//  Copyright © 2016 tecHindustan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RateView.h"

@interface CommentsAndRatingVC : UIViewController<RateViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *reviewsTableView;

@property (nonatomic,strong) NSMutableArray *userCommentsArray;

@property (nonatomic,strong) NSMutableArray *userNamesArray;
@property (nonatomic,strong) NSMutableArray *userRatingArray;
@property (nonatomic,strong) NSMutableArray *userImagesArray;
@property (nonatomic,strong) NSMutableArray *userCommemtTimeArray;

@property (nonatomic,strong) NSString *userCommentsString;
@property (nonatomic,strong) NSString *userCommemtTimeString;
@property (nonatomic,strong) NSString *userImagesString;
@property (nonatomic,strong) NSString *userNamesString;
@property (nonatomic,strong) NSString *userRatingsString;

@end
