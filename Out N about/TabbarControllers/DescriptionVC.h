//
//  DescriptionVC.h
//  OutNAbout
//
//  Created by Ram Kumar on 03/11/16.
//  Copyright © 2016 tecHindustan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DescriptionVC : UIViewController
@property (weak, nonatomic) IBOutlet UITextView *descriptionTextView;
@property(nonatomic,strong) NSString *descriptionText;

@end
