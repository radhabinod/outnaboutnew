//
//  DescriptionVC.m
//  OutNAbout
//
//  Created by Ram Kumar on 03/11/16.
//  Copyright © 2016 tecHindustan. All rights reserved.
//

#import "DescriptionVC.h"

@interface DescriptionVC ()

@end

@implementation DescriptionVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIButton *button =  [UIButton buttonWithType:UIButtonTypeSystem];
    [button setImage:[UIImage imageNamed:@"ic_arrow@3x.png"] forState:UIControlStateNormal];
    [button addTarget:self action:@selector(handleBack:)forControlEvents:UIControlEventTouchUpInside];
    [button setFrame:CGRectMake(0, 0, 53, 31)];
    [button setImageEdgeInsets:UIEdgeInsetsMake(0, -30, 0, 0)];
    button.backgroundColor = [UIColor clearColor];
    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(30, 5, 240, 20)];
    [label setFont:[UIFont fontWithName:@"Arial-BoldMT" size:17]];
    [label setText:@"Description"];
    label.textAlignment = NSTextAlignmentLeft;
    [label setTextColor:[UIColor whiteColor]];
    [label setBackgroundColor:[UIColor clearColor]];
    [button addSubview:label];
    
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    
    
    UIBarButtonItem *barButton = [[UIBarButtonItem alloc] initWithCustomView:button];
    self.navigationItem.leftBarButtonItem = barButton;
    
    self.navigationItem.hidesBackButton = YES;
    
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor whiteColor]};
    
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:0.95 green:0.28 blue:0.31 alpha:1.0];
    
    _descriptionTextView.text = _descriptionText;
    _descriptionTextView.textColor = [UIColor blackColor];
    _descriptionTextView.editable=NO;
    _descriptionTextView.font = [UIFont systemFontOfSize:13.0f];
}



- (void) handleBack:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}



@end
