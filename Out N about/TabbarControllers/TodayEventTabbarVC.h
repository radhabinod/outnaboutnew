//
//  TodayEventTabbarVC.h
//  OutNAbout
//
//  Created by Ram Kumar on 03/08/16.
//  Copyright © 2016 tecHindustan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TodayEventTabbarVC : UIViewController

@property (weak, nonatomic) IBOutlet UITableView *imagesTableView;

@property (retain, nonatomic) NSURLConnection *connection;

@property (retain, nonatomic) NSMutableData *receivedData;

@property (weak, nonatomic) IBOutlet UILabel *eventCountLabel;
@property (weak, nonatomic) IBOutlet UIView *dimCountBackground;
@end
