//
//  SingleEventVC.m
//  OutNAbout
//
//  Created by Ram Kumar on 22/06/16.
//  Copyright © 2016 tecHindustan. All rights reserved.
//

#import "SingleEventVC.h"
#import "BIZPopupViewController.h"
#import "PopUpVC.h"
#import "AppDelegate.h"
#import "SVProgressHUD.h"
#import "SDWebImageManager.h"
#import "AboutScreenVC.h"
#import "CommonAPI's.pch"
#import "DisplayMap.h"
#import "DescriptionVC.h"
#import "CommentsAndRatingVC.h"

#import <EventKitUI/EventKitUI.h>
#import <EventKit/EventKit.h>
//#import "EKEventEditViewController.h"

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wdeprecated-declarations"

@interface SingleEventVC ()<MKMapViewDelegate,CLLocationManagerDelegate,UIActionSheetDelegate,EKEventEditViewDelegate>

@property (nonatomic, strong) CLLocation* currentLocation;
@property (weak, nonatomic) IBOutlet UIView *bottomView;
@property (weak, nonatomic) IBOutlet UIButton *rsvpButton;

@end

@implementation SingleEventVC

{
    CLLocationManager *locationManager;
    CLGeocoder *geocoder;
    CLPlacemark *placemark;
    MKPointAnnotation *annotation;
    BOOL checkButtonSelected;
     NSString *access_token;
    int favButtonTapType;
    NSString *savedEventId;
    
    NSInteger rsvpNumber;
    NSString *rsvpTypeCheck;
    NSString *rsvpStatusNo;
    NSString *ticketLink;
    NSString *is_Paid;
    NSString *costOffer;
    NSString *userComment;
    NSString *userRating;
    
    NSString *message;
    NSString *OnCommentsType;
    NSMutableArray *user_nameArray;
    NSMutableArray *user_CommentsArray;
     NSMutableArray *user_RatingArray;
     NSMutableArray *user_CommentTimeArray;
    NSMutableArray *user_ImageArray;
    UILabel *userNameLabel;
    UITextView *userCommentSection;
    
    UIButton *infoButton;
    NSString *latitude;
    NSString *longitude;
   // CLLocation *CurrentLocation;
}

@synthesize imageView,scrollView,mapView,singleTitle,singleImageString,singleCategoryString,startDateOrTimeString,endDateOrTimeString,venueString,costString,descriptionString,viewsString,favoriteString,eventIdString,commentsString,favsString,userNameString,userCommentString,shareLinkString,currencyString;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    

    user_nameArray = [[NSMutableArray alloc]init];
    user_CommentsArray = [[NSMutableArray alloc]init];
    user_RatingArray= [[NSMutableArray alloc]init];
    user_ImageArray= [[NSMutableArray alloc]init];
    user_CommentTimeArray= [[NSMutableArray alloc]init];
    
    favButtonTapType=0;
    rsvpTypeCheck=nil;
    OnCommentsType=@"";
    _ticketButton.hidden=YES;
    
    _favButton.imageEdgeInsets = UIEdgeInsetsMake(5, 5, 5, 5);
    _favButton.contentMode = UIViewContentModeScaleToFill;

    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    access_token = [defaults objectForKey:@"access_token"];
    
    
    UIButton *button =  [UIButton buttonWithType:UIButtonTypeSystem];
    [button setImage:[UIImage imageNamed:@"ic_arrow@3x.png"] forState:UIControlStateNormal];
    [button addTarget:self action:@selector(handleBack:)forControlEvents:UIControlEventTouchUpInside];
    [button setFrame:CGRectMake(0, 0, 53, 31)];
    [button setImageEdgeInsets:UIEdgeInsetsMake(0, -30, 0, 0)];
    button.backgroundColor = [UIColor clearColor];
    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(30, 5, 240, 20)];
    [label setFont:[UIFont fontWithName:@"Arial-BoldMT" size:17]];
    [label setText:singleTitle];
    label.textAlignment = UITextAlignmentLeft;
    [label setTextColor:[UIColor whiteColor]];
    [label setBackgroundColor:[UIColor clearColor]];
    [button addSubview:label];
    UIBarButtonItem *barButton = [[UIBarButtonItem alloc] initWithCustomView:button];
    self.navigationItem.leftBarButtonItem = barButton;
    
    self.navigationItem.hidesBackButton = YES;
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];

    
    UIBarButtonItem *shareButton = [[UIBarButtonItem alloc]
                                     initWithImage:[UIImage imageNamed:@"ic_share@3x.png"]
                                     style:UIBarButtonItemStyleBordered
                                     target:self
                                     action:@selector(shareEvent)];
    shareButton.tintColor = [UIColor whiteColor];
    self.navigationItem.rightBarButtonItem = shareButton;
   
    _singleTitleLabel.text = singleTitle;
    _singleTitleLabel.textColor = [UIColor whiteColor];
    _singleTitleLabel.font = [UIFont fontWithName:@"Avenir-Medium" size:22.0f];
    
    _singleCatField.text = [NSString stringWithFormat:@"%@",singleCategoryString];
    _singleCatField.borderStyle = UITextBorderStyleLine;
    _singleCatField.font= [UIFont systemFontOfSize:12.0f];
    _singleCatField.layer.sublayerTransform = CATransform3DMakeTranslation(5, 0, 0);
    _singleCatField.backgroundColor = [UIColor colorWithRed:0.12 green:0.10 blue:0.27 alpha:1.0];
    _singleCatField.enabled=NO;
    [_singleCatField setAllowsEditingTextAttributes:NO];
    _singleCatField.textColor =[UIColor whiteColor];
//    _singleCatField.layer.borderColor = [UIColor whiteColor].CGColor;
//    _singleCatField.layer.borderWidth = 1.0f;
    
    _startDateOrTime.text = startDateOrTimeString;
    _startDateOrTime.textColor = [UIColor blackColor];
    _startDateOrTime.font = [UIFont systemFontOfSize:13.0f];
        
    _endDateOrTime.text = endDateOrTimeString;
    _endDateOrTime.textColor = [UIColor blackColor];
    _endDateOrTime.font = [UIFont systemFontOfSize:13.0f];
    
    
    
//    NSTextAttachment *attachment = [[NSTextAttachment alloc] init];
//    attachment.image = [UIImage imageNamed:@"ic_location@3x.png"];
//
//    NSAttributedString *attachmentString = [NSAttributedString attributedStringWithAttachment:attachment];
//    NSMutableAttributedString *myString= [[NSMutableAttributedString alloc] initWithString:venueString];
//
//    [myString replaceCharactersInRange:NSMakeRange(0, 0) withAttributedString:attachmentString];
//
//     //  [myString appendAttributedString:attachmentString];
//    _venueLabel.attributedText = myString;
//    
//
//    //_venueLabel.text = venueString;
//    [_venueLabel setTextAlignment:NSTextAlignmentLeft];
//
//    _venueLabel.textColor = [UIColor whiteColor];
//    _venueLabel.font = [UIFont systemFontOfSize:14.0f];
    
    
    UIImageView *venueImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"ic_location@3x.png"]];
    venueImage.frame = CGRectMake(0.0, 0.0, venueImage.image.size.width+35.0, venueImage.image.size.height);
    venueImage.contentMode = UIViewContentModeCenter;
    _venueTF.leftView = venueImage;
    _venueTF.leftViewMode = UITextFieldViewModeAlways;
    
    _venueTF.text = [NSString stringWithFormat:@"%@",venueString];
    _venueTF.borderStyle = UITextBorderStyleLine;
    _venueTF.font= [UIFont systemFontOfSize:12.0f];
   // _venueTF.layer.sublayerTransform = CATransform3DMakeTranslation(5, 0, 0);
    _venueTF.backgroundColor = [UIColor colorWithRed:0.12 green:0.10 blue:0.27 alpha:1.0];
    _venueTF.enabled=NO;
    [_venueTF setAllowsEditingTextAttributes:NO];
    _venueTF.textColor =[UIColor whiteColor];


    
    NSScanner *myScanner;
    NSString *text = nil;
    myScanner = [NSScanner scannerWithString:descriptionString];
    
    while ([myScanner isAtEnd] == NO) {
        
        [myScanner scanUpToString:@"<" intoString:NULL] ;
        
        [myScanner scanUpToString:@">" intoString:&text] ;
        
        descriptionString = [descriptionString stringByReplacingOccurrencesOfString:[NSString stringWithFormat:@"%@>", text] withString:@""];
    }
    
    descriptionString = [descriptionString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    _descriptionView.text = descriptionString;
    _descriptionView.textColor = [UIColor blackColor];
    _descriptionView.editable=NO;
    _descriptionView.scrollEnabled = NO;
    _descriptionView.font = [UIFont systemFontOfSize:13.0f];
    
//    CAGradientLayer *grad = [CAGradientLayer layer];
//    grad.frame = CGRectMake(0, _descriptionView.frame.size.height/2 , _descriptionView.frame.size.width, 100);
//    grad.colors = [NSArray arrayWithObjects:(id)[[UIColor clearColor] CGColor], (id)[[UIColor whiteColor] CGColor],nil];
//    
//    [_descriptionView.layer insertSublayer:grad atIndex:1];
    
    [scrollView insertSubview:_descriptionView atIndex:0];

    
    _descriptionViewOnPopup.text = descriptionString;
    _descriptionViewOnPopup.textColor = [UIColor blackColor];
    _descriptionViewOnPopup.editable=NO;
    _descriptionViewOnPopup.font = [UIFont systemFontOfSize:13.0f];


    //CGSize scrollableSize = CGSizeMake(scrollView.frame.size.width, 680);
  //  [scrollView setContentSize:scrollableSize];
  //  [scrollView setAlwaysBounceHorizontal:NO];
//    
//    if (IS_IPHONE_5) {
//        scrollView.contentSize =CGSizeMake(320, 670);
//
//    }
//    else{
//        scrollView.contentSize =CGSizeMake(320, 820);
//    }
    
    
    CGFloat scrollViewHeight = 0.0f;
    for (UIView* view in scrollView.subviews)
    {
        scrollViewHeight += view.frame.size.height-5;
    }
    
    [scrollView setContentSize:(CGSizeMake(320, scrollViewHeight))];
    scrollView.bounces = NO;
    self.navigationController.navigationBar.translucent = NO;
    
    
    [self.view addSubview:_dimBackgroundView];
    [_dimBackgroundView addSubview:_popupView];
    [_popupView.layer setCornerRadius:5.0];
    [_dimBackgroundView setHidden:YES];
    [_dimBackgroundView setBackgroundColor:[[UIColor blackColor] colorWithAlphaComponent:0.6]];
    

    [self setImageOnScreenWithAnimationAndShade];
    [self getCurrentLocation];
    [self createMapOnScreen];
    [self postMethodForSingleEventDetail];

    
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(receiveToggleAuthUINotifications:)
     name:@"RefreshSingleEventView"
     object:nil];
    
}



- (void) receiveToggleAuthUINotifications:(NSNotification *) notification {
    if ([[notification name] isEqualToString:@"RefreshSingleEventView"]) {
        
        
        
        message = [notification userInfo][@"message"];
        if ([notification userInfo][@"message"])
        {
            OnCommentsType=@"comments";
            [self postMethodForSingleEventDetail];
            
           
        }
        else  if ([notification userInfo][@"FavRefresh"])
        {
            favButtonTapType=0;
              [self postMethodForSingleEventDetail];
        }
        
        
        
       // [self viewWillAppear:YES];
       // [self viewDidLoad];
        
            }
    
}


- (CGSize)calculateHeightForString:(NSString *)str
{
    CGSize size = CGSizeZero;
    
    UIFont *labelFont = [UIFont systemFontOfSize:17.0f];
    NSDictionary *systemFontAttrDict = [NSDictionary dictionaryWithObject:labelFont forKey:NSFontAttributeName];
    
    NSMutableAttributedString *message = [[NSMutableAttributedString alloc] initWithString:str attributes:systemFontAttrDict];
    CGRect rect = [message boundingRectWithSize:(CGSize){320, MAXFLOAT}
                                        options:NSStringDrawingUsesLineFragmentOrigin
                                        context:nil];//you need to specify the some width, height will be calculated
    size = CGSizeMake(rect.size.width, rect.size.height + 5); //padding
    
    return size;
    
    
}


- (void) handleBack:(id)sender
{
    NSLog(@"back +++");
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:@"NowBack" forKey:@"backToView"];
    [defaults synchronize];
    
    [self.navigationController popViewControllerAnimated:YES];
    
    
    
}








-(void)setImageOnScreenWithAnimationAndShade
{
    //  UIImage * img = [UIImage imageWithData:[NSData dataWithContentsOfURL:     [NSURL URLWithString:singleImageString]]];
    // [imageView setImage:img];
    
    NSURL *url =[NSURL URLWithString:singleImageString];
    SDWebImageManager *manager = [SDWebImageManager sharedManager];
    [manager downloadImageWithURL:url
                          options:0
                         progress:^(NSInteger receivedSize, NSInteger  expectedSize) {
                             // progression tracking code
                         }
                        completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                            if (image) {
                                dispatch_async(dispatch_get_main_queue(), ^{
                                    
                                    
                                    imageView.alpha = 0.0;
                                    [UIView transitionWithView:imageView
                                                      duration:0.4
                                                       options:UIViewAnimationOptionTransitionCrossDissolve
                                                    animations:^{
                                                        
                                                        [imageView setImage:image];
                                                        imageView.alpha = 1.0;
                                                    } completion:NULL];
                                    
                                    
                                CAGradientLayer *grad = [CAGradientLayer layer];
                                    
                                if (IS_IPHONE_5) {
                                grad.frame = CGRectMake(0, imageView.frame.size.height/2 , imageView.frame.size.width, 95);

                                    }
                                else
                                {
                                     grad.frame = CGRectMake(0, imageView.frame.size.height/2 , imageView.frame.size.width, 116);
                                 }
                                    grad.colors = [NSArray arrayWithObjects:(id)[[UIColor clearColor] CGColor], (id)[[UIColor blackColor] CGColor],nil];
                                    
                                    [imageView.layer insertSublayer:grad atIndex:0];
                                    
                                    
                                });
                            }
                            
                            
                            else{
                                
                                CAGradientLayer *grad = [CAGradientLayer layer];
                                grad.frame = CGRectMake(0, imageView.frame.size.height/2 , imageView.frame.size.width, 116);
                                grad.colors = [NSArray arrayWithObjects:(id)[[UIColor clearColor] CGColor], (id)[[UIColor blackColor] CGColor],nil];
                                
                                [imageView.layer insertSublayer:grad atIndex:0];
                                
                            }
                        }
     ];

}







-(void)createMapOnScreen
{
   // mapView.showsUserLocation = YES;
    [mapView setZoomEnabled :YES];
    [mapView setScrollEnabled:YES];
    
    mapView.mapType = MKMapTypeStandard;
    mapView.delegate = self;
    [scrollView addSubview:mapView];
    
    
    NSString *latt = _eventLatitude;
    NSString *longgg = _eventLongitude;

    CLLocationCoordinate2D location;
    location.latitude=[latt doubleValue];
    location.longitude=[longgg doubleValue];
   // NSLog(@"lat %f long%f",location.latitude,location.longitude);
    
    
    MKCoordinateSpan spanObj;
    spanObj.latitudeDelta=0.2;
    spanObj.longitudeDelta=0.2;
    
    MKCoordinateRegion region;
    region.center=location;
    region.span=spanObj;
    
    [mapView setZoomEnabled:YES];
    [mapView setScrollEnabled:YES];
    [mapView setRegion:region animated:YES];
    [mapView regionThatFits:region];
    
//    Annotation *annotation=[[Annotation alloc] init];
//    annotation.title=self.dictProject[@"location"];
//    annotation.coordinate=region.center;
//    [mapView addAnnotation:annotation];
    
    
    
    DisplayMap *ann = [[DisplayMap alloc] init];
    ann.title=_eventState;
    ann.coordinate=region.center;
    [mapView addAnnotation:ann];
    

}


-(void)getCurrentLocation
{
    
    locationManager = [CLLocationManager new];
    locationManager.delegate = self;
    
    if ([locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)])
    {
        [locationManager requestWhenInUseAuthorization];
    }
    locationManager.desiredAccuracy = kCLLocationAccuracyKilometer;
    locationManager.activityType = CLActivityTypeFitness;
    locationManager.distanceFilter = 500;
    [locationManager startUpdatingLocation];

}



- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    _currentLocation = [locations objectAtIndex:0];
    [locationManager stopUpdatingLocation];
    geocoder = [[CLGeocoder alloc] init];

    CLLocationCoordinate2D coordinate = [_currentLocation coordinate];
    
    latitude = [NSString stringWithFormat:@"%f", coordinate.latitude];
    longitude = [NSString stringWithFormat:@"%f", coordinate.longitude];

    annotation = [[MKPointAnnotation alloc] init];
    [annotation setCoordinate:coordinate];
    
    
    //[NSThread sleepForTimeInterval:5.0];
    if (latitude.length && longitude.length) {
        
    }
    else
    {
        NSLog(@"Geocode failed with error");
        NSLog(@"\nCurrent Location Not Detected\n");
        [[[UIAlertView alloc]initWithTitle:@"OutNAbout" message:@"Can't Detect Your Location."  delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil]show];
        
    }
    
    NSLog(@"latitude :%@",latitude);
    NSLog(@"dLongitude : %@",longitude);
    
}






- (MKAnnotationView *)mapView:(MKMapView *)mV viewForAnnotation:(id <MKAnnotation>)annotations
{
    MKPinAnnotationView *pinAnnotation = nil;
    if(annotations != mapView.userLocation)
    {
        static NSString *defaultPinID = @"myPin";
        pinAnnotation.image = [UIImage imageNamed:@"map@3x.png"];
       // pinAnnotation.pinColor = MKPinAnnotationColorRed;
        pinAnnotation = (MKPinAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:defaultPinID];
        if ( pinAnnotation == nil )
            pinAnnotation = [[MKPinAnnotationView alloc] initWithAnnotation:annotations reuseIdentifier:defaultPinID] ;
        
        pinAnnotation.canShowCallout = YES;
        
        
        infoButton = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 25, 25)];
        [infoButton setBackgroundImage:[UIImage imageNamed:@"navigator@3x.png"] forState:UIControlStateNormal];
        pinAnnotation.rightCalloutAccessoryView = infoButton;
        [infoButton addTarget:self action:@selector(ToMakeARouteAction) forControlEvents:UIControlEventTouchUpInside];
    }
    return pinAnnotation;
}



-(void)ToMakeARouteAction
{
    NSURL *testURL = [NSURL URLWithString:@"comgooglemaps-x-callback://"];
    if ([[UIApplication sharedApplication] canOpenURL:testURL]) {
        
        NSString *direction=[NSString stringWithFormat:@"comgooglemaps-x-callback://?saddr=%0.4f,%0.4f&daddr=%0.4f,%0.4f&x-success=sourceapp://?resume=true&x-source=AirApp",[latitude floatValue],[longitude floatValue], [_eventLatitude floatValue],[_eventLongitude floatValue]];
        
        NSURL *directionsURL = [NSURL URLWithString:direction];
        [[UIApplication sharedApplication] openURL:directionsURL];
    }
    else {
    
    
    
    NSString *direction=[NSString stringWithFormat:@"https://www.google.co.in/maps/dir/%0.4f,%0.4f/%0.4f,%0.4f",[latitude floatValue],[longitude floatValue], [_eventLatitude floatValue],[_eventLongitude floatValue]];
    NSLog(@"directions %@",direction);
    
    NSURL* directionsURL = [[NSURL alloc] initWithString:[direction stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    [[UIApplication sharedApplication] openURL:directionsURL];
    
        
    }

}





- (IBAction)addEventToCalendarAction:(id)sender
{
    
 
    
    EKEventStore *eventSotre = [[EKEventStore alloc] init];
    
    EKEvent *event = [EKEvent eventWithEventStore:eventSotre];

    
    NSString *titlee =singleTitle;
    
    event.title= titlee;
    
    NSDate *duedate = [NSDate date];
    event.startDate =duedate;
    event.endDate= [[NSDate alloc] initWithTimeInterval:600 sinceDate:duedate];
    
    NSArray *arrAlarm = [NSArray arrayWithObject:[EKAlarm alarmWithAbsoluteDate:duedate]];
    event.alarms= arrAlarm;
    
    [event setCalendar:[eventSotre defaultCalendarForNewEvents]];
    NSError *err;
    BOOL isSuceess=[eventSotre saveEvent:event span:EKSpanThisEvent error:&err];
    
    if(isSuceess){
        
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"calshow://"]];
        [[UIApplication sharedApplication] openURL:url];
        
        UIAlertView *alertview = [[UIAlertView alloc] initWithTitle:@"OutNAbout" message:@"Event added in calendar." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alertview show];
    }
    else{
        UIAlertView *alertview = [[UIAlertView alloc] initWithTitle:@"OutNAbout" message:[err description] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alertview show];
    }
    
    

    

//    EKEventStore *store = [[EKEventStore alloc] init];
//    
//    if([store respondsToSelector:@selector(requestAccessToEntityType:completion:)])
//    {
//        // iOS 6
//        [store requestAccessToEntityType:EKEntityTypeEvent
//                              completion:^(BOOL granted, NSError *error) {
//                                  if (granted)
//                                  {
//                                      dispatch_async(dispatch_get_main_queue(), ^{
//                                          [self createEventAndPresentViewController:store];
//                                      });
//                                  }
//                              }];
//    } else
//    {
//        // iOS 5
//        [self createEventAndPresentViewController:store];
//    }
    
    

  
}




//- (void)createEventAndPresentViewController:(EKEventStore *)store
//{
//    EKEvent *event = [self findOrCreateEvent:store];
//    
//    EKEventEditViewController *controller = [[EKEventEditViewController alloc] init];
//    controller.event = event;
//    controller.eventStore = store;
//    controller.editViewDelegate = self;
//    
//    [self presentViewController:controller animated:YES completion:nil];
//}
//
//- (void)eventEditViewController:(EKEventEditViewController *)controller didCompleteWithAction:(EKEventEditViewAction)action
//{
//    [self dismissViewControllerAnimated:YES completion:nil];
//}
//
//
//
//
//- (EKEvent *)findOrCreateEvent:(EKEventStore *)store
//{
//    NSString *title = @"My event title";
//    
//    // try to find an event
//    
//    EKEvent *event = [self findEventWithTitle:title inEventStore:store];
//    
//    // if found, use it
//    
//    if (event)
//        return event;
//    
//    // if not, let's create new event
//    
//    event = [EKEvent eventWithEventStore:store];
//    
//    event.title = title;
//    
//    
//    event.URL = [NSURL URLWithString:@""];
//    event.alarms = @[@""];
//    event.recurrenceRules = @[@""];
//    event.location = @"";
//    
//    event.notes = @"My event notes";
//    event.location = @"My event location";
//    event.calendar = [store defaultCalendarForNewEvents];
//    
//    NSCalendar *calendar = [NSCalendar currentCalendar];
//    NSDateComponents *components = [[NSDateComponents alloc] init];
//    components.hour = 4;
//    event.startDate = [calendar dateByAddingComponents:components
//                                                toDate:[NSDate date]
//                                               options:0];
//    components.hour = 1;
//    event.endDate = [calendar dateByAddingComponents:components
//                                              toDate:event.startDate
//                                             options:0];
//    
//    return event;
//}
//
//- (EKEvent *)findEventWithTitle:(NSString *)title inEventStore:(EKEventStore *)store
//{
//    // Get the appropriate calendar
//    NSCalendar *calendar = [NSCalendar currentCalendar];
//    
//    // Create the start range date components
//    NSDateComponents *oneDayAgoComponents = [[NSDateComponents alloc] init];
//    oneDayAgoComponents.day = -1;
//    NSDate *oneDayAgo = [calendar dateByAddingComponents:oneDayAgoComponents
//                                                  toDate:[NSDate date]
//                                                 options:0];
//    
//    // Create the end range date components
//    NSDateComponents *oneWeekFromNowComponents = [[NSDateComponents alloc] init];
//    oneWeekFromNowComponents.day = 7;
//    NSDate *oneWeekFromNow = [calendar dateByAddingComponents:oneWeekFromNowComponents
//                                                       toDate:[NSDate date]
//                                                      options:0];
//    
//    // Create the predicate from the event store's instance method
//    NSPredicate *predicate = [store predicateForEventsWithStartDate:oneDayAgo
//                                                            endDate:oneWeekFromNow
//                                                          calendars:nil];
//    
//    // Fetch all events that match the predicate
//    NSArray *events = [store eventsMatchingPredicate:predicate];
//    
//    for (EKEvent *event in events)
//    {
//        if ([title isEqualToString:event.title])
//        {
//            return event;
//        }
//    }
//    
//    return nil;
//}







- (IBAction)actionTypeRSVP:(id)sender
{
    
    
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil
                                                             delegate:self
                                                    cancelButtonTitle:@"Cancel"
                                               destructiveButtonTitle:nil
                                                    otherButtonTitles:@"Yes", @"May be",@"No", nil];
    [actionSheet showInView:self.view];
    
    
}



-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    if (buttonIndex == 0)
    {
        
        rsvpNumber=1;
        rsvpTypeCheck=@"rsvp";
        [self postMethodForRSVP];
        [self viewDidLoad];
        [self viewWillAppear:YES];
    }
    
    else if (buttonIndex == 1)
    {
        rsvpNumber=2;
        rsvpTypeCheck=@"rsvp";

        [self postMethodForRSVP];
        [self viewDidLoad];
      [self viewWillAppear:YES];
    }
    
    else if (buttonIndex == 2)
    {
        rsvpNumber=0;
        rsvpTypeCheck=@"rsvp";

        [self postMethodForRSVP];
        [self viewDidLoad];
     [self viewWillAppear:YES];
    }
    
    
    
}




-(void)postMethodForRSVP
{
    [SVProgressHUD showWithStatus:@"Loading.." maskType:SVProgressHUDMaskTypeBlack];
    
    if (APP_DELEGATE.isServerReachable)
    {

    [self.connection cancel];
    
    //initialize new mutable data
    NSMutableData *data = [[NSMutableData alloc] init];
    self.receivedData = data;
    
    NSURL *url = [NSURL URLWithString:(API_URL @"updateRsvpStatus")];
    
    //initialize a request from url
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[url standardizedURL]];
    
    //set http method
    [request setHTTPMethod:@"POST"];
    //initialize a post data
    
    NSString *postData = [NSString stringWithFormat:@"event_id=%@ & app_token=%@ & rsvp=%ld ", eventIdString,access_token,(long)rsvpNumber];
    
    
    [request setValue:@"application/x-www-form-urlencoded; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    
    [request setHTTPBody:[postData dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    self.connection = connection;
    
    //start the connection
    [connection start];
        
    }
    else
    {
        [SVProgressHUD dismiss];
        rsvpNumber=3;
        rsvpTypeCheck=@"";
        [[[UIAlertView alloc]initWithTitle:@"OutNAbout" message:@"Internet connection is not available. Please try again."  delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil]show];
    }
    
    
    
}


- (IBAction)AddReviewAndCommentsAction:(id)sender
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    PopUpVC *smallViewController = [storyboard instantiateViewControllerWithIdentifier:@"bigViewController"];
    smallViewController.eventIdString = eventIdString;
    
    BIZPopupViewController *popupViewController = [[BIZPopupViewController alloc] initWithContentViewController:smallViewController contentSize:CGSizeMake(300, 400)];
    [self presentViewController:popupViewController animated:NO completion:nil];

}




- (IBAction)addCommentAction:(id)sender {
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        CommentsAndRatingVC *commentsView = [storyboard instantiateViewControllerWithIdentifier:@"commentsView"];
    commentsView.userCommentsString = [user_CommentsArray mutableCopy];
    commentsView.userNamesString = [user_nameArray mutableCopy];
    commentsView.userRatingsString =[user_RatingArray mutableCopy];
    commentsView.userImagesString =[user_ImageArray mutableCopy];
    commentsView.userCommemtTimeString =[user_CommentTimeArray mutableCopy];
    [ self.navigationController pushViewController:commentsView animated:YES];
    
    
//    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//    PopUpVC *smallViewController = [storyboard instantiateViewControllerWithIdentifier:@"bigViewController"];
//    smallViewController.eventIdString = eventIdString;
//    
//    BIZPopupViewController *popupViewController = [[BIZPopupViewController alloc] initWithContentViewController:smallViewController contentSize:CGSizeMake(300, 400)];
//    [self presentViewController:popupViewController animated:NO completion:nil];
}



- (IBAction)favoriteButtonAction:(id)sender
{
    [self postMethodForFavorite];
}


-(void)postMethodForFavorite
{
    [SVProgressHUD showWithStatus:@"Updating.." maskType:SVProgressHUDMaskTypeBlack];

    
    if (APP_DELEGATE.isServerReachable)
    {
        favButtonTapType=1;
        
    [self.connection cancel];
    
    //initialize new mutable data
    NSMutableData *data = [[NSMutableData alloc] init];
    self.receivedData = data;
    
    NSURL *url = [NSURL URLWithString:(API_URL @"updatefavourite")];
    
    //initialize a request from url
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[url standardizedURL]];
    
    //set http method
    [request setHTTPMethod:@"POST"];
    //initialize a post data
    
    NSString *postData = [NSString stringWithFormat:@"event_id=%@ & app_token=%@ ", eventIdString,access_token];
    
    
    [request setValue:@"application/x-www-form-urlencoded; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    
    [request setHTTPBody:[postData dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    self.connection = connection;
    
    //start the connection
    [connection start];
        
    }
    else
    {
        [SVProgressHUD dismiss];
        [[[UIAlertView alloc]initWithTitle:@"OutNAbout" message:@"Internet connection is not available. Please try again."  delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil]show];
    }


}


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex==0) {
         [self.navigationController popViewControllerAnimated:YES];
    }
    
}



-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:0.95 green:0.28 blue:0.31 alpha:1.0];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];

    //[[UIApplication sharedApplication] setStatusBarHidden:YES];
    
    
}


-(void)postMethodForSingleEventDetail
{
    [SVProgressHUD showWithStatus:@"Load Event.." maskType:SVProgressHUDMaskTypeBlack];

    if(APP_DELEGATE.isServerReachable)
    {

    
    [self.connection cancel];
    
    //initialize new mutable data
    NSMutableData *data = [[NSMutableData alloc] init];
    self.receivedData = data;
    
    NSURL *url = [NSURL URLWithString:(API_URL @"singleevent")];
    
    //initialize a request from url
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[url standardizedURL]];
    
    //set http method
    [request setHTTPMethod:@"POST"];
    //initialize a post data
    
    NSString *postData = [NSString stringWithFormat:@"event_id=%@ & app_token=%@ ", eventIdString,access_token];
    
    
    [request setValue:@"application/x-www-form-urlencoded; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    
    [request setHTTPBody:[postData dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    self.connection = connection;
    
    //start the connection
    [connection start];

    }
    else
    {
        [SVProgressHUD dismiss];
        [[[UIAlertView alloc]initWithTitle:@"OutNAbout" message:@"Internet connection is not available. Please try again."  delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil]show];
    }
    
}


#pragma mark - NSURL Post methods 


-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data{
    [self.receivedData appendData:data];
    [SVProgressHUD dismiss];
}


-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    [SVProgressHUD dismiss];
    [[[UIAlertView alloc]initWithTitle:@"OutNAbout" message:@"The network connection was lost."  delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil]show];
    NSLog(@"eeeeeeee  %@" , error);
    
    
    if (error) {
        [_favButton setImage:[UIImage imageNamed:@"ic_star_disable@3x.png"] forState:UIControlStateNormal];
       
        _viewsLabel.text = [NSString stringWithFormat:@"%@ View",@"0"];
        _viewsLabel.textColor = [UIColor whiteColor];
        _viewsLabel.font = [UIFont systemFontOfSize:11.0f];
        
     
        _totalCommentsLabel.text = [NSString stringWithFormat:@"%@ Comment",@"0"];
        _totalCommentsLabel.textColor = [UIColor whiteColor];
        _totalCommentsLabel.font = [UIFont systemFontOfSize:11.0f];
        

        _favCountLabel.text = [NSString stringWithFormat:@"%@ People",@"0"];
        _favCountLabel.textColor = [UIColor whiteColor];
        _favCountLabel.font = [UIFont systemFontOfSize:11.0f];
        
        
        self.rateView.notSelectedImage = [UIImage imageNamed:@"ic_star_Redblank@3x.png"];
        self.rateView.halfSelectedImage = [UIImage imageNamed:@"ic_star_Redhalf@3x.png"];
        self.rateView.fullSelectedImage = [UIImage imageNamed:@"ic_star_Redfill@3x.png"];
        self.rateView.rating = 0;
        self.rateView.editable = YES;
        self.rateView.maxRating = 5;
        self.rateView.delegate = self;
    }
    
}


-(void)connectionDidFinishLoading:(NSURLConnection *)connection{
    
    //initialize convert the received data to string with UTF8 encoding
    //NSString *htmlSTR = [[NSString alloc] initWithData:self.receivedData
                        //                      encoding:NSUTF8StringEncoding];
    // NSLog(@" ffff     %@" , htmlSTR);
    
    NSError *error = nil;
    NSDictionary *jsonArray = [NSJSONSerialization JSONObjectWithData: self.receivedData options: NSJSONReadingMutableContainers error: &error];  //I am using sbjson to parse
    
    
    if ([rsvpTypeCheck isEqualToString:@"rsvp"])
    {
    }
    
    else{
    
    NSDictionary *data = jsonArray[@"data"];
    viewsString = data[@"views"];
    commentsString = data[@"commentcount"];
    favsString = data[@"favcount"];
    rsvpStatusNo= data[@"rsvpstatus"];
    ticketLink = data[@"tickets"];
    is_Paid= data[@"ispaid"];
    costOffer= data[@"costoffer"];
    userRating= data[@"userrating"];
    userComment= data[@"usercomment"];
        

        
        
        if ([is_Paid integerValue]<=0) {
            _costLabel.text = @"Free Event";
        }
         if (!([costString integerValue]==0)) {
             _costLabel.text = [NSString stringWithFormat:@"Cost: %@ %@",costString,currencyString];

         }
        if ([costString integerValue]==0 && costOffer.length) {
            _costLabel.text = costOffer;
        }
        _costLabel.textColor = [UIColor whiteColor];
        _costLabel.font = [UIFont systemFontOfSize:13.5f];
        
//
        
        if ([ticketLink isEqualToString:@""]) {
            _ticketButton.hidden=YES;
        }
        else
        {
            NSURL *url = [NSURL URLWithString:ticketLink];
            if (url && url.scheme && url.host)
            {
                _ticketButton.hidden=NO;
                _costLabel.frame = CGRectMake(22, 5, 100, 15);
                [_ticketButton setTitle:@"GET TICKET" forState:UIControlStateNormal];
                [_ticketButton addTarget:self action:@selector(ticketURL) forControlEvents:UIControlEventTouchUpInside];
                
                //the url looks ok, do something with it
                NSLog(@"%@ is a valid URL", ticketLink);
            }
        }
        
//
        
        
        if (favButtonTapType==1) {
        
        
        if (favButtonTapType==0) {
            NSString *favString = data[@"favorite"];
            NSNumber  *favNo = [NSNumber numberWithInteger: [favString integerValue]];
            
            if ([favNo isEqualToNumber:[NSNumber numberWithInteger:0]]) {
                [_favButton setImage:[UIImage imageNamed:@"ic_star_disable@3x.png"] forState:UIControlStateNormal];
            }
            else{
                [_favButton setImage:[UIImage imageNamed:@"ic_star_enable@3x.png"] forState:UIControlStateNormal];
            }
        }
        
        else if (favButtonTapType==1) {
            
            NSString *favValue = jsonArray[@"favorite"];
            NSLog(@"fav value******* :%@",favValue);
            NSNumber  *favNumber = [NSNumber numberWithInteger: [favValue integerValue]];
            
            
            
            if ([favNumber isEqualToNumber:[NSNumber numberWithInteger:1]]) {
                [_favButton setImage:[UIImage imageNamed:@"ic_star_enable@3x.png"] forState:UIControlStateNormal];
            }
            else{
                [_favButton setImage:[UIImage imageNamed:@"ic_star_disable@3x.png"] forState:UIControlStateNormal];
            }
            
            NSDictionary *favMessage = @{@"FavRefresh":@"refresh"};
            [[NSNotificationCenter defaultCenter] postNotificationName:@"RefreshSingleEventView" object:self userInfo:favMessage];

        }
        else
        {
        }

        }
        
        else{
            
            if (favButtonTapType==0) {
                NSString *favString = data[@"favorite"];
                NSNumber  *favNo = [NSNumber numberWithInteger: [favString integerValue]];
                [self singleEventDetailLabel];
                
                if ([favNo isEqualToNumber:[NSNumber numberWithInteger:0]]) {
                    [_favButton setImage:[UIImage imageNamed:@"ic_star_disable@3x.png"] forState:UIControlStateNormal];
                }
                else{
                    [_favButton setImage:[UIImage imageNamed:@"ic_star_enable@3x.png"] forState:UIControlStateNormal];
                }
            }
        
 
        NSNumber  *rsvpNo = [NSNumber numberWithInteger: [rsvpStatusNo integerValue]];

        if ([rsvpNo isEqualToNumber:[NSNumber numberWithInteger:1]]) {
            [_rsvpButton setTitle:@"Yes" forState:UIControlStateNormal];
        }
        else
            if ([rsvpNo isEqualToNumber:[NSNumber numberWithInteger:2]]) {
                [_rsvpButton setTitle:@"May be" forState:UIControlStateNormal];
            }
            else  if ([rsvpNo isEqualToNumber:[NSNumber numberWithInteger:0]]) {
                [_rsvpButton setTitle:@"No" forState:UIControlStateNormal];
            }
        else if ([rsvpNo isEqualToNumber:[NSNumber numberWithInteger:3]])
        {
                [_rsvpButton setTitle:@"+RSVP" forState:UIControlStateNormal];
        }
            
            
            
        }

//
        
        float avgRating =[[data valueForKey:@"avgrating"] floatValue];
        self.rateView.notSelectedImage = [UIImage imageNamed:@"ic_star_Redblank@3x.png"];
        self.rateView.halfSelectedImage = [UIImage imageNamed:@"ic_star_Redhalf@3x.png"];
        self.rateView.fullSelectedImage = [UIImage imageNamed:@"ic_star_Redfill@3x.png"];
        self.rateView.rating = avgRating;
        self.rateView.editable = YES;
        self.rateView.maxRating = 5;
        self.rateView.delegate = self;
        
//
   
    NSMutableArray *commenting =[data valueForKey:@"allcommrating"];
    if (commenting.count)
    {
        [user_CommentsArray removeAllObjects];
        [user_nameArray removeAllObjects];
        [user_RatingArray removeAllObjects];
        
        for (NSDictionary *data in commenting) {
            
            userNameString = data[@"user_name"];
            [user_nameArray addObject:userNameString];
            
            userCommentString = data[@"comment"];
            [user_CommentsArray addObject:userCommentString];
            
            NSString *userRatingString = data[@"rating"];
            [user_RatingArray addObject:userRatingString];
            
            NSString *userImageString = data[@"userimage"];
            [user_ImageArray addObject:userImageString];
            
            NSString *userComntTimeString = data[@"created_at"];
            [user_CommentTimeArray addObject:userComntTimeString];
        }
        
        _noReviewLabel.hidden=YES;
        _addReviewButton.hidden=NO;
        
        if ([OnCommentsType isEqualToString:@"comments"]) {
            
             [[[UIAlertView alloc]initWithTitle:@"Submited" message:message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil]show];
            
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            CommentsAndRatingVC *commentsView = [storyboard instantiateViewControllerWithIdentifier:@"commentsView"];
            commentsView.userCommentsString = [user_CommentsArray mutableCopy];
            commentsView.userNamesString = [user_nameArray mutableCopy];
            commentsView.userRatingsString =[user_RatingArray mutableCopy];
            commentsView.userImagesString =[user_ImageArray mutableCopy];
            commentsView.userCommemtTimeString =[user_CommentTimeArray mutableCopy];
            [ self.navigationController pushViewController:commentsView animated:YES];
        }
    }
    else
    {
         _noReviewLabel.hidden=NO;
        _addReviewButton.hidden=NO;
    }
        
//
        
 
    NSLog(@"response :%@",data);
    
        
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults setObject:userComment forKey:@"comment"];
        [defaults setObject:userRating forKey:@"rating"];
        [defaults synchronize];
        
//
        
        [SVProgressHUD dismiss];

    }
    
}




-(void)singleEventDetailLabel
{
    if ([viewsString integerValue]<=1) {
    _viewsLabel.text = [NSString stringWithFormat:@"%@ View",viewsString];
    }
    else
    {
    _viewsLabel.text = [NSString stringWithFormat:@"%@ Views",viewsString];
    }
    _viewsLabel.textColor = [UIColor whiteColor];
    _viewsLabel.font = [UIFont systemFontOfSize:11.0f];
    
    if ([commentsString integerValue]<=1) {
    _totalCommentsLabel.text = [NSString stringWithFormat:@"%@ Comment",commentsString];
    }
    else{
        _totalCommentsLabel.text = [NSString stringWithFormat:@"%@ Comments",commentsString];

    }
    _totalCommentsLabel.textColor = [UIColor whiteColor];
    _totalCommentsLabel.font = [UIFont systemFontOfSize:11.0f];
    
    if ([favsString integerValue]<=1) {
        _favCountLabel.text = [NSString stringWithFormat:@"%@ People",favsString];
    }else{
    _favCountLabel.text = [NSString stringWithFormat:@"%@ Peoples",favsString];
    }
    _favCountLabel.textColor = [UIColor whiteColor];
    _favCountLabel.font = [UIFont systemFontOfSize:11.0f];
    
    
//    _userNameLabel.text = [NSString stringWithFormat:@"%@",userNameString];
//    _userNameLabel.textColor = [UIColor blackColor];
//    _userNameLabel.font = [UIFont systemFontOfSize:12.0f];
//
//    _userCommentSection.text = [NSString stringWithFormat:@"%@",userCommentString];
//    _userCommentSection.textColor = [UIColor blackColor];
//    //_userCommentSection.backgroundColor = [UIColor redColor];
//   // _userCommentSection.editable=NO;
//    _userCommentSection.font = [UIFont systemFontOfSize:12.0f];
//    [_userCommentSection sizeToFit]; //added
//    [_userCommentSection layoutIfNeeded];

//    CGFloat fixedWidth = _userCommentSection.frame.size.width;
//    CGSize newSize = [_userCommentSection sizeThatFits:CGSizeMake(fixedWidth, MAXFLOAT)];
//    CGRect newFrame =_userCommentSection.frame;
//    newFrame.size = CGSizeMake(fmaxf(newSize.width, fixedWidth), newSize.height);
//    //[scrollView setContentSize:CGSizeMake(fmaxf(newSize.width, fixedWidth), newSize.height)];
//    _userCommentSection.frame=newFrame;

}





/*- (void)textViewDidChange:(UITextView *)textView
{
    CGFloat fixedWidth = textView.frame.size.width;
    CGSize newSize = [textView sizeThatFits:CGSizeMake(fixedWidth, MAXFLOAT)];
    CGRect newFrame = textView.frame;
    newFrame.size = CGSizeMake(fmaxf(newSize.width, fixedWidth), newSize.height);
    textView.frame = newFrame;
}*/





















-(void)ticketURL
{
    UIStoryboard *story = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    AboutScreenVC *aboutView = [story instantiateViewControllerWithIdentifier:@"aboutView"];
    aboutView.link = ticketLink;
    [ self.navigationController pushViewController:aboutView animated:YES];
}


-(void)shareEvent
{
    
    NSLog(@"share &&&&");
    [self Url:[NSURL URLWithString:shareLinkString]];
}

- (void)Url:(NSURL *)url
{
    NSMutableArray *sharingItems = [NSMutableArray new];
    if (url) {
        [sharingItems addObject:url];
    }
    UIActivityViewController *activityController = [[UIActivityViewController alloc] initWithActivityItems:sharingItems applicationActivities:nil];
    [self presentViewController:activityController animated:YES completion:nil];
}



- (IBAction)readMoreButtonAction:(id)sender {
    
//    [_dimBackgroundView setHidden:NO];
//    [_popupView setHidden:NO];
    
    UIStoryboard *story = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    DescriptionVC *descriptionView  = [story instantiateViewControllerWithIdentifier:@"descriptionView"];
    descriptionView.descriptionText = descriptionString;
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:descriptionView];
    
//    CATransition *transition = [CATransition animation];
//    transition.duration = 0.3;
//    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn];
//    transition.type = kCATransitionMoveIn;//kCATransitionFade
//    transition.subtype = kCATransitionFromRight;
//    [self.view.window.layer addAnimation:transition forKey:nil];
    [self presentViewController:nav animated:YES completion:nil];
    //[self.view removeFromSuperview];


}

- (void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:YES];
    [_popupView setHidden:YES];
    [_dimBackgroundView setHidden:YES];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}



@end
