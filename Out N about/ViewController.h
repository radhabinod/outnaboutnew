//
//  ViewController.h
//  Out N about
//
//  Created by Ram Kumar on 03/05/16.
//  Copyright © 2016 tecHindustan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController <UITableViewDataSource,UITableViewDelegate,UITabBarControllerDelegate>

@property (strong, nonatomic) IBOutlet UIBarButtonItem *barButtonItem;
@property (nonatomic,strong) NSString *screenType;


//@property (strong, nonatomic) IBOutlet UILabel *nameLabel;
//
//@property(strong, nonatomic) UITableView *imagesTableView;

@property (retain, nonatomic) NSURLConnection *connection;
@property (retain, nonatomic) NSMutableData *receivedData;

@property (weak, nonatomic) IBOutlet UIView *dimBackgroundView;
@property (weak, nonatomic) IBOutlet UIView *popupView;
@property (weak, nonatomic) IBOutlet UITableView *tableViewListing;


@end

