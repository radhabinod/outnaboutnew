//
//  AppDelegate.m
//  Out N about
//
//  Created by Ram Kumar on 03/05/16.
//  Copyright © 2016 tecHindustan. All rights reserved.
//

#import "AppDelegate.h"
#import "LoginVC.h"
#import "SignupView.h"
#import "MainTabbarController.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import "LoginVC.h"
#import "SDWebImageManager.h"
#import "CommonAPI's.pch"
#import "Reachability.h"

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wdeprecated-declarations"

#define IS_OS_8_OR_LATER    ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)

#define GOOGLE_SCHEME @"597353559873"
#define FACEBOOK_SCHEME  @"fb696662503833038"

@interface AppDelegate ()

{
    LoginVC *loginViewObject;
}


@end

@implementation AppDelegate
@synthesize Devtoken;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // Override point for customization after application launch.
   // [self showLoginScreen:YES];
   
    
    NSError* configureError;
    [[GGLContext sharedInstance] configureWithError: &configureError];
    NSAssert(!configureError, @"Error configuring Google services: %@", configureError);
    
    [GIDSignIn sharedInstance].delegate = self;

    
    
    [[FBSDKApplicationDelegate sharedInstance] application:application
                             didFinishLaunchingWithOptions:launchOptions];

    
    
    

    NSDictionary *userInfo = launchOptions[UIApplicationLaunchOptionsRemoteNotificationKey];
    if (userInfo != nil) {
        NSString* payload = [userInfo objectForKey:@"spayload"];
        if (payload != nil) {
            NSLog(@"Tapjoy push notification with payload: %@", payload);
        }
    }
    
    
    
    
    
    if(IS_OS_8_OR_LATER)
    {
        
        
        UIUserNotificationSettings *settings=[UIUserNotificationSettings settingsForTypes:UIRemoteNotificationTypeBadge| UIRemoteNotificationTypeSound|UIRemoteNotificationTypeAlert categories:nil];
        [[UIApplication sharedApplication] registerForRemoteNotifications];
        [[UIApplication sharedApplication] registerUserNotificationSettings:settings];
        // [[UIApplication sharedApplication] registerUserNotificationSettings];
        //    [[UIApplication sharedApplication] registerUserNotificationSettings:];
    }
    else{
        
        [[UIApplication sharedApplication] registerForRemoteNotificationTypes:(UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeSound | UIRemoteNotificationTypeAlert)];
        
    }

    [self configureReachability];

    return YES;
}


- (void)application:(UIApplication*)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData*)deviceToken
{
    NSLog(@"My token is: %@", deviceToken);
    
    //    UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"Device Token" message:[NSString stringWithFormat:@"My token is: %@",deviceToken] delegate:nil cancelButtonTitle:nil otherButtonTitles:nil, nil] ;
    //    [alertView show];
    
    //Devtoken=[deviceToken description];
    Devtoken=[[deviceToken description] stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"<>"]];
    Devtoken=[Devtoken stringByReplacingOccurrencesOfString:@" " withString:@""];
    NSLog(@"dev value=%@",self.Devtoken);
    
    //    self.Devtoken=[deviceToken description];
    //    self.Devtoken=[self.Devtoken stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"<>"]];
    //    self.Devtoken=[self.Devtoken stringByReplacingOccurrencesOfString:@" " withString:@""];
    //    NSLog(@"dev value=%@",self.Devtoken);
    
}


- (void)application:(UIApplication*)application didFailToRegisterForRemoteNotificationsWithError:(NSError*)error
{
    NSLog(@"Failed to get token, error: %@", error);
   // APP_DELEGATE.Devtoken=@"d9f71204814c6e6979aa2d43248e3217ed1bf55c5bd8157ccdaf2ff12d9adaba";
}

-(void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
    
    
    NSString *payload = [userInfo valueForKey:@"spayload"];
    NSString *title = [[userInfo valueForKey:@"aps"] valueForKey:@"title"];
    if (payload != nil) {
        NSLog(@"Tapjoy push notification with payload: %@", payload);
        
        [[[UIAlertView alloc] initWithTitle:title message:payload delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
    }
    
    

//    NSString *message = [[userInfo valueForKey:@"aps"] valueForKey:@"alert"];
//    [[[UIAlertView alloc] initWithTitle:@"Out N About" message:message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
    
    
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
}



    

- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation {
    return [[FBSDKApplicationDelegate sharedInstance] application:application
                                                          openURL:url
                                                sourceApplication:sourceApplication
                                                       annotation:annotation];
}







- (BOOL)application:(UIApplication *)app openURL:(NSURL *)url options:(NSDictionary<NSString*, id> *)options {
    
    if ([[url scheme] isEqualToString:FACEBOOK_SCHEME]){

    return [[FBSDKApplicationDelegate sharedInstance] application:app
                                                          openURL:url
                                                sourceApplication:options[UIApplicationOpenURLOptionsSourceApplicationKey]
                                                       annotation:options[UIApplicationOpenURLOptionsAnnotationKey]];
    }
    else{
    return [[GIDSignIn sharedInstance] handleURL:url
                               sourceApplication:options[UIApplicationOpenURLOptionsSourceApplicationKey]
                                      annotation:options[UIApplicationOpenURLOptionsAnnotationKey]];
    }
    
}



//==========================================================================================//wrking
//- (BOOL)application:(UIApplication *)app
//            openURL:(NSURL *)url
//            options:(NSDictionary *)options {
//    return [[GIDSignIn sharedInstance] handleURL:url
//                               sourceApplication:options[UIApplicationOpenURLOptionsSourceApplicationKey]
//                                      annotation:options[UIApplicationOpenURLOptionsAnnotationKey]];
//}
//==========================================================================================





//- (BOOL)application:(UIApplication *)application
//            openURL:(NSURL *)url
//  sourceApplication:(NSString *)sourceApplication
//         annotation:(id)annotation {
//    return [[GIDSignIn sharedInstance] handleURL:url
//                               sourceApplication:sourceApplication
//                                      annotation:annotation];
//}


- (void)signIn:(GIDSignIn *)signIn
didSignInForUser:(GIDGoogleUser *)user
     withError:(NSError *)error {
    // Perform any operations on signed in user here.
    NSString *userId = user.userID;                  // For client-side use only!
//    NSString *idToken = user.authentication.idToken; // Safe to send to the server
    NSString *fullName = user.profile.name;
     NSURL *imageURL    = [user.profile imageURLWithDimension:200];
    NSString *gpUserPic = [imageURL absoluteString];
     NSLog(@"user image:%@",gpUserPic);
    
    NSLog(@"ProfileID:%@",fullName);
    NSArray * array = [fullName componentsSeparatedByString:@" "];
    
    NSString * gpfirstName = [array objectAtIndex:0];
    NSString * gplastName = [array objectAtIndex:1];
    NSLog(@"first name:%@  lastname:%@",gpfirstName,gplastName);
//    NSString *givenName = user.profile.givenName;
//    NSString *familyName = user.profile.familyName;
    NSString *email = user.profile.email;
    NSString *loginType = @"gp";
    NSLog(@"EmailID:%@   user id:%@",email,userId);
    
    if (userId.length) {
        NSDictionary *userDetails = @{@"firstName":gpfirstName,@"lastName":gplastName,
                                      @"userId":userId,
                                      @"email":email,@"type":loginType};
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"loginDetails" object:self userInfo:userDetails];
        
    }
    else{
         NSLog(@"google disconnect with nill details");
    }



    
    
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:gpfirstName forKey:@"gpFirstName"];
    [defaults setObject:gplastName forKey:@"gpLastName"];
    [defaults setObject:userId forKey:@"gpID"];
    [defaults setObject:email forKey:@"gpemail"];
    [defaults setObject:loginType forKey:@"loginType"];
    [defaults setObject:gpUserPic forKey:@"gpUserPic"];
    [defaults synchronize];
    // ...
    //[loginViewObject socialFacebookOrGooglePostMethod:gpfirstName last:gplastName emails:email id:userId logintype:loginType];

}


- (void)signIn:(GIDSignIn *)signIn
didDisconnectWithUser:(GIDGoogleUser *)user
     withError:(NSError *)error {
    // Perform any operations when the user disconnects from app here.
    // ...
    
    NSLog(@"google disconnect");
}


//-(void) showLoginScreen:(BOOL)animated
//{
//    
//    // Get login screen from storyboard and present it
////    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
////    SignupView *viewController = (SignupView *)[storyboard instantiateViewControllerWithIdentifier:@"signupView"];
////   [self.window makeKeyAndVisible];
////    [self.window.rootViewController presentViewController:viewController
////                                                 animated:animated
////                                               completion:nil];
//    
//    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//    LoginVC *viewController = (LoginVC *)[storyboard instantiateViewControllerWithIdentifier:@"loginVC"];
//    [self.window makeKeyAndVisible];
//    [self.window.rootViewController presentViewController:viewController
//                                                 animated:animated
//                                               completion:nil];

//}






- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
   [FBSDKAppEvents activateApp];

    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    // Saves changes in the application's managed object context before the application terminates.
    [self saveContext];
}








#pragma mark - Rechability..

-(void)configureReachability
{
    //    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    //    NSString *str=[userDefaults valueForKey:@"Hit"];
    //    if([str isEqualToString:@"Yes"])
    //    {
    //    [userDefaults setValue:@"No" forKey:@"Hit"];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kReachabilityChangedNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector: @selector(reachabilityChanged:) name: kReachabilityChangedNotification object: nil];
    
    serverReachability = [Reachability reachabilityForInternetConnection];
    [serverReachability startNotifier];
    [self updateInterfaceWithReachability:serverReachability];
    //    }
    //    [userDefaults synchronize];
}

//this method will be called on reach of internet reachability change notification..
- (void)reachabilityChanged:(NSNotification*)note
{
    
    Reachability* curReach = [note object];
    NSParameterAssert([curReach isKindOfClass: [Reachability class]]);
    [self updateInterfaceWithReachability:curReach];
}

//this method updates local variable responsible for internet checking in entire application
- (void)updateInterfaceWithReachability:(Reachability*)curReach
{
    NetworkStatus netStatus = [curReach currentReachabilityStatus];
    switch (netStatus)
    {
        case NotReachable:
            self.isServerReachable = NO;
            break;
        case ReachableViaWWAN:
            self.isServerReachable = YES;
            self.isWAN = YES;
            break;
        case ReachableViaWiFi:
            self.isServerReachable = YES;
            self.isWAN = NO;
            break;
    }
}





#pragma mark - Core Data stack

@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;

- (NSURL *)applicationDocumentsDirectory {
    // The directory the application uses to store the Core Data store file. This code uses a directory named "com.tecHindustan.Out_N_about" in the application's documents directory.
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

- (NSManagedObjectModel *)managedObjectModel {
    // The managed object model for the application. It is a fatal error for the application not to be able to find and load its model.
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"Out_N_about" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

- (NSPersistentStoreCoordinator *)persistentStoreCoordinator {
    // The persistent store coordinator for the application. This implementation creates and returns a coordinator, having added the store for the application to it.
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
    // Create the coordinator and store
    
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"Out_N_about.sqlite"];
    NSError *error = nil;
    NSString *failureReason = @"There was an error creating or loading the application's saved data.";
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {
        // Report any error we got.
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        dict[NSLocalizedDescriptionKey] = @"Failed to initialize the application's saved data";
        dict[NSLocalizedFailureReasonErrorKey] = failureReason;
        dict[NSUnderlyingErrorKey] = error;
        error = [NSError errorWithDomain:@"YOUR_ERROR_DOMAIN" code:9999 userInfo:dict];
        // Replace this with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _persistentStoreCoordinator;
}


- (NSManagedObjectContext *)managedObjectContext {
    // Returns the managed object context for the application (which is already bound to the persistent store coordinator for the application.)
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (!coordinator) {
        return nil;
    }
    _managedObjectContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSMainQueueConcurrencyType];
    [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    return _managedObjectContext;
}

#pragma mark - Core Data Saving support

- (void)saveContext {
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil) {
        NSError *error = nil;
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
            // Replace this implementation with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
    }
}

@end
