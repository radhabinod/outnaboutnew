//
//  ViewController.m
//  Out N about
//
//  Created by Ram Kumar on 03/05/16.
//  Copyright © 2016 tecHindustan. All rights reserved.
//

#import "ViewController.h"
#import "LoginVC.h"
#import "AppDelegate.h"
#import "CommonAPI's.pch"
#import "NavigationViewController.h"
#import "SearchEventsVC.h"

#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <Google/SignIn.h>
#import "CarbonKit.h"
#import "AllEventsTabBarVC.h"
//#import "PaidEventTabbarVC.h"
#import "FreeEventTabbarVC.h"
#import "TopEventTabbarVC.h"
#import "NearByEventsVC.h"
#import "PGDrawerTransition.h"

@interface ViewController ()<CarbonTabSwipeNavigationDelegate,GIDSignInUIDelegate,GIDSignInDelegate,UIAlertViewDelegate>
@property (nonatomic, strong) PGDrawerTransition *drawerTransition;
@property (nonatomic, strong) NavigationViewController *navigationViewController;

@end

@implementation ViewController
{
    NSArray *items;
    NSMutableArray *filterArray;
    NSString *access_token;
    NSUInteger indexNo;
    NSString *username;
    NSString *password;
    NSString *rememberCheck;
    
    CarbonTabSwipeNavigation *carbonTabSwipeNavigation;
}

@synthesize screenType;

- (void)viewDidLoad {
  [super viewDidLoad];
 
    UIStoryboard *story = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    self.navigationViewController = [story instantiateViewControllerWithIdentifier:@"drawerView"];
    
    self.drawerTransition = [[PGDrawerTransition alloc] initWithTargetViewController:self
                                                                drawerViewController:self.navigationViewController];

    
    
    
    items = @[@"NEAR BY EVENTS",@"ALL EVENTS",@"TODAY EVENTS",@"FREE EVENTS",@"TOP EVENTS"];
    
    carbonTabSwipeNavigation = [[CarbonTabSwipeNavigation alloc] initWithItems:items delegate:self];
    
    [carbonTabSwipeNavigation insertIntoRootViewController:self];
    
    [self style];


  
   
    


    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    username = [defaults objectForKey:@"username"];
    password = [defaults objectForKey:@"password"];
    rememberCheck = [defaults objectForKey:@"rememCheck"];
    access_token = [defaults objectForKey:@"access_token"];
    
    NSLog(@" access token in tab view@@@  %@",access_token );
    
    NSLog(@"username1: %@  pass1 :%@",username,password);
    
    
    UILabel* titleLabelNav = [[UILabel alloc] initWithFrame:CGRectMake(0,40,320,40)];
    titleLabelNav.textAlignment = NSTextAlignmentLeft;
    titleLabelNav.text = NSLocalizedString(@"OUT N ABOUT",@"");
    titleLabelNav.textColor = [UIColor whiteColor];
    self.navigationItem.titleView = titleLabelNav;
    
    //self.title = @"All event";
    
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor whiteColor]};
    
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:0.95 green:0.28 blue:0.31 alpha:1.0];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];

    
    if (access_token==nil) {
        NSLog(@"Access Token nil");
        
    }
    
    [self.view addSubview:_dimBackgroundView];
    [_dimBackgroundView addSubview:_popupView];
    [_dimBackgroundView setBackgroundColor:[[UIColor blackColor] colorWithAlphaComponent:0.6]];
    [_dimBackgroundView setHidden:YES];
    
    
    
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(receiveToggleForLogout:)
     name:@"didLogout"
     object:nil];
    
    
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(ViewReceiveToggleAuthUINotification:)
     name:@"callViewController"
     object:nil];

    
    }


- (void) receiveToggleForLogout:(NSNotification *) notification {
    if ([[notification name] isEqualToString:@"didLogout"]) {
        
        
        
        
        [[[UIAlertView alloc]initWithTitle:@"OutNAbout" message:@"Do you really want to logout?" delegate:self
    cancelButtonTitle:@"OK" otherButtonTitles:@"Cancel", nil]show];
        
        
        
//        NSLog(@"logout clicked");
//        
//        UIAlertController * alert=   [UIAlertController
//                                      alertControllerWithTitle:@"Message"
//                                      message:@"Do you really want to logout?"
//                                      preferredStyle:UIAlertControllerStyleAlert];
//        
//        UIAlertAction* okButton = [UIAlertAction
//                                   actionWithTitle:@"OK"
//                                   style:UIAlertActionStyleDefault
//                                   handler:^(UIAlertAction * action)
//                                   {
//                                       
//
//                                       
//                                       
//                                       
//                                   }];
//        
//        UIAlertAction* cancelButton = [UIAlertAction
//                                       actionWithTitle:@"Cancel"
//                                       style:UIAlertActionStyleDefault
//                                       handler:^(UIAlertAction * action)
//                                       {
//                                           //Handel your yes please button action here
//                                          
//                                           
//                                       }];
//        
//        [alert addAction:okButton];
//        [alert addAction:cancelButton];
//        
//        
//        [self presentViewController:alert animated:YES completion:nil];
        
        

        
    }
    
}




- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex==0) {
        
        
        
        
        if (APP_DELEGATE.isServerReachable)
        {
            
            
            //                                           NSHTTPCookieStorage* cookies = [NSHTTPCookieStorage sharedHTTPCookieStorage];
            //                                           NSArray* facebookCookies = [cookies cookiesForURL:
            //                                                                       [NSURL URLWithString:@"http://login.facebook.com"]];
            //                                           for (NSHTTPCookie* cookie in facebookCookies) {
            //                                               [cookies deleteCookie:cookie];
            //                                           }
            
            
            
            [self logoutFuction];
            
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            NSString *loginType = [defaults objectForKey:@"loginType"];
            if ([loginType isEqualToString:@"fb"]) {
                
                
                
                FBSDKLoginManager *loginManager = [[FBSDKLoginManager alloc] init];
                [loginManager logOut];
                
                [FBSDKAccessToken setCurrentAccessToken:nil];
                [FBSDKProfile setCurrentProfile:nil];
                
                
                
                FBSDKGraphRequest *request = [[FBSDKGraphRequest alloc]
                                              initWithGraphPath:@"me/permissions/" parameters:nil HTTPMethod:@"DELETE"];
                [request   startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
                    
                    NSLog(@"Delete facebook login");
                    
                }];
                
                
            }
            else if ([loginType isEqualToString:@"gp"])
                
            {
                [[GIDSignIn sharedInstance] signOut];
            }
            
            
            [[NSUserDefaults standardUserDefaults] setPersistentDomain:[NSDictionary dictionary] forName:[[NSBundle mainBundle] bundleIdentifier]];
            
            NSUserDefaults * defs = [NSUserDefaults standardUserDefaults];
            // [defs removeObjectForKey:@"username"];
            //    [defs removeObjectForKey:@"password"];
            NSDictionary * dict = [defs dictionaryRepresentation];
            for (id key in dict) {
                [defs removeObjectForKey:key];
            }
            [defs synchronize];
            
            
            
            [self resetDefaults];
            [self  dismissModalStack];
            [self performSegueWithIdentifier:@"tologin" sender:nil];
            
            
            access_token=nil;
            // [[NSUserDefaults standardUserDefaults] setPersistentDomain:[NSDictionary dictionary] forName:[[NSBundle mainBundle] bundleIdentifier]];
            
            screenType = nil;
            
            
            if (rememberCheck==nil) {
                rememberCheck=@"";
            }
            
            
            NSDictionary *userDetails = @{@"userName":username,@"password":password,@"ifCheck":rememberCheck};
            
            if ([rememberCheck isEqualToString:@"check"] && username.length) {
                // NSDictionary *userDetails = @{@"LoginUserName":username,@"LoginPassword":password};
                
                //                                           [[NSNotificationCenter defaultCenter] postNotificationName:@"LoginUserName" object:self];
                [[NSNotificationCenter defaultCenter] postNotificationName:@"UpdateRememberMe" object:self userInfo:userDetails];
                //    [[NSNotificationCenter defaultCenter] postNotificationName:@"ScanCompleted" object:nil];
                
            }
            else{
                //
                //                                           NSDictionary *userDetails = @{@"LoginUserName":@"",@"LoginPassword":@""};
                //
                //                                           [[NSNotificationCenter defaultCenter] postNotificationName:@"rememberLogin" object:self userInfo:userDetails];
            }
            
            
        }
        
        else
        {
            [[[UIAlertView alloc]initWithTitle:@"OutNAbout" message:@"Internet connection is not available. Please try again."  delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil]show];
        }
        
        
        
        
        
        
        
    }
    else
    {
         screenType=nil;
        
    }
}





- (void) ViewReceiveToggleAuthUINotification:(NSNotification *) notification {
    if ([[notification name] isEqualToString:@"callViewController"]) {
        
        
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        access_token = [defaults objectForKey:@"access_token"];
        username = [defaults objectForKey:@"username"];
        password = [defaults objectForKey:@"password"];
        rememberCheck = [defaults objectForKey:@"rememCheck"];
        //NSLog(@"Get access token after viewcontroller load*** %@",access_token);
        
        
        NSLog(@"remember check**** %@",rememberCheck);
        [self viewDidLoad];
        [self viewDidAppear:YES];

    }
}




-(void)searchView
{
    UIStoryboard *story = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    SearchEventsVC *searchEvent = [story instantiateViewControllerWithIdentifier:@"searchView"];
    [self.navigationController pushViewController:searchEvent animated:YES];
}


- (void)style {
    
    UIColor *color = [UIColor yellowColor];//[UIColor colorWithRed:24.0 / 255 green:75.0 / 255 blue:152.0 / 255 alpha:1]
    self.navigationController.navigationBar.translucent = NO;
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.navigationController.navigationBar.barTintColor = color;
    self.navigationController.navigationBar.barStyle = UIBarStyleBlackTranslucent;
    carbonTabSwipeNavigation.carbonSegmentedControl.backgroundColor = [UIColor colorWithRed:0.95 green:0.28 blue:0.31 alpha:1.0];
    carbonTabSwipeNavigation.toolbar.translucent = NO;
    [carbonTabSwipeNavigation setIndicatorColor:color];
    [carbonTabSwipeNavigation setTabExtraWidth:30];
    [carbonTabSwipeNavigation.carbonSegmentedControl setWidth:115 forSegmentAtIndex:0];
    [carbonTabSwipeNavigation.carbonSegmentedControl setWidth:100 forSegmentAtIndex:1];
   // [carbonTabSwipeNavigation.carbonSegmentedControl setWidth:100 forSegmentAtIndex:2];
    [carbonTabSwipeNavigation.carbonSegmentedControl setWidth:105 forSegmentAtIndex:2];
    [carbonTabSwipeNavigation.carbonSegmentedControl setWidth:100 forSegmentAtIndex:3];
    [carbonTabSwipeNavigation.carbonSegmentedControl setWidth:100 forSegmentAtIndex:4];

    
    // Custimize segmented control
    [carbonTabSwipeNavigation setNormalColor:[[UIColor whiteColor] colorWithAlphaComponent:1.0]
                                        font:[UIFont boldSystemFontOfSize:11]];
    [carbonTabSwipeNavigation setSelectedColor:color font:[UIFont boldSystemFontOfSize:12]];
}

#pragma mark - CarbonTabSwipeNavigation Delegate
// required

- (nonnull UIViewController *)carbonTabSwipeNavigation:
(nonnull CarbonTabSwipeNavigation *)carbontTabSwipeNavigation
                                 viewControllerAtIndex:(NSUInteger)index {
    switch (index) {
        case 0:
            return [self.storyboard instantiateViewControllerWithIdentifier:@"NearEventsView"];
            
        case 1:
            return [self.storyboard instantiateViewControllerWithIdentifier:@"allEventsView"];
            
//        case 2:
//            return [self.storyboard instantiateViewControllerWithIdentifier:@"paidEventsView"];//todayEventsView
            
        case 2:
            return [self.storyboard instantiateViewControllerWithIdentifier:@"todayEventsView"];//freeEventsView
            
        case 3:
            return [self.storyboard instantiateViewControllerWithIdentifier:@"freeEventsView"];//paidEventsView
            
        default:
            return [self.storyboard instantiateViewControllerWithIdentifier:@"topEventsView"];//topEventsView
    }
}

// optional
- (void)carbonTabSwipeNavigation:(nonnull CarbonTabSwipeNavigation *)carbonTabSwipeNavigation
                 willMoveAtIndex:(NSUInteger)index {
//    switch (index) {
//        case 0:
//            self.title = @"All Events";
//            break;
//        case 1:
//            self.title = @"Paid Events";
//            break;
//        case 2:
//            self.title = @"Top Events";
//            break;
//        case 3:
//            self.title = @"Free Events";
//            break;
//        case 4:
//            self.title = @"Today Events";
//            break;
//        default:
//            self.title = items[index];
//            break;
//    }
}

- (void)carbonTabSwipeNavigation:(nonnull CarbonTabSwipeNavigation *)carbonTabSwipeNavigation
                  didMoveAtIndex:(NSUInteger)index {
    indexNo =index;
    NSLog(@"Did move index: %ld", indexNo);
    

    
        if (indexNo==0)
        {
          UIBarButtonItem* searchButton1 = [[UIBarButtonItem alloc]
                            initWithImage:[UIImage imageNamed:@"ic_search@3x.png"]
                            style:UIBarButtonItemStylePlain
                            target:self
                            action:@selector(searchView)];
            searchButton1.tintColor = [UIColor whiteColor];
             self.navigationItem.rightBarButtonItems = [NSArray arrayWithObjects:searchButton1, nil];
        }
        else{
            
            
            
            if (indexNo==1) {
                
                filterArray = [[NSMutableArray alloc]initWithObjects:@"By Date",@"By Price",@"By Distance",@"Default", nil];

            }
            else
                if (indexNo==2) {
                    filterArray = [[NSMutableArray alloc]initWithObjects:@"By Price",@"By Distance",@"Default", nil];


                }
                else
                    if (indexNo==3) {
                        filterArray = [[NSMutableArray alloc]initWithObjects:@"By Date",@"By Distance",@"Default", nil];

                    }
                    else
                        if (indexNo==4) {
                            filterArray = [[NSMutableArray alloc]initWithObjects:@"By Date",@"By Price",@"Default", nil];

                        }

            
            
            
            
           UIBarButtonItem*  searchButton = [[UIBarButtonItem alloc]
                            initWithImage:[UIImage imageNamed:@"ic_search@3x.png"]
                            style:UIBarButtonItemStylePlain
                            target:self
                            action:@selector(searchView)];
            searchButton.tintColor = [UIColor whiteColor];
   UIBarButtonItem*  filterButton = [[UIBarButtonItem alloc]
                    initWithImage:[UIImage imageNamed:@"ic_filter@3x.png"]
                    style:UIBarButtonItemStylePlain
                    target:self
                    action:@selector(filterView)];
    filterButton.tintColor = [UIColor whiteColor];
            self.navigationItem.rightBarButtonItems = [NSArray arrayWithObjects:filterButton,searchButton, nil];

      }
    
    


}

- (UIBarPosition)barPositionForCarbonTabSwipeNavigation:
(nonnull CarbonTabSwipeNavigation *)carbonTabSwipeNavigation {
    return UIBarPositionTop; // default UIBarPositionTop
}




-(void)filterView
{
    
    [self popupTableView];
    
    if (indexNo==1) {
        _popupView.frame = CGRectMake(0, 0, 232, 200);

    }
    else{
    _popupView.frame = CGRectMake(0, 0, 232, 197-35);
    }
    
    _popupView.center = _dimBackgroundView.center;

    
    [_dimBackgroundView setHidden:NO];
    [_popupView setHidden:NO];
    [_tableViewListing reloadData];

}


-(void)popupTableView
{

   //    _tableViewListing = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height) style:UITableViewStylePlain];

    
    _tableViewListing.delegate = self;
    _tableViewListing.dataSource = self;
    
    [_popupView addSubview:_tableViewListing];
}



-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [filterArray count];
    
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *cellIdent = @"cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdent];
    
    if(cell == nil)
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdent];
    cell.textLabel.font = [UIFont systemFontOfSize:13.0f];
    
    if (indexNo==0) {
        cell.textLabel.text = [filterArray objectAtIndex:indexPath.row];
    }
    else
        if (indexNo==1) {
            cell.textLabel.text = [filterArray objectAtIndex:indexPath.row];
        }
        else
            if (indexNo==2) {
               // [filterArray removeObjectAtIndex:2];
                cell.textLabel.text = [filterArray objectAtIndex:indexPath.row];
            }
            else
                if (indexNo==3) {
                    cell.textLabel.text = [filterArray objectAtIndex:indexPath.row];
                }
                else
                    if (indexNo==4) {
                        cell.textLabel.text = [filterArray objectAtIndex:indexPath.row];
                    }
                else{
                    cell.textLabel.text = [filterArray objectAtIndex:indexPath.row];

                }

    return cell;
}



-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *byDate = @"by Date";
    NSString *byPrice = @"by Price";
    NSString *bydefault = @"by Default";
     NSString *byNear = @"by Near";

    NSDictionary *filterByDate = @{@"byDate":byDate};
    NSDictionary *filterByPrice = @{@"byPrice":byPrice};
    NSDictionary *filterByDefault = @{@"byDefault":bydefault};
     NSDictionary *filterByNear = @{@"byNear":byNear};

    
    if (indexNo==0) {

    }
    
    else if (indexNo==1)
    {
        
        
               if (indexPath.row==0) {
                [[NSNotificationCenter defaultCenter] postNotificationName:@"filterType1" object:self userInfo:filterByDate];
               }
                else if (indexPath.row==1) {
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"filterType1" object:self userInfo:filterByPrice];
                }
                else if (indexPath.row==2) {
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"filterType1" object:self userInfo:filterByNear];
                }
                else if (indexPath.row==3) {
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"filterType1" object:self userInfo:filterByDefault];
                }
        
    }
    
    else if (indexNo==2)
    {
        if (indexPath.row==0) {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"filterType3" object:self userInfo:filterByPrice];
        }
        else if (indexPath.row==1) {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"filterType3" object:self userInfo:filterByNear];
        }
        else if (indexPath.row==2) {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"filterType3" object:self userInfo:filterByDefault];
        }
    }
    
    else if (indexNo==3)
    {
        if (indexPath.row==0) {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"filterType4" object:self userInfo:filterByDate];
        }
        else if (indexPath.row==1) {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"filterType4" object:self userInfo:filterByNear];
        }
        else if (indexPath.row==2) {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"filterType4" object:self userInfo:filterByDefault];
        }
        
    }
    
    else if (indexNo==4)
    {
        if (indexPath.row==0) {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"filterType5" object:self userInfo:filterByDate];
        }
        else if (indexPath.row==1) {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"filterType5" object:self userInfo:filterByPrice];
        }
        else if (indexPath.row==2) {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"filterType5" object:self userInfo:filterByDefault];
        }
        
    }
    
//    else if (indexNo==5)
//    {
//        if (indexPath.row==0) {
//            [[NSNotificationCenter defaultCenter] postNotificationName:@"filterType6" object:self userInfo:userDetails1];
//        }
//        else if (indexPath.row==1) {
//            [[NSNotificationCenter defaultCenter] postNotificationName:@"filterType6" object:self userInfo:userDetails2];
//        }
//        else if (indexPath.row==2) {
//            [[NSNotificationCenter defaultCenter] postNotificationName:@"filterType6" object:self userInfo:userDetails3];
//        }
//        
//    }
    
    [_popupView setHidden:YES];
    [_dimBackgroundView setHidden:YES];
    
}



- (void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:YES];
    [_popupView setHidden:YES];
    [_dimBackgroundView setHidden:YES];
}


-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    UIBarButtonItem *left = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"ic_menu@3x.png"] style:UIBarButtonItemStyleDone target:self action:@selector(open)];
    self.navigationItem.leftBarButtonItem = left;


    
    
    if ([screenType isEqualToString:@"LogoutNav"]) {

    }
    else if ([screenType isEqualToString:@"mail"]) {
        
        
        
//        NSString *emailTitle = @"Test Email";
//        // Email Content
//        NSString *messageBody = @"iOS programming is so fun!";
//        // To address
//        NSArray *toRecipents = [NSArray arrayWithObject:@"support@appcoda.com"];
//        
//        MFMailComposeViewController *mc = [[MFMailComposeViewController alloc] init];
//        mc.mailComposeDelegate = self;
//        [mc setSubject:emailTitle];
//        [mc setMessageBody:messageBody isHTML:NO];
//        [mc setToRecipients:toRecipents];
//        [self presentViewController:mc animated:YES completion:NULL];
        
        
            NSArray *activityItems;
        
            NSString *texttoshare = [NSString stringWithFormat:@"Hey bro! check this info.\n%@\n%@", @"Its my email", @"Yeah"];
            UIImage *imagetoshare = [UIImage imageNamed:@"or_btn.png"];//this is your image to share
        
            if (imagetoshare != nil) {
                activityItems = @[imagetoshare, texttoshare];
            } else {
                activityItems = @[texttoshare];
            }
            NSArray *exTypes = @[UIActivityTypeAssignToContact, UIActivityTypePrint, UIActivityTypeCopyToPasteboard, UIActivityTypeSaveToCameraRoll];
        
        
            UIActivityViewController *activityController = [[UIActivityViewController alloc] initWithActivityItems:activityItems applicationActivities:nil];
            activityController.excludedActivityTypes = exTypes;
        
        
            [activityController setValue:@"Your email Subject" forKey:@"subject"];
            
            [self presentViewController:activityController animated:NO completion:nil];

        
        
        screenType=nil;
    }
    
    else{
        
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
       // NSString *loginInt = [defaults objectForKey:@"loginIntType"];
        int loggedIn = (int)[defaults integerForKey:@"isloggedIn"];

         if (loggedIn !=1)
        {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [self performSegueWithIdentifier:@"tologin" sender:nil];
            });

            
            NSLog(@"go to login view***** ");
        }
        else{
            NSLog(@"back to login view***** ");
     
        }
    }
    
}


-(void)logoutFuction
{
    
    if (APP_DELEGATE.isServerReachable)
    {
    
    [self.connection cancel];
    
    NSMutableData *data = [[NSMutableData alloc] init];
    self.receivedData = data;
    
    NSURL *url = [NSURL URLWithString:(API_URL @"logout")];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[url standardizedURL]];
    
    [request setHTTPMethod:@"POST"];
    NSString *postData = [NSString stringWithFormat:@"app_token=%@",access_token];
    
    
    [request setValue:@"application/x-www-form-urlencoded; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    
    //set post data of request
    [request setHTTPBody:[postData dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    self.connection = connection;
    
    [connection start];
        
    }
    
    else
    {
        [[[UIAlertView alloc]initWithTitle:@"OutNAbout" message:@"Internet connection is not available. Please try again."  delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil]show];
    }

}



#pragma mark - NSURLConnection delegate methods =  ==  == = = = = = = =  = = = = = = = = = = = = = = = = = = = = = = = = = = =

-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data{
    [self.receivedData appendData:data];
}


-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error{
    
    NSLog(@"eeeeeeee  %@" , error);
}



-(void)connectionDidFinishLoading:(NSURLConnection *)connection{
    
    //NSString *htmlSTR = [[NSString alloc] initWithData:self.receivedData
                              //            encoding:NSUTF8StringEncoding];
    // NSLog(@" ffff     %@" , htmlSTR);
    
    
   // NSError *error = nil;
   //NSDictionary *jsonArray = [NSJSONSerialization JSONObjectWithData: self.receivedData options: NSJSONReadingMutableContainers error: &error];  //I am using sbjson to parse

    
}



- (void)resetDefaults {
    NSUserDefaults * defs = [NSUserDefaults standardUserDefaults];
    // [defs removeObjectForKey:@"username"];
    //    [defs removeObjectForKey:@"password"];
    NSDictionary * dict = [defs dictionaryRepresentation];
    for (id key in dict) {
        [defs removeObjectForKey:key];
    }
    [defs synchronize];
}


-(void)dismissModalStack {
    UIViewController *vc = self.presentingViewController;
    while (vc.presentingViewController) {
        vc = vc.presentingViewController;
    }
    [vc dismissViewControllerAnimated:YES completion:NULL];
}




- (void)open
{
    [self.drawerTransition presentDrawerViewController];
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
