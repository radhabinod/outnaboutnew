//
//  SearchEventsVC.h
//  OutNAbout
//
//  Created by Ram Kumar on 21/07/16.
//  Copyright © 2016 tecHindustan. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SecondDelegate <NSObject>
-(void) secondViewControllerDismissed:(NSString *)stringForFirst;

@end

@interface SearchEventsVC : UIViewController

@property (weak, nonatomic) IBOutlet UITextField *categoryTF;
@property (weak, nonatomic) IBOutlet UITextField *stateTF;
@property (weak, nonatomic) IBOutlet UITextField *dateTF;
@property (weak, nonatomic) IBOutlet UITextField *eventNameTF;
@property (weak, nonatomic) IBOutlet UIButton *submitButton;

@property (weak, nonatomic) IBOutlet UITextField *toTimeTF;
@property (weak, nonatomic) IBOutlet UITextField *fromTimeTF;
@property (strong, nonatomic)  UIBarButtonItem *rightBarButtonItem;


@property(nonatomic,strong) NSString *catValue;
@property(nonatomic,strong) NSString *stateValue;

@property (weak, nonatomic) IBOutlet UIView *dimBackgroundView;
@property (weak, nonatomic) IBOutlet UIView *popupView;
@property (weak, nonatomic) IBOutlet UITableView *tableViewListing;
@property (weak, nonatomic) IBOutlet UILabel *selectHeadingLabel;


@property (weak, nonatomic) IBOutlet UIView *viewOnSelectDate;
@property (weak, nonatomic) IBOutlet UIView *viewOnToTime;
@property (weak, nonatomic) IBOutlet UIView *viewOnFromTime;

@property (weak, nonatomic) IBOutlet UIView *backTapCategoryView;
@property (weak, nonatomic) IBOutlet UIView *backTapStateView;


@property (nonatomic, assign) id<SecondDelegate>    myDelegate;

@end
