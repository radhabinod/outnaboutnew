//
//  AppDelegate.h
//  Out N about
//
//  Created by Ram Kumar on 03/05/16.
//  Copyright © 2016 tecHindustan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import <Google/SignIn.h>
@class Reachability;

@interface AppDelegate : UIResponder <UIApplicationDelegate,GIDSignInDelegate>

{
    Reachability *serverReachability;
}

@property (strong, nonatomic) UIWindow *window;
@property(nonatomic,strong)  NSString *Devtoken;

@property (assign, nonatomic) BOOL isServerReachable;
@property (assign, nonatomic) BOOL isWAN;

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;


@end

