//
//  SecondTabBarVC.m
//  Out N about
//
//  Created by Ram Kumar on 10/05/16.
//  Copyright © 2016 tecHindustan. All rights reserved.
//

#import "AllEventsTabBarVC.h"
#import "SingleEventVC.h"
#import "ALToastView.h"
#import <QuartzCore/QuartzCore.h>
#import "SDWebImageManager.h"
#import "CarbonKit.h"
#import "CommonAPI's.pch"
#import "SVProgressHUD.h"
#import "AppDelegate.h"

#import "UITableView+DragLoad.h"

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wdeprecated-declarations"



//@class FrameObservingView;
//
//@protocol FrameObservingViewDelegate <NSObject>
//- (void)frameObservingViewFrameChanged:(FrameObservingView *)view;
//@end
//
//@interface FrameObservingView : UIView
//@property (nonatomic,assign) id<FrameObservingViewDelegate>delegate;
//@end

//@implementation FrameObservingView
//- (void)setFrame:(CGRect)frame
//{
//    [super setFrame:frame];
//    [self.delegate frameObservingViewFrameChanged:self];
//}
//@end


@interface AllEventsTabBarVC ()<CLLocationManagerDelegate,UITableViewDragLoadDelegate>
@property (nonatomic, strong) CLLocation* currentLocation;


@end

@implementation AllEventsTabBarVC
{
    NSMutableArray *updatedArray;
    
    NSMutableArray *imagesArray;
    NSMutableArray *titleLabel;
    NSMutableArray *venueArray;
    NSMutableArray *dateArray;
    NSMutableArray *startTimeArray;
    NSMutableArray *endTimeArray;
    NSMutableArray *endDateArray;

    NSMutableArray *categoryArray;
    NSMutableArray *eventIdArray;
    NSMutableArray *favoriteArray;
    NSMutableArray *eventViewsArray;
    NSMutableArray *costArray;
    NSMutableArray *descriptionArray;
    NSMutableArray *shareArray;
    NSMutableArray *stateArray;
    NSMutableArray *latitudeArray;
    NSMutableArray *longitudeArray;
    
    NSMutableArray *eventTypeArray;
    NSMutableArray *currencyArray;
    NSArray *reverseDate;
    NSString *filterType;
    NSString *access_token;
    
    UIActivityIndicatorView *activityView;
    UIView* loadingView;

    UIRefreshControl *refresh;
    CGRect rectText;
    CGRect rectFavImage;
    CGRect rectCatText;
    CGRect rectSponsorImage;
    
    CLLocationManager *locationManager;
    CLGeocoder *geocoder;
    CLPlacemark *placemark;
    MKPointAnnotation *annotation;
    NSString *latitude;
    NSString *longitude;
    int limit;
    
    NSString *timezoneString;
    NSNumber *eventsCount;
    
    CarbonSwipeRefresh *newRefresh;
}


//- (void)frameObservingViewFrameChanged:(FrameObservingView *)view
//{
//    _imagesTableView.frame = self.view.bounds;
//}


- (void)viewDidLoad {
    [super viewDidLoad];
    
//    FrameObservingView *frameObservingView = [[FrameObservingView alloc] init];
//    frameObservingView.delegate = self;
//    self.view = frameObservingView;

    limit = 0;
    eventsCount=@0;

    updatedArray = [[NSMutableArray alloc]init];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    access_token = [defaults objectForKey:@"access_token"];
    timezoneString= [defaults objectForKey:@"timezone"];
    
    NSLog(@" access token in tab view %@",access_token );
    
    
//   refresh = [[UIRefreshControl alloc]init];
//   refresh.backgroundColor = [UIColor blackColor];
//   refresh.tintColor = [UIColor whiteColor];
//   //[refresh addTarget:self action:@selector(refreshTable) forControlEvents:UIControlEventValueChanged];
//  //  _imagesTableView.tableFooterView =refresh;
//   [_imagesTableView addSubview:refresh];
    
    [self.view addSubview:_dimCountBackground];
    

    newRefresh = [[CarbonSwipeRefresh alloc] initWithScrollView:_imagesTableView];
    [self.view addSubview:newRefresh];
    
    [newRefresh setColors:@[
                         [UIColor blackColor],
                         [UIColor redColor],
                         [UIColor colorWithRed:0.95 green:0.28 blue:0.31 alpha:1.0],
                         [UIColor greenColor]
                         ]];
    
    [newRefresh addTarget:self action:@selector(refresh:) forControlEvents:UIControlEventValueChanged];


    
    
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(receiveToggleAuthUINotifications:)
     name:@"filterType1"
     object:nil];
    
    
    
    [self postMethod];
    [self createTableView];

    
}



- (void) receiveToggleAuthUINotifications:(NSNotification *) notification {
    if ([[notification name] isEqualToString:@"filterType1"]) {
        
        latitude = @"";
        longitude = @"";
        
        if ([notification userInfo][@"byDate"])
        {
            filterType =@"1";
            limit=0;
            [updatedArray removeAllObjects];
            [self filterPostMethod];
           // [_imagesTableView reloadData];

        }else if ([notification userInfo][@"byPrice"])
        {
            filterType =@"2";
            limit=0;
            [updatedArray removeAllObjects];
            [self filterPostMethod];
           // [_imagesTableView reloadData];
            
        }
        else if ([notification userInfo][@"byNear"])
        {
            filterType =@"4";
            limit=0;
            [updatedArray removeAllObjects];
            [self getCurrentLoaction];
           // [_imagesTableView reloadData];
        }
        else{
            filterType =@"";
            limit=0;
            
            [updatedArray removeAllObjects];
            [self postMethod];
           // [_imagesTableView reloadData];
        }
        

    }
}


- (void)refresh:(id)sender {
    NSLog(@"REFRESH");
    if (APP_DELEGATE.isServerReachable)
    {
  
        [newRefresh startRefreshing];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 5 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            
            limit=0;
            [updatedArray removeAllObjects];
            [self postMethod];
            [_imagesTableView reloadData];
 
            [self endRefreshing];
            
        });

    }
    else
    {
        [newRefresh endRefreshing];
        [SVProgressHUD dismiss];
        _eventCountLabel.hidden=YES;
        [updatedArray removeAllObjects];
        [_imagesTableView reloadData];
        [[[UIAlertView alloc]initWithTitle:@"OutNAbout" message:@"Internet connection is not available. Please try again."  delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil]show];
    }
}


- (void)endRefreshing {
    [newRefresh endRefreshing];
    
}



-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    

    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    int loggedIn = (int)[defaults integerForKey:@"isloggedIn"];
    
    if (loggedIn !=1)
    {
        
    }
    else{
        //eventsCount=@0;
        //limit=0;
        //[updatedArray removeAllObjects];

        //[_imagesTableView reloadData];
    }

    
}




- (void)finishLoadMore
{
    
    if (filterType.length) {
        [SVProgressHUD showWithStatus:@"Loading Events..." maskType:SVProgressHUDMaskTypeBlack];

        [self filterPostMethod];
    }
    else
    {
        [self postMethod];
    }

}


//-(void)refreshTable
//{
//    [refresh beginRefreshing];
//    [self postMethod];
//    [_imagesTableView reloadData];
//    
//    if (refresh) {
//        
//        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
//        [formatter setDateFormat:@"MMM d, h:mm a"];
//        NSString *title = [NSString stringWithFormat:@"Last update: %@", [formatter stringFromDate:[NSDate date]]];
//        NSDictionary *attrsDictionary = [NSDictionary dictionaryWithObject:[UIColor whiteColor]
//                                                                    forKey:NSForegroundColorAttributeName];
//        NSAttributedString *attributedTitle = [[NSAttributedString alloc] initWithString:title attributes:attrsDictionary];
//        refresh.attributedTitle = attributedTitle;
//        
//        [ALToastView toastInView:self.view withText:@"Events Updated"];
//    }
//    
//    [refresh endRefreshing];
//}
//
//



-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}



-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];

}



-(void)getCurrentLoaction
{
    locationManager = [CLLocationManager new];
    locationManager.delegate = self;
    
    if ([locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)])
    {
        [locationManager requestWhenInUseAuthorization];
    }
    locationManager.desiredAccuracy = kCLLocationAccuracyKilometer;
    locationManager.activityType = CLActivityTypeFitness;
    locationManager.distanceFilter = 500;
    [locationManager startUpdatingLocation];
}



- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    _currentLocation = [locations objectAtIndex:0];
    [locationManager stopUpdatingLocation];
    geocoder = [[CLGeocoder alloc] init];
    
    
    CLLocationCoordinate2D coordinate = [_currentLocation coordinate];

    
    latitude = [NSString stringWithFormat:@"%f", coordinate.latitude];
    longitude = [NSString stringWithFormat:@"%f", coordinate.longitude];
    
    NSString *display_coordinates=[NSString stringWithFormat:@"Latitude %f and Longitude %f",coordinate.longitude,coordinate.latitude];
    
    annotation = [[MKPointAnnotation alloc] init];
    [annotation setCoordinate:coordinate];
    
    NSLog(@"latitude :%@",latitude);
    NSLog(@"dLongitude : %@",longitude);
    
    [geocoder reverseGeocodeLocation:_currentLocation completionHandler:^(NSArray *placemarks, NSError *error)
     {
         if (!(error))
         {
             placemark = [placemarks objectAtIndex:0];
             
             
             NSLog(@"\nCurrent Location Detected\n");
             NSLog(@"placemark %@",placemark);

             NSString *Area = [[NSString alloc]initWithString:placemark.locality];
             NSLog(@"Timezone -%@",placemark.timeZone);

             [annotation setTitle:Area];
             [annotation setSubtitle:display_coordinates];
             
             [self filterPostMethod];

         }
         else
         {
             NSLog(@"Geocode failed with error %@", error);
             NSLog(@"\nCurrent Location Not Detected\n");
         }
     }];
    
}





-(void)filterPostMethod
{

    if (APP_DELEGATE.isServerReachable)
    {
    [self.connection cancel];
    
    NSMutableData *datas = [[NSMutableData alloc] init];
    self.receivedData = datas;
    
    NSURL *url = [NSURL URLWithString:(API_URL @"allevents")];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[url standardizedURL]];
    
    [request setHTTPMethod:@"POST"];
        
        
    NSString *postData = [NSString stringWithFormat:@"app_token=%@&filter=%@&latitude=%@&longitude=%@&startlimit=%d",access_token,filterType,latitude,longitude,limit];
    
    
    [request setValue:@"application/x-www-form-urlencoded; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    
    //set post data of request
    [request setHTTPBody:[postData dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    self.connection = connection;
    
    [connection start];
    }
    else
    {
        [SVProgressHUD dismiss];
       [_imagesTableView finishLoadMore];
        [[[UIAlertView alloc]initWithTitle:@"OutNAbout" message:@"Internet connection is not available. Please try again."  delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil]show];
    }

}




-(void)postMethod
{
    [SVProgressHUD showWithStatus:@"Loading Events..." maskType:SVProgressHUDMaskTypeBlack];

    if (APP_DELEGATE.isServerReachable)
    {
      [self.connection cancel];
        
    NSMutableData *datas = [[NSMutableData alloc] init];
    self.receivedData = datas;
    
    NSURL *url = [NSURL URLWithString:(API_URL @"allevents")];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[url standardizedURL]];
    
    [request setHTTPMethod:@"POST"];
    NSString *postData = [NSString stringWithFormat:@"app_token=%@&startlimit=%d",access_token,limit];

    
    [request setValue:@"application/x-www-form-urlencoded; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    
    //set post data of request
    [request setHTTPBody:[postData dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    self.connection = connection;
    
    [connection start];
        
    }
    
    else
    {
        [SVProgressHUD dismiss];
        [_imagesTableView finishLoadMore];
         [[[UIAlertView alloc]initWithTitle:@"OutNAbout" message:@"Internet connection is not available. Please try again."  delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil]show];
    }
    
}



#pragma mark - NSURLConnection delegate methods =  ==  == = = = = = = =  = = = = = = = = = = = = = = = = = = = = = = = = = = =

-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)datas{
    [self.receivedData appendData:datas];
}


-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error{
    
    [SVProgressHUD dismiss];
    [_imagesTableView finishLoadMore];
    [[[UIAlertView alloc]initWithTitle:@"OutNAbout" message:@"The network connection was lost."  delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil]show];

    NSLog(@"eeeeeeee  %@" , error);
}



-(void)connectionDidFinishLoading:(NSURLConnection *)connection{
    
   // NSString *htmlSTR = [[NSString alloc] initWithData:self.receivedData
                        //                      encoding:NSUTF8StringEncoding];
    // NSLog(@" ffff     %@" , htmlSTR);
    
   
    NSError *error = nil;
    NSDictionary *jsonArray = [NSJSONSerialization JSONObjectWithData: self.receivedData options: NSJSONReadingMutableContainers error: &error];
    
    //  NSString *code = [jsonArray[@"code"] stringValue];
    NSMutableArray *data = jsonArray[@"data"];
    
//    NSUserDefaults *newDefaults = [NSUserDefaults standardUserDefaults];
//    NSString *whenBack = [newDefaults objectForKey:@"backToView"];
//    
//    if ([whenBack length] == 0) {
//    
//    limit=limit+5;

   // NSLog(@"All events response ****** %@  count :%lu",jsonArray,(unsigned long)data.count);

    [updatedArray addObjectsFromArray:data];
        
   // }
    
   

   //  [newDefaults removeObjectForKey:@"backToView"];
    //NSLog(@"limit no %d array count %lu",limit,(unsigned long)updatedArray.count);

    
    NSString *msg = jsonArray[@"msg"];
    NSLog(@"updates ***%@",updatedArray);
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    int loggedIn = (int)[defaults integerForKey:@"isloggedIn"];
    
    if (loggedIn !=1)
    {
        //login is 0
    }
    else{
        
        if ([msg isEqualToString:@"Permission denied Invalid access token!"]) {
            // [loadingView setHidden:YES];
            [SVProgressHUD dismiss];
            _eventCountLabel.hidden=YES;
            [[[UIAlertView alloc]initWithTitle:@"Warning!" message:@"Permission denied login again." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil]show];
        }else{
        }
        
    }
    
    imagesArray = [[NSMutableArray alloc]init];
    titleLabel = [[NSMutableArray alloc]init];
    venueArray = [[NSMutableArray alloc]init];
    dateArray = [[NSMutableArray alloc]init];
    startTimeArray = [[NSMutableArray alloc]init];
    categoryArray = [[NSMutableArray alloc]init];
    endTimeArray = [[NSMutableArray alloc]init];
    eventIdArray = [[NSMutableArray alloc]init];
    eventViewsArray = [[NSMutableArray alloc]init];
    favoriteArray = [[NSMutableArray alloc]init];
    costArray = [[NSMutableArray alloc]init];
    descriptionArray = [[NSMutableArray alloc]init];
    endDateArray = [[NSMutableArray alloc]init];
    shareArray= [[NSMutableArray alloc]init];
    stateArray= [[NSMutableArray alloc]init];
    latitudeArray= [[NSMutableArray alloc]init];
    longitudeArray= [[NSMutableArray alloc]init];
    eventTypeArray= [[NSMutableArray alloc]init];
    currencyArray= [[NSMutableArray alloc]init];
    
    
    NSString *msgs = jsonArray[@"msg"];
    if ([msgs isEqualToString:@"No record found!"]) {
        [SVProgressHUD dismiss];
        [_imagesTableView finishLoadMore];
        _eventCountLabel.hidden=YES;
        [[[UIAlertView alloc]initWithTitle:@"Message!" message:msg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil]show];
    }
    else{
        
        eventsCount = jsonArray[@"count"];
        NSLog(@"count&&&&& :%@",eventsCount);
        _eventCountLabel.hidden=NO;
        _eventCountLabel.text = [NSString stringWithFormat:@"Showing Events : %lu/%@",(unsigned long)updatedArray.count, eventsCount];
        
        if (updatedArray.count==[eventsCount doubleValue]) {
            _imagesTableView.showLoadMoreView=NO;
        }
        else
        {
            _imagesTableView.showLoadMoreView=YES;

        }

        

    for (NSDictionary *d  in  updatedArray) {
        NSString *imageString = d[@"image"];
        [imagesArray  addObject:imageString];
        
        NSString *titleText = d[@"title"];
        [titleLabel addObject:titleText];
        
        NSString *venueString = d[@"venue"];
        [venueArray  addObject:venueString];
        
        NSString *stateString = d[@"state"];
        [stateArray addObject:stateString];
        
        NSString *currencyString = d[@"currency"];
        [currencyArray addObject:currencyString];

        NSString *shareString = d[@"share"];
        [shareArray  addObject:shareString];
        
        NSString *latitudeString = d[@"latitude"];
        [latitudeArray  addObject:latitudeString];
        
        NSString *longitudeString = d[@"longitude"];
        [longitudeArray  addObject:longitudeString];
        
        NSString *eventTypeString = d[@"event_type"];
        [eventTypeArray  addObject:eventTypeString];
        
        NSString *dateString = d[@"date"];
        [dateArray addObject:dateString];
        
        NSString *strtTimeString = d[@"start_time"];
        [startTimeArray  addObject:strtTimeString];
        
        NSString *endTimeString = d[@"end_time"];
        [endTimeArray  addObject:endTimeString];
        
        NSString *endDateString = d[@"enddate"];
        [endDateArray  addObject:endDateString];
        
        NSString *eventIdString = d[@"event_id"];
        [eventIdArray  addObject:eventIdString];
        
        NSString *eventViewsString = d[@"event_views"];
        [eventViewsArray  addObject:eventViewsString];
        
        NSString *favoriteString = d[@"favorite"];
        [favoriteArray  addObject:favoriteString];
        
        NSString *costString = d[@"cost"];
        [costArray  addObject:costString];
        
        NSString *descriptionString = d[@"description"];
        [descriptionArray addObject:descriptionString];
        

        NSString *cats = @"";
        NSMutableArray *response =d[@"categories"];
        
        for (NSDictionary *dict in response) {
            
            cats = [cats stringByAppendingString:[NSString stringWithFormat:@"%@,",dict[@"cat_title"]]];
            
        }
        
        if([[cats substringFromIndex:cats.length - 1] isEqualToString:@","])
        {
            cats = [cats substringToIndex:cats.length - 1];
        }
        [categoryArray addObject:cats];

        
//        NSLog(@"Categories in ALl events@@@@ %@",categoryArray);
        
    }
    }
    
     [_imagesTableView reloadData];
    
    
}






-(void)createTableView
{

    self.imagesTableView.delegate=self;
    self.imagesTableView.dataSource=self;
    self.imagesTableView.backgroundColor=[UIColor blackColor];

    [_imagesTableView setDragDelegate:self refreshDatePermanentKey:@"FriendList"];
    _imagesTableView.showLoadMoreView = YES;
    _imagesTableView.showRefreshView=NO;
    
    self.imagesTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view insertSubview:self.imagesTableView atIndex:0];
    
  //  [self.view addSubview:self.imagesTableView];

    
}



#pragma mark - tableview delegate methods ================= ================ ================ ================ ================


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        return  300;
    }
    else{
        return  190;
    }
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    NSInteger numOfSections = 0;
    if ([imagesArray count] > 0)
    {
       // self.imagesTableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
        numOfSections                 = 1;
        self.imagesTableView.backgroundView = nil;
    }
    else
    {
        UILabel *noDataLabel         = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.imagesTableView.bounds.size.width, self.imagesTableView.bounds.size.height)];
        noDataLabel.text             = @"No events available now.";
        noDataLabel.textColor        = [UIColor blackColor];
        noDataLabel.textAlignment    = NSTextAlignmentCenter;
        noDataLabel.font = [UIFont fontWithName:@"EuphemiaUCAS-Bold " size:16.0f];
        self.imagesTableView.backgroundView = noDataLabel;
        self.imagesTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    }
    return numOfSections;
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return imagesArray.count;
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil)
    {
        
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        self.imagesTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        
        cell.textLabel.text = nil;
        cell.imageView.image = nil;
        cell.layer.borderWidth = 5.0;
        cell.layer.borderColor = [UIColor colorWithRed:0.13 green:0.19 blue:0.25 alpha:1.0].CGColor;
        
       
        
        UILabel *TitleTextLabel = [[UILabel alloc]initWithFrame:CGRectMake(cell.frame.origin.x+12, 50, cell.frame.size.width-20, 50)];
        TitleTextLabel.tag = 101;
        [cell.contentView addSubview:TitleTextLabel];
        
        UILabel *dateLabel = [[UILabel alloc]initWithFrame:CGRectMake(cell.frame.origin.x+35, 75, 300, 50)];
        dateLabel.tag = 102;
        [cell.contentView addSubview:dateLabel];
        
        UILabel *endDateLabel = [[UILabel alloc]initWithFrame:CGRectMake(cell.frame.origin.x+122, 75, 300, 50)];
        endDateLabel.tag = 109;
        [cell.contentView addSubview:endDateLabel];
        
        UILabel *startTimeLabel = [[UILabel alloc]initWithFrame:CGRectMake(cell.frame.origin.x+35, 95, 300, 50)];
        startTimeLabel.tag = 103;
        [cell.contentView addSubview:startTimeLabel];

        UILabel *timezoneLabel = [[UILabel alloc]initWithFrame:CGRectMake(cell.frame.origin.x+100, 95, cell.frame.size.width-20, 50)];
        timezoneLabel.tag = 108;
        [cell.contentView addSubview:timezoneLabel];
        
        UILabel *stateLabel = [[UILabel alloc]initWithFrame:CGRectMake(cell.frame.origin.x+35, 115, 283, 50)];
        stateLabel.tag = 104;
        [cell.contentView addSubview:stateLabel];
        
        if (IS_IPHONE_5)
        {
            rectSponsorImage =CGRectMake(cell.frame.size.width - 53, 5, 48, 48);
        }else{
            
            rectSponsorImage =CGRectMake(cell.frame.size.width - 2, 7, 50, 50);
        }
        UIImageView *sponsorImage = [[UIImageView alloc]initWithFrame:rectSponsorImage];
        sponsorImage.tag = 105;
        [cell.contentView addSubview:sponsorImage];
        
        
       UITextField *catField = [[UITextField alloc]initWithFrame:rectCatText];
        catField.tag = 1023;
        [cell.contentView addSubview:catField];
        
        
        if (IS_IPHONE_5)
        {
        rectText = CGRectMake(cell.frame.size.width - 55, 152, 60, 30);
        }else
        {
        rectText = CGRectMake(cell.frame.size.width - 20, 150, 80, 30);
        }
        UILabel *viewsLabel = [[UILabel alloc]initWithFrame:rectText];
        viewsLabel.tag = 106;
        [cell.contentView addSubview:viewsLabel];
        
        
        if (IS_IPHONE_5)
        {
            rectFavImage = CGRectMake(cell.frame.size.width - 65, 162, 23, 10);
        }else{
        rectFavImage = CGRectMake(cell.frame.size.width - 25, 160, 23, 10);
        }
        UIImageView *favImage = [[UIImageView alloc]initWithFrame:rectFavImage];
        favImage.tag = 1024;
        [cell.contentView addSubview:favImage];
        

//        UIView *_lineDiv=[[UIView alloc] init];
//        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
//        {
//            _lineDiv.frame=CGRectMake(0, 300.0f, self.view.frame.size.width, 5.0);
//        }else{
//            _lineDiv.frame=CGRectMake(0, 190.0f, self.view.frame.size.width, 1.0);
//            
//        }
//        _lineDiv.tag = 1005;
//        //_lineDiv.backgroundColor=[UIColor colorWithRed:221.0f/255.0 green:223.0f/255.0 blue:224.0f/255.0 alpha:1.0];
//        [cell.contentView addSubview:_lineDiv];
        
        
        cell.tag = indexPath.row ;
        
    }

    
    
    
    
    NSString *arrayString = [NSString stringWithFormat:(FULL_IMAGE @"%@"),[imagesArray objectAtIndex:indexPath.row]];
    
    NSURL *url =[NSURL URLWithString:arrayString];
    
    SDWebImageManager *manager = [SDWebImageManager sharedManager];
    [manager downloadImageWithURL:url
                          options:0
                         progress:^(NSInteger receivedSize, NSInteger  expectedSize) {
                             // progression tracking code
                         }
                        completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                            if (image) {
                                dispatch_async(dispatch_get_main_queue(), ^{
                                    
                                    
                                    
                                    
                                    UITableViewCell *updateCell = (id)[tableView cellForRowAtIndexPath:indexPath];
                                    if (updateCell)
                                        cell.backgroundView = [[UIImageView alloc] initWithImage:image];

                                    
                                    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, cell.frame.size.width, cell.frame.size.height)];
                                    CAGradientLayer *gradient = [CAGradientLayer layer];
                                    gradient.frame = view.bounds;
                                    
                                    gradient.colors = [NSArray arrayWithObjects:(id)[[UIColor clearColor] CGColor], (id)[[UIColor colorWithRed:0.10 green:0.10 blue:0.10 alpha:1.0] CGColor], nil];
                                    
                                    
                                    [cell.backgroundView.layer insertSublayer:gradient atIndex:0];

                                    
                                    
                                    
                                    
                                });
                            }
                            
                            //[loadingView setHidden:YES];
                            [SVProgressHUD dismiss];
                            [_imagesTableView finishLoadMore];
                            
                          //  [ALToastView toastInView:self.view withText:@"Events Updated"];
                    }
     ];
    
    
    
    
    UIImageView *favImage = (UIImageView *)[cell viewWithTag:1024];
    favImage.image=[UIImage imageNamed:@"ic_view@3x.png"];
    
    UIImageView *sponsorImage = (UIImageView *)[cell viewWithTag:105];
    NSString *eventType = [eventTypeArray objectAtIndex:indexPath.row];
    if ([eventType isEqualToString:@"1"]) {
    sponsorImage.image=[UIImage imageNamed:@"ic_sponsor@3x.png"];
    }else{
    sponsorImage.image=nil;
    }
    
    UIImageView *timeImage = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"ic_timer@3x.png"]];
    timeImage.frame = CGRectMake(12, 90, 15, 15);
    [cell.contentView addSubview:timeImage];
    
    UIImageView *timeImage1 = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"ic_timer@3x.png"]];
    timeImage1.frame = CGRectMake(12, 110, 15, 15);
    [cell.contentView addSubview:timeImage1];

    UIImageView *locationImage = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"ic_location@3x.png"]];
    locationImage.frame = CGRectMake(12, 130, 15, 18);
    [cell.contentView addSubview:locationImage];
    
    UILabel *timezoneLabel = (UILabel *)[cell viewWithTag:108];
    timezoneLabel.text = [NSString stringWithFormat:@"%@ ",timezoneString] ;
    [timezoneLabel setTextColor:[UIColor whiteColor]];
    timezoneLabel.numberOfLines = 0;
    timezoneLabel.backgroundColor = [UIColor clearColor];
    [timezoneLabel setFont:[UIFont fontWithName:@"Avenir-Medium" size:13.0f]];
    
    UILabel *TitleTextLabel = (UILabel *)[cell viewWithTag:101];
    TitleTextLabel.text = [NSString stringWithFormat:@"%@ ",[titleLabel objectAtIndex:indexPath.row]];
    [TitleTextLabel setTextColor:[UIColor whiteColor]];
    TitleTextLabel.backgroundColor = [UIColor clearColor];
    [TitleTextLabel setFont:[UIFont fontWithName:@"EuphemiaUCAS-Bold" size:17.0f]];
    
    
    NSString *startDateMatch = [dateArray objectAtIndex:indexPath.row];
    NSString *endDateMatch =  [endDateArray objectAtIndex:indexPath.row];
    
    UILabel *dateLabel = (UILabel *)[cell viewWithTag:102];
    UILabel *endDateLabel = (UILabel *)[cell viewWithTag:109];
     if ([startDateMatch isEqualToString:endDateMatch]) {
        endDateLabel.text = @"";
        dateLabel.text = [NSString stringWithFormat:@"%@ ",[dateArray objectAtIndex:indexPath.row]];
    }
    else
    {
    dateLabel.text = [NSString stringWithFormat:@"%@ -",[dateArray objectAtIndex:indexPath.row]];
    endDateLabel.text = [NSString stringWithFormat:@"%@",[endDateArray objectAtIndex:indexPath.row]];
    }
    
    [endDateLabel setTextColor:[UIColor whiteColor]];
    [endDateLabel setFont:[UIFont fontWithName:@"Avenir-Medium" size:13.0f]];
    
    [dateLabel setTextColor:[UIColor whiteColor]];
    [dateLabel setFont:[UIFont fontWithName:@"Avenir-Medium" size:13.0f]];
    

    UILabel *startTimeLabel = (UILabel *)[cell viewWithTag:103];
    startTimeLabel.text = [NSString stringWithFormat:@"%@ ",[startTimeArray objectAtIndex:indexPath.row]];
    [startTimeLabel setTextColor:[UIColor whiteColor]];
    [startTimeLabel setFont:[UIFont fontWithName:@"Avenir-Medium" size:13.0f]];
    
    UILabel *stateLabel = (UILabel *)[cell viewWithTag:104];
    stateLabel.text = [NSString stringWithFormat:@"%@ ",[stateArray objectAtIndex:indexPath.row]];
    [stateLabel setTextColor:[UIColor whiteColor]];
    [stateLabel setFont:[UIFont fontWithName:@"Avenir-Medium" size:13.0f]];
  
    
    UITextField *catField=(UITextField *)[cell viewWithTag:1023];
  // catField.text = [NSString stringWithFormat:@"%@  ",[categoryArray objectAtIndex:indexPath.row]];
    catField.borderStyle = UITextBorderStyleLine;
    catField.font= [UIFont systemFontOfSize:12.0f];
    catField.layer.sublayerTransform = CATransform3DMakeTranslation(5, 0, 0);
    catField.backgroundColor = [UIColor clearColor];
    catField.enabled=NO;
    [catField setAllowsEditingTextAttributes:NO];
    catField.textColor =[UIColor whiteColor];
    catField.layer.borderColor = [UIColor whiteColor].CGColor;
    catField.layer.borderWidth = 1.0f;
    

    NSString *name = [categoryArray objectAtIndex:indexPath.row];
    if (IS_IPHONE_6)
    {
        NSRange stringRange = {0, MIN([name length], 45)};
        catField.text = [name substringWithRange:stringRange];
    }
    else{
        NSRange stringRange = {0, MIN([name length], 38)};
        catField.text = [name substringWithRange:stringRange];
    }

    CGFloat width =  [catField.text sizeWithFont:catField.font].width;
    rectCatText = CGRectMake(10, 155, width+10,26);
    catField.frame=rectCatText;
    

    UILabel *viewLabel = (UILabel *)[cell viewWithTag:106];
    viewLabel.text = [NSString stringWithFormat:@"%@ ",[eventViewsArray objectAtIndex:indexPath.row]];
    [viewLabel setTextColor:[UIColor whiteColor]];
    viewLabel.textAlignment = NSTextAlignmentCenter;
    viewLabel.backgroundColor = [UIColor clearColor];
    [viewLabel setFont:[UIFont fontWithName:@"Avenir-Medium" size:15.0f]];
    
//    UIView *_lineDiv=[[UIView alloc] init];
//    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
//    {
//        _lineDiv.frame=CGRectMake(0, 300.0f, self.view.frame.size.width, 5.0);
//    }else{
//        _lineDiv.frame=CGRectMake(0, 190.0f, self.view.frame.size.width, 5.0);
//        
//    }
//    _lineDiv.backgroundColor=[UIColor blackColor];
//    //_lineDiv.backgroundColor=[UIColor colorWithRed:221.0f/255.0 green:223.0f/255.0 blue:224.0f/255.0 alpha:1.0];
//    [cell addSubview:_lineDiv];
    
    
    return cell;
    
}



-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UIStoryboard *story = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    SingleEventVC *singleEvent = [story instantiateViewControllerWithIdentifier:@"singleView"];
    singleEvent.singleTitle =[titleLabel objectAtIndex:indexPath.row];
    singleEvent.singleCategoryString =[categoryArray objectAtIndex:indexPath.row];
    singleEvent.costString =[costArray objectAtIndex:indexPath.row];
    singleEvent.descriptionString =[descriptionArray objectAtIndex:indexPath.row];
    singleEvent.eventIdString =[eventIdArray objectAtIndex:indexPath.row];
    singleEvent.shareLinkString = [shareArray objectAtIndex:indexPath.row];
    
    singleEvent.eventLatitude = [latitudeArray objectAtIndex:indexPath.row];
    singleEvent.eventLongitude = [longitudeArray objectAtIndex:indexPath.row];
    singleEvent.eventState = [stateArray objectAtIndex:indexPath.row];

    singleEvent.currencyString = [currencyArray objectAtIndex:indexPath.row];
    
    singleEvent.venueString =[venueArray objectAtIndex:indexPath.row];
    singleEvent.endDateOrTimeString =[NSString stringWithFormat:@" End Date: %@ (%@)",[endDateArray objectAtIndex:indexPath.row],[endTimeArray objectAtIndex:indexPath.row]];

    singleEvent.startDateOrTimeString =[NSString stringWithFormat:@" Start Date: %@ (%@)",[dateArray objectAtIndex:indexPath.row],[startTimeArray objectAtIndex:indexPath.row]];

    singleEvent.singleImageString =[NSString stringWithFormat:(FULL_IMAGE @"%@"),[imagesArray objectAtIndex:indexPath.row]];

    
    [self.navigationController pushViewController:singleEvent animated:YES];

    

}







-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    CAGradientLayer *grad = [CAGradientLayer layer];
    grad.frame = CGRectMake(0, cell.contentView.frame.size.height/2 - 24, cell.frame.size.width, 120);
    grad.colors = [NSArray arrayWithObjects:(id)[[UIColor clearColor] CGColor], (id)[[UIColor blackColor] CGColor],nil];

    [cell setBackgroundView:[[UIView alloc] init]];
    [cell.backgroundView.layer insertSublayer:grad atIndex:0];
    
//    CGRect rectFavImage = CGRectMake(cell.frame.size.width - 45, 153, 25, 10); // maybe even y and height is calculateable
//    UIImageView *favImage = [[UIImageView alloc]initWithFrame:rectFavImage];
//    favImage.image=[UIImage imageNamed:@"ic_view@3x.png"];
//    [cell addSubview:favImage];
    
//    CGRect rectShareImage = CGRectMake(cell.frame.size.width - 25, 153, 20, 20); // maybe even y and height is calculateable
//    UIImageView *shareImage = [[UIImageView alloc]initWithFrame:rectShareImage];
//    shareImage.image=[UIImage imageNamed:@"ic_favorite_disable@3x.png"];
//    [cell addSubview:shareImage];
    
}








#pragma mark - Drag delegate methods
- (void)dragTableDidTriggerLoadMore:(UITableView *)tableView
{
    //send load more request(generally network request) here
    
    [self performSelector:@selector(finishLoadMore) withObject:nil afterDelay:2];
}

- (void)dragTableLoadMoreCanceled:(UITableView *)tableView
{
    //cancel load more request(generally network request) here
    
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(finishLoadMore) object:nil];
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];

}



@end
