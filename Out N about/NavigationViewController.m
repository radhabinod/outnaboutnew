//
//  NavigationViewController.m
//  SlideOutMenu
//
//  Created by Jared Davidson on 7/14/14.
//  Copyright (c) 2014 Archetapp. All rights reserved.
//

#import "NavigationViewController.h"
#import "CommonAPI's.pch"
#import "ViewController.h"
#import "MyEventsVC.h"
#import "AppDelegate.h"
#import "CreateEventVC.h"
#import "FavoriteEventsVC.h"
#import "SettingScreenVC.h"
#import "UpcomingEventsVC.h"
#import "HistoryEventsVC.h"
#import "SDWebImageManager.h"
#import "EditProfileVC.h"
#import "MainTabbarController.h"
#import "PGDrawerTransition.h"

#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h>


#import "SVProgressHUD.h"


@interface NavigationViewController ()<UIGestureRecognizerDelegate,MFMailComposeViewControllerDelegate>

@property (nonatomic, strong) ViewController *viewControllerObject;
@property (nonatomic, strong) MyEventsVC *myEventsVCObject;
@property (nonatomic, strong) CreateEventVC *createEventVCObject;
@property (nonatomic, strong) FavoriteEventsVC *favEventVCObject;
@property (nonatomic, strong) SettingScreenVC *settingVCObject;
@property (nonatomic, strong) UpcomingEventsVC *upcomingsVCObject;
@property (nonatomic, strong) HistoryEventsVC *historyEventVCObject;
@property (nonatomic, strong) EditProfileVC *editProfileVCObject;
@property (nonatomic, strong) PGDrawerTransition *drawerTransition;
@property (nonatomic, strong) NavigationViewController *navigationViewController;


@end

@implementation NavigationViewController
{
    NSArray *menu;
    NSArray *menuIcons;
    
    NSString *firstName;
    NSString *lastName;
    UIImageView *proflePic;
    UILabel *nameLabel;
}


- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationController.navigationBar.barStyle = UIBarStyleBlackTranslucent;
    
    menu = @[@"All Events", @"My Events", @"My Favorite Events",@"My Upcoming Events",@"Event History",@"Create Event", @"Settings", @"Logout"];
    
    menuIcons = @[@"",@"ic_all_events@3x.png",@"ic_my_events.png",@"ic_fav_events@3x.png",@"ic_my_events.png",@"ic_event_history@3x.png",@"ic_create_events@3x.png",@"ic_setting@3x.png",@"ic_logout@3x.png"];
    
  
 }




- (IBAction)facebookAction:(id)sender {
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString: @"https://www.facebook.com/OutnAboutME"]];

}


- (IBAction)gmailAction:(id)sender {
    
    
   


//    NSString *emailTitle = @"Test Email";
//    // Email Content
//    NSString *messageBody = @"iOS programming is so fun!";
//    // To address
//    NSArray *toRecipents = [NSArray arrayWithObject:@"support@appcoda.com"];
//    
//    MFMailComposeViewController *mc = [[MFMailComposeViewController alloc] init];
//    mc.mailComposeDelegate = self;
//    [mc setSubject:emailTitle];
//    [mc setMessageBody:messageBody isHTML:NO];
//    [mc setToRecipients:toRecipents];
//    
//    // Present mail view controller on screen
//    //[self.view removeFromSuperview];
//    
////    CATransition *transition = [CATransition animation];
////    transition.duration = 0.3;
////    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
////    transition.type = kCATransitionFromBottom;//kCATransitionFade
////    transition.subtype = kCATransitionFromRight;
////    [self.view.window.layer addAnimation:transition forKey:nil];
//   [self presentViewController:mc animated:YES completion:NULL];
    
    
    
//    NSArray *activityItems;
//    
//    NSString *texttoshare = [NSString stringWithFormat:@"Hey bro! check this info.\n%@\n%@", @"Its my email", @"hahahahahhahahahahahhahahahahahahhhahadjdkkflkflwflwe"];
//    UIImage *imagetoshare = [UIImage imageNamed:@"or_btn.png"];//this is your image to share
//    
//    if (imagetoshare != nil) {
//        activityItems = @[imagetoshare, texttoshare];
//    } else {
//        activityItems = @[texttoshare];
//    }
//    NSArray *exTypes = @[UIActivityTypeAssignToContact, UIActivityTypePrint, UIActivityTypeCopyToPasteboard, UIActivityTypeSaveToCameraRoll];
//    
//    
//    UIActivityViewController *activityController = [[UIActivityViewController alloc] initWithActivityItems:activityItems applicationActivities:nil];
//    activityController.excludedActivityTypes = exTypes;
//    
//    
//    [activityController setValue:@"Your email Subject" forKey:@"subject"];
//    
//    [self presentViewController:activityController animated:NO completion:nil];
    
    
  //  [self Url:[NSURL URLWithString:@"OutNAbout,Let me recommend you this application.Enjoy! download this app from App Store."]];

    
    
    UIStoryboard *story = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    _viewControllerObject  = [story instantiateViewControllerWithIdentifier:@"viewController"];
    _viewControllerObject.screenType=@"mail";
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:_viewControllerObject];
    
    CATransition *transition = [CATransition animation];
    transition.duration = 0.3;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn];
    transition.type = kCATransitionMoveIn;//kCATransitionFade
    transition.subtype = kCATransitionFromRight;
    [self.view.window.layer addAnimation:transition forKey:nil];
    [self presentViewController:nav animated:NO completion:nil];
    [self.view removeFromSuperview];

}

- (void)Url:(NSURL *)url
{
    NSMutableArray *sharingItems = [NSMutableArray new];
    if (url) {
        [sharingItems addObject:url];
    }
    UIActivityViewController *activityController = [[UIActivityViewController alloc] initWithActivityItems:sharingItems applicationActivities:nil];
    [self presentViewController:activityController animated:YES completion:nil];
}


- (void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail sent");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail sent failure: %@", [error localizedDescription]);
            break;
        default:
            break;
    }
    
    // Close the Mail Interface
    [self dismissViewControllerAnimated:YES completion:NULL];
    
}




- (IBAction)instagramAction:(id)sender {
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString: @"https://www.instagram.com/outnaboutme/"]];

}

- (IBAction)twitterAction:(id)sender {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString: @"https://twitter.com/OutNAboutME"]];

}





- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - Table view data source



//- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//
//        return  50;
//}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return [menuIcons count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
   // NSString *cellIdentifier = [menu objectAtIndex:indexPath.row];
    NSString *cellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell==nil)
    {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        
        self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        cell.selectionStyle = UITableViewCellSelectionStyleDefault;

        UILabel *actionLabel = [[UILabel alloc] initWithFrame:CGRectMake(55.0f, 15.0f, 180.0f, 20.0f)];
        actionLabel.tag = 101; // define this as a constant
        [cell.contentView addSubview:actionLabel];
        
        UIImageView *listIcons = [[UIImageView alloc]initWithFrame:CGRectMake(15.0f, 15.0f, 20.0f, 20.0f)];
        listIcons.tag=102;
        [cell.contentView addSubview:listIcons];

    }

    
    
    
    if (indexPath.row==0)
    {
        
        UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(goToProfileScreen:)];
        tapGestureRecognizer.delegate=self;
        tapGestureRecognizer.numberOfTapsRequired = 1;
//        [proflePic setUserInteractionEnabled:YES];
        [cell addGestureRecognizer:tapGestureRecognizer];

        
     cell.backgroundColor = [UIColor colorWithRed:0.00 green:0.20 blue:0.40 alpha:1.0];
        cell.backgroundView= [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"xxxhdpi_pbg.png"]];
        
        
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        NSString *loginType = [defaults objectForKey:@"loginType"];
        NSString *UserPicture = [defaults objectForKey:@"user_pic"];
        NSString *imageURL = @"http://agiledevelopers.in/outnabout/uploads/";
        NSString *url_Img_FULL = [imageURL stringByAppendingPathComponent:UserPicture];
        NSURL *userImagePic = [NSURL URLWithString:url_Img_FULL];
        NSString *gpUserPic = [defaults objectForKey:@"gpUserPic"];
        NSURL *gpurl=[NSURL URLWithString:gpUserPic];
        
       // NSLog(@"google + user pic :%@",url);
        if (IS_IPHONE_6) {
            proflePic = [[UIImageView alloc]initWithFrame:CGRectMake(100, 30, 70, 70)];

        }else{
        proflePic = [[UIImageView alloc]initWithFrame:CGRectMake(90, 30, 70, 70)];
        }
        // proflePic.contentMode = UIViewContentModeScaleAspectFit;
        proflePic.layer.cornerRadius =proflePic.frame.size.width / 2;
        proflePic.layer.borderWidth=2;
       // proflePic.center = CGPointMake(cell.contentView.bounds.size.width/2,cell.contentView.bounds.size.height/2);
        proflePic.layer.borderColor = [UIColor whiteColor].CGColor;
        proflePic.clipsToBounds = YES;
        
        if ([loginType isEqualToString:@"fb"]) {

            
        FBSDKProfilePictureView *profilePictureview = [[FBSDKProfilePictureView alloc]initWithFrame:proflePic.frame];
        profilePictureview.layer.cornerRadius = 35.0f;
        profilePictureview.layer.masksToBounds = YES;
        profilePictureview.layer.borderWidth=2;
        profilePictureview.layer.borderColor = [UIColor whiteColor].CGColor;
        //[cell.contentView addSubview:proflePic];
        [cell.contentView addSubview:profilePictureview];
            
            firstName = [defaults objectForKey:@"fbFirstName"];
            lastName = [defaults objectForKey:@"fbLastName"];
        }
        
        else if ([loginType isEqualToString:@"gp"])
        {

            SDWebImageManager *manager = [SDWebImageManager sharedManager];
            [manager downloadImageWithURL:gpurl
                                  options:1
                                 progress:^(NSInteger receivedSize, NSInteger  expectedSize) {
                                 }
                                completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                                    if (image) {
                                        dispatch_async(dispatch_get_main_queue(), ^{
                                            proflePic.image = image;
                                            
                                        });
                                    }
                                }
             ];

            firstName = [defaults objectForKey:@"gpFirstName"];
            lastName = [defaults objectForKey:@"gpLastName"];
            [cell.contentView addSubview:proflePic];
        }
        
        else
        {
        firstName = [defaults objectForKey:@"first_name"];
        lastName = [defaults objectForKey:@"last_name"];
            
            // set user profile picture=====

            if ([UserPicture isEqualToString:@""]) {
                proflePic.image = [UIImage imageNamed:@"icon-user-default.png"];

            }
            
            else{
                
            SDWebImageManager *manager = [SDWebImageManager sharedManager];
            [manager downloadImageWithURL:userImagePic
                                  options:0
                                 progress:^(NSInteger receivedSize, NSInteger  expectedSize) {
                                 }
                                completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                                    if (image) {
                                        dispatch_async(dispatch_get_main_queue(), ^{
                                            proflePic.image = image;
                                            
                                        });
                                    }
                            }
             ];
                
            }

        [cell.contentView addSubview:proflePic];

        }
        
        if (IS_IPHONE_6) {
            nameLabel = [[UILabel alloc]initWithFrame:CGRectMake(22, 100, 230, 30)];

        }
        else{
        nameLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 100, cell.frame.size.width-70, 30)];
        }
        nameLabel.text = [NSString stringWithFormat:@"%@ %@",firstName,lastName];
        nameLabel.textColor = [UIColor whiteColor];
        nameLabel.font = [UIFont systemFontOfSize:15.0f];
        nameLabel.backgroundColor = [UIColor clearColor];
        nameLabel.textAlignment = NSTextAlignmentCenter;
        [cell.contentView addSubview:nameLabel];
    }
    else
    {
        
        UILabel *actionLabel = (UILabel *)[cell.contentView viewWithTag:101];
        actionLabel.textColor =[UIColor whiteColor];
        actionLabel.font=[UIFont systemFontOfSize:14.0f];
        actionLabel.highlightedTextColor = [UIColor blackColor];
        [actionLabel setText:[menu objectAtIndex:(indexPath.row - 1)]];

        UIImageView *listIcons = (UIImageView *)[cell.contentView viewWithTag:102];
        //[listIcons setImage:[UIImage imageNamed:[menuIcons objectAtIndex:indexPath.row]]];
        listIcons.tintColor = [UIColor whiteColor];
        UIImage *image = [[UIImage imageNamed:[menuIcons objectAtIndex:indexPath.row]] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        [listIcons setImage:image];
        cell.backgroundColor  = [UIColor colorWithRed:0.12 green:0.11 blue:0.20 alpha:1.0];
        
        UIView *_lineDiv=[[UIView alloc] init];
        _lineDiv.frame=CGRectMake(55, 50.0f, self.view.frame.size.width-90, 0.5);
        _lineDiv.alpha=0.2;
        _lineDiv.backgroundColor=[UIColor whiteColor];
        //_lineDiv.backgroundColor=[UIColor colorWithRed:221.0f/255.0 green:223.0f/255.0 blue:224.0f/255.0 alpha:1.0];
        [cell addSubview:_lineDiv];

    }
    
    
    return cell;
    
}



-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row==0)
    {
        NSLog(@"0");
    }
    else if (indexPath.row==1)
    {
        NSLog(@"1");
        
        UIStoryboard *story = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        _viewControllerObject  = [story instantiateViewControllerWithIdentifier:@"viewController"];
        UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:_viewControllerObject];
        
        CATransition *transition = [CATransition animation];
        transition.duration = 0.3;
        transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn];
        transition.type = kCATransitionMoveIn;//kCATransitionFade
        transition.subtype = kCATransitionFromRight;
        [self.view.window.layer addAnimation:transition forKey:nil];
        [self presentViewController:nav animated:NO completion:nil];
        [self.view removeFromSuperview];
        
        
        
        
       // [self performSegueWithIdentifier:@"toMainEventsView" sender:nil];
    }
    else if (indexPath.row==2)
    {
        
        UIStoryboard *story = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        _myEventsVCObject  = [story instantiateViewControllerWithIdentifier:@"myEventsView"];
        UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:_myEventsVCObject];
                
        CATransition *transition = [CATransition animation];
        transition.duration = 0.3;
        transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
        transition.type = kCATransitionMoveIn;//kCATransitionFade
        transition.subtype = kCATransitionFromRight;
        [self.view.window.layer addAnimation:transition forKey:nil];
        [self presentViewController:nav animated:NO completion:nil];
        [self.view removeFromSuperview];
        
        
//        [self  dismissModalStack];
        
        
        //[self performSegueWithIdentifier:@"toMyEventsView" sender:nil];

    }else if (indexPath.row==3)
    {
        
        UIStoryboard *story = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        _favEventVCObject  = [story instantiateViewControllerWithIdentifier:@"favEventsView"];
        UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:_favEventVCObject];
        
        CATransition *transition = [CATransition animation];
        transition.duration = 0.3;
        transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
        transition.type = kCATransitionMoveIn;//kCATransitionFade
        transition.subtype = kCATransitionFromRight;
        [self.view.window.layer addAnimation:transition forKey:nil];
        [self presentViewController:nav animated:NO completion:nil];
        [self.view removeFromSuperview];
        
       // [self performSegueWithIdentifier:@"toFavoriteView" sender:nil];
    }
    else if (indexPath.row==4)//historyEventVCObject
    {
        
        
        UIStoryboard *story = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        _upcomingsVCObject  = [story instantiateViewControllerWithIdentifier:@"upcomingEventView"];
        UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:_upcomingsVCObject];
        
        CATransition *transition = [CATransition animation];
        transition.duration = 0.3;
        transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
        transition.type = kCATransitionMoveIn;//kCATransitionFade
        transition.subtype = kCATransitionFromRight;
        [self.view.window.layer addAnimation:transition forKey:nil];
        [self presentViewController:nav animated:NO completion:nil];
         [self.view removeFromSuperview];

        // [self performSegueWithIdentifier:@"toCreateEventView" sender:nil];
    }
    else if (indexPath.row==5)
    {
        
        
        UIStoryboard *story = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        _historyEventVCObject  = [story instantiateViewControllerWithIdentifier:@"historyEventView"];
        UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:_historyEventVCObject];
        
        CATransition *transition = [CATransition animation];
        transition.duration = 0.3;
        transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
        transition.type = kCATransitionMoveIn;//kCATransitionFade
        transition.subtype = kCATransitionFromRight;
        [self.view.window.layer addAnimation:transition forKey:nil];
        [self presentViewController:nav animated:NO completion:nil];
         [self.view removeFromSuperview];

        // [self performSegueWithIdentifier:@"toCreateEventView" sender:nil];
    }

    
    else if (indexPath.row==6)
    {
        [SVProgressHUD showWithStatus:@"Loading.." maskType:SVProgressHUDMaskTypeBlack];
        
        UIStoryboard *story = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        _createEventVCObject  = [story instantiateViewControllerWithIdentifier:@"createEventView"];
        UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:_createEventVCObject];
        
//        CATransition *transition = [CATransition animation];
//        transition.duration = 0.1;
//        transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
//        transition.type = kCATransition;//kCATransitionFade
//        transition.subtype = kCATransitionFromRight;
//        [self.view.window.layer addAnimation:transition forKey:nil];
        [self presentViewController:nav animated:NO completion:nil];
       // [self.view removeFromSuperview];
        
        
        
       // [self performSegueWithIdentifier:@"toCreateEventView" sender:nil];
    }
    else if (indexPath.row==7)
    {
        
        
        UIStoryboard *story = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        _settingVCObject  = [story instantiateViewControllerWithIdentifier:@"settingsView"];
        UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:_settingVCObject];
        
        CATransition *transition = [CATransition animation];
        transition.duration = 0.3;
        transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
        transition.type = kCATransitionMoveIn;//kCATransitionFade
        transition.subtype = kCATransitionFromRight;
        [self.view.window.layer addAnimation:transition forKey:nil];
        [self presentViewController:nav animated:NO completion:nil];
        [self.view removeFromSuperview];
        
      //  [self performSegueWithIdentifier:@"toSettingView" sender:nil];
    }
    else if (indexPath.row==8)
    {
        
//        UIStoryboard *story = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//        _viewControllerObject  = [story instantiateViewControllerWithIdentifier:@"viewController"];
//         _viewControllerObject.screenType =@"LogoutNav";
//        UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:_viewControllerObject];
//        
//        CATransition *transition = [CATransition animation];
//        transition.duration = 0.2;
//        transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
//        transition.type = kCATransitionFade;//kCATransitionFade
//        transition.subtype = kCATransitionFromRight;
//        [self.view.window.layer addAnimation:transition forKey:nil];
//        [self presentViewController:nav animated:NO completion:nil];
        
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"dismissDrawer" object:self userInfo:nil];
//        [NSThread sleepForTimeInterval:.1];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"didLogout" object:self userInfo:nil];

        [self  dismissModalStack];

        
       //   [self performSegueWithIdentifier:@"toViewFromLogout" sender:nil];
    }
}



-(void)dismissModalStack {
    UIViewController *vc = self.presentingViewController;
    while (vc.presentingViewController) {
        vc = vc.presentingViewController;
    }
    [vc dismissViewControllerAnimated:YES completion:NULL];
}





- (void) goToProfileScreen: (UITapGestureRecognizer *)gesture
{
//    NSLog(@"to edit view****");
//    
//    UIStoryboard *story = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//    _editProfileVCObject  = [story instantiateViewControllerWithIdentifier:@"editProfile"];
//    _editProfileVCObject.screenType = @"UpdateProfile";
//    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:_editProfileVCObject];
//    nav.navigationBar.tintColor = [UIColor whiteColor];
//    nav.navigationBar.barTintColor = [UIColor colorWithRed:0.95 green:0.28 blue:0.31 alpha:1.0];
//    
//    
//    CATransition *transition = [CATransition animation];
//    transition.duration = 0.2;
//    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
//    transition.type = kCATransitionMoveIn;//kCATransitionFade
//    transition.subtype = kCATransitionFromRight;
//    [self.view.window.layer addAnimation:transition forKey:nil];
//    [self presentViewController:nav animated:NO completion:nil];
    
    UIStoryboard *story = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    _settingVCObject  = [story instantiateViewControllerWithIdentifier:@"settingsView"];
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:_settingVCObject];
    
    CATransition *transition = [CATransition animation];
    transition.duration = 0.3;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionMoveIn;//kCATransitionFade
    transition.subtype = kCATransitionFromRight;
    [self.view.window.layer addAnimation:transition forKey:nil];
    [self presentViewController:nav animated:NO completion:nil];
     [self.view removeFromSuperview];
    
    
}


- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:(BOOL)animated];
    
    [SVProgressHUD dismiss];
}




// In a storyboard-based application, you will often want to do a little preparation before navigation
//
//- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
//{
//    
//        if ([segue.identifier isEqualToString:@"toViewFromLogout"]) {
//            //    NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
//            MainTabbarController *destViewController = segue.destinationViewController;
//            destViewController.screenType =@"LogoutNav";
//            // destViewController.recipeName = [recipes objectAtIndex:indexPath.row];
//        }
//
//    
//    if ( [segue isKindOfClass: [SWRevealViewControllerSegue class]] ) {
//        SWRevealViewControllerSegue *swSegue = (SWRevealViewControllerSegue*) segue;
//        
//        swSegue.performBlock = ^(SWRevealViewControllerSegue* rvc_segue, UIViewController* svc, UIViewController* dvc) {
//            
//            UINavigationController* navController = (UINavigationController*)self.revealViewController.frontViewController;
//            [navController setViewControllers: @[dvc] animated: NO ];
//            [self.revealViewController setFrontViewPosition: FrontViewPositionLeft animated: YES];
//        };
//        
//    }

//}


@end
