//
//  ForgetPasswordVC.m
//  OutNAbout
//
//  Created by Ram Kumar on 29/08/16.
//  Copyright © 2016 tecHindustan. All rights reserved.
//

#import "ForgetPasswordVC.h"
#import "CommonAPI's.pch"
#import "AppDelegate.h"
#import "SVProgressHUD.h"

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wdeprecated-declarations"

@interface ForgetPasswordVC ()<UITextFieldDelegate,NSURLConnectionDataDelegate>

@end

@implementation ForgetPasswordVC
{
    NSPredicate *emailTest;

}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _emailIdTF.delegate = self;
    
    [_emailIdTF.layer setMasksToBounds:YES];
    [_emailIdTF.layer setCornerRadius:4.0f];
    [_resetButton.layer setCornerRadius:4.0f];
    
    NSString *emailReg = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailReg];
    
    UIImageView *backgroundImage = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    [backgroundImage setImage:[UIImage imageNamed:@"login_bg.jpg"]];
    [self.view insertSubview:backgroundImage atIndex:0];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)resetPasswordAction:(id)sender
{
    if ([_emailIdTF.text isEqualToString:@""]) {
        
        [[[UIAlertView alloc]initWithTitle:@"Reset!" message:@"Email field is required." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil]show];
    }
//    else if ([emailTest evaluateWithObject:_emailIdTF.text] == NO ) {
//        
//        
//        [[[UIAlertView alloc]initWithTitle:@"Invalid!" message:@"Invalid email address!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil]show];
//    }

    else
    {
        [self postMethod];
 
    }
    
}





-(void)postMethod
{
    [SVProgressHUD showWithStatus:@"Reset.." maskType:SVProgressHUDMaskTypeBlack];

    if (APP_DELEGATE.isServerReachable)
    {
    [self.connection cancel];
    
    //initialize new mutable data
    NSMutableData *data = [[NSMutableData alloc] init];
    self.receivedData = data;
    
    NSURL *url = [NSURL URLWithString:(API_URL @"forgetpassword")]; //http://192.168.1.22/outnabout/webservices/users/login/
    
    //initialize a request from url
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[url standardizedURL]];
    
    //set http method
    [request setHTTPMethod:@"POST"];
    //initialize a post data
    NSString *postData = [NSString stringWithFormat:@"email=%@",_emailIdTF.text];
    //   NSLog(@"username****: %@  pass**** :%@",usernameTF.text,passwordTF.text);
    
    
    [request setValue:@"application/x-www-form-urlencoded; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    
    [request setHTTPBody:[postData dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    self.connection = connection;
    
    //start the connection
    [connection start];
        
    }
    else
    {
        [SVProgressHUD dismiss];
        
        [[[UIAlertView alloc]initWithTitle:@"OutNAbout" message:@"Internet connection is not available. Please try again."  delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil]show];
    }

    
}





#pragma mark - NSURLConnection delegate methods =  ==  == = = = = = = =  = = = = = = = = = = = = = = = = = = = = = = = = = = = = =

-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data{
    [self.receivedData appendData:data];
}


-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error{
    
    NSLog(@"eeeeeeee  %@" , error);
}


-(void)connectionDidFinishLoading:(NSURLConnection *)connection{
    
   // NSString *htmlSTR = [[NSString alloc] initWithData:self.receivedData
                   //                           encoding:NSUTF8StringEncoding];
    // NSLog(@" ffff     %@" , htmlSTR);
    
    NSError *error = nil;
    NSDictionary *jsonArray = [NSJSONSerialization JSONObjectWithData: self.receivedData options: NSJSONReadingMutableContainers error: &error];  //I am using sbjson to parse

     NSString *msg = jsonArray[@"msg"];
    NSString *code = [jsonArray[@"code"] stringValue];
      if ([code isEqualToString:@"200"]) {
          
          [[[UIAlertView alloc]initWithTitle:@"Reset Password" message:msg delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil]show];
          [SVProgressHUD dismiss];
      }
      else{
           [[[UIAlertView alloc]initWithTitle:@"Reset Password" message:msg delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil]show];
          [SVProgressHUD dismiss];
          
      }

}


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex==0) {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}



-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    
    return YES;
}


- (void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [[self view] endEditing:YES];
    
}



- (IBAction)loginViewActions:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];

}





-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:.3];
    [UIView setAnimationBeginsFromCurrentState:TRUE];
    self.view.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y -80., self.view.frame.size.width, self.view.frame.size.height);
    
    [UIView commitAnimations];
    
    
}


-(void)textFieldDidEndEditing:(UITextField *)textField
{
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:.3];
    [UIView setAnimationBeginsFromCurrentState:TRUE];
    self.view.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y +80., self.view.frame.size.width, self.view.frame.size.height);
    
    [UIView commitAnimations];
    
}



@end
