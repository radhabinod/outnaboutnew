//
//  LoginVC.m
//  Out N about
//
//  Created by Ram Kumar on 03/05/16.
//  Copyright © 2016 tecHindustan. All rights reserved.
//

#import "LoginVC.h"
#import "ViewController.h"
#import "AppDelegate.h"
#import "MainTabbarController.h"
#import "AllEventsTabBarVC.h"
#import "CommonAPI's.pch"
#import "AppDelegate.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import "SVProgressHUD.h"

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wdeprecated-declarations"
@interface LoginVC ()<NSURLConnectionDataDelegate,FBSDKLoginButtonDelegate,UITextFieldDelegate,FBSDKGraphRequestConnectionDelegate>


@end

@implementation LoginVC
{
    int loginInteger;
    UIAlertController * alertForFields;
    
    NSMutableArray *infoArray;
    NSArray *data;
    NSString *accesstoken;
    
    NSString *firstName;
    NSString *lastName;
    NSString *email;
    NSString *userImage;
    
    NSString *postData;
    BOOL ischecked;
    NSString *rememberChecked;
     NSString *timezone;

}
@synthesize usernameTF,passwordTF,loginButtonObject;

- (void)viewDidLoad
{
    [super viewDidLoad];
    loginInteger = 0;
    rememberChecked = @"";
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setInteger:loginInteger forKey:@"isloggedIn"];
    [defaults synchronize];

//    if (loginInteger==0) {
//        [[NSNotificationCenter defaultCenter] postNotificationName:@"callLoader" object:self userInfo:nil];
//
//    }
    
    infoArray = [[NSMutableArray alloc]init];
    
    [self manageTextfields];
    
    [GIDSignIn sharedInstance].uiDelegate = self;
    [GIDSignIn sharedInstance].shouldFetchBasicProfile = YES;

    
    UIImageView *backgroundImage = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    [backgroundImage setImage:[UIImage imageNamed:@"login_bg.jpg"]];
    [self.view insertSubview:backgroundImage atIndex:0];

    
    
    
    //self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"login_bg.jpg"]];
    
    
//    FBSDKLoginButton *loginButton = [[FBSDKLoginButton alloc] init];
//   // loginButton.center = self.view.center;
//    loginButton.frame = CGRectMake(100, 460, 180, 40);
//   // [loginButton setImage:[UIImage imageNamed:@"login_fb.png"] forState:UIControlStateNormal];
//    loginButton.readPermissions =
//    @[@"public_profile", @"email", @"user_friends"];
//    [loginButton setTranslatesAutoresizingMaskIntoConstraints:NO];
//    [loginButton addTarget:self action:@selector(loginButtonClicked) forControlEvents:UIControlEventTouchUpInside];
//    [self.view addSubview:loginButton];
    
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(receiveToggleAuth:)
     name:@"UpdateRememberMe"
     object:nil];
    
    

    
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(receiveToggleAuthUINotification:)
     name:@"loginDetails"
     object:nil];


}



-(void)manageTextfields
{
    usernameTF.delegate = self;
    passwordTF.delegate = self;
    
    [usernameTF.layer setMasksToBounds:YES];
    [usernameTF.layer setCornerRadius:3.0f];
    
    [passwordTF.layer setMasksToBounds:YES];
    [passwordTF.layer setCornerRadius:3.0f];

    
    usernameTF.layer.sublayerTransform = CATransform3DMakeTranslation(4, 0, 0);
    passwordTF.layer.sublayerTransform = CATransform3DMakeTranslation(4, 0, 0);
    
    UIImageView *userNameImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"ic_email@3x.png"]];
    userNameImage.frame = CGRectMake(0.0, 0.0, userNameImage.image.size.width+20.0, userNameImage.image.size.height);
    userNameImage.contentMode = UIViewContentModeCenter;
    usernameTF.rightView = userNameImage;
    usernameTF.rightViewMode = UITextFieldViewModeAlways;
    
    UIImageView *passImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"ic_password@3x.png"]];
    passImage.frame = CGRectMake(0.0, 0.0, passImage.image.size.width+20.0, passImage.image.size.height);
    passImage.contentMode = UIViewContentModeCenter;
    passwordTF.rightView = passImage;
    passwordTF.rightViewMode = UITextFieldViewModeAlways;

}






- (void) receiveToggleAuthUINotification:(NSNotification *) notification {
    if ([[notification name] isEqualToString:@"loginDetails"]) {
        
        NSString *GPuserID = [notification userInfo][@"userId"];
        NSString *GPfirstname = [notification userInfo][@"firstName"];
        NSString *GPlastName = [notification userInfo][@"lastName"];
        NSString *GPemail = [notification userInfo][@"email"];
        NSString *GPlogintype = [notification userInfo][@"type"];

        NSLog(@"ns notification text**** :%@   %@   %@  %@  %@",GPfirstname,GPlastName,GPemail,GPuserID,GPlogintype);
        
        
     
        
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//        NSString *loginType = [defaults objectForKey:@"loginType"];
//        // NSString *loginType = @"gp";
//        if ([loginType isEqualToString:@"gp"]) {
//            NSString *gpFirstName = [defaults objectForKey:@"gpFirstName"];
//            NSString *gpLastName = [defaults objectForKey:@"gpLastName"];
//            NSString *gpEmail = [defaults objectForKey:@"gpemail"];
//            NSString *gpID = [defaults objectForKey:@"gpID"];
        
            loginInteger = 1;
            [defaults setInteger:loginInteger forKey:@"isloggedIn"];
            [defaults synchronize];
            
            [self socialFacebookOrGooglePostMethod:GPfirstname last:GPlastName emails:GPemail id:GPuserID logintype:GPlogintype];
            
     //   }

        
    }
}


- (IBAction)actionToLoginFacebook:(id)sender {
    
 
    
//    NSArray* fbSchemes = @[
//                           @"fbapi://", @"fb-messenger-api://", @"fbauth2://", @"fbshareextension://"];
//    BOOL isInstalled = false;
//    
//    for (NSString* fbScheme in fbSchemes) {
//        isInstalled = [[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:fbScheme]];
//        if(isInstalled) break;
//    }
//    
//    if (!isInstalled) {
//        // code
//        return;
//    }
    
    
    
//    BOOL isInstalled = [[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"fbapi://"]];
//    
//    if (isInstalled) {
//        
//         [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"fb://profile/200538917420"]];
//        
//    } else {
//        
    

    
    
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
//    [login setLoginBehavior:FBSDKLoginBehaviorWeb];
        login.loginBehavior = FBSDKLoginBehaviorSystemAccount;
    [login
     logInWithReadPermissions: @[@"public_profile",@"email",@"user_likes"]
     fromViewController:self
     handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
         if (error) {
             NSLog(@"Process error");
             
             [[[UIAlertView alloc]initWithTitle:@"Message" message:@"Failed login with facebook!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil]show];
         } else if (result.isCancelled) {
             NSLog(@"Cancelled");
             
             [[[UIAlertView alloc]initWithTitle:@"Message" message:@"Cancel login with facebook!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil]show];
         } else {
             NSLog(@"Logged in");
             
             
             
             [self getFacebookUserdetails];
             
             
             loginInteger = 1;
             

             NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
             [defaults setInteger:loginInteger forKey:@"isloggedIn"];
             [defaults synchronize];

             [self dismissViewControllerAnimated:YES completion:nil];
             [SVProgressHUD showWithStatus:@"Log in" maskType:SVProgressHUDMaskTypeBlack];

             
            }
         
     }];
   // }

}






-(void)getFacebookUserdetails{

#pragma mark- To Get User Info & Profile Picture==============================================================================
if ([FBSDKAccessToken currentAccessToken])
{
    FBSDKGraphRequest *request = [[FBSDKGraphRequest alloc]
                                  initWithGraphPath:@"me" parameters:@{@"fields": @"id, name, link, first_name, last_name, picture.type(large), email,age_range, gender,birthday,friends"} HTTPMethod:@"GET"];
    [request   startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
        
//        UIImageView *imageView = [[UIImageView alloc]initWithFrame:CGRectMake(110, 50, 100, 100)];
//        [self.view addSubview:imageView];
//        
//        FBSDKProfilePictureView *profilePictureview = [[FBSDKProfilePictureView alloc]initWithFrame:imageView.frame];
//        [profilePictureview setProfileID:result[@"id"]];
//        [self.view addSubview:profilePictureview];
        
//        emailLabel = [[UILabel alloc]initWithFrame:CGRectMake(100, 150, 200, 60)];
//        emailLabel.font=[UIFont fontWithName:@"Baskerville-SemiBold" size:20.0f];
//        emailLabel.text = [result  objectForKey:@"name"];
//        emailLabel.userInteractionEnabled=YES;
//        [emailLabel setTextColor:[UIColor blackColor]];
//        [self.view addSubview:emailLabel];
        
#pragma mark-There are all labels in which we will store the user data==================================================
        
//        birthdayLabel = [[UILabel alloc]initWithFrame:CGRectMake(70, 200, 200, 60)];
//        birthdayLabel.userInteractionEnabled=YES;
//        birthdayLabel.text = [result objectForKey:@"email"];
//        [self.view addSubview:birthdayLabel];
        
        if (!error)
        {
            NSString *accessToken =[FBSDKAccessToken currentAccessToken].tokenString;
            NSString *surl = @"https://graph.facebook.com/v2.5/131180217248846/friends?access_token=%@&fields=picture,name";
            NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:surl,accessToken]];
            NSURLRequest *request = [NSURLRequest requestWithURL:url];
            [NSURLConnection sendAsynchronousRequest:request queue:[[NSOperationQueue alloc]init] completionHandler:^(NSURLResponse *r , NSData *d, NSError *e){
                if (e==nil)
                {
                    
                    
                    
                    NSDictionary *jsonData = [NSJSONSerialization JSONObjectWithData:d options:NSJSONReadingMutableContainers error:nil];
                    infoArray=[NSMutableArray arrayWithArray:jsonData[@"data"]];
                    
                    dispatch_async(dispatch_get_main_queue(),^{
                    });
                }
            }];
            
 
            NSLog(@"fetched user and Name : %@ Email:%@ Gender:%@ Age:%@ Birday:%@ Friends:%@ ",[result objectForKey:@"name"],[result objectForKey:@"email"],[result objectForKey:@"gender"],[result objectForKey:@"age_range"],[result objectForKey:@"birthday"],[result objectForKey:@"friends"]);
            
            NSString *fbFirst_Name = [result objectForKey:@"first_name"];
            NSString *fbLast_Name  = [result objectForKey:@"last_name"];
            NSString *fbEmail = [result objectForKey:@"email"];
            NSString *fbId = [result objectForKey:@"id"];
            NSDictionary *profileData = [result objectForKey:@"picture"];
            NSString *fbProfilePic =profileData[@"data"][@"url"];
            NSLog(@"fb picture@@@@ %@",fbProfilePic);
            NSString *loginType = @"fb";

            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            [defaults setObject:fbFirst_Name forKey:@"fbFirstName"];
            [defaults setObject:fbLast_Name forKey:@"fbLastName"];
            [defaults setObject:fbId forKey:@"fbID"];
            [defaults setObject:fbEmail forKey:@"fbemail"];
            [defaults setObject:loginType forKey:@"loginType"];
            [defaults setObject:fbProfilePic forKey:@"fbProfilepic"];
            [defaults synchronize];
            
            
            [self socialFacebookOrGooglePostMethod:fbFirst_Name last:fbLast_Name emails:fbEmail id:fbId logintype:loginType];
            
        }
        
        

    }];
    
    
}




}


- (IBAction)actionToLoginGoogle:(id)sender {
    
    [[GIDSignIn sharedInstance] signIn];
    
    [GIDSignIn sharedInstance].uiDelegate = self;
    
}


#pragma mark - Google Sign in delegates methods 



- (void)signInWillDispatch:(GIDSignIn *)signIn error:(NSError *)error {
    //  [myActivityIndicator stopAnimating];
    
    NSLog(@"&&&&&&&&&&&&&&&");
    
//    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//    NSString *loginType = [defaults objectForKey:@"loginType"];
//   // NSString *loginType = @"gp";
//    if ([loginType isEqualToString:@"gp"]) {
//        NSString *gpFirstName = [defaults objectForKey:@"gpFirstName"];
//        NSString *gpLastName = [defaults objectForKey:@"gpLastName"];
//        NSString *gpEmail = [defaults objectForKey:@"gpemail"];
//        NSString *gpID = [defaults objectForKey:@"gpID"];
//        
//        loginInteger = 1;
//        [defaults setInteger:loginInteger forKey:@"isloggedIn"];
//        [defaults synchronize];
//        
//        [self socialFacebookOrGooglePostMethod:gpFirstName last:gpLastName emails:gpEmail id:gpID logintype:loginType];
//        
//    }
    
}

// Present a view that prompts the user to sign in with Google

- (void)signIn:(GIDSignIn *)signIn
presentViewController:(UIViewController *)viewController {
    [self presentViewController:viewController animated:YES completion:nil];
    NSLog(@"%%%%%%%%%%%%%%%%%%");
}

// Dismiss the "Sign in with Google" view
- (void)signIn:(GIDSignIn *)signIn
dismissViewController:(UIViewController *)viewController {
    [self dismissViewControllerAnimated:YES completion:nil];
    NSLog(@"***************");
    [SVProgressHUD showWithStatus:@"Log in" maskType:SVProgressHUDMaskTypeBlack];

    
}

//


- (IBAction)loginButtonAction:(id)sender
{
    [usernameTF resignFirstResponder];
    [passwordTF resignFirstResponder];
 
    
//    NSString *emailReg = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
//    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailReg];
//    
//    
//    if ([emailTest evaluateWithObject:usernameTF.text] == NO)
//    {
//        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"OutNAbout" message:@"Please enter a valid email address." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
//        [alert show];
//
//        return;
//    }
    
    
     if(![usernameTF.text length])
     {
         UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"OutNAbout" message:@"Email field is required."  delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
         [alert show];
         return;

     }
    else
        if(![passwordTF.text length])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"OutNAbout" message:@"Password field is required."  delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        return;
    }
    else
    {
        NSLog(@"device token:%@",APP_DELEGATE.Devtoken);
         [self postMethod];

    }

    
    

}








-(void)socialFacebookOrGooglePostMethod:(NSString *)firstname last:(NSString *)lastName emails:(NSString *)emailId id:(NSString *)fbID logintype:(NSString *)type
{
    
    [self.connection cancel];
    
    //initialize new mutable data
    NSMutableData *data = [[NSMutableData alloc] init];
    self.receivedData = data;
    
    NSURL *url = [NSURL URLWithString:(API_URL @"sociallogin")];
    //initialize a request from url
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[url standardizedURL]];
    
    //set http method
    [request setHTTPMethod:@"POST"];
    //initialize a post data
    
    NSString *device_token = APP_DELEGATE.Devtoken;
   
    postData = [NSString stringWithFormat:@"iostoken=%@ & email=%@ & social_id=%@ & firstname=%@ & lastname=%@ & type=%@",device_token,emailId,fbID,firstname,lastName,type];
    
    [request setValue:@"application/x-www-form-urlencoded; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    
    [request setHTTPBody:[postData dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    self.connection = connection;
    
    //start the connection
    [connection start];
    
    
}











-(void)postMethod
{
   [SVProgressHUD showWithStatus:@"Log in" maskType:SVProgressHUDMaskTypeBlack];

    if (APP_DELEGATE.isServerReachable)
    {
    
    
    [self.connection cancel];
    
    //initialize new mutable data
    NSMutableData *datas = [[NSMutableData alloc] init];
    self.receivedData = datas;
    
    NSURL *url = [NSURL URLWithString:(API_URL @"login")];
    //initialize a request from url
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[url standardizedURL]];
    
    //set http method
    [request setHTTPMethod:@"POST"];
    //initialize a post data
    NSString *postData = [NSString stringWithFormat:@"email=%@&password=%@&iostoken=%@",usernameTF.text,passwordTF.text,APP_DELEGATE.Devtoken];
    
    [request setValue:@"application/x-www-form-urlencoded; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    
    [request setHTTPBody:[postData dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    self.connection = connection;
    
    //start the connection
    [connection start];
        
    }
    
    else
    {
        [SVProgressHUD dismiss];
        
        [[[UIAlertView alloc]initWithTitle:@"OutNAbout" message:@"Internet connection is not available. Please try again."  delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil]show];
    }
}





#pragma mark - NSURLConnection delegate methods =  ==  == = = = = = = =  = = = = = = = = = = = = = = = = = = = = = = = = = = = = =

-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)datas{
    [self.receivedData appendData:datas];
}


-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error{
    
    NSLog(@"eeeeeeee  %@" , error);
}


-(void)connectionDidFinishLoading:(NSURLConnection *)connection{
    
    //initialize convert the received data to string with UTF8 encoding
   // NSString *htmlSTR = [[NSString alloc] initWithData:self.receivedData
                           //                   encoding:NSUTF8StringEncoding];
   // NSLog(@" ffff     %@" , htmlSTR);
    
    
    NSError *error = nil;
    NSDictionary *jsonArray = [NSJSONSerialization JSONObjectWithData: self.receivedData options: NSJSONReadingMutableContainers error: &error];  //I am using sbjson to parse
    
//    NSLog(@"response on login :%@",jsonArray);  //here is your output
    NSString *msg = jsonArray[@"msg"];
     NSLog(@"msg string :%@",msg);
    

    NSString *response = [jsonArray[@"code"] stringValue];
    data = jsonArray[@"data"];
    
   

    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *loginType = [defaults objectForKey:@"loginType"];
    if ([loginType isEqualToString:@"fb"]) {
        
        accesstoken = [jsonArray valueForKey:@"access_token"];
        timezone = [jsonArray valueForKey:@"timezone"];
        
    }
    else if ([loginType isEqualToString:@"gp"]) {
        
        accesstoken = [jsonArray valueForKey:@"access_token"];
        timezone = [jsonArray valueForKey:@"timezone"];
    }
    
    
    else{
    
    firstName =[data valueForKey:@"firstname"];
    lastName =[data valueForKey:@"lastname"];
    email =[data valueForKey:@"email"];
    accesstoken =[data valueForKey:@"access_token"];
    userImage =[data valueForKey:@"userimage"];
    timezone = [data valueForKey:@"timezone"];

        
    
    NSLog(@"%@ %@ %@ %@ ********%@",response,firstName,lastName,email,accesstoken);
    }
    if ([response isEqualToString:@"200"]) {
        NSLog(@"Login successfully");
      [SVProgressHUD dismiss];
        
        NSString *userName = [usernameTF text];
        NSString *passWord  = [passwordTF text];
        
        NSString *first_Name = firstName;
        NSString *last_Name  = lastName;
        NSString *_email = email;
        NSString *userPic = userImage;
        NSString *access_tokenn  = accesstoken;
        loginInteger = 1;
        
        
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults setObject:timezone forKey:@"timezone"];

        [defaults setObject:userName forKey:@"username"];
        [defaults setObject:passWord forKey:@"password"];
        
        [defaults setObject:first_Name forKey:@"first_name"];
        [defaults setObject:last_Name forKey:@"last_name"];
        
        [defaults setObject:_email forKey:@"_email"];
        [defaults setObject:access_tokenn forKey:@"access_token"];
        [defaults setObject:userPic forKey:@"user_pic"];
        [defaults setObject:@"NowLoggedIn" forKey:@"loginTypeReceive"];

        //[defaults setObject:@"0" forKey:@"loginIntType"];
        [defaults setInteger:loginInteger forKey:@"isloggedIn"];
        [defaults synchronize];
        
        NSLog(@"Data saved");
        NSLog(@"username: %@  pass :%@ access_Token :%@",userName,passWord,access_tokenn);
        
        //    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        //    ViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"viewcontroller"];
        //    [self.navigationController pushViewController:viewController animated:YES];
        [self dismissViewControllerAnimated:YES completion:nil];
        
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"callViewController" object:self userInfo:nil];
        //  [self performSegueWithIdentifier:@"toViewController" sender:nil];

    }
    
    
    else if([response isEqualToString:@"406"]){
        
    [SVProgressHUD dismiss];
    NSLog(@"Failed to login");
    loginInteger = 0;
    [[[UIAlertView alloc]initWithTitle:@"OutNAbout" message:msg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil]show];
//    UIAlertController * alert=   [UIAlertController
//                                  alertControllerWithTitle:@"Sign in Failed!"
//                                  message:msg
//                                  preferredStyle:UIAlertControllerStyleAlert];
//    
//    UIAlertAction* okButton = [UIAlertAction
//                               actionWithTitle:@"OK"
//                               style:UIAlertActionStyleDefault
//                               handler:^(UIAlertAction * action)
//                               {
//                                   //Handel your yes please button action here
//                                   
//                                   
//                               }];
//    [alert addAction:okButton];
//    
//    [self presentViewController:alert animated:YES completion:nil];
        
    }
    
}





- (IBAction)rememberMeAction:(id)sender
{
    ischecked =!ischecked;
    //UIButton *check = (UIButton*)sender;
//    if(ischecked == NO){
//        [check setImage:[UIImage imageNamed:@"check_disable@30=x.png"] forState:UIControlStateNormal];
//    }
//    else{
//        [check setImage:[UIImage imageNamed:@"check_enable@3x.png"] forState:UIControlStateNormal];
//    }
    
    
    
    if(ischecked == NO){
        [_rememberMeButton setImage:[UIImage imageNamed:@"ic_box_disable@3x.png"] forState:UIControlStateNormal];
        rememberChecked = @"noncheck";
        
    }
    else{
        [_rememberMeButton setImage:[UIImage imageNamed:@"ic_box_enable@3x.png"] forState:UIControlStateNormal];
        
        
        rememberChecked = @"check";
    }
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    [defaults setObject:rememberChecked forKey:@"rememCheck"];
    [defaults synchronize];
    
}


//- (IBAction)rememberAction:(id)sender {
//
//    ischecked =!ischecked;
//    //UIButton *check = (UIButton*)sender;
//    if(ischecked == NO){
//        [_rememberMeButton setImage:[UIImage imageNamed:@"ic_box_disable@1x.png"] forState:UIControlStateNormal];
//        rememberChecked = @"noncheck";
//        
//    }
//    else{
//        [_rememberMeButton setImage:[UIImage imageNamed:@"ic_box_enable@1x.png"] forState:UIControlStateNormal];

//    rememberChecked = @"check";
//    }
//
//    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//    
//     [defaults setObject:rememberChecked forKey:@"rememCheck"];
//    [defaults synchronize];
//    }


- (void)viewWillAppear:(BOOL)animated
{

    [super viewWillAppear:animated];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *userNameRemember = [defaults objectForKey:@"rememberUserName"];
    NSString *passwordRemember = [defaults objectForKey:@"rememberPassword"];
    NSString *ifCheckRemember = [defaults objectForKey:@"rememberIfCheck"];
    if ([ifCheckRemember isEqualToString:@"check"]) {
        ischecked =!ischecked;
        [_rememberMeButton setImage:[UIImage imageNamed:@"ic_box_enable@3x.png"] forState:UIControlStateNormal];
    }
    else{
        [_rememberMeButton setImage:[UIImage imageNamed:@"ic_box_disable@3x.png"] forState:UIControlStateNormal];

    }

    usernameTF.text = userNameRemember;
    passwordTF.text =passwordRemember;
    
}




- (void) receiveToggleAuth:(NSNotification *) notifications {
    if ([[notifications name] isEqualToString:@"UpdateRememberMe"]) {
        
        
         NSString * checkIf = [notifications userInfo][@"ifCheck"];
        if ([checkIf isEqualToString:@"check"]) {
            
            NSString * userName = [notifications userInfo][@"userName"];
            NSString * password = [notifications userInfo][@"password"];
            
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            [defaults setObject:userName forKey:@"rememberUserName"];
            [defaults setObject:password forKey:@"rememberPassword"];
            [defaults setObject:checkIf forKey:@"rememberIfCheck"];
            [defaults synchronize];
            
            ischecked =!ischecked;
            [_rememberMeButton setImage:[UIImage imageNamed:@"ic_box_enable@3x.png"] forState:UIControlStateNormal];
            
            usernameTF.text = userName;
            passwordTF.text =password;
            
       NSLog(@"get login user details here------------ %@ pass  %@",userName,password);
        }
        else if([checkIf isEqualToString:@"noncheck"])
        {
            [_rememberMeButton setImage:[UIImage imageNamed:@"ic_box_disable@3x.png"] forState:UIControlStateNormal];
            usernameTF.text = @"";
            passwordTF.text =@"";
        }
        
        
        
//        usernameTF.hidden=YES;
//        passwordTF.hidden=YES;
       

        
    }
    
}





-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    return YES;
}


- (void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [[self view] endEditing:YES];
}



//-(void)textFieldDidBeginEditing:(UITextField *)textField
//{
//    [UIView beginAnimations:nil context:nil];
//    [UIView setAnimationDuration:.3];
//    [UIView setAnimationBeginsFromCurrentState:TRUE];
//    self.view.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y -100., self.view.frame.size.width, self.view.frame.size.height);
//    
//    [UIView commitAnimations];
//    
//    
//}
//
//
//-(void)textFieldDidEndEditing:(UITextField *)textField
//{
//    [UIView beginAnimations:nil context:nil];
//    [UIView setAnimationDuration:.3];
//    [UIView setAnimationBeginsFromCurrentState:TRUE];
//    self.view.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y +100., self.view.frame.size.width, self.view.frame.size.height);
//    
//    [UIView commitAnimations];
//    
//}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


@end
