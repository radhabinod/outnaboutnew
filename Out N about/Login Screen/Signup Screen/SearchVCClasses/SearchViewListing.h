//
//  SearchViewListing.h
//  OutNAbout
//
//  Created by Ram Kumar on 03/08/16.
//  Copyright © 2016 tecHindustan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearchViewListing : UIViewController
@property (weak, nonatomic) IBOutlet UITableView *listTableView;

@property(nonatomic,strong) NSMutableArray *categoriesTitle;
@property(nonatomic,strong) NSMutableArray *categoriesID;
@property(nonatomic,strong) NSMutableArray *statesListing;
@property(nonatomic,strong) NSString *screenTypeForStates;

@property (weak, nonatomic) IBOutlet UILabel *headerLabel;

@end
