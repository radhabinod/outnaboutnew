//
//  SearchViewListing.m
//  OutNAbout
//
//  Created by Ram Kumar on 03/08/16.
//  Copyright © 2016 tecHindustan. All rights reserved.
//

#import "SearchViewListing.h"
#import "SearchEventsVC.h"

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wdeprecated-declarations"

@interface SearchViewListing ()<UITableViewDataSource,UITableViewDelegate,SecondDelegate>
@end

@implementation SearchViewListing
{
    NSMutableArray *selectedCat;
    NSMutableArray *selectedState;

}


@synthesize categoriesID,categoriesTitle,statesListing,screenTypeForStates;


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    selectedCat = [[NSMutableArray alloc]init];
    selectedState= [[NSMutableArray alloc]init];
    NSLog(@"category array ID * %@",categoriesID);
    NSLog(@"category array title * %@",categoriesTitle);

    if ([screenTypeForStates isEqualToString:@"states"]) {
        _headerLabel.text = @"Select States";
    }
    else{
        _headerLabel.text = @"Select Categories";

    }

    
    [self createTableView];
}

-(void)createTableView
{

    self.listTableView.delegate=self;
    self.listTableView.dataSource=self;
    self.listTableView.backgroundColor=[UIColor whiteColor];
    self.listTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:self.listTableView];
}



#pragma mark - tableview delegate methods ================= ================ ================ ================ ================

//- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    
//    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
//    {
//        return  300;
//    }
//    else{
//        return  190;
//    }
//    
//}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if ([screenTypeForStates isEqualToString:@"states"]) {
        return statesListing.count;

    }
    else{
    return categoriesTitle.count;
    }
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        if ([screenTypeForStates isEqualToString:@"states"]) {

            cell.textLabel.text = [statesListing objectAtIndex:indexPath.row];

        }else{
        cell.textLabel.text = [categoriesTitle objectAtIndex:indexPath.row];
        }
    }
    return cell;
}



- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    
    
    if ([screenTypeForStates isEqualToString:@"states"]) {
        
        NSString *selectedStateValue = [statesListing objectAtIndex:indexPath.row];
        
        if (cell.accessoryType == UITableViewCellAccessoryCheckmark) {
            [selectedState removeObject:selectedStateValue];
            cell.accessoryType = UITableViewCellAccessoryNone;
        } else {
            [selectedState addObject:selectedStateValue];
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
        }

    }
    
    else{
    NSString *selectedCatValue = [categoriesTitle objectAtIndex:indexPath.row];

    if (cell.accessoryType == UITableViewCellAccessoryCheckmark) {
        [selectedCat removeObject:selectedCatValue];
        cell.accessoryType = UITableViewCellAccessoryNone;
    } else {
        [selectedCat addObject:selectedCatValue];
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    }
    
    }
    NSLog(@"values** %@",selectedCat);
    
//    SearchEventsVC *searchView = [[SearchEventsVC alloc]init];
//    NSString *selectedValue = [categoriesTitle objectAtIndex:indexPath.row];
//    searchView.categoryTF.text = [NSString stringWithFormat:@"%@",selectedValue];
//    [self.navigationController pushViewController:searchView animated:YES];
   // yourTextView.text = selectedValue;
  //  yourTableView.hide = YES;
    
}



- (IBAction)submitButtonAction:(id)sender
{
    
        UIStoryboard *story = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        SearchEventsVC *searchView = [story instantiateViewControllerWithIdentifier:@"searchView"];
        //searchView.categoryTF.text = [NSString stringWithFormat:@"%@",selectedCat];
    if ([screenTypeForStates isEqualToString:@"states"]) {
        searchView.stateValue = [NSString stringWithFormat:@"%@",selectedState];

        
    }else{
        searchView.catValue = [NSString stringWithFormat:@"%@",selectedCat];
    }
    searchView.myDelegate = self;
        searchView.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
        [self presentModalViewController:searchView animated:YES];


}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
    
}



@end
