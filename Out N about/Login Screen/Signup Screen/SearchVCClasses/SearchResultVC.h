//
//  SearchResultVC.h
//  OutNAbout
//
//  Created by Ram Kumar on 08/08/16.
//  Copyright © 2016 tecHindustan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearchResultVC : UIViewController
@property (weak, nonatomic) IBOutlet UITableView *imagesTableView;

@property (retain, nonatomic) NSURLConnection *connection;

@property (retain, nonatomic) NSMutableData *receivedData;



@property(nonatomic,strong) NSString *eventNameString;
@property(nonatomic,strong) NSString *stateValueString;
@property(nonatomic,strong) NSString *catValueString;

@property(nonatomic,strong) NSString *dateString;

@property(nonatomic,strong) NSString *toTimeString;
@property(nonatomic,strong) NSString *fromTimeString;

@property (weak, nonatomic) IBOutlet UILabel *eventCountLabel;
@property (weak, nonatomic) IBOutlet UIView *dimCountBackground;
@end
