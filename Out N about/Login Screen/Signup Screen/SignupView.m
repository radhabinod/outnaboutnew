//
//  SignupView.m
//  Out N about
//
//  Created by Ram Kumar on 03/05/16.
//  Copyright © 2016 tecHindustan. All rights reserved.
//

#import "SignupView.h"
#import "LoginVC.h"
#import "AppDelegate.h"
#import "CommonAPI's.pch"
#import "SVProgressHUD.h"


#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wdeprecated-declarations"

//#import "BSKeyboardControls.h"

@interface SignupView ()<NSURLConnectionDataDelegate,UITextFieldDelegate>

#define kOFFSET_FOR_KEYBOARD 80.0


@end

@implementation SignupView

{
    UIAlertController * alertForFields;
    UIAlertView *alert;
    
    NSPredicate *emailTest;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    

    [self manageTextfields];
    
    NSString *emailReg = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailReg];
    
    
    
    UIImageView *backgroundImage = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    [backgroundImage setImage:[UIImage imageNamed:@"login_bg.jpg"]];
    [self.view insertSubview:backgroundImage atIndex:0];
    
    

}



-(void)manageTextfields
{
    [_firstNameTF.layer setCornerRadius:4.0f];
    [_lastNameTF.layer setCornerRadius:4.0f];
    [_emailTF.layer setCornerRadius:4.0f];
    [_passwordTF.layer setCornerRadius:4.0f];
    [_confirmPassTF.layer setCornerRadius:4.0f];
    [_userNameTF.layer setCornerRadius:4.0f];
    [_registerButton.layer setCornerRadius:4.0f];

    
    _firstNameTF.delegate = self;
    _lastNameTF.delegate = self;
    _emailTF.delegate = self;
    _passwordTF.delegate = self;
    _confirmPassTF.delegate = self;
    _userNameTF.delegate = self;
    
    _firstNameTF.layer.sublayerTransform = CATransform3DMakeTranslation(4, 0, 0);
    _lastNameTF.layer.sublayerTransform = CATransform3DMakeTranslation(4, 0, 0);
    _emailTF.layer.sublayerTransform = CATransform3DMakeTranslation(4, 0, 0);
    _passwordTF.layer.sublayerTransform = CATransform3DMakeTranslation(4, 0, 0);
    _confirmPassTF.layer.sublayerTransform = CATransform3DMakeTranslation(4, 0, 0);
    _userNameTF.layer.sublayerTransform = CATransform3DMakeTranslation(4, 0, 0);

}



- (IBAction)registerWithNewUserAction:(id)sender
{
    
    BOOL isValidString = [self checkTextfieldValidations];
    if (isValidString) {
        
        [self postMethod];
        
    }
    
    
}






-(BOOL)checkTextfieldValidations{
    Boolean isValid = true;
    
    if ([_firstNameTF.text isEqualToString:@""]) {
        isValid = false;

        alert = [[UIAlertView alloc] initWithTitle:@"OutNAbout" message:@"First name is required."  delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        
    } else if ([_lastNameTF.text isEqualToString:@""]) {
        
        isValid = false;

        alert = [[UIAlertView alloc] initWithTitle:@"OutNAbout" message:@"Last name is required."  delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }
    else if ([_userNameTF.text isEqualToString:@""]) {
        
        isValid = false;
        
        alert = [[UIAlertView alloc] initWithTitle:@"OutNAbout" message:@"User name is required."  delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }
    
    else if ([_emailTF.text isEqualToString:@""] ) {
        
        isValid = false;

        alert = [[UIAlertView alloc] initWithTitle:@"OutNAbout" message:@"Email is required."  delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }
    
    else if ([emailTest evaluateWithObject:_emailTF.text] == NO ) {
        
        isValid = false;

        alert = [[UIAlertView alloc] initWithTitle:@"OutNAbout" message:@"Invalid email address!"  delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }
     else if ([_passwordTF.text isEqualToString:@""]) {
         
         isValid = false;

         alert = [[UIAlertView alloc] initWithTitle:@"OutNAbout" message:@"Password is required."  delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
         [alert show];
     }
     else if ([_confirmPassTF.text isEqualToString:@""]) {
         
         isValid = false;

         alert = [[UIAlertView alloc] initWithTitle:@"OutNAbout" message:@"Confirm password is required."  delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
         [alert show];
     }

     else if (![_passwordTF.text isEqualToString:_confirmPassTF.text]) {
         
         isValid = false;

         alert = [[UIAlertView alloc] initWithTitle:@"OutNAbout" message:@"Password did not match!"  delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
         [alert show];
     }


    return isValid;
    
}






#pragma mark - post method to create new user 


-(void)postMethod
{
    [SVProgressHUD showWithStatus:@"Sign Up.." maskType:SVProgressHUDMaskTypeBlack];

    if (APP_DELEGATE.isServerReachable)
    {
    [self.connection cancel];
    
    //initialize new mutable data
    NSMutableData *data = [[NSMutableData alloc] init];
    self.receivedData = data;
    
    NSURL *url = [NSURL URLWithString:(API_URL @"signup")];
    
    //initialize a request from url
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[url standardizedURL]];
    
    //set http method
    [request setHTTPMethod:@"POST"];
    //initialize a post data
   NSString *postData = [NSString stringWithFormat:@"firstname=%@&lastname=%@&email=%@&password=%@&username=%@", _firstNameTF.text,_lastNameTF.text,_emailTF.text,_passwordTF.text,_userNameTF.text];
    //   NSLog(@"username****: %@  pass**** :%@",usernameTF.text,passwordTF.text);
    
    
    [request setValue:@"application/x-www-form-urlencoded; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    
    [request setHTTPBody:[postData dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    self.connection = connection;
    
    //start the connection
    [connection start];
    
    }
    else
    {
        [SVProgressHUD dismiss];
        
        [[[UIAlertView alloc]initWithTitle:@"OutNAbout" message:@"Internet connection is not available. Please try again."  delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil]show];
    }

}




#pragma mark - NSURL Post methods 


-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data{
    [self.receivedData appendData:data];
}


-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error{
    
    NSLog(@"eeeeeeee  %@" , error);
}


-(void)connectionDidFinishLoading:(NSURLConnection *)connection{
    
    //initialize convert the received data to string with UTF8 encoding
   // NSString *htmlSTR = [[NSString alloc] initWithData:self.receivedData
                             //                 encoding:NSUTF8StringEncoding];
    // NSLog(@" ffff     %@" , htmlSTR);
    
    
    NSError *error = nil;
    NSDictionary *jsonArray = [NSJSONSerialization JSONObjectWithData: self.receivedData options: NSJSONReadingMutableContainers error: &error];  //I am using sbjson to parse
    NSLog(@"response :%@",jsonArray);//You have registered successfully
    
    if ([jsonArray[@"msg"] isEqualToString:@"You have registered successfully"]) {
        [[[UIAlertView alloc]initWithTitle:@"Message" message:@"Successfully Registered" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil]show];
        
        [SVProgressHUD dismiss];

    }
    
     else  {
        [[[UIAlertView alloc]initWithTitle:@"Message" message:jsonArray[@"msg"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil]show];
        
        [SVProgressHUD dismiss];
    }
    
    
    //NSLog(@"dataaaaaaaa %@",jsonArray);  //here is your output

}


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex==0) {
          [self dismissViewControllerAnimated:YES completion:nil];
    }
}




- (IBAction)loginViewAction:(id)sender {
    
   // [self performSegueWithIdentifier:@"tologin" sender:nil];
    [self dismissViewControllerAnimated:YES completion:nil];
    
    
    
}







-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    return YES;
}




- (void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [[self view] endEditing:YES];
}


-(void)textFieldDidBeginEditing:(UITextField *)textField
{
//    [UIView beginAnimations:nil context:nil];
//    [UIView setAnimationDuration:.3];
//    [UIView setAnimationBeginsFromCurrentState:TRUE];
//    self.view.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y -100., self.view.frame.size.width, self.view.frame.size.height);
//    
//    [UIView commitAnimations];
    
    
    
}


-(void)textFieldDidEndEditing:(UITextField *)textField
{
//    [UIView beginAnimations:nil context:nil];
//    [UIView setAnimationDuration:.3];
//    [UIView setAnimationBeginsFromCurrentState:TRUE];
//    self.view.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y +100., self.view.frame.size.width, self.view.frame.size.height);
//    
//    [UIView commitAnimations];
    

}


- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    if (textField==_passwordTF ) {
        
    
    
        if (textField.text.length <= 8 && range.length == 0)
        {
                    [[[UIAlertView alloc]initWithTitle:@"OutNAbout" message:@"Password length must be greater then 8" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil]show];
            return NO; // return NO to not change text
    

        }

        
  }
    else
    {
        return YES;
    }
    return YES;

}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


@end
