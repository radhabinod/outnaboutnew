//
//  ForgetPasswordVC.h
//  OutNAbout
//
//  Created by Ram Kumar on 29/08/16.
//  Copyright © 2016 tecHindustan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ForgetPasswordVC : UIViewController
@property (weak, nonatomic) IBOutlet UITextField *emailIdTF;
@property (weak, nonatomic) IBOutlet UIButton *resetButton;

@property (retain, nonatomic) NSURLConnection *connection;

@property (retain, nonatomic) NSMutableData *receivedData;
@end
