//
//  SearchEventsVC.m
//  OutNAbout
//
//  Created by Ram Kumar on 21/07/16.
//  Copyright © 2016 tecHindustan. All rights reserved.
//

#import "SearchEventsVC.h"
#import "AppDelegate.h"
#import "SearchViewListing.h"
#import "BIZPopupViewController.h"
#import "SearchResultVC.h"
#import "CommonAPI's.pch"
#import "LPPopupListView.h"

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wdeprecated-declarations"

@interface SearchEventsVC ()<UITextFieldDelegate,LPPopupListViewDelegate,UITableViewDelegate,UITableViewDataSource,UIGestureRecognizerDelegate>

@property (nonatomic, strong) NSMutableIndexSet *selectedIndexes;
@property (nonatomic, strong) NSMutableIndexSet *selectedIndexes1;


@end

@implementation SearchEventsVC
{
    NSMutableArray *json;
    NSMutableArray *catTitle;
    NSMutableArray *catId;
    NSMutableArray *statesList;
    NSString *catIDValue;
    
    NSString *categoryIDNo;
    NSString *catOrArtistKeyType;
    NSMutableArray *arrayArtistSelectedRows;
    NSMutableArray *arrayCategorySelectedRows;
    
    UIDatePicker *datePicker;
    UITapGestureRecognizer *dateTapGesture;
    UITapGestureRecognizer *toTimeTapGesture;
    UITapGestureRecognizer *fromTimeTapGesture;
}

@synthesize categoryTF,stateTF,myDelegate,catValue,stateValue;


- (void)viewDidLoad {
    [super viewDidLoad];

    catOrArtistKeyType=@"";
    categoryIDNo=@"";

 //   [self pickerViewsForAllFields];
    
    statesList = [[NSMutableArray alloc]init];
    [statesList addObject:@"Dubai"];
    [statesList addObject:@"Abu Dhabi"];
    [statesList addObject:@"Sharjah"];
    [statesList addObject:@"Al Ain"];
    [statesList addObject:@"Fujairah"];
    [statesList addObject:@"Ras al-khaimah"];
    [statesList addObject:@"Ajman"];
    [statesList addObject:@"Umm al-Quwain"];
    
    UIButton *button =  [UIButton buttonWithType:UIButtonTypeSystem];
    [button setImage:[UIImage imageNamed:@"ic_arrow@3x.png"] forState:UIControlStateNormal];
    [button addTarget:self action:@selector(handleBack:)forControlEvents:UIControlEventTouchUpInside];
    [button setFrame:CGRectMake(0, 0, 53, 31)];
    [button setImageEdgeInsets:UIEdgeInsetsMake(0, -30, 0, 0)];
    button.backgroundColor = [UIColor clearColor];
    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(30, 5, 240, 20)];
    [label setFont:[UIFont fontWithName:@"Arial-BoldMT" size:17]];
    [label setText:@"Search"];
    label.textAlignment = NSTextAlignmentLeft;
    [label setTextColor:[UIColor blackColor]];
    [label setBackgroundColor:[UIColor clearColor]];
    [button addSubview:label];
    UIBarButtonItem *barButton = [[UIBarButtonItem alloc] initWithCustomView:button];
    barButton.tintColor = [UIColor blackColor];
    self.navigationItem.leftBarButtonItem = barButton;

    
    _rightBarButtonItem = [[UIBarButtonItem alloc]init];
    [_rightBarButtonItem setImage:[UIImage imageNamed:@"ic_cross@3x.png"]];
    _rightBarButtonItem.tintColor = [UIColor blackColor];
    [_rightBarButtonItem setTarget:self];
    [_rightBarButtonItem setAction:@selector(crossButtonAction)];
    self.navigationItem.rightBarButtonItem = _rightBarButtonItem;

    self.navigationItem.hidesBackButton = YES;
    
    _eventNameTF.layer.sublayerTransform = CATransform3DMakeTranslation(5, 0, 0);
    categoryTF.layer.sublayerTransform = CATransform3DMakeTranslation(5, 0, 0);
    stateTF.layer.sublayerTransform = CATransform3DMakeTranslation(5, 0, 0);
    _dateTF.layer.sublayerTransform = CATransform3DMakeTranslation(5, 0, 0);
    _toTimeTF.layer.sublayerTransform = CATransform3DMakeTranslation(5, 0, 0);
    _fromTimeTF.layer.sublayerTransform = CATransform3DMakeTranslation(5, 0, 0);
    
    categoryTF.enabled = NO;
    stateTF.enabled = NO;
    _eventNameTF.delegate = self;
    _dateTF.delegate = self;
    _toTimeTF.delegate = self;
    _fromTimeTF.delegate =self;
    categoryTF.delegate =self;
    stateTF.delegate =self;
    
    UITapGestureRecognizer *categoryTapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(selectCategories:)];
    categoryTapGesture.delegate=self;
    [_backTapCategoryView addGestureRecognizer:categoryTapGesture];
    
    UITapGestureRecognizer *stateTapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(selectStates:)];
    stateTapGesture.delegate=self;
    [_backTapStateView addGestureRecognizer:stateTapGesture];
    
    dateTapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(selectDateAndTime:)];
    dateTapGesture.delegate=self;
    [_viewOnSelectDate addGestureRecognizer:dateTapGesture];
    
    toTimeTapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(selectDateAndTime:)];
    toTimeTapGesture.delegate=self;
    [_viewOnToTime addGestureRecognizer:toTimeTapGesture];
    
    fromTimeTapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(selectDateAndTime:)];
    fromTimeTapGesture.delegate=self;
    [_viewOnFromTime addGestureRecognizer:fromTimeTapGesture];
    
    
    [self getCategoriesFromURL];
    [self.view addSubview:_dimBackgroundView];
    [_dimBackgroundView addSubview:_popupView];
    [_dimBackgroundView setBackgroundColor:[[UIColor blackColor] colorWithAlphaComponent:0.6]];

    [_dimBackgroundView setHidden:YES];
    [self popupTableView];
    [self allTextfieldLeftImages];

     self.navigationController.navigationBar.barTintColor = [UIColor whiteColor];
     self.navigationController.navigationBar.tintColor = [UIColor blackColor];
}



- (void) selectDateAndTime: (UITapGestureRecognizer *)gesture
{
    CGRect toolbarTargetFrame = CGRectMake(0, self.view.bounds.size.height-216-44, 320, 44);
    CGRect datePickerTargetFrame = CGRectMake(0, self.view.bounds.size.height-216, 320, 216);
    
    UIView *darkView1 = [[UIView alloc] initWithFrame:self.view.bounds] ;
    darkView1.alpha = 0;
    darkView1.backgroundColor = [UIColor blackColor];
    darkView1.tag = 9;
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissPickerView:)] ;
    [darkView1 addGestureRecognizer:tapGesture];
    [self.view addSubview:darkView1];
    
    
    datePicker = [[UIDatePicker alloc]initWithFrame:CGRectMake(0, self.view.bounds.size.height+44, 320, 216)];
    [datePicker setDate:[NSDate date]];
    datePicker.backgroundColor = [UIColor whiteColor];
    datePicker.tag = 10;
    
    UIToolbar *toolBar = [[UIToolbar alloc] initWithFrame:CGRectMake(20, self.view.bounds.size.height, 320, 44)];
    toolBar.tag = 11;
    toolBar.barStyle = UIBarStyleBlackTranslucent;
    
    UIBarButtonItem *spacer = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    
    UIBarButtonItem *cancelButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemUndo target:self action:@selector(cancelPicker:)];
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(donePickerView:)];
    
    if (gesture==dateTapGesture) {
        
        doneButton.tag=100;
        cancelButton.tag=100;
        datePicker.datePickerMode = UIDatePickerModeDate;
        [datePicker addTarget:self action:@selector(dateTextField:) forControlEvents:UIControlEventValueChanged];
        
    }
    
    else if (gesture==toTimeTapGesture) {
        doneButton.tag=101;
        cancelButton.tag=101;
        datePicker.datePickerMode = UIDatePickerModeTime;
            [datePicker addTarget:self action:@selector(ToTextField:) forControlEvents:UIControlEventValueChanged];
        
    }
    else if (gesture==fromTimeTapGesture) {
        doneButton.tag=102;
        cancelButton.tag=102;
        datePicker.datePickerMode = UIDatePickerModeTime;
        [datePicker addTarget:self action:@selector(fromTextField:) forControlEvents:UIControlEventValueChanged];
        
    }
    

    
    [self.view addSubview:datePicker];

    [toolBar setItems:[NSArray arrayWithObjects:cancelButton,spacer, doneButton, nil]];
    [self.view addSubview:toolBar];
    
    [UIView beginAnimations:@"MoveIn" context:nil];
    toolBar.frame = toolbarTargetFrame;
    datePicker.frame = datePickerTargetFrame;
    darkView1.alpha = 0.5;
    [UIView commitAnimations];

}




- (void)removedViews:(id)object {
    [[self.view viewWithTag:9] removeFromSuperview];
    [[self.view viewWithTag:10] removeFromSuperview];
    [[self.view viewWithTag:11] removeFromSuperview];
    
}


- (void)cancelPicker:(id)sender {
    
    UIBarButtonItem *tag =(UIBarButtonItem*)sender;
    
    if (tag.tag==100) {
        _dateTF.text = nil;
    }
    else if (tag.tag==101) {
        _toTimeTF.text = nil;
    }
    
    else if (tag.tag==102) {
        _fromTimeTF.text = nil;
    }
    
    
    CGRect toolbarTargetFrame = CGRectMake(0, self.view.bounds.size.height, 320, 44);
    CGRect datePickerTargetFrame = CGRectMake(0, self.view.bounds.size.height+44, 320, 216);
    
    [UIView beginAnimations:@"MoveOut" context:nil];
    [self.view viewWithTag:9].alpha = 0;
    [self.view viewWithTag:10].frame = datePickerTargetFrame;
    [self.view viewWithTag:11].frame = toolbarTargetFrame;
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDidStopSelector:@selector(removedViews:)];
    [UIView commitAnimations];
    
    
    
}

- (void)dismissPickerView:(id)sender {
    
    CGRect toolbarTargetFrame = CGRectMake(0, self.view.bounds.size.height, 320, 44);
    CGRect datePickerTargetFrame = CGRectMake(0, self.view.bounds.size.height+44, 320, 216);
    
    [UIView beginAnimations:@"MoveOut" context:nil];
    [self.view viewWithTag:9].alpha = 0;
    [self.view viewWithTag:10].frame = datePickerTargetFrame;
    [self.view viewWithTag:11].frame = toolbarTargetFrame;
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDidStopSelector:@selector(removedViews:)];
    [UIView commitAnimations];
    
}



- (void)donePickerView:(id)sender {
    
    UIBarButtonItem *tag =(UIBarButtonItem*)sender;
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    NSDate *eventDate = datePicker.date;
    if (tag.tag==100) {
        
        [dateFormat setDateFormat:@"yyyy/MM/dd"];
        NSString *dateString = [dateFormat stringFromDate:eventDate];
        _dateTF.text = [NSString stringWithFormat:@"%@",dateString];
        
    }
    else if (tag.tag==101) {
        
        
        [dateFormat setDateFormat:@"hh:mm a"];
        NSString *timeString = [dateFormat stringFromDate:eventDate];
        _toTimeTF.text = [NSString stringWithFormat:@"%@",timeString];
        
    }
    else if (tag.tag==102) {
        
        [dateFormat setDateFormat:@"hh:mm a"];
        NSString *timeString = [dateFormat stringFromDate:eventDate];
        _fromTimeTF.text = [NSString stringWithFormat:@"%@",timeString];
        
    }
    
    
    
    CGRect toolbarTargetFrame = CGRectMake(0, self.view.bounds.size.height, 320, 44);
    CGRect datePickerTargetFrame = CGRectMake(0, self.view.bounds.size.height+44, 320, 216);
    
    [UIView beginAnimations:@"MoveOut" context:nil];
    [self.view viewWithTag:9].alpha = 0;
    [self.view viewWithTag:10].frame = datePickerTargetFrame;
    [self.view viewWithTag:11].frame = toolbarTargetFrame;
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDidStopSelector:@selector(removedViews:)];
    [UIView commitAnimations];
}





//-(void)pickerViewsForAllFields
//{
//    
//    UIDatePicker *datePicker = [[UIDatePicker alloc]init];
//    [datePicker setDate:[NSDate date]];
//    datePicker.datePickerMode = UIDatePickerModeDate;
//    [datePicker addTarget:self action:@selector(dateTextField:) forControlEvents:UIControlEventValueChanged];
//    [_dateTF setInputView:datePicker];
//
//    
//    UIDatePicker *toTimePicker = [[UIDatePicker alloc]init];
//    [toTimePicker setDate:[NSDate date]];
//    toTimePicker.datePickerMode = UIDatePickerModeTime;
//    [toTimePicker addTarget:self action:@selector(ToTextField:) forControlEvents:UIControlEventValueChanged];
//    [_toTimeTF setInputView:toTimePicker];
//
//    UIDatePicker *fromTimePicker = [[UIDatePicker alloc]init];
//    [fromTimePicker setDate:[NSDate date]];
//    fromTimePicker.datePickerMode = UIDatePickerModeTime;
//    [fromTimePicker addTarget:self action:@selector(fromTextField:) forControlEvents:UIControlEventValueChanged];
//    [_fromTimeTF setInputView:fromTimePicker];
//
//}


-(void) fromTextField:(id)sender
{
    UIDatePicker *picker = (UIDatePicker*)_fromTimeTF.inputView;
    //[picker setMaximumDate:[NSDate date]];
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    NSDate *eventDate = picker.date;
    [dateFormat setDateFormat:@"hh:mm a"];
    
    NSString *dateString = [dateFormat stringFromDate:eventDate];
    _fromTimeTF.text = [NSString stringWithFormat:@"%@",dateString];
}


-(void) ToTextField:(id)sender
{
    UIDatePicker *picker = (UIDatePicker*)_toTimeTF.inputView;
    //[picker setMaximumDate:[NSDate date]];
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    NSDate *eventDate = picker.date;
    [dateFormat setDateFormat:@"hh:mm a"];
    
    NSString *dateString = [dateFormat stringFromDate:eventDate];
    _toTimeTF.text = [NSString stringWithFormat:@"%@",dateString];
}


-(void) dateTextField:(id)sender
{
    UIDatePicker *picker = (UIDatePicker*)_dateTF.inputView;
    //[picker setMaximumDate:[NSDate date]];
    [picker setMinimumDate:[NSDate date]];
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    NSDate *eventDate = picker.date;
    [dateFormat setDateFormat:@"yyyy-MM-dd"];
    
    NSString *dateString = [dateFormat stringFromDate:eventDate];
    _dateTF.text = [NSString stringWithFormat:@"%@",dateString];
}





-(void)allTextfieldLeftImages
{
    
    UIImageView *categoryImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"ic_dropdown@3x.png"]];
    categoryImage.frame = CGRectMake(0.0, 0.0, categoryImage.image.size.width+35.0, categoryImage.image.size.height);
    categoryImage.contentMode = UIViewContentModeCenter;
    categoryTF.rightView = categoryImage;
    categoryTF.rightViewMode = UITextFieldViewModeAlways;
    
    UIImageView *stateImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"ic_dropdown@3x.png"]];
    stateImage.frame = CGRectMake(0.0, 0.0, stateImage.image.size.width+35.0, stateImage.image.size.height);
    stateImage.contentMode = UIViewContentModeCenter;
    stateTF.rightView = stateImage;
    stateTF.rightViewMode = UITextFieldViewModeAlways;
    
    
    UIImageView *dateImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"ic_date@3x.png"]];
    dateImage.frame = CGRectMake(0.0, 0.0, dateImage.image.size.width+30.0, dateImage.image.size.height);
    dateImage.contentMode = UIViewContentModeCenter;
    _dateTF.rightView = dateImage;
    _dateTF.rightViewMode = UITextFieldViewModeAlways;
    
    UIImageView *toImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"ic_selectGray@3x.png"]];
    toImage.frame = CGRectMake(0.0, 0.0, toImage.image.size.width+35.0, toImage.image.size.height);
    toImage.contentMode = UIViewContentModeCenter;
    _toTimeTF.rightView = toImage;
    _toTimeTF.rightViewMode = UITextFieldViewModeAlways;
    
    
    UIImageView *fromImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"ic_selectGray@3x.png"]];
    fromImage.frame = CGRectMake(0.0, 0.0, fromImage.image.size.width+35.0, fromImage.image.size.height);
    fromImage.contentMode = UIViewContentModeCenter;
    _fromTimeTF.rightView = fromImage;
    _fromTimeTF.rightViewMode = UITextFieldViewModeAlways;
    
    
    [_eventNameTF.layer setMasksToBounds:YES];
    [_eventNameTF.layer setCornerRadius:3.0f];
    
    [categoryTF.layer setMasksToBounds:YES];
    [categoryTF.layer setCornerRadius:3.0f];

    [stateTF.layer setMasksToBounds:YES];
    [stateTF.layer setCornerRadius:3.0f];
    
    [_dateTF.layer setMasksToBounds:YES];
    [_dateTF.layer setCornerRadius:3.0f];
    
    [_toTimeTF.layer setMasksToBounds:YES];
    [_toTimeTF.layer setCornerRadius:3.0f];
    
    [_fromTimeTF.layer setMasksToBounds:YES];
    [_fromTimeTF.layer setCornerRadius:3.0f];
    
    [_submitButton.layer setMasksToBounds:YES];
    [_submitButton.layer setCornerRadius:2.0f];

}


-(void)getCategoriesFromURL
{
    json = [[NSMutableArray alloc]init];
    catTitle = [[NSMutableArray alloc]init];
    catId = [[NSMutableArray alloc]init];
    
    if (APP_DELEGATE.isServerReachable)
    {

    NSError *error;
    NSString *url_string = [NSString stringWithFormat: (API_URL @"allCategories")];
    NSData *data = [NSData dataWithContentsOfURL: [NSURL URLWithString:url_string]];
    NSDictionary *jsonData = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
    json = jsonData[@"data"];
    
    for (NSDictionary *cat in json)
    {
        
        NSString *categoryId = cat[@"cat_id"];
        [catId addObject:categoryId];
        
        NSString *categoryTitle = cat[@"cat_title"];
        [catTitle addObject:categoryTitle];
        
    }
    [_tableViewListing reloadData];
        
    }
    else
    {
        [[[UIAlertView alloc]initWithTitle:@"OutNAbout" message:@"Internet connection is not available. Please try again."  delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil]show];
    }
    
   // NSLog(@"json: %@", json);
    
}



- (void) crossButtonAction
{
 [self.navigationController popViewControllerAnimated:YES];
    
}
- (void) handleBack:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}



- (void) selectCategories: (UITapGestureRecognizer *)gesture
{
  
            
    catOrArtistKeyType=@"catagory";
    _selectHeadingLabel.text = @"Select Categories";
    [_dimBackgroundView setHidden:NO];
    [_popupView setHidden:NO];
    [_tableViewListing reloadData];
   

}
- (void) selectStates: (UITapGestureRecognizer *)gesture
{

            
    catOrArtistKeyType=@"state";
    _selectHeadingLabel.text = @"Select States";
    
    [_dimBackgroundView setHidden:NO];
    [_popupView setHidden:NO];
    [_tableViewListing reloadData];
    
  
}


//#pragma mark - LPPopupListViewDelegate
//
//- (void)popupListView:(LPPopupListView *)popUpListView didSelectIndex:(NSInteger)index
//{
//    NSLog(@"popUpListView - didSelectIndex: %ld", (long)index);
//    state=NO;
//}
//
//- (void)popupListViewDidHide:(LPPopupListView *)popUpListView selectedIndexes:(NSIndexSet *)selectedIndexes
//{
//    
//    //NSLog(@"popupListViewDidHide - selectedIndexes: %@", selectedIndexes.description);
//     catIDValue=@"";
//    
//
//
//            if (state==YES) {
//                self.stateTF.text = @"";
//                self.selectedIndexes=nil;
//               
//                [selectedIndexes enumerateIndexesUsingBlock:^(NSUInteger idxi, BOOL *stop) {
//     
////                    if ([[[self stateList] objectAtIndex:idxi] compare:@"Dubai"]== NSOrderedSame) {
////                        self.stateTF.text= [self.stateTF.text stringByAppendingFormat:@"%@", [[self stateList] objectAtIndex:idxi]];
////                    }
////                    else{
//                 self.stateTF.text= [self.stateTF.text stringByAppendingFormat:@"%@", [[self stateList] objectAtIndex:idxi]];
//                      
//              //      }
//                }];
//                
//                state=NO;
//                self.selectedIndexes = [[NSMutableIndexSet alloc] initWithIndexSet:selectedIndexes];
//
//            }
//    
//            else
//            {
//                self.categoryTF.text = @"";
//                catIDValue=@"";
//                self.selectedIndexes1=nil;
//                [selectedIndexes enumerateIndexesUsingBlock:^(NSUInteger idx, BOOL *stop) {
//                    
//                    catIDValue =[catIDValue stringByAppendingFormat:@"%@", [[self categoryIDs] objectAtIndex:idx]] ;
//                    NSLog(@"cat id*** %@", catIDValue);
//
//                self.categoryTF.text = [self.categoryTF.text stringByAppendingFormat:@"%@", [[self categoryList] objectAtIndex:idx]];
//                   // NSLog(@"cat id*** %@", self.categoryTF.text );
//                    
//                    
//                    
//                    self.selectedIndexes1 = [[NSMutableIndexSet alloc] initWithIndexSet:selectedIndexes];
//
//        }];
//            }
//    
//    
//}


//#pragma mark - Array List
//
//- (NSArray *)categoryList
//{
//    return catTitle;
//}
//
//- (NSArray *)stateList
//{
//    return statesList;
//}
//
//- (NSArray *)categoryIDs
//{
//    return catId;
//}



- (IBAction)submitCatAndArtistsList:(id)sender {
    
    
    if ([catOrArtistKeyType isEqualToString:@"state"])
    {
        stateTF.text =[[self getstateTitle] componentsJoinedByString:@","];
        
    }else{
        categoryTF.text =[[self getCategoryTitle] componentsJoinedByString:@","];
        categoryIDNo=[[self getCategoryId] componentsJoinedByString:@","];
        
    }
    NSLog(@"state text :%@",stateTF.text);
    NSLog(@"category ID :%@",categoryIDNo);
    
    [_dimBackgroundView setHidden:YES];
    catOrArtistKeyType = @"";

}



// get artist and category index by title and ID's 

-(NSArray *)getstateTitle {
    NSMutableArray *selections = [[NSMutableArray alloc] init];
    
    for(NSIndexPath *indexPath in arrayArtistSelectedRows) {
        [selections addObject:[statesList objectAtIndex:indexPath.row]];
    }
    return selections;
}


-(NSArray *)getCategoryTitle {
    NSMutableArray *selections = [[NSMutableArray alloc] init];
    
    for(NSIndexPath *indexPath in arrayCategorySelectedRows) {
        [selections addObject:[catTitle objectAtIndex:indexPath.row]];
    }
    return selections;
}


-(NSArray *)getCategoryId {
    NSMutableArray *selections = [[NSMutableArray alloc] init];
    
    for(NSIndexPath *indexPath in arrayCategorySelectedRows) {
        [selections addObject:[catId objectAtIndex:indexPath.row]];
    }
    return selections;
}




//



#pragma mark - Show table view popup using tableview delegate methods = = = = === == =  == = = = = = = = = ==

-(void)popupTableView
{
    arrayCategorySelectedRows= [[NSMutableArray alloc] init];
    arrayArtistSelectedRows= [[NSMutableArray alloc] init];

    //    _tableViewListing = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height) style:UITableViewStylePlain];
    _tableViewListing.delegate = self;
    _tableViewListing.dataSource = self;
    [_popupView addSubview:_tableViewListing];
}



-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if ([catOrArtistKeyType isEqualToString:@"state"])
    {
        return statesList.count;
    }
    else{
        return catTitle.count;
    }
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *cellIdent = @"cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdent];
    
    if(cell == nil)
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdent];
    cell.textLabel.font = [UIFont systemFontOfSize:13.0f];
    
    if ([catOrArtistKeyType isEqualToString:@"state"])
    {
        cell.textLabel.text = [statesList objectAtIndex:indexPath.row];
        
        if([arrayArtistSelectedRows containsObject:indexPath])
        {
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
        } else
        { cell.accessoryType = UITableViewCellAccessoryNone;
        }
        
    }else{
        cell.textLabel.text = [catTitle objectAtIndex:indexPath.row];//
        if([arrayCategorySelectedRows containsObject:indexPath])
        { cell.accessoryType = UITableViewCellAccessoryCheckmark;
        } else
        { cell.accessoryType = UITableViewCellAccessoryNone;
        }
    }
    
    return cell;
}



-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    if ([catOrArtistKeyType isEqualToString:@"state"])
    {
        
        if(cell.accessoryType == UITableViewCellAccessoryNone) {
            
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
            [arrayArtistSelectedRows addObject:indexPath];
        }
        else {
            cell.accessoryType = UITableViewCellAccessoryNone;
            [arrayArtistSelectedRows removeObject:indexPath];
        }
    }
    else{
        if(cell.accessoryType == UITableViewCellAccessoryNone) {
            
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
            [arrayCategorySelectedRows addObject:indexPath];
        }
        else {
            cell.accessoryType = UITableViewCellAccessoryNone;
            [arrayCategorySelectedRows removeObject:indexPath];
        }
    }
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}






- (IBAction)submitSearchAction:(id)sender
{
    
//    if (![_eventNameTF.text isEqualToString:@""] || ![categoryTF.text isEqualToString:@""] || ![stateTF.text isEqualToString:@""] || ![_dateTF.text isEqualToString:@""] || ![_toTimeTF.text isEqualToString:@""] || ![_toTimeTF.text isEqualToString:@""])
//    {
    
        UIStoryboard *story = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        SearchResultVC *searchResult = [story instantiateViewControllerWithIdentifier:@"searchResultView"];
        searchResult.eventNameString = _eventNameTF.text;
    
       if ([categoryIDNo isEqualToString:@""]) {
           searchResult.catValueString = @"";
       }else{
        searchResult.catValueString = categoryIDNo;
        }
        searchResult.stateValueString = stateTF.text;
        searchResult.dateString = _dateTF.text;

        searchResult.toTimeString = _toTimeTF.text;

        searchResult.fromTimeString = _fromTimeTF.text;

        [self.navigationController pushViewController:searchResult animated:YES];

   // }
    
//    else if ([_eventNameTF.text isEqualToString:@""] || [categoryTF.text isEqualToString:@""] || [stateTF.text isEqualToString:@""] || [_dateTF.text isEqualToString:@""] || [_toTimeTF.text isEqualToString:@""] || [_fromTimeTF.text isEqualToString:@""])
//    {
//    
//    UIStoryboard *story = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//    SearchResultVC *searchResult = [story instantiateViewControllerWithIdentifier:@"searchResultView"];
//        
//        searchResult.eventNameString = @"";
//        searchResult.catValueString = @"";
//        
//        searchResult.stateValueString = @"";
//        
//        searchResult.dateString = @"";
//        
//        searchResult.toTimeString = @"";
//        
//        searchResult.fromTimeString =@"";
//
//    [self.navigationController pushViewController:searchResult animated:YES];
//        
//    }
    
}




- (void)viewWillAppear:(BOOL)animated
{

    
    UIApplication *app = [UIApplication sharedApplication];
    CGFloat statusBarHeight = app.statusBarFrame.size.height;
    
    UIView *statusBarView = [[UIView alloc] initWithFrame:CGRectMake(0, -statusBarHeight, [UIScreen mainScreen].bounds.size.width, statusBarHeight)];
    statusBarView.backgroundColor = [UIColor colorWithRed:0.95 green:0.28 blue:0.31 alpha:1.0];
    [self.navigationController.navigationBar addSubview:statusBarView];

    [super viewWillAppear:animated];
}


-(void)viewWillDisappear:(BOOL)animated
{
   self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:0.95 green:0.28 blue:0.31 alpha:1.0];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];

    [super viewWillDisappear:animated];
}



-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    return YES;
}


- (void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [[self view] endEditing:YES];
}




-(void)textFieldDidBeginEditing:(UITextField *)textField

{
//    if (textField.text.length) {
//        [_submitButton setBackgroundColor:[UIColor colorWithRed:0.95 green:0.28 blue:0.31 alpha:1.0]];
//        
//    }
//    else
//    {
//        [_submitButton setBackgroundColor:[UIColor grayColor]];
//    }

}
-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    
//    if (textField.text.length) {
//        [_submitButton setBackgroundColor:[UIColor colorWithRed:0.95 green:0.28 blue:0.31 alpha:1.0]];
//        
//    }
//    else
//        
//    {
//          [_submitButton setBackgroundColor:[UIColor grayColor]];
//    }
    return YES;
}



//-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
//{
//    
//    if([string length] > 0)
//    {
//        [_submitButton setBackgroundColor:[UIColor colorWithRed:0.95 green:0.28 blue:0.31 alpha:1.0]];
//
//    }
//    else
//    {
//        [_submitButton setBackgroundColor:[UIColor grayColor]];
//
//    }
//    
//    return YES;
//}







@end
